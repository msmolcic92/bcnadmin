import { AdminFEPage } from './app.po';

describe('admin-fe App', () => {
  let page: AdminFEPage;

  beforeEach(() => {
    page = new AdminFEPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
