import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { Actions } from '@ngrx/effects';

// Export for convenience.
export { Actions };

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  setSource(source: Observable<any>) {
    this.source = source;
  }
}
