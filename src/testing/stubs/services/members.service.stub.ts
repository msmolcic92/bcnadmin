import { Observable } from 'rxjs/Observable';

export class MembersServiceStub {
  fetchList(params: any): Observable<any> {
    return;
  }

  fetch(params: any): Observable<any> {
    return;
  }

  fetchInvoices(params: any): Observable<any> {
    return;
  }

  updateUsername(memberId: number, requestData: any): Observable<any> {
    return;
  }

  updateEmail(memberId: number, requestData: any): Observable<any> {
    return;
  }

  updateRank(memberId: number, requestData: any): Observable<any> {
    return;
  }

  creditWallet(wallet: string, requestData: any): Observable<any> {
    return;
  }
}