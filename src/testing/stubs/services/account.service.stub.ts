import { Observable } from 'rxjs/Observable';

export class AccountServiceStub {
  fetch(params: any): Observable<any> {
    return;
  }

  fetchLoginAttempts(params: any): Observable<any> {
    return;
  }

  update(accountData: any): Observable<any> {
    return;
  }
}
