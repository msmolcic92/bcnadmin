import { Injectable } from '@angular/core';
import { Store, ActionReducer, Action, ActionsSubject } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from 'rxjs/operator/map';

// Export for convenience.
export { Store };

export class StoreMock<T> {
  // private actionsObserver: ActionsSubject;

  reducers = new Map<string, BehaviorSubject<any>>();

  select(name) {
    if (!this.reducers.has(name)) {
      this.reducers.set(name, new BehaviorSubject({}));
    }
    return this.reducers.get(name);
  }

  mockState(stateAttr, data) {
    this.select(stateAttr).next(data);
  }

  dispatch(action: any) {
    // spy me
  }
}
