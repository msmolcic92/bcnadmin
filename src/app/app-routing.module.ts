import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from './core/app-layout.component';
import { AuthenticatedGuard } from './auth/authenticated.guard';
import { PageNotFoundComponent } from './pages/page-not-found.component';
import { InternalServerErrorPageComponent } from './pages/internal-server-error-page.component';
import { LogoutComponent } from './core/logout.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthenticatedGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'account',
        loadChildren: './account/account.module#AccountModule'
      },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'members',
        loadChildren: './members/members.module#MembersModule'
      },
      {
        path: 'unsubscribe-list',
        loadChildren: './unsubscribe-list/unsubscribe-list.module#UnsubscribeListModule'
      },
      {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
      },
      {
        path: 'wallets',
        loadChildren: './transactions/transactions.module#TransactionsModule'
      },
      {
        path: 'invoices',
        loadChildren: './invoices/invoices.module.ts#InvoicesModule'
      },
      {
        path: 'founders',
        loadChildren: './founders/founders.module.ts#FoundersModule'
      },
      {
        path: 'founders-pools',
        loadChildren: './founders-pools/founders-pools.module.ts#FoundersPoolsModule'
      },
      {
        path: 'volumes',
        loadChildren: './volumes/volumes.module.ts#VolumesModule'
      },
      {
        path: 'binary-credit-flow',
        loadChildren: './binary-credit-flow/binary-credit-flow.module#BinaryCreditFlowModule'
      },
      {
        path: 'binary-credit-cycles',
        loadChildren: './binary-credit-cycles/binary-credit-cycles.module#BinaryCreditCyclesModule'
      },
      {
        path: 'support-tickets',
        loadChildren: './support-tickets/support-tickets.module#SupportTicketsModule'
      },
      {
        path: 'email-queue',
        loadChildren: './email-queue/email-queue.module#EmailQueueModule'
      },
      {
        path: 'admin-logs',
        loadChildren: './admin-logs/admin-logs.module#AdminLogsModule'
      },
      {
        path: 'earnings',
        loadChildren: './earnings/earnings.module#EarningsModule'
      },
      {
        path: 'news',
        loadChildren: './news/news.module#NewsModule'
      },
      {
        path: 'crons',
        loadChildren: './crons/crons.module#CronsModule'
      },
      {
        path: 'news-categories',
        loadChildren: './news-categories/news-categories.module#NewsCategoriesModule'
      },
      {
        path: 'predefined-replies',
        loadChildren: './predefined-replies/predefined-replies.module#PredefinedRepliesModule'
      }
    ]
  },
  {
    path: '500',
    component: InternalServerErrorPageComponent
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
