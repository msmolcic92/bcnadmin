import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { supportTicketsReducer } from './store/support-tickets.state';
import { SupportTicketsService } from '../../api-client/support-tickets.service';
import { SupportTicketsEffects } from './store/support-tickets.effects';
import { SupportTicketsRoutingModule } from './support-tickets-routing.module';
import { SupportTicketsRootComponent } from './support-tickets-root.component';
import { SupportTicketListScreenComponent } from './support-ticket-list-screen.component';
import { SupportTicketListResolver } from './support-ticket-list-resolver.service';
import { SupportTicketDetailScreenComponent } from './support-ticket-detail-screen.component';
import { SupportTicketDetailResolver } from './support-detail-resolver.service';
import { SupportTicketListFilterComponent } from './support-ticket-list-filter.component';
import { AdminService } from 'api-client/admin.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    SupportTicketsRoutingModule,
    StoreModule.forFeature('supportTickets', supportTicketsReducer),
    EffectsModule.forFeature([SupportTicketsEffects]),
    NgbModule
  ],
  declarations: [
    SupportTicketsRootComponent,
    SupportTicketListScreenComponent,
    SupportTicketDetailScreenComponent,
    SupportTicketListFilterComponent
  ],
  providers: [
    SupportTicketsService,
    SupportTicketListResolver,
    SupportTicketDetailResolver,
    AdminService
  ]
})
export class SupportTicketsModule {}
