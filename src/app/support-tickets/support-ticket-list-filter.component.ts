import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { SECONDS_IN_A_DAY } from '../shared/constants';
import { normalizeFilterFormDates } from '../shared/helpers/filter-form-helpers';
import {
  SupportTicketListFilterData,
  SupportTicketsState
} from './store/support-tickets.state';
import {
  MemberRanksState,
  MemberRank
} from '../shared/store/lookup-objects.state';
import DateUtility from 'app/shared/helpers/date-utility';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import * as lookupObjectsActions from '../shared/store/lookup-objects.actions';
import * as actions from './store/support-tickets.actions';

@Component({
  selector: 'support-ticket-list-filter',
  templateUrl: './support-ticket-list-filter.component.html'
})
export class SupportTicketListFilterComponent implements OnInit, OnDestroy {
  @Input() value: SupportTicketListFilterData;
  @Output()
  valueChange: EventEmitter<SupportTicketListFilterData> = new EventEmitter<
    SupportTicketListFilterData
  >();

  form: FormGroup;
  isAlive = true;

  rankList: MemberRank[] = [];
  subjectOptionList: Array<any>;

  constructor(private store: Store<any>, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();
    this.adjustControlState(this.form.value.status, this.form.value.state);

    this.store
      .select('memberRanks')
      .filter(
        (memberRanksState: MemberRanksState) =>
          memberRanksState && memberRanksState.isFetchSuccess
      )
      .map((memberRanksState: MemberRanksState) => memberRanksState.entities)
      .takeWhile(() => this.isAlive)
      .subscribe((rankList: MemberRank[]) => {
        this.rankList = rankList;
      });

    this.store
      .select('supportTickets')
      .filter(
        (supportTicketsState: SupportTicketsState) =>
          !supportTicketsState.requesting && !supportTicketsState.hasError
      )
      .map(
        (supportTicketsState: SupportTicketsState) =>
          supportTicketsState.subjectList
      )
      .takeWhile(() => this.isAlive)
      .subscribe((subjectList: Array<any>) => {
        if (subjectList) {
          this.subjectOptionList = [
            {
              id: null,
              title: 'Topic: Any'
            },
            ...subjectList.map(subject => ({
              id: subject.title,
              title: subject.title
            }))
          ];
        } else {
          this.store.dispatch({ type: actions.FETCH_SUBJECT_LIST });
        }
      });

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        this.adjustControlState(data.status, data.state);
        normalizeFilterFormDates(this.form, {
          startDateKey: 'date_start',
          endDateKey: 'date_end'
        });
      });

    this.store.dispatch({ type: lookupObjectsActions.MEMBER_RANKS_FETCH });
  }

  isStatusOpen(status: string) {
    return (
      status === 'opened' || status === 'open_user' || status === 'open_admin'
    );
  }

  getStatusFromState(state: string, currentStatus: string) {
    if (state === 'user') {
      return 'open_user';
    } else if (state === 'admin') {
      return 'open_admin';
    }

    return currentStatus;
  }

  getStateFromStatus(status: string, currentState: string) {
    if (status === 'open_user') {
      return 'user';
    } else if (status === 'open_admin') {
      return 'admin';
    }

    return currentState;
  }

  adjustControlState(status: string, state: string) {
    const isStatusOpen = this.isStatusOpen(status);

    if (isStatusOpen) {
      if (this.form.controls.state.enabled) {
        this.form.controls.state.disable();
      }

      const newState = this.getStateFromStatus(status, state);
      if (this.form.controls.state.value !== newState) {
        this.form.patchValue({ state: newState }, { emitEvent: false });
      }

      const newStatus = this.getStatusFromState(state, status);
      if (this.form.controls.status.value !== newStatus) {
        this.form.patchValue({ status: newStatus }, { emitEvent: false });
      }
    } else if (!isStatusOpen && this.form.controls.state.disabled) {
      this.form.controls.state.enable();
      this.form.patchValue({ state: null }, { emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  buildForm() {
    this.form = this.formBuilder.group({
      status: [this.value.status],
      subject: [this.value.subject],
      username: [this.value.username],
      email: [this.value.email],
      memid: [this.value.memid],
      date_start: [this.value.date_start],
      date_end: [this.value.date_end],
      state: [this.value.state],
      rank: [this.value.rank],
      assigned_to: [this.value.assigned_to]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.getRawValue() };

    formData.rank = +formData.rank || null;
    formData.subject =
      formData.subject === null || formData.subject === 'null'
        ? null
        : formData.subject;

    if (this.isStatusOpen(formData.status)) {
      formData.state = this.getStateFromStatus(formData.status, formData.state);
      formData.status = 'opened';
    }

    if (formData.date_end) {
      formData.date_end = endOfTheDayTimestamp(formData.date_end);
    }

    this.valueChange.emit(formData);
  }
}
