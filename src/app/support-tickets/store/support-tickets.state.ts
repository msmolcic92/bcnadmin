import { Action } from '@ngrx/store';
import {
  AutoStates,
  DataList,
  DefaultState,
  defaultStateValues,
  makeDefaultHandlers
} from '../../core/store/helpers';
import { Admin } from '../../admin/store/admin.state';
import * as actions from './support-tickets.actions';

export class SupportTicketListFilterData {
  subject?: string;
  status?: string;
  state?: string;
  memid?: number;
  rank?: number;
  username?: string;
  email?: string;
  date_start?: number;
  date_end?: number;
  assigned_to?: string;
}

export interface SupportTicket {
  id: number;
  memid: number;
  username: string;
  assigned_to: {
    id: number;
    fname: string;
    lname: string;
    username: string;
  };
  date_made: number;
  date_closed: number;
  status: string;
  state: string;
  last_reply_date: number;
  subject: string;
  deparment_id: number;
  deparment_name: string;
}

export interface Reply {
  id: number;
  owner: string;
  date_made: number;
  content: string;
}

// ANTIPATTERN: The types shouldn't necessarily reflect
// the HTTP response shape but instead be what makes most sense.
// The attributes of this type are named incorrectly (well,
// the overall structure is OK for the data transfer but
// should have been done differently for client-side state management)
export interface TicketDetails {
  page_number: number;
  results_per_page: number;
  total_results: number;
  last_page: number;
  data: Reply[];
  ticket: SupportTicket;
}

const INITIAL_STATE = {
  supportTickets: {
    data: null,
    page_number: 0,
    results_per_page: 0,
    total_results: 0,
    last_page: 0
  },
  filterData: null,
  currentTicket: null,
  subjectList: null,
  adminList: null,
  ...defaultStateValues
};

export interface SupportTicketsState extends DefaultState {
  supportTickets: DataList<SupportTicket>;
  filterData: SupportTicketListFilterData;
  currentTicket: TicketDetails;
  subjectList: {
    id: number;
    title: string;
  }[];
  adminList: DataList<Admin>;
}

const ACTION_HANDLERS = {
  [actions.RESET_CURRENT]: (
    state: SupportTicketsState
  ): SupportTicketsState => {
    return { ...state, currentTicket: null };
  },
  [actions.FETCH_LIST]: (state: SupportTicketsState): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_LIST_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      supportTickets: (<any>action).payload.data,
      filterData: (<any>action).payload.filterData
    };
  },
  [actions.FETCH_REPLY_LIST]: (state: SupportTicketsState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_REPLY_LIST_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      currentTicket: (<any>action).payload
    };
  },
  [actions.REPLY]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.REPLY_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.success() };
  },
  [actions.CLOSE]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.CLOSE_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.success() };
  },
  [actions.FETCH_SUBJECT_LIST]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_SUBJECT_LIST_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return {
      ...state,
      ...AutoStates.success(),
      subjectList: (<any>action).payload
    };
  },
  [actions.ASSIGN_TO_ADMIN]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.ASSIGN_TO_ADMIN_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    const selectedAdmin = state.adminList.data.find(
      admin => admin.id === (<any>action).payload.adminId
    );
    return {
      ...state,
      ...AutoStates.success(),
      currentTicket: {
        ...state.currentTicket,
        ticket: {
          ...state.currentTicket.ticket,
          assigned_to: selectedAdmin
            ? {
                id: selectedAdmin.id,
                fname: selectedAdmin.fname,
                lname: selectedAdmin.lname,
                username: selectedAdmin.username
              }
            : null
        }
      }
    };
  },
  [actions.FETCH_ADMIN_LIST]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ADMIN_LIST_SUCCESS]: (
    state: SupportTicketsState,
    action: Action
  ): SupportTicketsState => {
    return {
      ...state,
      ...AutoStates.success(),
      adminList: (<any>action).payload
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

export function supportTicketsReducer(
  state: SupportTicketsState = INITIAL_STATE,
  action: Action
): SupportTicketsState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
