export const START_REQUEST = 'supportTickets/START_REQUEST';
export const END_REQUEST = 'supportTickets/END_REQUEST';
export const ON_ERROR = 'supportTickets/ON_ERROR';

export const FETCH_LIST = 'supportTickets/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'supportTickets/FETCH_LIST_SUCCESS';
export const RESET_LIST_FILTER = 'supportTickets/RESET_LIST_FILTER';

export const FETCH_REPLY_LIST = 'supportTickets/FETCH_REPLY_LIST';
export const FETCH_REPLY_LIST_SUCCESS =
  'supportTickets/FETCH_REPLY_LIST_SUCCESS';

export const REPLY = 'supportTickets/REPLY';
export const REPLY_SUCCESS = 'supportTickets/REPLY_SUCCESS';

export const RESET_CURRENT = 'supportTickets/RESET_CURRENT';

export const CLOSE = 'supportTickets/CLOSE';
export const CLOSE_SUCCESS = 'supportTickets/CLOSE_SUCCESS';

export const FETCH_SUBJECT_LIST = 'supportTickets/FETCH_SUBJECT_LIST';
export const FETCH_SUBJECT_LIST_SUCCESS = 'supportTickets/FETCH_SUBJECT_LIST_SUCCESS';

export const FETCH_ADMIN_LIST = 'supportTickets/FETCH_ADMIN_LIST';
export const FETCH_ADMIN_LIST_SUCCESS = 'supportTickets/FETCH_ADMIN_LIST_SUCCESS';

export const ASSIGN_TO_ADMIN = 'supportTickets/ASSIGN_TO_ADMIN';
export const ASSIGN_TO_ADMIN_SUCCESS = 'supportTickets/ASSIGN_TO_ADMIN_SUCCESS';
