import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect, PaginationConfig } from '../../core/store/helpers';
import { SupportTicketsService } from '../../../api-client/support-tickets.service';
import { AdminService } from '../../../api-client/admin.service';
import { SupportTicketListFilterData } from './support-tickets.state';
import * as actions from './support-tickets.actions';

@Injectable()
export class SupportTicketsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private supportTicketsService: SupportTicketsService,
    private adminService: AdminService,
    private router: Router
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.actions$
  .ofType(actions.FETCH_LIST)
  .map(toPayload)
  .switchMap((filterData: SupportTicketListFilterData) => {
    return this.supportTicketsService
      .fetchTicketLists(filterData)
      .map((response: any) => ({
        type: actions.FETCH_LIST_SUCCESS,
        payload: {
          data: response,
          filterData
        }
      }))
      .catch(errorResponse =>
        Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        })
      );
  });

  @Effect()
  fetchTicketReplies$ = makeSimpleEffect(
    this.actions$,
    payload =>
      this.supportTicketsService.fetchTicketReplyList(payload.id, {
        page_number: payload.page_number,
        results_per_page: payload.results_per_page
      }),
    actions.FETCH_REPLY_LIST,
    actions.FETCH_REPLY_LIST_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  postReply$ = makeSimpleEffect(
    this.actions$,
    this.supportTicketsService.submitReply,
    actions.REPLY,
    actions.END_REQUEST,
    actions.ON_ERROR,
    (res, payload) => {
      // ANTIPATTERN: Use reactive operators instead,
      // you already have those values on the stream.
      // Note: The makeSimpleEffect(...) abstraction
      // is not enough for this.
      this.store.dispatch({
        type: actions.FETCH_REPLY_LIST,
        payload: {
          id: payload.id,
          currentPage: 1,
          totalItems: 0,
          itemPerPage: 25
        }
      });
    }
  );

  @Effect()
  closeTicket$ = makeSimpleEffect(
    this.actions$,
    this.supportTicketsService.closeTicket,
    actions.CLOSE,
    actions.END_REQUEST,
    actions.ON_ERROR,
    // ANTIPATTERN: See above ^^
    (res, payload) => {
      this.store.dispatch({
        type: actions.FETCH_REPLY_LIST,
        payload: {
          id: payload.id,
          currentPage: 1,
          totalItems: 0,
          itemPerPage: 25
        }
      });
    }
  );

  @Effect()
  fetchSubjectList$ = makeSimpleEffect(
    this.actions$,
    this.supportTicketsService.fetchSubjectList,
    actions.FETCH_SUBJECT_LIST,
    actions.FETCH_SUBJECT_LIST_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  assignToAdmin: Observable<Action> = this.actions$
  .ofType(actions.ASSIGN_TO_ADMIN)
  .map(toPayload)
  .switchMap((params: any) => {
    return this.supportTicketsService
      .assignToAdmin(params)
      .map((response: any) => ({
        type: actions.ASSIGN_TO_ADMIN_SUCCESS,
        payload: {
          adminId: params.adminId
        }
      }))
      .catch(errorResponse =>
        Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        })
      );
  });

  @Effect()
  fetchAdminList$ = makeSimpleEffect(
    this.actions$,
    this.adminService.fetchList,
    actions.FETCH_ADMIN_LIST,
    actions.FETCH_ADMIN_LIST_SUCCESS,
    actions.ON_ERROR
  );
}
