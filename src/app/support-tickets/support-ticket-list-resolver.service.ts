import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  SupportTicket,
  SupportTicketsState
} from './store/support-tickets.state';
import * as actions from './store/support-tickets.actions';

@Injectable()
export class SupportTicketListResolver
  implements Resolve<DataList<SupportTicket>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot) {
    const state$ = this.store.select('supportTickets');

    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        state: 'user',
        status: 'opened',
        assigned_to: 'me_or_unassigned'
      }
    });

    return state$
      .filter(
        (state: SupportTicketsState) => !!(state.hasError || state.supportTickets)
      )
      .map((state: SupportTicketsState) => state.supportTickets)
      .take(1);
  }
}
