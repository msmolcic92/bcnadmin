import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  SupportTicket,
  SupportTicketsState,
  SupportTicketListFilterData
} from './store/support-tickets.state';
import * as actions from './store/support-tickets.actions';

@Component({
  selector: 'support-ticket-list-screen',
  templateUrl: './support-ticket-list-screen.component.html',
  styleUrls: ['./support-ticket-list-screen.component.scss']
})
export class SupportTicketListScreenComponent implements OnInit, OnDestroy {
  supportTickets: DataList<SupportTicket> = null;
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  filterShown = false;
  filterData = new SupportTicketListFilterData();

  isTableLoading = true;
  isAlive = true;

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit() {
    this.store
      .select('supportTickets')
      .takeWhile(() => this.isAlive)
      .subscribe((data: SupportTicketsState) => {
        this.isTableLoading = data.requesting;
        if (data.supportTickets) {
          this.supportTickets = data.supportTickets;
          this.pagination.totalItems = this.supportTickets.total_results;
          this.pagination.itemPerPage = this.supportTickets.results_per_page;
          this.pagination.currentPage = this.supportTickets.page_number;
        }

        if (data.filterData) {
          this.filterData = data.filterData;
        }
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  handleFilterChange(filterData: SupportTicketListFilterData) {
    this.filterData = filterData;
    this.fetchData();
  }

  handlePageChange(currentPage: any) {
    this.pagination.currentPage = currentPage;
    this.fetchData();
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }
}
