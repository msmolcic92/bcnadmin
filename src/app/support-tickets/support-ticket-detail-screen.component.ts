import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Params } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { DataList, PaginationConfig } from '../core/store/helpers';
import { AppActions } from '../shared/store/app-actions.service';
import {
  TicketDetails,
  SupportTicketsState
} from './store/support-tickets.state';
import { ITEM_NAME_PLACEHOLDER } from '../shared/components';
import * as actions from './store/support-tickets.actions';

@Component({
  selector: 'support-ticket-detail-screen',
  templateUrl: './support-ticket-detail-screen.component.html',
  styleUrls: ['./support-ticket-detail-screen.component.scss']
})
export class SupportTicketDetailScreenComponent implements OnInit, OnDestroy {
  ticketDetails: TicketDetails = null;
  replyContent: string = null;

  adminList: Array<any>;
  selectedAdminId: number;
  confirmMessage = `Are you sure you want to set the assignee to ${ITEM_NAME_PLACEHOLDER}?`;

  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  isAlive = true;

  constructor(
    private route: ActivatedRoute,
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.store.dispatch({
      type: actions.FETCH_ADMIN_LIST,
      // 1000 - max. number of results to fetch.
      payload: { results_per_page: 1000 }
    });

    this.route.data.subscribe(data => {
      this.ticketDetails = data.ticketData;
    });

    this.subscribeToActions();

    this.store
      .select('supportTickets')
      .filter(
        (state: SupportTicketsState) => !state.requesting && !state.hasError
      )
      .takeWhile(() => this.isAlive)
      .subscribe((state: SupportTicketsState) => {
        if (state.currentTicket) {
          this.ticketDetails = state.currentTicket;
          this.pagination.totalItems = this.ticketDetails.total_results;
          this.pagination.itemPerPage = this.ticketDetails.results_per_page;
          this.pagination.currentPage = this.ticketDetails.page_number;
        }

        if (state.adminList) {
          this.adminList = [
            {
              id: null,
              label: 'Unassigned'
            },
            ...state.adminList.data.map(admin => ({
              id: admin.id,
              label: `${admin.fname} ${admin.lname}`
            }))
          ];

          this.selectedAdminId =
            this.ticketDetails.ticket.assigned_to &&
            this.ticketDetails.ticket.assigned_to.id;
        }
      });
  }

  ngOnDestroy() {
    this.store.dispatch({ type: actions.RESET_CURRENT });
    this.isAlive = false;
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.ASSIGN_TO_ADMIN_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe(() => {
        this.toasterService.pop(
          'success',
          'Success!',
          'Assigned user changed.'
        );
      });
  }

  submitReply(value) {
    this.store.dispatch({
      type: actions.REPLY,
      payload: {
        id: this.ticketDetails.ticket.id,
        content: value
      }
    });
    this.replyContent = '';
  }

  closeTicket() {
    if (confirm('Are you sure to close this ticket?')) {
      this.store.dispatch({
        type: actions.CLOSE,
        payload: {
          id: this.ticketDetails.ticket.id
        }
      });
    }
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.FETCH_REPLY_LIST,
      payload: {
        id: this.ticketDetails.ticket.id,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
  }

  pageChanged(currentPage: any): void {
    this.pagination.currentPage = currentPage;
    this.fetchData();
  }

  handleItemChange(adminId: number) {
    this.store.dispatch({
      type: actions.ASSIGN_TO_ADMIN,
      payload: {
        ticketId: this.ticketDetails.ticket.id,
        adminId
      }
    });
  }
}
