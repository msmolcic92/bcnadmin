import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import {
  TicketDetails,
  SupportTicketsState
} from './store/support-tickets.state';
import * as actions from './store/support-tickets.actions';

@Injectable()
export class SupportTicketDetailResolver implements Resolve<TicketDetails> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routeState: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_REPLY_LIST,
      payload: {
        id: route.params.id
      }
    });

    return this.store
      .select('supportTickets')
      .filter(
        (state: SupportTicketsState) => state.currentTicket && !state.requesting
      )
      .map((state: SupportTicketsState) => state.currentTicket)
      .take(1);
  }
}
