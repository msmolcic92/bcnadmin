import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupportTicketsRootComponent } from './support-tickets-root.component';
import { SupportTicketListScreenComponent } from './support-ticket-list-screen.component';
import { SupportTicketListResolver } from './support-ticket-list-resolver.service';
import { SupportTicketDetailScreenComponent } from './support-ticket-detail-screen.component';
import { SupportTicketDetailResolver } from './support-detail-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Support Tickets'
    },
    component: SupportTicketsRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: SupportTicketListScreenComponent,
        resolve: {
          supportTickets: SupportTicketListResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Support ticket details'
        },
        component: SupportTicketDetailScreenComponent,
        resolve: {
          ticketData: SupportTicketDetailResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportTicketsRoutingModule {}
