// Generic actions.
export const START_REQUEST = 'volumes/START_REQUEST';
export const END_REQUEST = 'volumes/END_REQUEST';
export const ON_ERROR = 'volumes/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'volumes/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'volumes/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'volumes/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'volumes/RESET_LIST_FILTER';

export const FETCH_ITEM = 'volumes/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'volumes/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_STATE_RESET = 'volumes/FETCH_ITEM_STATE_RESET';
