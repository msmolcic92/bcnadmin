import { Action } from '@ngrx/store';
import * as actions from './volumes.actions';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';

export interface Volume {
  id: number;
  volume: number;
  from_invoiceid: number;
  date_added: number;
  memid: number;
  username: string;
  email: string;
  fname: string;
  lname: string;
  from_memid: number;
  from_member_username: string;
  from_member_email: string;
  from_member_fname: string;
  from_member_lname: string;
}

export class VolumeListFilterData {
  page_number? = 1;
  results_per_page? = 25;
  memid_filter?: number;
  username_filter?: string;
  email_filter?: string;
  from_invoiceid_filter?: number;
  date_from_filter?: number;
  date_to_filter?: number;
  from_memid_filter?: number;
  from_member_username_filter?: string;
  from_member_email_filter?: string;
  order_by?: string;
  order_direction?: string;
}

export interface VolumesState extends DefaultState {
  volumes: DataList<Volume>;
  selectedItem: Volume;
  filterData: VolumeListFilterData;
}

// State initialization.
export const INITIAL_STATE: VolumesState = {
  volumes: null,
  selectedItem: null,
  filterData: new VolumeListFilterData(),
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: VolumesState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: VolumesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      volumes: { ...(<any>action).payload.data },
      filterData: { ...state.filterData, ...(<any>action).payload.filterData }
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: VolumesState) => {
    return {
      ...state,
      data: null,
      filterData: null
    };
  },
  [actions.RESET_LIST_FILTER]: (state: VolumesState) => {
    return {
      ...state,
      filterData: null
    };
  },
  [actions.FETCH_ITEM]: (state: VolumesState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: VolumesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      selectedItem: { ...state.selectedItem, ...(<any>action).payload }
    };
  },
  [actions.FETCH_ITEM_STATE_RESET]: (state: VolumesState) => {
    return {
      ...state,
      selectedItem: null
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers

export function volumesReducer(
  state: VolumesState = INITIAL_STATE,
  action: Action
): VolumesState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
