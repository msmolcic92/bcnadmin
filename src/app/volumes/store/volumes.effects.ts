import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { VolumesService } from 'api-client/volumes.service';
import { VolumeListFilterData } from './volumes.state';
import * as actions from './volumes.actions';

@Injectable()
export class VolumesEffects {
  constructor(
    private action$: Actions,
    private volumesService: VolumesService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: VolumeListFilterData) => {
      return this.volumesService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        }));
    });

  @Effect()
  fetch$ = makeSimpleEffect(
    this.action$,
    this.volumesService.fetch,
    actions.FETCH_ITEM,
    actions.FETCH_ITEM_SUCCESS,
    actions.ON_ERROR
  );
}
