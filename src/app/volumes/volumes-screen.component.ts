import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig, DataList } from '../core/store/helpers';
import {
  Volume,
  VolumesState,
  VolumeListFilterData
} from './store/volumes.state';
import * as actions from './store/volumes.actions';

@Component({
  selector: 'volumes-screen',
  templateUrl: './volumes-screen.component.html'
})
export class VolumesScreenComponent implements OnDestroy, OnInit {
  private subscription: any;

  volumes: Volume[];
  filterShown = false;
  filterData = new VolumeListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isVolumeLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const volumesState = routeData.volumes as VolumesState;
      this.volumes = volumesState.volumes && volumesState.volumes.data;
      this.filterData = volumesState.filterData;

      this.subscription = this.store
        .select('volumes')
        .subscribe((state: VolumesState) => {
          this.isVolumeLoading = state.requesting;
          if (state.volumes) {
            this.volumes = state.volumes.data;
            this.pagination = {
              currentPage: state.volumes.page_number,
              itemPerPage: state.volumes.results_per_page,
              totalItems: state.volumes.total_results
            };
          } else {
            this.volumes = null;
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isVolumeLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: VolumeListFilterData = new VolumeListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
