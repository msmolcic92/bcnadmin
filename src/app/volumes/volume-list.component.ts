import { Component, Input } from '@angular/core';

@Component({
  selector: 'volume-list',
  templateUrl: './volume-list.component.html'
})
export class VolumeListComponent {
  @Input() volumes: any = [];
  @Input() isLoading: boolean;
}
