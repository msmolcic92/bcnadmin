import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { VolumeListFilterData } from './store/volumes.state';

@Component({
  selector: 'volume-list-filter',
  templateUrl: 'volume-list-filter.component.html'
})
export class VolumeListFilterComponent implements OnInit, OnDestroy, OnChanges {
  @Input() value: VolumeListFilterData;
  @Output()
  valueChange: EventEmitter<VolumeListFilterData> = new EventEmitter<
    VolumeListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      memid_filter: [this.value.memid_filter],
      username_filter: [this.value.username_filter],
      email_filter: [this.value.email_filter],
      from_invoiceid_filter: [this.value.from_invoiceid_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter],
      from_memid_filter: [this.value.from_memid_filter],
      from_member_username_filter: [this.value.from_member_username_filter],
      from_member_email_filter: [this.value.from_member_email_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    this.valueChange.emit(formData);
  }
}
