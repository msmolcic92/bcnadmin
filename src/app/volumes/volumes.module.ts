import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VolumesEffects } from './store/volumes.effects';
import { VolumesRoutingModule } from './volumes-routing.module';
import { VolumesRootComponent } from './volumes-root.component';
import { VolumesScreenComponent } from './volumes-screen.component';
import { VolumeListComponent } from './volume-list.component';
import { VolumeListFilterComponent } from './volume-list-filter.component';
import { VolumeListResolver } from './volume-list-resolver.service';
import { VolumesService } from '../../api-client/volumes.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([VolumesEffects]),
    VolumesRoutingModule,
    NgbModule,
    SharedModule
  ],
  declarations: [
    VolumesRootComponent,
    VolumesScreenComponent,
    VolumeListComponent,
    VolumeListFilterComponent
  ],
  providers: [VolumeListResolver, VolumesService]
})
export class VolumesModule {}
