import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { VolumesState, Volume } from './store/volumes.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/volumes.actions';

@Injectable()
export class VolumeListResolver implements Resolve<DataList<Volume>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const state$ = this.store.select('volumes');

    state$
      .take(1)
      .subscribe((state: VolumesState) => {
        this.store.dispatch({
          type: actions.FETCH_LIST,
          payload: { ...state.filterData }
        });
      });

    return state$
      .filter((state: VolumesState) => !!(state.hasError || state.volumes))
      .take(1);
  }
}
