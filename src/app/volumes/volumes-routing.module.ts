import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolumesRootComponent } from './volumes-root.component';
import { VolumesScreenComponent } from './volumes-screen.component';
import { VolumeListResolver } from './volume-list-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Volumes'
    },
    component: VolumesRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: VolumesScreenComponent,
        resolve: {
          volumes: VolumeListResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VolumesRoutingModule {}
