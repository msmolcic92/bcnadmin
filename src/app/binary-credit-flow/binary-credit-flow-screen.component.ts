import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationConfig } from '../core/store/helpers';
import {
  BinaryCreditFlowDataList,
  BinaryCreditFlow,
  BinaryCreditFlowState,
  BinaryCreditFlowListFilterData
} from './store/binary-credit-flow.state';
import * as actions from './store/binary-credit-flow.actions';

@Component({
  selector: 'binary-credit-flow-screen',
  templateUrl: './binary-credit-flow-screen.component.html',
  styleUrls: ['binary-credit-flow-screen.component.scss']
})
export class BinaryCreditFlowScreenComponent implements OnInit, OnDestroy {
  private subscription: any;

  binaryCreditFlowList: BinaryCreditFlowDataList<BinaryCreditFlow>;
  filterShown = false;
  filterData = new BinaryCreditFlowListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  totalCredits: number;
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.binaryCreditFlowList =
        routeData.binaryCreditFlow.binaryCreditFlowList;
      this.filterData = routeData.binaryCreditFlow.filterData;

      this.subscription = this.store
        .select('binaryCreditFlow')
        .subscribe((state: BinaryCreditFlowState) => {
          this.isTableLoading = state.requesting;
          if (state.binaryCreditFlowList) {
            this.binaryCreditFlowList = state.binaryCreditFlowList;
            this.totalCredits = state.binaryCreditFlowList.total_credits;
            this.pagination = {
              currentPage: state.binaryCreditFlowList.page_number,
              itemPerPage: state.binaryCreditFlowList.results_per_page,
              totalItems: state.binaryCreditFlowList.total_results
            };
          } else {
            this.binaryCreditFlowList = null;
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: BinaryCreditFlowListFilterData = new BinaryCreditFlowListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
