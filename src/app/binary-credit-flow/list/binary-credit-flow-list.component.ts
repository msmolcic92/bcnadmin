import { Component, Input } from '@angular/core';

@Component({
  selector: 'binary-credit-flow-list',
  templateUrl: './binary-credit-flow-list.component.html',
  styleUrls: ['binary-credit-flow-list.component.scss']
})
export class BinaryCreditFlowListComponent {
  @Input() items: Array<any> = [];
  @Input() isLoading: boolean;
  expandedItems = [];

  toggleItemExpanded(item) {
    if (this.isItemExpanded(item)) {
      this.expandedItems = this.expandedItems.filter(i => i !== item);
    } else {
      this.expandedItems = this.expandedItems.concat(item);
    }
  }

  isItemExpanded(item) {
    return this.expandedItems.filter(i => i === item).length;
  }
}
