import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList, PaginationConfig } from '../../core/store/helpers';
import {
  BinaryCreditFlowDataList,
  BinaryCreditFlow,
  BinaryCreditFlowState
} from '../store/binary-credit-flow.state';
import * as actions from '../store/binary-credit-flow.actions';

@Injectable()
export class BinaryCreditFlowListDataResolver
  implements Resolve<BinaryCreditFlowDataList<BinaryCreditFlow>> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('binaryCreditFlow');

    state$.take(1).subscribe((state: BinaryCreditFlowState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter(
        (state: BinaryCreditFlowState) =>
          !!(state.hasError || state.binaryCreditFlowList)
      )
      .take(1);
  }
}
