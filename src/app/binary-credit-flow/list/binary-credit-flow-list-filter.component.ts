import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { BinaryCreditFlowListFilterData } from '../store/binary-credit-flow.state';

@Component({
  selector: 'binary-credit-flow-list-filter',
  templateUrl: 'binary-credit-flow-list-filter.component.html'
})
export class BinaryCreditFlowListFilterComponent implements OnInit, OnDestroy {
  @Input() value: BinaryCreditFlowListFilterData;
  @Output()
  valueChange: EventEmitter<BinaryCreditFlowListFilterData> = new EventEmitter<
    BinaryCreditFlowListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  sideOptionList = [
    { value: null, label: 'Both sides' },
    { value: 1, label: 'Left side' },
    { value: 2, label: 'Right side' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter],
      receiver_id_filter: [this.value.receiver_id_filter],
      receiver_username_filter: [this.value.receiver_username_filter],
      receiver_email_filter: [this.value.receiver_email_filter],
      invoice_id_filter: [this.value.invoice_id_filter],
      side_filter: [this.value.side_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    formData.side_filter = +formData.side_filter || null;
    this.valueChange.emit(formData);
  }
}
