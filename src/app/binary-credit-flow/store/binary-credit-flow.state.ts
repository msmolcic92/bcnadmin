import { Action } from '@ngrx/store';
import {
  AutoStates,
  DataList,
  DefaultState,
  defaultStateValues,
  makeDefaultHandlers
} from '../../core/store/helpers';
import * as actions from './binary-credit-flow.actions';

export interface BinaryCreditFlowDataList<T> extends DataList<T> {
  total_credits?: number;
}

export interface BinaryCreditFlow {
  id: number;
  invoice_id: number;
  receiver_id: number;
  receiver_username: string;
  receiver_email: string;
  receiver_fname: string;
  credits: number;
  side: number;
  timestmp: number;
}

export class BinaryCreditFlowListFilterData {
  page_number? = 1;
  results_per_page? = 25;
  date_from_filter?: number;
  date_to_filter?: number;
  receiver_id_filter?: number;
  receiver_username_filter?: string;
  receiver_email_filter?: string;
  invoice_id_filter?: number;
  side_filter?: number;
}

export interface BinaryCreditFlowState extends DefaultState {
  binaryCreditFlowList: BinaryCreditFlowDataList<BinaryCreditFlow>;
  filterData: BinaryCreditFlowListFilterData;
}

// State initialization.
export const INITIAL_STATE: BinaryCreditFlowState = {
  binaryCreditFlowList: null,
  filterData: new BinaryCreditFlowListFilterData(),
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: BinaryCreditFlowState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: BinaryCreditFlowState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      binaryCreditFlowList: { ...(<any>action).payload.data },
      filterData: { ...(<any>action).payload.filterData }
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: BinaryCreditFlowState) => {
    return {
      ...state,
      binaryCreditFlowList: null,
      filterData: null
    };
  },
  [actions.RESET_LIST_FILTER]: (state: BinaryCreditFlowState) => {
    return {
      ...state,
      filterData: null
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers.
export function binaryCreditFlowReducer(
  state: BinaryCreditFlowState = INITIAL_STATE,
  action: Action
): BinaryCreditFlowState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
