import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { BinaryCreditFlowService } from '../../../api-client/binary-credit-flow.service';
import { BinaryCreditFlowListFilterData } from './binary-credit-flow.state';
import * as actions from './binary-credit-flow.actions';

@Injectable()
export class BinaryCreditFlowEffects {
  constructor(
    private action$: Actions,
    private binaryCreditFlowService: BinaryCreditFlowService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: BinaryCreditFlowListFilterData) => {
      return this.binaryCreditFlowService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        }));
    });
}
