// Generic actions.
export const START_REQUEST = 'binaryCreditFlow/START_REQUEST';
export const END_REQUEST = 'binaryCreditFlow/END_REQUEST';
export const ON_ERROR = 'binaryCreditFlow/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'binaryCreditFlow/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'binaryCreditFlow/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'binaryCreditFlow/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'binaryCreditFlow/RESET_LIST_FILTER';
