import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { binaryCreditFlowReducer } from './store/binary-credit-flow.state';
import { BinaryCreditFlowRoutingModule } from './binary-credit-flow-routing.module';
import { BinaryCreditFlowRootComponent } from './binary-credit-flow-root.component';
import { BinaryCreditFlowScreenComponent } from './binary-credit-flow-screen.component';
import { BinaryCreditFlowListFilterComponent } from './list/binary-credit-flow-list-filter.component';
import { BinaryCreditFlowListComponent } from './list/binary-credit-flow-list.component';
import { BinaryCreditFlowEffects } from './store/binary-credit-flow.effects';
import { BinaryCreditFlowService } from '../../api-client/binary-credit-flow.service';
import { BinaryCreditFlowListDataResolver } from './list/binary-credit-flow-list-data-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([BinaryCreditFlowEffects]),
    StoreModule.forFeature('binaryCreditFlow', binaryCreditFlowReducer),
    BinaryCreditFlowRoutingModule
  ],
  declarations: [
    BinaryCreditFlowRootComponent,
    BinaryCreditFlowScreenComponent,
    BinaryCreditFlowListFilterComponent,
    BinaryCreditFlowListComponent
  ],
  providers: [BinaryCreditFlowService, BinaryCreditFlowListDataResolver]
})
export class BinaryCreditFlowModule {}
