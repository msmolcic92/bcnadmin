import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as actions from './store/binary-credit-flow.actions';

@Component({
  template: '<router-outlet></router-outlet>'
})
export class BinaryCreditFlowRootComponent implements OnDestroy {
  constructor(private store: Store<any>) {}

  ngOnDestroy() {
    this.store.dispatch({
      type: actions.RESET_LIST_FILTER
    });
  }
}
