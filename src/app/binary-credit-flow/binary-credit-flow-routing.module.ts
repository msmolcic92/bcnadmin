import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinaryCreditFlowRootComponent } from './binary-credit-flow-root.component';
import { BinaryCreditFlowScreenComponent } from './binary-credit-flow-screen.component';
import { BinaryCreditFlowListDataResolver } from './list/binary-credit-flow-list-data-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Binary Credit Flow'
    },
    component: BinaryCreditFlowRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: BinaryCreditFlowScreenComponent,
        resolve: {
          binaryCreditFlow: BinaryCreditFlowListDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BinaryCreditFlowRoutingModule {}
