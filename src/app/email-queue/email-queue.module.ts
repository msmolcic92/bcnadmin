import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { emailQueueReducer } from './store/email-queue.state';
import { EmailQueueEffects } from './store/email-queue.effects';
import { EmailQueueRoutingModule } from './email-queue-routing.module';
import { EmailQueueRootComponent } from './email-queue-root.component';
import { EmailQueueScreenComponent } from './email-queue-screen.component';
import { EmailQueueDetailsScreenComponent } from './email-queue-details-screen.component';
import { EmailQueueListComponent } from './email-queue-list.component';
import { EmailQueueListFilterComponent } from './email-queue-list-filter.component';
import { EmailQueueListResolver } from './email-queue-list-resolver.service';
import { EmailQueueDetailsResolver } from './email-queue-details-resolver.service';
import { EmailQueueService } from '../../api-client/email-queue.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([EmailQueueEffects]),
    StoreModule.forFeature('emailQueue', emailQueueReducer),
    EmailQueueRoutingModule,
    NgbModule,
    SharedModule
  ],
  declarations: [
    EmailQueueRootComponent,
    EmailQueueDetailsScreenComponent,
    EmailQueueScreenComponent,
    EmailQueueListComponent,
    EmailQueueListFilterComponent
  ],
  providers: [
    EmailQueueListResolver,
    EmailQueueDetailsResolver,
    EmailQueueService
  ]
})
export class EmailQueueModule {}
