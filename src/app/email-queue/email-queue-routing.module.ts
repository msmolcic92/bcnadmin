import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailQueueRootComponent } from './email-queue-root.component';
import { EmailQueueScreenComponent } from './email-queue-screen.component';
import { EmailQueueDetailsScreenComponent } from './email-queue-details-screen.component';
import { EmailQueueDetailsResolver } from './email-queue-details-resolver.service';
import { EmailQueueListResolver } from './email-queue-list-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Email Queue'
    },
    component: EmailQueueRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: EmailQueueScreenComponent,
        resolve: {
          emailQueue: EmailQueueListResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Email Details'
        },
        component: EmailQueueDetailsScreenComponent,
        resolve: {
          emailQueue: EmailQueueDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailQueueRoutingModule {}
