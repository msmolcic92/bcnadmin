import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from './store/email-queue.actions';
import {
  EmailQueueState,
  Email
} from './store/email-queue.state';

@Injectable()
export class EmailQueueDetailsResolver implements Resolve<Email> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('emailQueue')
      .filter(
        (emailQueueState: EmailQueueState) =>
          emailQueueState.selectedItem && !emailQueueState.requesting
      )
      .map((emailQueueState: EmailQueueState) => {
        return emailQueueState.selectedItem;
      })
      .take(1);
  }
}
