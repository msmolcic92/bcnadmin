import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { EmailQueueService } from 'api-client/email-queue.service';
import { EmailQueueListFilterData } from './email-queue.state';
import * as actions from './email-queue.actions';

@Injectable()
export class EmailQueueEffects {
  constructor(
    private action$: Actions,
    private emailQueueService: EmailQueueService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: EmailQueueListFilterData) => {
      return this.emailQueueService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        }));
    });

  @Effect()
  fetch$ = makeSimpleEffect(
    this.action$,
    this.emailQueueService.fetch,
    actions.FETCH_ITEM,
    actions.FETCH_ITEM_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  fetchTemplates$ = makeSimpleEffect(
    this.action$,
    this.emailQueueService.fetchTemplates,
    actions.FETCH_TEMPLATES,
    actions.FETCH_TEMPLATES_SUCCESS,
    actions.ON_ERROR
  );
}
