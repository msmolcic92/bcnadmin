// Generic actions.
export const START_REQUEST = 'emailQueue/START_REQUEST';
export const END_REQUEST = 'emailQueue/END_REQUEST';
export const ON_ERROR = 'emailQueue/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'emailQueue/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'emailQueue/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'emailQueue/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'emailQueue/RESET_LIST_FILTER';

export const FETCH_ITEM = 'emailQueue/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'emailQueue/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_STATE_RESET = 'emailQueue/FETCH_ITEM_STATE_RESET';

export const FETCH_TEMPLATES = 'emailQueue/FETCH_TEMPLATES';
export const FETCH_TEMPLATES_SUCCESS = 'emailQueue/FETCH_TEMPLATES_SUCCESS';
