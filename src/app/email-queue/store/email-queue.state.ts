import { Action } from '@ngrx/store';
import * as actions from './email-queue.actions';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';

export interface EmailTemplate {
  name: string;
  title: string;
}

export interface Email {
  id: number;
  memid: number;
  username: string;
  email: string;
  template: string;
  from_email: string;
  to_email: string;
  subject: string;
  created_on: number;
  sent_on: number;
}

export class EmailQueueListFilterData {
  page_number? = 1;
  results_per_page? = 25;
  memid_filter?: number;
  username_filter?: string;
  email_filter?: string;
  template_filter?: string;
  from_email_filter?: string;
  to_email_filter?: string;
  date_from_filter?: number;
  date_to_filter?: number;
  sent_filter?: number;
  order_by?: string;
  order_direction?: string;
}

export interface EmailQueueState extends DefaultState {
  emailQueue: DataList<Email>;
  selectedItem: Email;
  filterData: EmailQueueListFilterData;
  emailTemplates: EmailTemplate[];
}

// State initialization.
export const INITIAL_STATE: EmailQueueState = {
  emailQueue: null,
  selectedItem: null,
  filterData: new EmailQueueListFilterData(),
  emailTemplates: null,
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: EmailQueueState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: EmailQueueState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      emailQueue: { ...(<any>action).payload.data },
      filterData: { ...state.filterData, ...(<any>action).payload.filterData }
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: EmailQueueState) => {
    return {
      ...state,
      data: null,
      filterData: null
    };
  },
  [actions.RESET_LIST_FILTER]: (state: EmailQueueState) => {
    return {
      ...state,
      filterData: null
    };
  },
  [actions.FETCH_ITEM]: (state: EmailQueueState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: EmailQueueState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      selectedItem: { ...state.selectedItem, ...(<any>action).payload }
    };
  },
  [actions.FETCH_ITEM_STATE_RESET]: (state: EmailQueueState) => {
    return {
      ...state,
      selectedItem: null
    };
  },
  [actions.FETCH_TEMPLATES]: (state: EmailQueueState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_TEMPLATES_SUCCESS]: (
    state: EmailQueueState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      emailTemplates: (<any>action).payload.items
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers

export function emailQueueReducer(
  state: EmailQueueState = INITIAL_STATE,
  action: Action
): EmailQueueState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
