import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Email } from './store/email-queue.state';
import * as actions from './store/email-queue.actions';

@Component({
  selector: 'email-queue-details-screen',
  templateUrl: 'email-queue-details-screen.component.html'
})
export class EmailQueueDetailsScreenComponent implements OnDestroy, OnInit {
  email: Email;

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.email = data.emailQueue;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
  }
}
