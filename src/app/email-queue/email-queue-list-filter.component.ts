import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { isNumeric } from 'rxjs/util/isNumeric';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import {
  EmailQueueListFilterData,
  EmailQueueState
} from './store/email-queue.state';
import * as actions from './store/email-queue.actions';

@Component({
  selector: 'email-queue-list-filter',
  templateUrl: 'email-queue-list-filter.component.html'
})
export class EmailQueueListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: EmailQueueListFilterData;
  @Output()
  valueChange: EventEmitter<EmailQueueListFilterData> = new EventEmitter<
    EmailQueueListFilterData
  >();

  form: FormGroup;
  isAlive = true;

  templateOptionList: Array<any>;
  sentOptionList = [
    { value: null, label: 'Both sent and not sent' },
    { value: 1, label: 'Sent only' },
    { value: 0, label: 'Not sent only' }
  ];

  constructor(private store: Store<any>, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        normalizeFilterFormDates(this.form);
      });

    this.store
      .select('emailQueue')
      .filter((state: EmailQueueState) => !state.requesting)
      .takeWhile(() => this.isAlive)
      .subscribe((state: EmailQueueState) => {
        if (state.emailTemplates) {
          this.templateOptionList = new Array<any>();
          this.templateOptionList.push({
            name: null,
            title: 'All Templates'
          });
          this.templateOptionList.push(...state.emailTemplates);
        } else {
          this.store.dispatch({ type: actions.FETCH_TEMPLATES });
        }
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      memid_filter: [this.value.memid_filter],
      username_filter: [this.value.username_filter],
      email_filter: [this.value.email_filter],
      template_filter: [this.value.template_filter],
      from_email_filter: [this.value.from_email_filter],
      to_email_filter: [this.value.to_email_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter],
      sent_filter: [this.value.sent_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    formData.template_filter =
      formData.template_filter === 'null' || formData.template_filter === null
        ? null
        : formData.template_filter;

    formData.sent_filter = isNumeric(formData.sent_filter)
      ? +formData.sent_filter
      : null;

    this.valueChange.emit(formData);
  }
}
