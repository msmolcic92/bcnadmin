import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig, DataList } from '../core/store/helpers';
import {
  Email,
  EmailQueueState,
  EmailQueueListFilterData
} from './store/email-queue.state';
import * as actions from './store/email-queue.actions';

@Component({
  selector: 'email-queue-screen',
  templateUrl: './email-queue-screen.component.html'
})
export class EmailQueueScreenComponent implements OnDestroy, OnInit {
  private subscription: any;

  emailQueue: Email[];
  filterShown = false;
  filterData = new EmailQueueListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const emailQueueState = routeData.emailQueue as EmailQueueState;
      this.emailQueue = emailQueueState.emailQueue.data;
      this.filterData = emailQueueState.filterData;

      this.subscription = this.store
        .select('emailQueue')
        .subscribe((state: EmailQueueState) => {
          this.isTableLoading = state.requesting;

          if (state.emailQueue) {
            this.emailQueue = state.emailQueue.data;
            this.pagination = {
              currentPage: state.emailQueue.page_number,
              itemPerPage: state.emailQueue.results_per_page,
              totalItems: state.emailQueue.total_results
            };
          } else {
            this.emailQueue = null;
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: EmailQueueListFilterData = new EmailQueueListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
