import { Component, Input } from '@angular/core';

@Component({
  selector: 'email-queue-list',
  templateUrl: './email-queue-list.component.html'
})
export class EmailQueueListComponent {
  @Input() emailQueue: any = [];
  @Input() isLoading: boolean;
}
