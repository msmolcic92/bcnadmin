import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { EmailQueueState, Email } from './store/email-queue.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/email-queue.actions';

@Injectable()
export class EmailQueueListResolver implements Resolve<DataList<Email>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('emailQueue');

    state$.take(1).subscribe((state: EmailQueueState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: {
          filterData: state.filterData
        }
      });
    });

    return state$
      .filter(
        (state: EmailQueueState) => !!(state.hasError || state.emailQueue)
      )
      .take(1);
  }
}
