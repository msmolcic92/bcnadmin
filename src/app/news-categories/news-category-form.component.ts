import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { BaseForm } from '../shared/base-form';
import { AppActions } from '../shared/store/app-actions.service';
import { NewsCategoriesState } from './store/news-categories.state';
import * as actions from './store/news-categories.actions';

export class NewsCategoryFormData {
  id?: number;
  title?: string;
  show_order?: number;
  status?: number;
}

const FORM_VALIDATION_MESSAGES = {
  title: {
    required: 'Title is required.'
  },
  show_order: {
    required: 'Priority is required.'
  }
};

@Component({
  selector: 'news-category-form',
  templateUrl: 'news-category-form.component.html'
})
export class NewsCategoryFormComponent extends BaseForm implements OnInit {
  @Input() formData: NewsCategoryFormData;
  @Input() isEdit: boolean;

  categoryOptionList: Array<any>;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  buildForm(): void {
    if (!this.formData) {
      this.formData = new NewsCategoryFormData();
    }

    this.form = this.formBuilder.group({
      title: [this.formData.title, Validators.required],
      show_order: [this.formData.show_order, Validators.required],
      status: [this.formData.status || 0]
    });

  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? actions.UPDATE_SUCCESS
      : actions.CREATE_SUCCESS;

    const failureType = this.isEdit
      ? actions.UPDATE_FAILURE
      : actions.CREATE_FAILURE;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(failureType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload);
      });
  }

  submit(): void {
    this.clearFormErrors();
    const actionType = this.isEdit ? actions.UPDATE : actions.CREATE;

    const data = { ...this.form.value };
    data.status = +(data.status || 0);
    data.show_order = +(data.show_order || 0);

    this.store.dispatch({
      type: actionType,
      payload: {
        id: this.formData.id,
        data
      }
    });
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
