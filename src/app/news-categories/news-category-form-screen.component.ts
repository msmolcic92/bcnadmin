import { Component, OnInit, Input } from '@angular/core';
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import {
  NewsCategoryItem,
  NewsCategoriesState
} from './store/news-categories.state';
import { NewsCategoryFormData } from './news-category-form.component';
import * as actions from './store/news-categories.actions';

@Component({
  selector: 'news-cagtegory-form-screen',
  templateUrl: 'news-category-form-screen.component.html'
})
export class NewsCategoryFormScreenComponent implements OnInit {
  formData: NewsCategoryFormData;
  currency: string;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (!this.isEdit) {
        this.formData = {
          show_order: 0,
          status: 1
        };
      } else {
        const selectedItem = routeData.newsCategoryData;
        this.formData = {
          id: selectedItem.id,
          title: selectedItem.title,
          show_order: selectedItem.show_order,
          status: selectedItem.status
        };
      }
    });
  }

  handleFormSuccess($event): void {
    this.navigateToList();
  }

  handleFormCancel(): void {
    this.navigateToList();
  }

  navigateToList(): void {
    this.router.navigate(['news-categories']);
  }
}
