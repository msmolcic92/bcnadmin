import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsCategoryDataResolver } from './news-category-data-resolver.service';
import { NewsCategoriesListResolver } from './news-categories-list-data-resolver.service';
import { NewsCategoriesRootComponent } from './news-categories-root.component';
import { NewsCategoriesScreenComponent } from './news-categories-screen.component';
import { NewsCategoryFormScreenComponent } from './news-category-form-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'News Categories'
    },
    component: NewsCategoriesRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: NewsCategoriesScreenComponent,
        resolve: {
          newsCategories: NewsCategoriesListResolver
        }
      },
      {
        path: 'add',
        data: {
          title: 'Add news category',
          isEdit: false
        },
        component: NewsCategoryFormScreenComponent
      },
      {
        path: ':id/edit',
        data: {
          title: 'Edit news category',
          isEdit: true
        },
        component: NewsCategoryFormScreenComponent,
        resolve: {
          newsCategoryData: NewsCategoryDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsCategoriesRoutingModule { }
