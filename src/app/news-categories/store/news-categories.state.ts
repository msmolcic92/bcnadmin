import { Action } from '@ngrx/store';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';
import * as actions from './news-categories.actions';

export interface NewsCategoryItem {
  id: number;
  title: string;
  status: number;
  show_order: number;
  date_added: number;
}

export interface NewsCategoriesState extends DefaultState {
  data: DataList<NewsCategoryItem>;
}

// State initialization.
export const INITIAL_STATE: NewsCategoriesState = {
  data: {
    data: [],
    total_results: 0,
    page_number: 1,
    last_page: 1,
    results_per_page: 25
  },
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: NewsCategoriesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (
    state: NewsCategoriesState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: { ...(<any>action).payload }
    };
  },
  [actions.FETCH_ITEM]: (state: NewsCategoriesState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: NewsCategoriesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: {
        ...state.data,
        data: addOrUpdateItem(state, (<any>action).payload)
      }
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: NewsCategoriesState) => {
    return {
      ...state,
      data: null
    };
  },
  [actions.CREATE]: (state: NewsCategoriesState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.CREATE_SUCCESS]: (state: NewsCategoriesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: {
        ...updatePagination(state, 1),
        data: addOrUpdateItem(state, (<any>action).payload)
      }
    };
  },
  [actions.UPDATE]: (state: NewsCategoriesState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.UPDATE_SUCCESS]: (state: NewsCategoriesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: {
        ...state.data,
        data: addOrUpdateItem(state, (<any>action).payload)
      }
    };
  },
  [actions.DELETE]: (state: NewsCategoriesState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.DELETE_SUCCESS]: (state: NewsCategoriesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: {
        ...updatePagination(state, -1),
        data: state.data.data.filter(
          earning => earning.id !== (<any>action).payload.id
        )
      }
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers

export function newsCategoriesReducer(
  state: NewsCategoriesState = INITIAL_STATE,
  action: Action
): NewsCategoriesState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}

// Local helpers

function addOrUpdateItem(
  state: NewsCategoriesState,
  item: NewsCategoryItem
): NewsCategoryItem[] {
  if (!state.data.data.find(i => i.id === item.id)) {
    return [...state.data.data, { ...item }];
  }

  return state.data.data.filter(
    i => (i.id === item.id ? { ...i, ...item } : i)
  );
}

function updatePagination(
  state: NewsCategoriesState,
  countIncrement: number
): DataList<any> {
  const totalResults = state.data.total_results + countIncrement;
  const lastPage = Math.ceil(totalResults / state.data.results_per_page);
  const pageNumber =
    state.data.page_number > lastPage ? lastPage : state.data.page_number;

  return {
    ...state.data,
    last_page: lastPage,
    page_number: pageNumber,
    total_results: totalResults
  };
}

// Selectors

export const getItemById = (state: NewsCategoriesState, id: number): NewsCategoryItem => {
  return state.data.data.filter(item => item.id === id)[0];
};
