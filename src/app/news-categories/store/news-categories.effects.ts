import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { NewsCategoriesService } from 'api-client/news-categories.service';
import * as actions from './news-categories.actions';

@Injectable()
export class NewsCategoriesEffects {
  constructor(
    private action$: Actions,
    private newsCategoriesService: NewsCategoriesService
  ) {}

  @Effect()
  fetchList$ = makeSimpleEffect(
    this.action$,
    this.newsCategoriesService.fetchList,
    actions.FETCH_LIST,
    actions.FETCH_LIST_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.newsCategoriesService
        .fetch(params.id)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_SUCCESS,
          payload: response
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  create$: Observable<Action> = this.action$
    .ofType(actions.CREATE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.newsCategoriesService
        .create(payload.data)
        .map((response: any) => ({
          type: actions.CREATE_SUCCESS,
          payload: {
            id: response.id,
            ...payload
          }
        }))
        .do((value: any) => { console.log(value) })
        .catch(errorResponse => Observable.of({
          type: actions.CREATE_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  update$: Observable<Action> = this.action$
    .ofType(actions.UPDATE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.newsCategoriesService
        .update(payload.id, payload.data)
        .map((response: any) => ({
          type: actions.UPDATE_SUCCESS,
          payload
        }))
        .catch(errorResponse => Observable.of({
          type: actions.UPDATE_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  delete$: Observable<Action> = this.action$
    .ofType(actions.DELETE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.newsCategoriesService
        .delete(payload.id)
        .map((response: any) => ({
          type: actions.DELETE_SUCCESS,
          payload
        }))
        .catch(errorResponse => Observable.of({
          type: actions.DELETE_FAILURE,
          payload: errorResponse.error
        }));
    });
}
