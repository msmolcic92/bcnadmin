// Generic actions.
export const START_REQUEST = 'newsCategories/START_REQUEST';
export const END_REQUEST = 'newsCategories/END_REQUEST';
export const ON_ERROR = 'newsCategories/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'newsCategories/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'newsCategories/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'newsCategories/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'newsCategories/RESET_LIST_FILTER';

export const FETCH_ITEM = 'newsCategories/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'newsCategories/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_FAILURE = 'newsCategories/FETCH_ITEM_FAILURE';
export const FETCH_ITEM_STATE_RESET = 'newsCategories/FETCH_ITEM_STATE_RESET';

export const CREATE = 'newsCategories/CREATE';
export const CREATE_SUCCESS = 'newsCategories/CREATE_SUCCESS';
export const CREATE_FAILURE = 'newsCategories/CREATE_FAILURE';

export const UPDATE = 'newsCategories/UPDATE';
export const UPDATE_SUCCESS = 'newsCategories/UPDATE_SUCCESS';
export const UPDATE_FAILURE = 'newsCategories/UPDATE_FAILURE';

export const DELETE = 'newsCategories/DELETE';
export const DELETE_SUCCESS = 'newsCategories/DELETE_SUCCESS';
export const DELETE_FAILURE = 'newsCategories/DELETE_FAILURE';
