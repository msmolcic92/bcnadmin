import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from './store/news-categories.actions';
import {
  NewsCategoriesState,
  NewsCategoryItem,
  getItemById
} from './store/news-categories.state';

@Injectable()
export class NewsCategoryDataResolver implements Resolve<NewsCategoryItem> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('newsCategories')
      .filter((state: NewsCategoriesState) => !state.requesting)
      .map((state: NewsCategoriesState) => {
        return getItemById(state, +route.params.id);
      })
      .take(1);
  }
}
