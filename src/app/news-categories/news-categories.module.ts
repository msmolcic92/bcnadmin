import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { NewsCategoryDataResolver } from './news-category-data-resolver.service';
import { newsCategoriesReducer } from './store/news-categories.state';
import { NewsCategoriesEffects } from './store/news-categories.effects';
import { NewsCategoriesService } from '../../api-client/news-categories.service';
import { NewsCategoriesRoutingModule } from './news-categories-routing.module';
import { NewsCategoriesListResolver } from './news-categories-list-data-resolver.service';
import { NewsCategoriesRootComponent } from './news-categories-root.component';
import { NewsCategoriesScreenComponent } from './news-categories-screen.component';
import { NewsCategoriesListComponent } from './news-categories-list.component';
import { NewsCategoryFormScreenComponent } from './news-category-form-screen.component';
import { NewsCategoryFormComponent } from './news-category-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([NewsCategoriesEffects]),
    StoreModule.forFeature('newsCategories', newsCategoriesReducer),
    NewsCategoriesRoutingModule
  ],
  declarations: [
    NewsCategoriesRootComponent,
    NewsCategoriesScreenComponent,
    NewsCategoriesListComponent,
    NewsCategoryFormComponent,
    NewsCategoryFormScreenComponent
  ],
  providers: [
    NewsCategoriesService,
    NewsCategoriesListResolver,
    NewsCategoryDataResolver
  ]
})
export class NewsCategoriesModule {}
