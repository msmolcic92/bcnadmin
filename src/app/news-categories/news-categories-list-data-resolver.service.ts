import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { NewsCategoriesState } from './store/news-categories.state';
import * as actions from './store/news-categories.actions';

@Injectable()
export class NewsCategoriesListResolver implements Resolve<any> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot) {
    this.store.dispatch({ type: actions.FETCH_LIST });

    return this.store
      .select('newsCategories')
      .filter(
        (state: NewsCategoriesState) => !state.requesting && !state.hasError
      )
      .take(1);
  }
}
