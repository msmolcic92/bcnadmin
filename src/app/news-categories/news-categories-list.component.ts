import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NewsCategoryItem } from './store/news-categories.state';

@Component({
  selector: 'news-categories-list',
  templateUrl: 'news-categories-list.component.html'
})
export class NewsCategoriesListComponent {
  @Input() newsCategoriesList: NewsCategoryItem[];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  handleItemDeleteClick(id: number) {
    this.itemDeleteClick.emit(id);
  }
}
