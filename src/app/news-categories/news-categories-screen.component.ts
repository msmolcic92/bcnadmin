import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  NewsCategoryItem,
  NewsCategoriesState
} from './store/news-categories.state';
import * as actions from './store/news-categories.actions';

@Component({
  selector: 'news-categories-screen',
  templateUrl: 'news-categories-screen.component.html'
})
export class NewsCategoriesScreenComponent implements OnDestroy, OnInit {
  newsCategoriesList: DataList<NewsCategoryItem>;
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  filterShown = false;
  isLoading = true;
  isAlive = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.newsCategoriesList = routeData.newsCategories;

      this.store
        .select('newsCategories')
        .takeWhile(() => this.isAlive)
        .subscribe((state: NewsCategoriesState) => {
          this.isLoading = state.requesting;

          if (state.data) {
            this.newsCategoriesList = state.data;
            this.pagination = {
              currentPage: this.newsCategoriesList.page_number,
              itemPerPage: this.newsCategoriesList.results_per_page,
              totalItems: this.newsCategoriesList.total_results
            };
          } else {
            this.newsCategoriesList = null;
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });

    this.isLoading = true;
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }

  handleItemDeleteClick(id: number): void {
    if (!confirm('Are you sure you want to permanently delete this item?')) {
      return;
    }

    this.store.dispatch({
      type: actions.DELETE,
      payload: {
        id: id
      }
    });
  }
}
