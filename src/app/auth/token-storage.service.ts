import { Injectable } from '@angular/core';

const TOKEN_STORAGE_KEY = 'token_data';
const localStorage = window.localStorage;

@Injectable()
export class TokenStorageService {

  store(tokenData: object) {
    if (!tokenData) {
      this.clear();
      return;
    }

    localStorage.setItem(TOKEN_STORAGE_KEY, JSON.stringify(tokenData));
  }

  retrieve() {
    return JSON.parse(localStorage.getItem(TOKEN_STORAGE_KEY) || '{}');
  }

  clear() {
    localStorage.removeItem(TOKEN_STORAGE_KEY);
  }
}
