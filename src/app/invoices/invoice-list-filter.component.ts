import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { isNumeric } from 'rxjs/util/isNumeric';
import { Subscription } from 'rxjs/Subscription';
import { SECONDS_IN_A_DAY } from '../shared/constants';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { InvoiceListFilterData } from './store/invoices.state';

@Component({
  selector: 'invoice-list-filter',
  templateUrl: 'invoice-list-filter.component.html'
})
export class InvoiceListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: InvoiceListFilterData;
  @Output()
  valueChange: EventEmitter<InvoiceListFilterData> = new EventEmitter<
    InvoiceListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  productOptionList = [
    { value: null, label: 'All' },
    { value: 'clubcoin', label: 'ClubCoin' },
    { value: 'membership_fee', label: 'Membership Fee' },
    { value: 'wallet_deposit', label: 'Wallet Deposit' },
    { value: 'mining_pools', label: 'Full Shares' },
    { value: 'mining_pools_partial', label: 'Partial Shares' },
    { value: 'ethereum_pool', label: 'ETH Shares' },
    { value: 'zcash_pool', label: 'Zcash Shares' }
  ];

  paidOptionList = [
    { value: null, label: 'Both paid and non-paid' },
    { value: 1, label: 'Paid only' },
    { value: 0, label: 'Non-paid only' }
  ];

  confirmedOptionList = [
    { value: null, label: 'Both confirmed and non-confirmed' },
    { value: 1, label: 'Confirmed only' },
    { value: 0, label: 'Non-confirmed only' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      invoice_id_filter: [this.value.invoice_id_filter],
      memid_filter: [this.value.memid_filter],
      username_filter: [this.value.username_filter],
      email_filter: [this.value.email_filter],
      paid_filter: [this.value.paid_filter],
      confirmed_filter: [this.value.confirmed_filter],
      product_filter: [this.value.product_filter],
      option_filter: [this.value.option_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    formData.product_filter =
      formData.product_filter === 'null' || formData.product_filter === null
        ? null
        : formData.product_filter;

    formData.paid_filter = isNumeric(formData.paid_filter)
      ? +formData.paid_filter
      : null;

    formData.confirmed_filter = isNumeric(formData.confirmed_filter)
      ? +formData.confirmed_filter
      : null;

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    this.valueChange.emit(formData);
  }
}
