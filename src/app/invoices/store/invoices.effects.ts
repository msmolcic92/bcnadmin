import { Injectable } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { saveAs } from 'file-saver';
import { InvoicesService } from '../../../api-client/invoices.service';
import { VolumesService } from '../../../api-client/volumes.service';
import { BinaryCreditFlowService } from '../../../api-client/binary-credit-flow.service';
import { MembersService } from '../../../api-client/members.service';
import { InvoiceListFilterData } from './invoices.state';
import * as actions from './invoices.actions';

@Injectable()
export class InvoicesEffects {
  constructor(
    private store: Store<any>,
    private action$: Actions,
    private invoicesService: InvoicesService,
    private membersService: MembersService,
    private volumesService: VolumesService,
    private binaryCreditFlowService: BinaryCreditFlowService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: InvoiceListFilterData) => {
      return this.invoicesService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((params: any) => {
      return Observable.zip(
        this.invoicesService.fetch(params.id),
        this.volumesService.fetchList({ from_invoiceid_filter: params.id }),
        this.binaryCreditFlowService.fetchList({
          invoice_id_filter: params.id
        }),
        (invoice, volumes, flowList) => ({
          invoice: invoice,
          volumes: volumes,
          flowList: flowList
        })
      )
        .concatMap((invoiceDetails: any) => {
          return this.membersService
            .fetch(invoiceDetails.invoice.memid)
            .map((member: any) => {
              return {
                type: actions.FETCH_ITEM_SUCCESS,
                payload: { ...invoiceDetails, member: member }
              };
            });
        })
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  // TODO: Combine with current state's `pdf_status`
  // and check if the latter is `ready`, then stop the
  // effect for that particular flow (TODO: find out how)
  @Effect()
  fetchItemPdfStatus$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM_PDF_STATUS)
    .map(toPayload)
    .switchMap((requestData: any) => {
      return this.invoicesService
        .fetchPdfStatus(requestData.invoiceId)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_PDF_STATUS_SUCCESS,
          payload: {
            invoiceId: requestData.invoiceId,
            pdfStatusData: {
              pdf_num_status: response.status,
              pdf_status: response.statusDescription
            }
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_ITEM_PDF_STATUS_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchItemPdfContents$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM_PDF)
    .map(toPayload)
    .switchMap((requestData: any) => {
      return this.invoicesService
        .fetchPdf(requestData.invoiceId)
        .map((fileContents) => {
          // Side effect of actually sending the file to the user
          saveAs(fileContents, `invoice_${requestData.invoiceId}`);

          // Then return the SUCCESS action
          return {
            type: actions.FETCH_ITEM_PDF_SUCCESS,
            payload: {
              invoiceId: requestData.invoiceId,
              // fileContents: response
            }
          };
        })
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_ITEM_PDF_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  confirmInvoice$: Observable<Action> = this.action$
    .ofType(actions.CONFIRM_INVOICE)
    .map(toPayload)
    .switchMap((requestData: any) => {
      return this.invoicesService
        .confirmInvoice(requestData.invoiceId)
        .map((response: any) => ({
          type: actions.CONFIRM_INVOICE_SUCCESS,
          payload: {
            invoiceId: requestData.invoiceId
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.CONFIRM_INVOICE_FAILURE,
            payload: errorResponse.error
          })
        );
    })
}
