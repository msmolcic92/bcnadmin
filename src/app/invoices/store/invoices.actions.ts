// Generic actions.
export const START_REQUEST = 'invoices/START_REQUEST';
export const END_REQUEST = 'invoices/END_REQUEST';
export const ON_ERROR = 'invoices/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'invoices/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'invoices/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'invoices/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'invoices/RESET_LIST_FILTER';

export const FETCH_ITEM = 'invoices/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'invoices/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_STATE_RESET = 'invoices/FETCH_ITEM_STATE_RESET';

export const FETCH_ITEM_PDF_STATUS = 'invoices/FETCH_ITEM_PDF_STATUS';
export const FETCH_ITEM_PDF_STATUS_SUCCESS = 'invoices/FETCH_ITEM_PDF_STATUS_SUCCESS';
export const FETCH_ITEM_PDF_STATUS_FAILURE = 'invoices/FETCH_ITEM_PDF_STATUS_FAILURE';

export const FETCH_ITEM_PDF = 'invoices/FETCH_ITEM_PDF';
export const FETCH_ITEM_PDF_SUCCESS = 'invoices/FETCH_ITEM_PDF_SUCCESS';
export const FETCH_ITEM_PDF_FAILURE = 'invoices/FETCH_ITEM_PDF_FAILURE';

export const CONFIRM_INVOICE = 'invoices/CONFIRM_INVOICE';
export const CONFIRM_INVOICE_SUCCESS = 'invoices/CONFIRM_INVOICE_SUCCESS';
export const CONFIRM_INVOICE_FAILURE = 'invoices/CONFIRM_INVOICE_FAILURE';
