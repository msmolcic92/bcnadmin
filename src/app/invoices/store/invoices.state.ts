import { isPlatformBrowser } from '@angular/common';
import { Action } from '@ngrx/store';
import { Member } from '../../members/store/members.state';
import * as actions from './invoices.actions';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';

export class InvoiceListFilterData {
  invoice_id_filter?: number;
  memid_filter?: number;
  username_filter?: string;
  email_filter?: string;
  paid_filter?: number;
  confirmed_filter?: number;
  product_filter?: string;
  option_filter?: string;
  date_from_filter?: number;
  date_to_filter?: number;
}

export interface Invoice {
  id: number;
  memid: number;
  username: string;
  pay_amount: number;
  confirmed: string;
  amount: number;
  amount_cc: number;
  amount_value: number;
  btc_usd_value: number;
  product: string;
  prod_option: number;
  product_name: string;
  product_price: number;
  product_fee: number;
  product_description: string;
  product_recurring_label: string;
  pay_wallet: string;
  date_made: number;
  exportable: boolean;
  pdf_status?: string;
  pdf_num_status?: number;
  time_left: number;
  is_expired: boolean;
  date_paid: number;
  paid_by: string;
  pay_gateway: string;
  pay_memid: number;
}

export interface VolumeData {
  id: number;
  volume: number;
  from_invoiceid: number;
  date_added: number;
  memid: number;
  username: string;
  email: string;
  fname: string;
  lname: string;
  from_memid: number;
  from_member_username: string;
  from_member_email: string;
  from_member_fname: string;
  from_member_lname: string;
}

export interface FlowData {
  id: number;
  invoice_id: number;
  receiver_id: number;
  receiver_username: string;
  receiver_email: string;
  receiver_fname: string;
  receiver_lname: string;
  credits: number;
  side: number;
  timestmp: number;
}

export interface InvoiceDetails {
  invoice: Invoice;
  member: Member;
  volumes: DataList<VolumeData>;
  flowList: DataList<FlowData>;
  isPdfStatusLoading?: boolean;
}

export interface InvoicesState extends DefaultState {
  data: DataList<Invoice>;
  selectedItem: InvoiceDetails;
  filterData: InvoiceListFilterData;
}

// State initialization.
const INITIAL_DATA_STATE = {
  data: [],
  total_results: 0,
  page_number: 1,
  last_page: 1,
  results_per_page: 25
};

export const INITIAL_STATE: InvoicesState = {
  data: INITIAL_DATA_STATE,
  selectedItem: null,
  filterData: null,
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: InvoicesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: InvoicesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      data: { ...(<any>action).payload.data },
      filterData: { ...state.filterData, ...(<any>action).payload.filterData }
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: InvoicesState) => {
    return {
      ...state,
      data: INITIAL_DATA_STATE,
      filterData: null
    };
  },
  [actions.RESET_LIST_FILTER]: (state: InvoicesState) => {
    return {
      ...state,
      filterData: null
    };
  },
  [actions.FETCH_ITEM]: (state: InvoicesState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: InvoicesState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      selectedItem: {
        ...state.selectedItem,
        ...(<any>action).payload,
        isPdfStatusLoading: false
      }
    };
  },
  [actions.FETCH_ITEM_PDF_STATUS]: (state: InvoicesState, action: Action) => {
    return {
      ...state,
      selectedItem: {
        ...state.selectedItem,
        isPdfStatusLoading: true
      }
    };
  },
  [actions.FETCH_ITEM_PDF_STATUS_SUCCESS]: (
    state: InvoicesState,
    action: Action
  ) => {
    return {
      ...state,
      data: {
        ...state.data,
        data: state.data
          ? state.data.data.map((invoice: Invoice) => {
              return invoice.id === (<any>action).payload.invoiceId
                ? { ...invoice, ...(<any>action).payload.pdfStatusData }
                : invoice;
            })
          : []
      },
      selectedItem: {
        ...state.selectedItem,
        invoice: {
          ...state.selectedItem.invoice,
          ...(<any>action).payload.pdfStatusData
        },
        isPdfStatusLoading: false
      }
    };
  },
  [actions.FETCH_ITEM_PDF_STATUS_FAILURE]: (
    state: InvoicesState,
    action: Action
  ) => {
    return {
      ...state,
      selectedItem: {
        ...state.selectedItem,
        isPdfStatusLoading: false
      }
    };
  },
  [actions.CONFIRM_INVOICE_SUCCESS]: (state: InvoicesState, action: Action) => {
    return {
      ...state,
      data: {
        ...state.data,
        data: state.data.data.filter(
          invoice =>
            invoice.id === (<any>action).payload.invoiceId
              ? { ...invoice, confirmed: 'Confirmed' }
              : invoice
        )
      },
      selectedItem: {
        ...state.selectedItem,
        invoice: {
          ...state.selectedItem.invoice,
          confirmed: 'Confirmed'
        }
      }
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers

export function invoicesReducer(
  state: InvoicesState = INITIAL_STATE,
  action: Action
): InvoicesState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
