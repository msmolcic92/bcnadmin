import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList, PaginationConfig } from '../core/store/helpers';
import { Invoice, InvoicesState, InvoiceListFilterData } from './store/invoices.state';
import * as actions from './store/invoices.actions';

@Component({
  selector: 'invoices-screen',
  templateUrl: 'invoices-screen.component.html'
})
export class InvoicesScreenComponent implements OnDestroy, OnInit {
  private subscription: any;

  invoiceList: DataList<Invoice>;
  filterShown = false;
  filterData = new InvoiceListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.invoiceList = routeData.invoiceListData.data;
      this.filterData = routeData.invoiceListData.filterData;

      this.subscription = this.store
        .select('invoices')
        .subscribe((invoicesState: InvoicesState) => {
          this.isTableLoading = invoicesState.requesting;

          if (invoicesState.data) {
            this.invoiceList = invoicesState.data;
            this.pagination = {
              currentPage: this.invoiceList.page_number,
              itemPerPage: this.invoiceList.results_per_page,
              totalItems: this.invoiceList.total_results
            };
          } else {
            this.invoiceList = null;
          }

          if (invoicesState.filterData) {
            this.filterData = invoicesState.filterData;
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: InvoiceListFilterData = new InvoiceListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
