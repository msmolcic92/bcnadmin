import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { InvoicesState, Invoice } from '../invoices/store/invoices.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/invoices.actions';

@Injectable()
export class InvoiceListResolver implements Resolve<DataList<Invoice>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const state$ = this.store.select('invoices');

    state$.take(1).subscribe((state: InvoicesState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter(
        (state: InvoicesState) =>
          !state.requesting &&
          !!((state.data && state.data.data) || state.hasError)
      )
      .take(1);
  }
}
