import { NgModule, OnDestroy, Component, Injectable } from '@angular/core';
import { Routes, RouterModule, CanDeactivate } from '@angular/router';
import { InvoicesRootComponent } from './invoices-root.component';
import { InvoicesScreenComponent } from './invoices-screen.component';
import { InvoiceDetailsScreenComponent } from './invoice-details-screen.component';
import { InvoiceDetailsResolver } from './invoice-details-resolver.service';
import { InvoiceListResolver } from './invoice-list-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Invoices'
    },
    component: InvoicesRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: InvoicesScreenComponent,
        resolve: {
          invoiceListData: InvoiceListResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Invoice Details'
        },
        component: InvoiceDetailsScreenComponent,
        resolve: {
          invoiceDetails: InvoiceDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule {}
