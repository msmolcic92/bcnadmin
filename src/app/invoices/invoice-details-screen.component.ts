import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import { DataList } from '../core/store/helpers';
import {
  InvoicesState,
  InvoiceDetails,
  Invoice,
  VolumeData,
  FlowData
} from './store/invoices.state';
import { Member } from '../members/store/members.state';
import * as actions from './store/invoices.actions';
import { environment } from 'environments/environment';

@Component({
  selector: 'invoice-details-screen',
  templateUrl: 'invoice-details-screen.component.html'
})
export class InvoiceDetailsScreenComponent implements OnDestroy, OnInit {
  isAlive = true;
  invoice: Invoice;
  member: Member;
  volumes: DataList<VolumeData>;
  flowList: DataList<FlowData>;
  isPdfStatusLoading: boolean = false;
  pdfStatusSubscription: Subscription;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      const details = data.invoiceDetails;
      this.invoice = details.invoice;
      this.member = details.member;
      this.volumes = details.volumes;
      this.flowList = details.flowList;

      if (this.invoice && this.invoice.pdf_status === 'pending') {
        this.startWatchingForPdfStatus();
      }
    });

    this.store
      .select('invoices')
      .filter(
        (invoicesState: InvoicesState) =>
          invoicesState.selectedItem && !invoicesState.requesting
      )
      .map((invoicesState: InvoicesState) => {
        return invoicesState.selectedItem;
      })
      .takeWhile(() => this.isAlive)
      .subscribe((invoiceDetails: InvoiceDetails) => {
        this.invoice = invoiceDetails.invoice;
        this.isPdfStatusLoading = invoiceDetails.isPdfStatusLoading;
      });

    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
    this.isAlive = false;
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.CONFIRM_INVOICE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'The invoice was marked as confirmed.'
        );
      });

    this.appAction$
      .ofType(actions.CONFIRM_INVOICE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        const errors = (<any>action).payload.error_messages;
        let message = '';

        if (errors && errors.length) {
          for (let i = 0; i < errors.length; i++) {
            message += `\n${errors[i].message}`;
          }
        } else {
          message = 'An error occured while processing your request.'
        }

        this.toasterService.pop('error', 'Ooops!', message);
      });
  }

  isConfirmButtonHidden(): boolean {
    return (
      this.invoice &&
      (this.invoice.confirmed === 'Confirmed' ||
        !!this.invoice.date_paid ||
        this.invoice.product !== 'membership_fee')
    );
  }

  confirmInvoice(): void {
    this.store.dispatch({
      type: actions.CONFIRM_INVOICE,
      payload: {
        invoiceId: this.invoice.id
      }
    });
  }

  isPdfAvailable() {
    return this.invoice && this.invoice.pdf_status === 'ready';
  }

  isPdfPending() {
    return (
      this.isPdfStatusLoading ||
      (this.invoice && this.invoice.pdf_status === 'pending')
    );
  }

  requestPdf() {
    this.startWatchingForPdfStatus();
  }

  downloadPdf() {
    this.store.dispatch({
      type: actions.FETCH_ITEM_PDF,
      payload: { invoiceId: this.invoice.id }
    });
  }

  startWatchingForPdfStatus() {
    if (this.pdfStatusSubscription) {
      return;
    }

    const pdfAvailable$ = this.store
      .select('invoices')
      .map((invoicesState: InvoicesState) => {
        const invoices = ((invoicesState.data && invoicesState.data.data) ||
          []
        ).filter((invoice: Invoice) => invoice.id === this.invoice.id);

        const invoice = invoices[0];
        return (invoice && invoice.pdf_num_status === 2) || false;
      })
      .filter((isAvailable: boolean) => isAvailable);

    this.pdfStatusSubscription = Observable.interval(1000)
      .takeWhile(() => this.isAlive)
      .takeUntil(pdfAvailable$)
      .subscribe(() => {
        if (this.invoice) {
          this.store.dispatch({
            type: actions.FETCH_ITEM_PDF_STATUS,
            payload: { invoiceId: this.invoice.id }
          });
        }
      });
  }
}
