import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from './store/invoices.actions';
import {
  InvoicesState,
  InvoiceDetails
} from '../invoices/store/invoices.state';

@Injectable()
export class InvoiceDetailsResolver implements Resolve<InvoiceDetails> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('invoices')
      .filter(
        (invoicesState: InvoicesState) =>
          invoicesState.selectedItem && !invoicesState.requesting
      )
      .map((invoicesState: InvoicesState) => {
        return invoicesState.selectedItem;
      })
      .take(1);
  }
}
