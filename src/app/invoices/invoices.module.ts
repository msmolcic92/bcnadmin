import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { VolumesService } from '../../api-client/volumes.service';
import { BinaryCreditFlowService } from '../../api-client/binary-credit-flow.service';
import { MembersService } from '../../api-client/members.service';
import { InvoicesService } from '../../api-client/invoices.service';
import { InvoicesEffects } from './store/invoices.effects';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesRootComponent } from './invoices-root.component';
import { InvoicesScreenComponent } from './invoices-screen.component';
import { InvoiceDetailsScreenComponent } from './invoice-details-screen.component';
import { InvoiceListComponent } from './invoice-list.component';
import { InvoiceListFilterComponent } from './invoice-list-filter.component';
import { InvoiceListResolver } from './invoice-list-resolver.service';
import { InvoiceDetailsResolver } from './invoice-details-resolver.service';
import { invoicesReducer } from './store/invoices.state';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([InvoicesEffects]),
    StoreModule.forFeature('invoices', invoicesReducer),
    InvoicesRoutingModule,
    NgbModule,
    SharedModule,
  ],
  declarations: [
    InvoicesRootComponent,
    InvoiceDetailsScreenComponent,
    InvoicesScreenComponent,
    InvoiceListComponent,
    InvoiceListFilterComponent,
  ],
  providers: [
    InvoiceListResolver,
    InvoiceDetailsResolver,
    InvoicesService,
    VolumesService,
    BinaryCreditFlowService,
    MembersService
  ]
})
export class InvoicesModule {}
