import { Component, Input } from '@angular/core';
import { Invoice } from './store/invoices.state';

@Component({
  selector: 'invoice-list',
  templateUrl: 'invoice-list.component.html'
})
export class InvoiceListComponent {
  @Input() invoices: Invoice[];
  @Input() isLoading: boolean;

  constructor() {}
}
