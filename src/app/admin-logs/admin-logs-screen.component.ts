import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  AdminLog,
  AdminLogsState,
  AdminLogsListFilterData
} from './store/admin-logs.state';
import * as actions from './store/admin-logs.actions';

@Component({
  selector: 'admin-logs-screen',
  templateUrl: './admin-logs-screen.component.html'
})
export class AdminLogsScreenComponent implements OnInit, OnDestroy {
  private subscription: any;

  logList: DataList<AdminLog>;
  filterShown = false;
  filterData = new AdminLogsListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.logList = routeData.adminLogs.logList;
      this.filterData = routeData.adminLogs.filterData;

      this.subscription = this.store
        .select('adminLogs')
        .subscribe((state: AdminLogsState) => {
          this.isTableLoading = state.requesting;
          if (state.logList) {
            this.logList = state.logList;
            this.pagination = {
              currentPage: state.logList.page_number,
              itemPerPage: state.logList.results_per_page,
              totalItems: state.logList.total_results
            };
          } else {
            this.logList = null;
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: AdminLogsListFilterData = new AdminLogsListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
