import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLogsRootComponent } from './admin-logs-root.component';
import { AdminLogsScreenComponent } from './admin-logs-screen.component';
import { AdminLogListDataResolver } from './list/admin-log-list-data-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Admin Logs'
    },
    component: AdminLogsRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: AdminLogsScreenComponent,
        resolve: {
          adminLogs: AdminLogListDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminLogsRoutingModule {}
