import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { logsReducer } from './store/admin-logs.state';
import { AdminLogsRoutingModule } from './admin-logs-routing.module';
import { AdminLogsRootComponent } from './admin-logs-root.component';
import { AdminLogsScreenComponent } from './admin-logs-screen.component';
import { AdminLogListFilterComponent } from './list/admin-log-list-filter.component';
import { AdminLogListComponent } from './list/admin-log-list.component';
import { AdminLogsEffects } from './store/admin-logs.effects';
import { AdminLogsService } from '../../api-client/admin-logs.service';
import { AdminLogListDataResolver } from './list/admin-log-list-data-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([AdminLogsEffects]),
    StoreModule.forFeature('adminLogs', logsReducer),
    AdminLogsRoutingModule
  ],
  declarations: [
    AdminLogsRootComponent,
    AdminLogsScreenComponent,
    AdminLogListFilterComponent,
    AdminLogListComponent
  ],
  providers: [AdminLogsService, AdminLogListDataResolver]
})
export class AdminLogsModule {}
