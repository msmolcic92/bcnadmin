import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { AdminLogsService } from '../../../api-client/admin-logs.service';
import { AdminLogsListFilterData } from '../store/admin-logs.state';
import * as actions from './admin-logs.actions';
import { makeSimpleEffect } from '../../core/store/helpers';

@Injectable()
export class AdminLogsEffects {
  constructor(
    private action$: Actions,
    private logsService: AdminLogsService,
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: AdminLogsListFilterData) => {
      return this.logsService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData,
          },
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          }),
        );
    });
}
