import { Action } from '@ngrx/store';
import {
  AutoStates,
  DataList,
  DefaultState,
  defaultStateValues,
  makeDefaultHandlers,
} from '../../core/store/helpers';
import * as actions from './admin-logs.actions';

export interface AdminLog {
  id: number;
  method: string;
  memid: number;
  status_code: number;
  memory: number;
  time: number;
  api_endpoint: string;
  post_vars: string;
  get_vars: string;
  is_error: number;
  date_made: number;
}

export class AdminLogsListFilterData {
  page_number? = 1;
  results_per_page? = 25;
  date_from_filter?: number;
  date_to_filter?: number;
  id_filter?: number;
  method_filter?: string;
  endpoint_filter?: string;
}

export interface AdminLogsState extends DefaultState {
  logList: DataList<AdminLog>;
  filterData: AdminLogsListFilterData;
}

// State initialization.
export const INITIAL_STATE: AdminLogsState = {
  logList: null,
  filterData: new AdminLogsListFilterData(),
  ...defaultStateValues,
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FETCH_LIST]: (state: AdminLogsState) => {
    return {
      ...state,
      ...AutoStates.new(),
    };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: AdminLogsState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      logList: { ...(<any>action).payload.data },
      filterData: { ...(<any>action).payload.filterData },
    };
  },
  [actions.FETCH_LIST_STATE_RESET]: (state: AdminLogsState) => {
    return {
      ...state,
      logList: null,
      filterData: null,
    };
  },
  [actions.RESET_LIST_FILTER]: (state: AdminLogsState) => {
    return {
      ...state,
      filterData: null,
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR,
  ),
};

// Reducers.
export function logsReducer(
  state: AdminLogsState = INITIAL_STATE,
  action: Action,
): AdminLogsState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
