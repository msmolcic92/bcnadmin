// Generic actions.
export const START_REQUEST = 'adminLogs/START_REQUEST';
export const END_REQUEST = 'adminLogs/END_REQUEST';
export const ON_ERROR = 'adminLogs/ON_ERROR';

// Specific actions.
export const FETCH_LIST = 'adminLogs/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'adminLogs/FETCH_LIST_SUCCESS';
export const FETCH_LIST_STATE_RESET = 'adminLogs/FETCH_LIST_STATE_RESET';
export const RESET_LIST_FILTER = 'adminLogs/RESET_LIST_FILTER';
