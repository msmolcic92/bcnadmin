import { Component, Input } from '@angular/core';

@Component({
  selector: 'admin-log-list',
  templateUrl: './admin-log-list.component.html',
  styleUrls: ['admin-log-list.component.scss']
})
export class AdminLogListComponent {
  @Input() items: Array<any> = [];
  @Input() isLoading: boolean;
  expandedItems = [];

  toggleItemExpanded(item) {
    if (this.isItemExpanded(item)) {
      this.expandedItems = this.expandedItems.filter(i => i !== item);
    } else {
      this.expandedItems = this.expandedItems.concat(item);
    }
  }

  isItemExpanded(item) {
    return this.expandedItems.filter(i => i === item).length;
  }
  
  decodeVars(vars) {
    let result = [];
    let object = JSON.parse(vars);
    for (let key in object) {
      result.push({key: key, val: object[key]})
    }
    return result;
  }
}
