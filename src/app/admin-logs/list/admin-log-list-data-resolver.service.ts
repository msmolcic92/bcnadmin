import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList, PaginationConfig } from '../../core/store/helpers';
import { AdminLog, AdminLogsState } from '../store/admin-logs.state';
import * as actions from '../store/admin-logs.actions';

@Injectable()
export class AdminLogListDataResolver implements Resolve<DataList<AdminLog>> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const state$ = this.store.select('adminLogs');

    state$.take(1).subscribe((state: AdminLogsState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter((state: AdminLogsState) => !!(state.hasError || state.logList))
      .take(1);
  }
}
