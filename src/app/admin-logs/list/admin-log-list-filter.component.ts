import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { SECONDS_IN_A_DAY } from '../../shared/constants';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { AdminLogsListFilterData } from '../store/admin-logs.state';

@Component({
  selector: 'admin-log-list-filter',
  templateUrl: 'admin-log-list-filter.component.html'
})
export class AdminLogListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: AdminLogsListFilterData;
  @Output()
  valueChange: EventEmitter<AdminLogsListFilterData> = new EventEmitter<
    AdminLogsListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  methodOptionList = [
    { value: null, label: 'All HTTP Methods' },
    { value: 'POST', label: 'POST' },
    { value: 'GET', label: 'GET' },
    { value: 'PUT', label: 'PUT' },
    { value: 'DELETE', label: 'DELETE' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter],
      id_filter: [this.value.id_filter],
      method_filter: [this.value.method_filter],
      endpoint_filter: [this.value.endpoint_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    formData.method_filter =
      formData.method_filter === 'null' || formData.method_filter === null
        ? null
        : formData.method_filter;

    this.valueChange.emit(formData);
  }
}
