import { TestBed, inject } from '@angular/core/testing';
import { Store, StoreMock } from 'testing/store-helpers';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub
} from 'testing/router-helpers';
import { AccountServiceStub } from 'testing/stubs/services/account.service.stub';
import { AccountDataResolver } from './account-data-resolver.service';
import { AccountService } from '../../api-client/account.service';
import * as actions from './store/account.actions';


describe('AccountDataResolver', () => {
  const TEST_ACCOUNT_DATA = {
    id: 1,
    username: 'test',
    email: 'test@test.com',
    fname: 'Pedrito',
    lname: 'Fernandez'
  };

  const TEST_ACCOUNT_STATE = {
    data: {
      data: TEST_ACCOUNT_DATA
    },
    isFetchSuccess: true
  };

  let activatedRouteSnapshot;
  let resolver: AccountDataResolver;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;
  let router: RouterStub;

  beforeEach(() => {
    activatedRouteSnapshot = {
      params: {
        id: 1
      }
    };
    router = new RouterStub();
    activatedRoute = new ActivatedRouteStub;
    store = new StoreMock<any>();
    store.mockState('account', TEST_ACCOUNT_STATE);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AccountDataResolver,
        { provide: AccountService, useClass: AccountServiceStub },
        { provide: Store, useValue: store },
        { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ]
    });

    resolver = TestBed.get(AccountDataResolver);
  });

  it('should dispatch the FETCH_ITEM action', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    resolver.resolve(activatedRouteSnapshot, null);

    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.ACCOUNT_FETCH);
  });

  it('should obtain the correct member from the store', () => {
    let result;
    resolver
      .resolve(activatedRouteSnapshot, null)
      .subscribe(_result => (result = _result));
    expect(result.data).toEqual(TEST_ACCOUNT_DATA);
  });
});
