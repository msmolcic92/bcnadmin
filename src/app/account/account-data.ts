export class AccountData {
  id: number;
  username?: string;
  email?: string;
  fname?: string;
  lname?: string;
}
