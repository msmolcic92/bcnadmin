import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { AccountService } from '../../api-client/account.service';
import { AccountData } from './account-data';
import * as actions from './store/account.actions';
import { AccountState } from './store/account.state';

@Injectable()
export class AccountDataResolver implements Resolve<AccountData> {

  constructor(
    private accountService: AccountService,
    private router: Router,
    private store: Store<any>,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({ type: actions.ACCOUNT_FETCH });

    return this.store
      .select('account')
      .filter((account: AccountState) => account.isFetchSuccess || account.isFetchFailure)
      .map((accountState: AccountState) => accountState.data)
      .take(1);
  }
}
