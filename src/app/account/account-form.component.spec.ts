import { FormGroup } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { StoreMock } from 'testing/store-helpers';
import * as actions from 'app/account/store/account.actions';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountFormComponent } from './account-form.component';
import { AccountModule } from './account.module';

describe('AccountFormComponent', () => {
  let component: AccountFormComponent;
  let fixture: ComponentFixture<AccountFormComponent>;
  let store: StoreMock<any>;
  const formDataValid = {
    username: 'test',
    email: 'test@test.com',
    fname: 'Pedrito',
    lname: 'Fernandez'
  }
  const formDataInvalid = {
    username: '',
    email: '',
    fname: '',
    lname: ''
  };

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('account', {
      accountData: {
        data: []
      },
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function updateForm(formData: any) {
    component.form.setValue({
      username: formData.username,
      email: formData.email,
      fname: formData.fname,
      lname: formData.lname
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form object on component instantiation', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should get the passed input account', () => {
    component.formData = formDataValid;
    expect(component.formData).toEqual(formDataValid);
  });

  it('should dispatch with the ACCOUNT_SAVE action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.submit();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.ACCOUNT_SAVE);
  });

  it('should dispatch with the ACCOUNT_FORM_STATE_RESET action when canceling', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.cancel();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.ACCOUNT_FORM_STATE_RESET);
  });

  it('form value should update from form changes and still be valid', () => {
    updateForm(formDataValid);
    expect(component.form.value).toEqual(formDataValid);
    expect(component.form.valid).toBeTruthy();
  });

  it('form.valid should be false when data is invalid', () => {
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
    // Checking for invalid email
    formDataInvalid.email = 'Something';
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
  });
});
