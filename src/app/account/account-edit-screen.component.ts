import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../../api-client/account.service';

@Component({
  selector: 'account-edit-screen',
  templateUrl: './account-edit-screen.component.html'
})
export class AccountEditScreenComponent implements OnInit {

  accountData: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(routeData => {
      this.accountData = routeData.accountData;
    });
  }

  handleFormSuccess(response) {
    // TODO: Notify success
    this.router.navigate(['account']);
  }

  handleFormCancel() {
    this.router.navigate(['account']);
  }
}
