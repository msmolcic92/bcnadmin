import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub
} from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';
import * as actions from 'app/account/store/account.actions';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountScreenComponent } from './account-screen.component';
import { AccountModule } from './account.module';

describe('AccountScreenComponent', () => {
  let component: AccountScreenComponent;
  let fixture: ComponentFixture<AccountScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;
  let router: RouterStub;
  const accountDataMock = {
    username: 'test',
    email: 'test@test.com',
    fname: 'Pedrito',
    lname: 'Fernandez'
  }

  beforeEach(() => {
    router = new RouterStub();
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      accountData: accountDataMock
    });

    store = new StoreMock();
    store.mockState('account', {
      accountData: {
        data: []
      }
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [
          { provide: Router, useValue: router },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should pass the account data from Router', () => {
    expect(component.accountData).toEqual(accountDataMock);
  });
});
