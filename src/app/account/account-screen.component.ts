import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../../api-client/account.service';

@Component({
  selector: 'account-screen',
  templateUrl: './account-screen.component.html'
})
export class AccountScreenComponent implements OnInit {

  accountData: any;

  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(routeData => {
      this.accountData = routeData.accountData;
    });
  }
}
