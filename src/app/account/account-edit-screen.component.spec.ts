import { FormGroup } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub
} from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';
import * as actions from 'app/account/store/account.actions';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountEditScreenComponent } from './account-edit-screen.component';
import { AccountModule } from './account.module';

describe('AccountEditScreenComponent', () => {
  let component: AccountEditScreenComponent;
  let fixture: ComponentFixture<AccountEditScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;
  let router: RouterStub;
  const accountDataMock = {
    username: 'test',
    email: 'test@test.com',
    fname: 'Pedrito',
    lname: 'Fernandez'
  }

  beforeEach(() => {
    store = new StoreMock();
    activatedRoute = new ActivatedRouteStub();
    router = new RouterStub();
    store.mockState('account', {
      sessionData: {
        data: [1, 2, 3]
      },
      isAuthenticated: true,
      filterData: {}
    });
    activatedRoute.dataSubject.next({
      accountData: accountDataMock
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [
          { provide: Store, useValue: store },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Router, useValue: router },
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountEditScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should pass the account data from Router', () => {
    expect(component.accountData).toEqual(accountDataMock);
  });

  it('should navigate to "/account" when handling form success', () => {
    let redirectUrl;
    spyOn(router, 'navigate').and.callFake(
      url => (redirectUrl = url)
    );
    component.handleFormSuccess({});
    expect(router.navigate).toHaveBeenCalled();
    expect(redirectUrl).toEqual(['account']);
  });

  it('should navigate to "/account" when handling form cancel', () => {
    let redirectUrl;
    spyOn(router, 'navigate').and.callFake(
      url => (redirectUrl = url)
    );
    component.handleFormCancel();
    expect(router.navigate).toHaveBeenCalled();
    expect(redirectUrl).toEqual(['account']);
  });
});
