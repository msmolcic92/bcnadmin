import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AccountState } from './store/account.state';
import { BaseForm } from '../shared/base-form';
import * as actions from './store/account.actions';

const FORM_VALIDATION_MESSAGES = {
  username: {
    required: 'Username is required.'
  },
  email: {
    required: 'Email address is required.',
    email: 'Invalid email address.'
  }
};

export class AccountFormData {
  username?: string;
  email?: string;
  fname?: string;
  lname?: string;
}

@Component({
  selector: 'account-form',
  templateUrl: './account-form.component.html'
})
export class AccountFormComponent extends BaseForm implements OnInit {
  @Input() formData: AccountFormData;

  constructor(private formBuilder: FormBuilder, private store: Store<any>) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToStore();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new AccountFormData();
    }

    this.form = this.formBuilder.group({
      username: [this.formData.username, Validators.required],
      email: [
        this.formData.email,
        Validators.compose([Validators.required, Validators.email])
      ],
      fname: [this.formData.fname],
      lname: [this.formData.lname]
    });
  }

  subscribeToStore(): void {
    this.store
      .select('account')
      .takeWhile(() => this.isAlive)
      .subscribe((accountState: AccountState) => {
        if (accountState.isSaveSuccess) {
          this.store.dispatch({ type: actions.ACCOUNT_FORM_STATE_RESET });
          return this.formSuccess.next(accountState.data);
        }

        if (accountState.formError) {
          this.onSubmitError(accountState.formError);
        }
      });
  }

  submit(): void {
    this.store.dispatch({
      type: actions.ACCOUNT_SAVE,
      payload: {
        ...this.form.value
      }
    });
  }

  cancel(): void {
    this.store.dispatch({ type: actions.ACCOUNT_FORM_STATE_RESET });
    this.formCancel.next(true);
  }
}
