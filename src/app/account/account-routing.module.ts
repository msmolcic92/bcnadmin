import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountScreenComponent } from './account-screen.component';
import { AccountEditScreenComponent } from './account-edit-screen.component';
import { AccountDataResolver } from './account-data-resolver.service';
import { SecurityScreenComponent } from './security/security-screen.component';
import { SecurityDataResolver } from './security/security-data-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Account'
    },
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: AccountScreenComponent,
        resolve: {
          accountData: AccountDataResolver
        },
      },
      {
        path: 'edit',
        component: AccountEditScreenComponent,
        data: {
          title: 'Edit'
        },
        resolve: {
          accountData: AccountDataResolver
        }
      },
      {
        path: 'security',
        component: SecurityScreenComponent,
        data: {
          title: 'Security'
        },
        resolve: {
          tfaData: SecurityDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
