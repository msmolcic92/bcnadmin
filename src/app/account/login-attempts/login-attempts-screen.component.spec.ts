import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';
import { PaginationConfig } from 'app/core/store/helpers';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { Store, StoreMock } from 'testing/store-helpers';
import { AccountModule } from '../account.module';
import * as actions from '../store/account.actions';
import { AccountState } from './../store/account.state';
import { LoginAttemptListFilterData } from '../store/account.state';
import { LoginAttemptsScreenComponent } from './login-attempts-screen.component';

describe('LoginAttemptsScreenComponent', () => {
  const TEST_ACCOUNT_DATA = {
    id: 1,
    username: 'test',
    email: 'test@test.com',
    fname: 'Pedrito',
    lname: 'Fernandez'
  };

  const paginationMock: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  }

  const TEST_ACCOUNT_STATE: AccountState = {
    data: TEST_ACCOUNT_DATA,
    loginAttemptList: {
      data: [],
      last_page: 1,
      page_number: paginationMock.currentPage,
      results_per_page: paginationMock.itemPerPage,
      total_results: paginationMock.totalItems,
    },
    loginAttemptListFilterData: new LoginAttemptListFilterData(),
    isFetchingLoginAttemptList: false,
    isFetchLoginAttemptListSuccess: true
  };


  let component: LoginAttemptsScreenComponent;
  let fixture: ComponentFixture<LoginAttemptsScreenComponent>;
  let store: StoreMock<any>;

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('account', TEST_ACCOUNT_STATE);
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [LoginAttemptsScreenComponent],
        providers: [
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAttemptsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should try dispatch account/LOGIN_ATTEMPT_LIST_FETCH action get the list when creating the component ', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = dispatchedAction ? dispatchedAction : action)
    );
    component.ngOnInit();
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.LOGIN_ATTEMPT_LIST_FETCH);
  });

  // Filter tests
  it('should render the filter component when filterShown is true', () => {
    component.filterShown = true;
    fixture.detectChanges();
    const filterElement = fixture.debugElement.query(
      By.css('login-attempt-list-filter')
    );
    expect(filterElement).toBeTruthy();
  });

  it('should not render the filter component when filterShown is false', () => {
    component.filterShown = false;
    fixture.detectChanges();
    const filterElement = fixture.debugElement.query(
      By.css('login-attempt-list-filter')
    );
    expect(filterElement).toBeNull();
  });

  it('should toggle the filterShown value on toggleFilter() call', () => {
    component.filterShown = true;
    component.toggleFilter();
    fixture.detectChanges();
    let filterElement = fixture.debugElement.query(
      By.css('login-attempt-list-filter')
    );
    expect(filterElement).toBeNull();
    component.toggleFilter();
    fixture.detectChanges();
    filterElement = fixture.debugElement.query(By.css('login-attempt-list-filter'));
    expect(filterElement).toBeTruthy();
  });

  it('should dispatch the "account/LOGIN_ATTEMPT_LIST_FETCH" action on filter change', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    component.handleFilterChange(new LoginAttemptListFilterData());
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.LOGIN_ATTEMPT_LIST_FETCH);
  });

  // Pagination
  it('should initialize pagination from passed state', () => {
    expect(component.pagination).toEqual(paginationMock);
  });

  it('should update current page value on page change', () => {
    component.handlePageChange(5);
    expect(component.pagination.currentPage).toBe(5);
  });

  it('should dispatch the "account/LOGIN_ATTEMPT_LIST_FETCH" action on page change', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    component.handlePageChange(5);
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.LOGIN_ATTEMPT_LIST_FETCH);
  });
});
