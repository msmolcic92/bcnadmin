import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { LoginAttemptListFilterData } from '../store/account.state';

@Component({
  selector: 'login-attempt-list-filter',
  templateUrl: 'login-attempt-list-filter.component.html'
})
export class LoginAttemptListFilterComponent implements OnInit, OnDestroy {
  @Input() value: LoginAttemptListFilterData;
  @Output()
  valueChange: EventEmitter<LoginAttemptListFilterData> = new EventEmitter<
    LoginAttemptListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  typeOptionList = [
    { value: null, label: 'Both success and fail' },
    { value: 'success', label: 'Success only' },
    { value: 'fail', label: 'Fail only' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form, {
          startDateKey: 'filter_date_from',
          endDateKey: 'filter_date_to'
        });
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      type: [this.value.type],
      filter_date_from: [this.value.filter_date_from],
      filter_date_to: [this.value.filter_date_to]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };
    formData.type =
      formData.type === 'null' || formData.type === null ? null : formData.type;

    if (formData.filter_date_to) {
      formData.filter_date_to = endOfTheDayTimestamp(formData.filter_date_to);
    }

    this.valueChange.emit(formData);
  }
}
