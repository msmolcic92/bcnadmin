import { Component, Input } from '@angular/core';
import { LoginAttempt } from '../store/account.state';

@Component({
  selector: 'login-attempt-list',
  templateUrl: 'login-attempt-list.component.html'
})
export class LoginAttemptListComponent {
  @Input() loginAttempts: LoginAttempt[];
  @Input() isLoading: boolean;

  constructor() {}
}
