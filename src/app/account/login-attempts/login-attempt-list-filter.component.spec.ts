import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';
import { PaginationConfig } from 'app/core/store/helpers';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountModule } from '../account.module';
import * as actions from '../store/account.actions';
import { AccountState, LoginAttempt } from './../store/account.state';
import { LoginAttemptListFilterData } from '../store/account.state';
import { LoginAttemptListFilterComponent } from './login-attempt-list-filter.component';

describe('LoginAttemptListComponent', () => {
  let component: LoginAttemptListFilterComponent;
  let fixture: ComponentFixture<LoginAttemptListFilterComponent>;
  const typeOptionList = [
    { value: null, label: 'Both success and fail' },
    { value: 'success', label: 'Success only' },
    { value: 'fail', label: 'Fail only' }
  ];
  const expectedFilterData: LoginAttemptListFilterData = {
    type: 'success',
    filter_date_from: 0,
    filter_date_to: 0
  };


  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [LoginAttemptListFilterComponent],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAttemptListFilterComponent);
    component = fixture.componentInstance;
    component.value = expectedFilterData;
    fixture.detectChanges();
  });


  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should obtain the list of member ranks from the store', () => {
    expect(component.typeOptionList).toEqual(typeOptionList);
  });

  it('should accept the filter data as input "value" parameter', () => {
    expect(component.value).toBe(expectedFilterData);
  });

  it('should emit the filterChange event with updated filter data on submit', () => {
    let outputValue: LoginAttemptListFilterComponent;
    component.valueChange.subscribe((filterData: LoginAttemptListFilterComponent) =>
    outputValue = filterData);
    component.submit();
    expect(outputValue).toBeDefined();
  });
});
