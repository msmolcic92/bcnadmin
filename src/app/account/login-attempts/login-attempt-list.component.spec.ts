import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';
import { PaginationConfig } from 'app/core/store/helpers';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountModule } from '../account.module';
import * as actions from '../store/account.actions';
import { AccountState, LoginAttempt } from './../store/account.state';
import { LoginAttemptListFilterData } from '../store/account.state';
import { LoginAttemptListComponent } from './login-attempt-list.component';

describe('LoginAttemptListComponent', () => {
  const TEST_ACCOUNT_DATA: LoginAttempt[] = [
    { ip: '0.0.0.0', date_made: 1239842398, success: true },
    { ip: '1.1.1.1', date_made: 1229842398, success: false }
  ];
  let component: LoginAttemptListComponent;
  let fixture: ComponentFixture<LoginAttemptListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [LoginAttemptListComponent],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAttemptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAttemptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be able to receive input isLoading parameter', () => {
    component.isLoading = true;
    expect(component.isLoading).toBeTruthy();
  });

  it('should be able to receive input loginAttempts parameter', () => {
    component.loginAttempts = [];
    expect(component.loginAttempts).toEqual([]);
  });

  it('should show a spinner if input `isLoading` param is truthy', () => {
    component.isLoading = true;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeTruthy();
  });

  it('should not show a spinner if input `isLoading` param is falsy', () => {
    component.isLoading = false;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeNull();
  });

  it('should render the passed login attempts list rows', () => {
    component.loginAttempts = TEST_ACCOUNT_DATA;
    fixture.detectChanges();
    const memberEls = fixture.debugElement.queryAll(
      By.css('.login-attempt-list-item')
    );
    expect(memberEls.length).toBe(TEST_ACCOUNT_DATA.length);
  });
});
