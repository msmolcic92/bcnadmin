import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig, DataList } from '../../core/store/helpers';
import { AccountService } from '../../../api-client/account.service';
import {
  AccountState,
  LoginAttemptListFilterData,
  LoginAttempt
} from '../store/account.state';
import * as actions from '../store/account.actions';

@Component({
  selector: 'login-attempts-screen',
  templateUrl: 'login-attempts-screen.component.html'
})
export class LoginAttemptsScreenComponent implements OnDestroy, OnInit {
  private subscription: any;

  loginAttemptList: DataList<LoginAttempt>;
  filterData = new LoginAttemptListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 10,
    currentPage: 1,
    totalItems: 0
  };

  filterShown = false;
  isTableLoading = true;

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.store.dispatch({
      type: actions.LOGIN_ATTEMPT_LIST_FETCH,
      payload: {
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });

    this.subscription = this.store
      .select('account')
      .subscribe((accountState: AccountState) => {
        this.isTableLoading = accountState.isFetchingLoginAttemptList;

        if (accountState.isFetchLoginAttemptListSuccess) {
          this.store.dispatch({
            type: actions.LOGIN_ATTEMPT_LIST_FETCH_STATE_RESET
          });

          this.loginAttemptList = accountState.loginAttemptList;
          this.filterData = accountState.loginAttemptListFilterData;

          // TODO: Reduce the pagination-related boilerplate
          this.pagination = {
            itemPerPage: accountState.loginAttemptList.results_per_page,
            currentPage: accountState.loginAttemptList.page_number,
            totalItems: accountState.loginAttemptList.total_results
          };
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.LOGIN_ATTEMPT_LIST_FETCH,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });

    this.isTableLoading = true;
  }

  toggleFilter(): void {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: LoginAttemptListFilterData = new LoginAttemptListFilterData()
  ): void {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any): void {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
