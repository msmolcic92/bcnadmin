import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';
import { AccountService } from '../../api-client/account.service';
import { AccountDataResolver } from './account-data-resolver.service';
import { AccountScreenComponent } from './account-screen.component';
import { AccountEditScreenComponent } from './account-edit-screen.component';
import { AccountFormComponent } from './account-form.component';
import { SecurityScreenComponent } from './security/security-screen.component';
import { ChangePasswordFormComponent } from './security/change-password-form.component';
import { SecurityDataResolver } from './security/security-data-resolver.service';
import { TfaService } from 'api-client/tfa.service';
import { TfaScreenComponent } from './security/tfa-screen.component';
import { TfaInitializationComponent } from './security/tfa-initialization.component';
import { TfaSetupComponent } from './security/tfa-setup.component';
import { TfaRemoveFormComponent } from './security/tfa-remove-form.component';
import { TfaEnableFormComponent } from './security/tfa-enable-form.component';
import { LoginAttemptsScreenComponent } from './login-attempts/login-attempts-screen.component';
import { LoginAttemptListComponent } from './login-attempts/login-attempt-list.component';
import { LoginAttemptListFilterComponent } from './login-attempts/login-attempt-list-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AccountRoutingModule,
    NgbModule
  ],
  declarations: [
    AccountScreenComponent,
    AccountEditScreenComponent,
    AccountFormComponent,
    SecurityScreenComponent,
    ChangePasswordFormComponent,
    TfaScreenComponent,
    TfaInitializationComponent,
    TfaSetupComponent,
    TfaRemoveFormComponent,
    TfaEnableFormComponent,
    LoginAttemptsScreenComponent,
    LoginAttemptListComponent,
    LoginAttemptListFilterComponent
  ],
  providers: [
    AccountService,
    AccountDataResolver,
    SecurityDataResolver,
    TfaService
  ]
})
export class AccountModule { }
