import { Action } from '@ngrx/store';
import * as actions from './account.actions';
import { SubmitError } from '../../shared/base-form';
import { DataList } from 'app/core/store/helpers';

// Model

export interface LoginAttempt {
  date_made: number;
  ip: string;
  success: boolean;
}

export class LoginAttemptListFilterData {
  type?: string;
  filter_date_from?: number;
  filter_date_to?: number;
}

export interface AccountState {
  data?: {
    id: number;
    username: string;
    email: string;
    fname: string;
    lname: string;
    groups?: any;
    access?: any;
  };
  loginAttemptList: DataList<LoginAttempt>;
  loginAttemptListFilterData: LoginAttemptListFilterData;
  isFetching?: boolean;
  isFetchSuccess?: boolean;
  isFetchFailure?: boolean;
  isFetchingLoginAttemptList?: boolean;
  isFetchLoginAttemptListSuccess?: boolean;
  isFetchLoginAttemptListFailure?: boolean;
  isSaving?: boolean;
  isSaveSuccess?: boolean;
  isSaveFailure?: boolean;
  isChangingPass?: boolean;
  isChangePassSuccess?: boolean;
  isChangePassFailure?: boolean;
  // TEMP (until a different unified way of handling forms state has emerged)
  formError?: SubmitError;
}

export const INITIAL_STATE: AccountState = {
  data: null,
  loginAttemptList: null,
  loginAttemptListFilterData: new LoginAttemptListFilterData(),
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false,
  isFetchingLoginAttemptList: false,
  isFetchLoginAttemptListSuccess: false,
  isFetchLoginAttemptListFailure: false,
  isSaving: false,
  isSaveSuccess: false,
  isSaveFailure: false
};

// Reducers

export function accountReducer(state = INITIAL_STATE, action: Action): AccountState {
  switch (action.type) {
    case actions.ACCOUNT_FETCH:
      return {
        ...state,
        isFetching: true,
      };

    case actions.ACCOUNT_FETCH_SUCCESS:
      return {
        ...state,
        data: { ...(<any>action).payload },
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false,
      };

    case actions.ACCOUNT_FETCH_FAILURE:
      return {
        ...state,
        data: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true,
      };

    case actions.ACCOUNT_SAVE:
      return {
        ...state,
        isSaving: true,
      };

    case actions.ACCOUNT_SAVE_SUCCESS:
      return {
        ...state,
        data: { ...(<any>action).payload },
        isSaving: false,
        isSaveSuccess: true,
        isSaveFailure: false,
      };

    case actions.ACCOUNT_SAVE_FAILURE:
      return {
        ...state,
        data: null,
        isSaving: false,
        isSaveSuccess: false,
        isFetchFailure: true,
        formError: { ...(<any>action).payload }
      };

    case actions.ACCOUNT_FORM_STATE_RESET:
      return {
        ...state,
        isSaving: false,
        isSaveSuccess: false,
        isSaveFailure: false,
        formError: null,
      };

    case actions.ACCOUNT_CHANGE_PASSWORD:
      return {
        ...state,
        isChangingPass: true
      };

    case actions.ACCOUNT_CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        isChangingPass: false,
        isChangePassSuccess: true,
        isChangePassFailure: false
      };

    case actions.ACCOUNT_CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        isChangingPass: false,
        isChangePassSuccess: false,
        isChangePassFailure: true,
        formError: { ...(<any>action).payload }
      };

    case actions.ACCOUNT_CHANGE_PASSWORD_RESET:
      return {
        ...state,
        isChangingPass: false,
        isChangePassSuccess: false,
        isChangePassFailure: false,
        formError: null
      };

    case actions.LOGIN_ATTEMPT_LIST_FETCH:
      return {
        ...state,
        isFetchingLoginAttemptList: true
      };

    case actions.LOGIN_ATTEMPT_LIST_FETCH_SUCCESS:
      return {
        ...state,
        loginAttemptList: { ...state.loginAttemptList, ...(<any>action).payload.loginAttemptList },
        loginAttemptListFilterData: { ...state.loginAttemptListFilterData, ...(<any>action).payload.loginAttemptListFilterData },
        isFetchingLoginAttemptList: false,
        isFetchLoginAttemptListSuccess: true,
        isFetchLoginAttemptListFailure: false
      };

    case actions.LOGIN_ATTEMPT_LIST_FETCH_FAILURE:
      return {
        ...state,
        loginAttemptList: null,
        loginAttemptListFilterData: null,
        isFetchingLoginAttemptList: false,
        isFetchLoginAttemptListSuccess: false,
        isFetchLoginAttemptListFailure: true
      };

    case actions.LOGIN_ATTEMPT_LIST_FETCH_STATE_RESET:
      return {
        ...state,
        isFetchingLoginAttemptList: false,
        isFetchLoginAttemptListSuccess: false,
        isFetchLoginAttemptListFailure: false
      }

    default:
      return state;
  }
}

// Selectors

// [TODO]
