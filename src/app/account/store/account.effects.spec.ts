import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import { AccountService } from 'api-client/account.service';
import { Actions, TestActions } from 'testing/effects-helpers';
import { AccountEffects } from './account.effects';
import { AccountServiceStub } from 'testing/stubs/services/account.service.stub';
import * as actions from './account.actions';

describe('MembersEffects', () => {
  const accountService: AccountServiceStub = new AccountServiceStub();
  let effects: AccountEffects;
  // tslint:disable-next-line:prefer-const
  let actions$: ReplaySubject<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AccountEffects,
        provideMockActions(() => actions$),
        { provide: AccountService, useValue: accountService }
      ]
    });
    effects = TestBed.get(AccountEffects);
  });

  describe('fetch$', () => {
    it('should call the account.fetch(...) method', () => {
      spyOn(accountService, 'fetch').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_FETCH,
        payload: {}
      });
      effects.fetch$.subscribe();
      expect(accountService.fetch).toHaveBeenCalled();
    });

    it('should map to account/ACCOUNT_FETCH_SUCCESS action on success', () => {
      spyOn(accountService, 'fetch').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_FETCH
      });
      effects.fetch$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_FETCH_SUCCESS);
    });

    it('should map to account/ACCOUNT_FETCH_FAILURE action on failure', () => {
      spyOn(accountService, 'fetch').and.returnValue(
        Observable.throw(new Error('An error ocurred'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_FETCH
      });
      effects.fetch$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_FETCH_FAILURE);
    });
  });

  describe('fetchLoginAttempts$', () => {
    it('should call the account.fetchLoginAttempts(...) method', () => {
      spyOn(accountService, 'fetchLoginAttempts').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.LOGIN_ATTEMPT_LIST_FETCH,
        payload: {}
      });
      effects.fetchLoginAttempts$.subscribe();
      expect(accountService.fetchLoginAttempts).toHaveBeenCalled();
    });

    it('should map to account/LOGIN_ATTEMPT_LIST_FETCH_SUCCESS action on success', () => {
      spyOn(accountService, 'fetchLoginAttempts').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.LOGIN_ATTEMPT_LIST_FETCH
      });
      effects.fetchLoginAttempts$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.LOGIN_ATTEMPT_LIST_FETCH_SUCCESS);
    });

    it('should map to account/LOGIN_ATTEMPT_LIST_FETCH_FAILURE action on failure', () => {
      spyOn(accountService, 'fetchLoginAttempts').and.returnValue(
        Observable.throw(new Error('An error ocurred'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.LOGIN_ATTEMPT_LIST_FETCH
      });
      effects.fetchLoginAttempts$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.LOGIN_ATTEMPT_LIST_FETCH_FAILURE);
    });
  });

  describe('save$', () => {
    it('should call the account.update(...) method', () => {
      spyOn(accountService, 'update').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_SAVE,
        payload: {}
      });
      effects.save$.subscribe();
      expect(accountService.update).toHaveBeenCalled();
    });

    it('should map to account/ACCOUNT_SAVE_SUCCESS action on success', () => {
      spyOn(accountService, 'update').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_SAVE
      });
      effects.save$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_SAVE_SUCCESS);
    });

    it('should map to account/ACCOUNT_SAVE_FAILURE action on failure', () => {
      spyOn(accountService, 'update').and.returnValue(
        Observable.throw(new Error('An error ocurred'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_SAVE
      });
      effects.save$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_SAVE_FAILURE);
    });
  });

  describe('changePassword$', () => {
    it('should call the account.update(...) method', () => {
      spyOn(accountService, 'update').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_CHANGE_PASSWORD,
        payload: {}
      });
      effects.changePassword$.subscribe();
      expect(accountService.update).toHaveBeenCalled();
    });

    it('should map to account/ACCOUNT_CHANGE_PASSWORD_SUCCESS action on success', () => {
      spyOn(accountService, 'update').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_CHANGE_PASSWORD,
        payload: {}
      });
      effects.changePassword$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_CHANGE_PASSWORD_SUCCESS);
    });

    it('should map to account/ACCOUNT_CHANGE_PASSWORD_FAILURE action on failure', () => {
      spyOn(accountService, 'update').and.returnValue(
        Observable.throw(new Error('An error ocurred'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.ACCOUNT_CHANGE_PASSWORD,
        payload: {}
      });
      effects.changePassword$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.ACCOUNT_CHANGE_PASSWORD_FAILURE);
    });
  });
});
