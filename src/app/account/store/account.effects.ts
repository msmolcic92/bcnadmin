import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AccountService } from '../../../api-client/account.service';
import { LoginAttemptListFilterData } from './account.state';
import * as actions from './account.actions';

function parseAccountGroups(groups: Array<any> = []) {
  return groups.map(group => {
    return {
      ...group,
      group_template_data: JSON.parse(group.group_template_data || '{}')
    };
  });
}

@Injectable()
export class AccountEffects {
  constructor(
    private action$: Actions,
    private accountService: AccountService
  ) {}

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType(actions.ACCOUNT_FETCH)
    .switchMap(() => {
      return this.accountService
        .fetch()
        .map((response: any) => ({
          type: actions.ACCOUNT_FETCH_SUCCESS,
          payload: {
            ...response,
            groups: parseAccountGroups(response.groups || [])
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ACCOUNT_FETCH_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchLoginAttempts$: Observable<Action> = this.action$
    .ofType(actions.LOGIN_ATTEMPT_LIST_FETCH)
    .map(toPayload)
    .switchMap((loginAttemptListFilterData: LoginAttemptListFilterData) => {
      return this.accountService
        .fetchLoginAttempts(loginAttemptListFilterData)
        .map((response: any) => ({
          type: actions.LOGIN_ATTEMPT_LIST_FETCH_SUCCESS,
          payload: {
            loginAttemptList: response,
            loginAttemptListFilterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.LOGIN_ATTEMPT_LIST_FETCH_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  save$: Observable<Action> = this.action$
    .ofType(actions.ACCOUNT_SAVE)
    .map(toPayload)
    .switchMap(accountData => {
      return this.accountService
        .update(accountData)
        .map((response: any) => ({
          type: actions.ACCOUNT_SAVE_SUCCESS,
          payload: { ...accountData }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ACCOUNT_SAVE_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  changePassword$: Observable<Action> = this.action$
    .ofType(actions.ACCOUNT_CHANGE_PASSWORD)
    .map(toPayload)
    .switchMap(newPasswordInfo => {
      return this.accountService
        .update(newPasswordInfo)
        .map((response: any) => ({
          type: actions.ACCOUNT_CHANGE_PASSWORD_SUCCESS
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ACCOUNT_CHANGE_PASSWORD_FAILURE,
            payload: errorResponse.error
          })
        );
    });
}
