import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { TfaData } from 'app/core/store/session.state';
import { ActivatedRoute, ActivatedRouteStub, Router, RouterStub } from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';

import { AccountModule } from '../account.module';
import { SecurityScreenComponent } from './security-screen.component';

describe('SecurityScreenComponent', () => {
  let component: SecurityScreenComponent;
  let fixture: ComponentFixture<SecurityScreenComponent>;
  let store: StoreMock<any>;
  let router: RouterStub;
  let activatedRoute: ActivatedRouteStub;
  const tfaDataMock: TfaData = {
    id: 1,
    tfa_method: 'method',
    qr_url: 'url',
    key: 'key'
  };

  beforeEach(() => {
    store = new StoreMock();
    router = new RouterStub();
    activatedRoute = new ActivatedRouteStub();
    store.mockState('account', {});
    activatedRoute.dataSubject.next({tfaData: tfaDataMock})
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [
          { provide: Store, useValue: store },
          { provide: Router, useValue: router },
          { provide: ActivatedRoute, useValue: activatedRoute }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the tfaData on component initialization', () => {
    expect(component.tfaData).toEqual(tfaDataMock);
  });

  it('should navigate to redirectUrl when handling login success', () => {
    let redirectUrl;
    spyOn(router, 'navigate').and.callFake(url => (redirectUrl = url));
    component.handleChangePasswordSuccess();
    expect(router.navigate).toHaveBeenCalled();
    expect(redirectUrl).toEqual(['account']);
  });

  it('should navigate to redirectUrl when handling login cancel', () => {
    let redirectUrl;
    spyOn(router, 'navigate').and.callFake(url => (redirectUrl = url));
    component.handleChangePasswordCancel();
    expect(router.navigate).toHaveBeenCalled();
    expect(redirectUrl).toEqual(['account']);
  });
});
