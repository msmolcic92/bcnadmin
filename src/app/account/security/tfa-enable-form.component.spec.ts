import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import { StoreMock } from 'testing/store-helpers';

import { AccountModule } from '../account.module';
import * as actions from '../store/account.actions';
import { TfaEnableFormComponent } from './tfa-enable-form.component';

describe('TfaEnableFormComponent', () => {
  let component: TfaEnableFormComponent;
  let fixture: ComponentFixture<TfaEnableFormComponent>;
  let store: StoreMock<any>;

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('account', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TfaEnableFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function updateForm(formData: any) {
    component.form.setValue({
      code: formData.code
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form object on component instantiation', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should call the onFormSubmit output to dispatch when submitting the form', () => {
    let formValue;
    const formDataValid = {
      code: '123456'
    };
    updateForm(formDataValid);
    spyOn(component.onFormSubmit, 'next').and.callFake(
      value => (formValue = value)
    );
    component.submit();
    expect(component.onFormSubmit.next).toHaveBeenCalled();
    expect(formValue).toEqual(formDataValid);
  });

  it('form.valid should be true when password is set', fakeAsync(() => {
    const formDataValid = {
      code: '123456'
    };
    updateForm(formDataValid);
    expect(component.form.valid).toBeTruthy();
  }));

  it('form.valid should be false when password is empty', fakeAsync(() => {
    const formDataInvalid = {
      code: ''
    };
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
  }));
});
