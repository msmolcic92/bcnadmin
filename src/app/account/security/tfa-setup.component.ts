import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TfaData } from '../../core/store/session.state';
import { TfaMethods } from './tfa-methods';

@Component({
  selector: 'tfa-setup',
  templateUrl: 'tfa-setup.component.html'
})
export class TfaSetupComponent {
  @Input() tfaData: TfaData;
  @Output() enableTfa: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeTfa: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  getMethodName(key: string): string {
    const method = TfaMethods.get(key);
    return method ? method.name : null;
  }

  enable(formData): void {
    this.enableTfa.next(formData);
  }

  remove(formData): void {
    this.removeTfa.next(formData);
  }
}
