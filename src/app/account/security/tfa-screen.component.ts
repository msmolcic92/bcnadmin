import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { TfaData, SessionState } from '../../core/store/session.state';
import { Store } from '@ngrx/store';
import * as actions from '../../core/store/session.actions';
import { Subscription } from 'rxjs/Subscription';

export class AuthMethod {
  name: string;
  value: string;
}

@Component({
  selector: 'tfa-screen',
  templateUrl: './tfa-screen.component.html'
})
export class TfaScreenComponent implements OnDestroy, OnInit {
  @Input() tfaData: TfaData;

  errorMessage: string;
  private subscription: Subscription;

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.subscription = this.store
      .select('session')
      .subscribe((sessionState: SessionState) => {
        this.errorMessage = null;

        if (
          sessionState.isTfaInitializeSuccess ||
          sessionState.isTfaEnableSuccess ||
          sessionState.isTfaRemoveSuccess
        ) {
          this.tfaData = sessionState.tfaData;
        }

        if (sessionState.isTfaInitializeFailure) {
          this.errorMessage =
            'Failed to initialize the two-step authentication setup.';
        }

        if (sessionState.isTfaEnableFailure) {
          this.errorMessage =
            'Invalid verification code.';
        }

        if (sessionState.isTfaRemoveFailure) {
          this.errorMessage =
            'Invalid verification code or password.';
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  handleTfaInitialized(method: string): void {
    this.store.dispatch({
      type: actions.TFA_INITIALIZE_SETUP,
      payload: {
        tfa_method: method
      }
    });
  }

  handleEnableTfa(formData): void {
    this.store.dispatch({
      type: actions.TFA_ENABLE,
      payload: {
        tfa_method: this.tfaData.tfa_method,
        code: formData.code
      }
    });
  }

  handleRemoveTfa(formData): void {
    this.store.dispatch({
      type: actions.TFA_REMOVE,
      payload: formData
    });
  }
}
