import { RouterStub } from 'testing/router-helpers';
import { Router } from '@angular/router';
import { SecurityDataResolver } from './security-data-resolver.service';
import { TestBed, inject } from '@angular/core/testing';
import { Store, StoreMock } from 'testing/store-helpers';
import { AccountModule } from '../account.module';
import * as actions from 'app/core/store/session.actions';

describe('SecurityDataResolver', () => {
  let activatedRouteSnapshot;
  let routerStateSnapshot;
  let store: StoreMock<any>;
  let router: RouterStub;
  let resolver: SecurityDataResolver;

  beforeEach(() => {
    activatedRouteSnapshot = {};
    routerStateSnapshot = {};
    store = new StoreMock<any>();
    router = new RouterStub();
    store.mockState('account', {});
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecurityDataResolver, { provide: Store, useValue: store }, { provide: Router, useValue: store }]
    });

    resolver = TestBed.get(SecurityDataResolver);
  });

  it('should dispatch the TFA_FETCH action', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    resolver.resolve(activatedRouteSnapshot, routerStateSnapshot);
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.TFA_FETCH);
  });
});
