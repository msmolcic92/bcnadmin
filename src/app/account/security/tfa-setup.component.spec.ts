import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { TfaData } from 'app/core/store/session.state';
import { SharedModule } from 'app/shared/shared.module';
import { StoreMock } from 'testing/store-helpers';

import { AccountModule } from '../account.module';
import { TfaSetupComponent } from './tfa-setup.component';

describe('TfaSetupComponent', () => {
  let component: TfaSetupComponent;
  let fixture: ComponentFixture<TfaSetupComponent>;
  let store: StoreMock<any>;
  const tfaData: TfaData = {
    id: 1,
    tfa_method: 'GoogleAuth',
    qr_url: '',
    key: '123456'
  }

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('account', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TfaSetupComponent);
    component = fixture.componentInstance;
    component.tfaData = tfaData;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the tfaData when data is passed on input', () => {
    expect(component.tfaData).toBe(tfaData);
  });

  it('should call the enableTfa output to dispatch when enabling', () => {
    spyOn(component.enableTfa, 'next');
    component.enable({});
    expect(component.enableTfa.next).toHaveBeenCalled();
  });

  it('should call the removeTfa output to dispatch when removing', () => {
    spyOn(component.removeTfa, 'next');
    component.remove({});
    expect(component.removeTfa.next).toHaveBeenCalled();
  });
});
