import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseForm } from '../../shared/base-form';
import { Store } from '@ngrx/store';
import { SessionState } from '../../core/store/session.state';
import * as actions from '../../core/store/session.actions';

const FORM_VALIDATION_MESSAGES = {
  code: {
    required: 'Please enter the verification code.'
  }
};

@Component({
  selector: 'tfa-enable-form',
  templateUrl: 'tfa-enable-form.component.html'
})
export class TfaEnableFormComponent extends BaseForm {
  @Output() onFormSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder, private store: Store<any>) {
    super(FORM_VALIDATION_MESSAGES);
  }

  protected buildForm(): void {
    this.form = this.formBuilder.group({
      code: ['', Validators.required]
    });
  }

  submit(): void {
    this.onFormSubmit.next(this.form.value);
  }
}
