import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from '../../core/store/session.actions';
import { SessionState, TfaData } from '../../core/store/session.state';

@Injectable()
export class SecurityDataResolver implements Resolve<TfaData> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({ type: actions.TFA_FETCH });

    return this.store
      .select('session')
      .filter(
        (session: SessionState) =>
          session.isTfaFetchSuccess || session.isTfaFetchFailure
      )
      .map((session: SessionState) => session.tfaData)
      .take(1);
  }
}
