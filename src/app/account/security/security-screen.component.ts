import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { TfaData } from '../../core/store/session.state';

@Component({
  selector: 'security-screen',
  templateUrl: 'security-screen.component.html'
})
export class SecurityScreenComponent implements OnInit {
  tfaData: TfaData;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.tfaData = routeData.tfaData;
    });
  }

  handleChangePasswordSuccess(): void {
    this.router.navigate(['account']);
  }

  handleChangePasswordCancel(): void {
    this.router.navigate(['account']);
  }
}
