import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AccountState } from '../store/account.state';
import { BaseForm } from '../../shared/base-form';
import * as actions from '../store/account.actions';
import { Validators, FormBuilder } from '@angular/forms';
import { equalsTo } from '../../shared/validation/equals-to.validator';

const FORM_VALIDATION_MESSAGES = {
  password: {
    required: 'You must enter a password.',
    minlength: 'The password must be at least 6 characters long.'
  },
  password_confirmation: {
    equalsTo: 'Passwords do not match.'
  }
};

@Component({
  selector: 'change-password-form',
  templateUrl: './change-password-form.component.html'
})
export class ChangePasswordFormComponent extends BaseForm implements OnInit {
  constructor(private formBuilder: FormBuilder, private store: Store<any>) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToStore();
  }

  protected buildForm(): void {
    this.form = this.formBuilder.group({
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      password_confirmation: ['', equalsTo('password')]
    });
  }

  subscribeToStore(): void {
    this.store
      .select('account')
      .takeWhile(() => this.isAlive)
      .subscribe((accountState: AccountState) => {
        if (accountState.isChangePassSuccess) {
          this.store.dispatch({ type: actions.ACCOUNT_CHANGE_PASSWORD_RESET });
          return this.formSuccess.next(true);
        }

        if (accountState.formError) {
          super.onSubmitError(accountState.formError);
        }
      });
  }

  submit(): void {
    this.store.dispatch({
      type: actions.ACCOUNT_CHANGE_PASSWORD,
      payload: {
        ...this.form.value
      }
    });
  }

  cancel(): void {
    this.store.dispatch({ type: actions.ACCOUNT_CHANGE_PASSWORD_RESET });
    this.formCancel.next(true);
  }
}
