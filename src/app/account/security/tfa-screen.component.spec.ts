import { TfaScreenComponent } from './tfa-screen.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { TfaData } from 'app/core/store/session.state';
import { SharedModule } from 'app/shared/shared.module';
import { StoreMock } from 'testing/store-helpers';
import * as actions from 'app/core/store/session.actions';

import { AccountModule } from '../account.module';
import { TfaSetupComponent } from './tfa-setup.component';

describe('TfaScreenComponent', () => {
  let component: TfaScreenComponent;
  let fixture: ComponentFixture<TfaScreenComponent>;
  let store: StoreMock<any>;
  const tfaData: TfaData = {
    id: 1,
    tfa_method: 'GoogleAuth',
    qr_url: '',
    key: '123456'
  }

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('account', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TfaScreenComponent);
    component = fixture.componentInstance;
    component.tfaData = tfaData;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the tfaData when data is passed on input', () => {
    expect(component.tfaData).toBe(tfaData);
  });

  it('should dispatch with the ACCOUNT_CHANGE_PASSWORD action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.handleEnableTfa({});
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.TFA_ENABLE);
  });

  it('should dispatch with the ACCOUNT_CHANGE_PASSWORD action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.handleRemoveTfa({});
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.TFA_REMOVE);
  });

  it('should dispatch with the ACCOUNT_CHANGE_PASSWORD action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.handleTfaInitialized('');
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.TFA_INITIALIZE_SETUP);
  });
});
