import { FormGroup } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import * as actions from '../store/account.actions';
import { StoreMock } from 'testing/store-helpers';
import { AccountModule } from '../account.module';
import { ChangePasswordFormComponent } from './change-password-form.component';

describe('ChangePasswordFormComponent', () => {
  let component: ChangePasswordFormComponent;
  let fixture: ComponentFixture<ChangePasswordFormComponent>;
  let store: StoreMock<any>;

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('foundersPools', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function updateForm(formData: any) {
    component.form.setValue({
      password: formData.password,
      password_confirmation: formData.password_confirmation
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form object on component instantiation', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should dispatch with the ACCOUNT_CHANGE_PASSWORD action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.submit();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.ACCOUNT_CHANGE_PASSWORD);
  });

  it('should dispatch with the ACCOUNT_CHANGE_PASSWORD_RESET action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.cancel();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.ACCOUNT_CHANGE_PASSWORD_RESET);
  });

  it('form value should update from form changes and still be valid', () => {
    const formDataValid = {
      password: '123456',
      password_confirmation: '123456'
    };
    updateForm(formDataValid);
    expect(component.form.value).toEqual(formDataValid);
    expect(component.form.valid).toBeTruthy();
  });

  it('form.valid should be false when password is empty', fakeAsync(() => {
    const formDataInvalid = {
      password: '',
      password_confirmation: ''
    };
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
  }));

  it('form.valid should be false when password is less than 6 characters', fakeAsync(() => {
    const formDataInvalid = {
      password: '12345',
      password_confirmation: '12345'
    };
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
  }));

  it('form.valid should be false when passwords are different', fakeAsync(() => {
    const formDataInvalid = {
      password: '1234567',
      password_confirmation: '123456'
    };
    updateForm(formDataInvalid);
    fixture.detectChanges();
    expect(component.form.valid).toBeFalsy();
  }));
});
