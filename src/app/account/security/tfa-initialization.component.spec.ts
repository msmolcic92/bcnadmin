import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';

import { AccountModule } from '../account.module';
import { TfaInitializationComponent } from './tfa-initialization.component';
import { TfaMethods } from './tfa-methods';

describe('TfaInitializationComponent', () => {
  let component: TfaInitializationComponent;
  let fixture: ComponentFixture<TfaInitializationComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, AccountModule],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TfaInitializationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should get all tfaMethods when created', () => {
    expect(component.tfaMethods).toEqual(TfaMethods.all());
  });

  it('should call the tfaInitialized EventEmitter when initializeTfaSetup', () => {
    let tfaSelected;
    spyOn(component.tfaInitialized, 'next').and.callFake(
      value => (tfaSelected = value)
    );
    component.initializeTfaSetup();
    expect(component.tfaInitialized.next).toHaveBeenCalled();
    expect(tfaSelected).toEqual(component.tfaMethods[0].key);
  });
});
