export class TfaMethod {
  key: string;
  name: string;
}

export class TfaMethods {
  private static existingMethods: TfaMethod[] = [
    {
      key: 'GoogleAuth',
      name: 'Google Authentication'
    }
  ];

  static all(): TfaMethod[] {
    return this.existingMethods;
  }

  static get(key: string): TfaMethod {
    return this.existingMethods.find(method => method.key === key);
  }
}
