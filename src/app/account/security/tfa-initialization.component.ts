import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TfaMethod, TfaMethods } from './tfa-methods';

@Component({
  selector: 'tfa-initialization',
  templateUrl: 'tfa-initialization.component.html'
})
export class TfaInitializationComponent {
  @Output() tfaInitialized: EventEmitter<string> = new EventEmitter<string>();

  tfaMethods: TfaMethod[];

  selectedTfaMethod: string;

  constructor() {
    this.tfaMethods = TfaMethods.all();
    this.selectedTfaMethod = this.tfaMethods[0].key;
  }

  initializeTfaSetup(): void {
    this.tfaInitialized.next(this.selectedTfaMethod);
  }
}
