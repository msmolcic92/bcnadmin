import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { BinaryCreditCycleListFilterData } from './store/binary-credit-cycles.state';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import * as moment from 'moment';

@Component({
  selector: 'binary-credit-cycle-list-filter',
  templateUrl: 'binary-credit-cycle-list-filter.component.html'
})
export class BinaryCreditCycleListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: BinaryCreditCycleListFilterData;
  @Output()
  valueChange: EventEmitter<BinaryCreditCycleListFilterData> = new EventEmitter<
    BinaryCreditCycleListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      const filters = this.prepareFilters(changes.value.currentValue);
      this.form.patchValue(filters);
    }
  }

  buildForm() {
    const filters: BinaryCreditCycleListFilterData = this.prepareFilters(
      this.value
    );

    this.form = this.formBuilder.group({
      username_filter: [filters.username_filter],
      email_filter: [filters.email_filter],
      date_from_filter: [filters.date_from_filter],
      date_to_filter: [filters.date_to_filter],
      memid_filter: [filters.memid_filter],
      commission_filter: [filters.commission_filter],
      has_commission_id: [filters.has_commission_id]
    });
  }

  submit() {
    this.emitFormValue();
  }

  prepareFilters(
    value: BinaryCreditCycleListFilterData
  ): BinaryCreditCycleListFilterData {
    const filters: BinaryCreditCycleListFilterData = { ...this.value };

    if (filters.commission_filter == -1) {
      filters.commission_filter = null;
      filters.has_commission_id = true;
    }

    if (value.date_to_filter) {
      filters.date_to_filter = moment
        .utc(moment.unix(filters.date_to_filter))
        .startOf('day')
        .unix();
    }

    return filters;
  }

  emitFormValue() {
    const formData = { ...this.form.value };
    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    formData.memid_filter = formData.memid_filter || null;
    formData.commission_filter = formData.commission_filter || null;

    const hasCommissionId = formData.has_commission_id;
    if (formData.commission_filter === null && hasCommissionId) {
      formData.commission_filter = -1;
    }
    formData.has_commission_id = null;

    this.valueChange.emit(formData);
  }
}
