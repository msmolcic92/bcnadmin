import { Action } from '@ngrx/store';
import { DataList } from '../../core/store/helpers';
import { Transaction } from '../../transactions/store/transactions.state';
import * as actions from './binary-credit-cycles.actions';
import * as moment from 'moment';

// Feature types

export class BinaryCreditCycleRecord {
  id: number;
  memid: number;
  username: string;
  email: string;
  fname: string;
  lname: string;
  rank: number;
  credits_left: number;
  credits_right: number;
  blocked_left: number;
  blocked_right: number;
  started_on?: Date;
  finished_on?: Date;
  expired_on?: Date;
  commission_id: number;
  nr_cycles: number;
  status: number;
  transactions: DataList<Transaction>;
}

export class BinaryCreditCycleList {
  data: BinaryCreditCycleRecord[];
  page_number?: number;
  results_per_page?: number;
  total_results?: number;
}

export class BinaryCreditCycleListFilterData {
  page_number?: number = 1;
  results_per_page?: number = 25;
  username_filter?: string;
  email_filter?: string;
  date_from_filter?: number;
  date_to_filter?: number = moment.utc().endOf('day').unix();
  memid_filter?: number = null;
  commission_filter?: number = -1;
  order_by?: string;
  order?: string;
  has_commission_id?: boolean = true;
}

export interface BinaryCreditCyclesState {
  data: BinaryCreditCycleList;
  filterData?: BinaryCreditCycleListFilterData;
  currentBinaryCreditCycle?: BinaryCreditCycleRecord;
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchFailure: boolean;
  isFetchingTransactions: boolean;
  isFetchTransactionsSuccess: boolean;
  isFetchTransactionsFailure: boolean;
}

export const INITIAL_STATE: BinaryCreditCyclesState = {
  data: null,
  currentBinaryCreditCycle: null,
  filterData: new BinaryCreditCycleListFilterData(),
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false,
  isFetchingTransactions: false,
  isFetchTransactionsSuccess: false,
  isFetchTransactionsFailure: false
};

// Reducers

export function binaryCreditCyclesReducer(
  state: BinaryCreditCyclesState = INITIAL_STATE,
  action: Action
): BinaryCreditCyclesState {
  switch (action.type) {
    case actions.FETCH_LIST:
      return {
        ...state,
        isFetching: true
      };

    case actions.FETCH_LIST_SUCCESS:
      return {
        ...state,
        data: { ...state.data, ...(<any>action).payload.data },
        filterData: {
          ...state.filterData,
          ...(<any>action).payload.filterData
        },
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_FAILURE:
      return {
        ...state,
        data: null,
        filterData: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.RESET_LIST_FILTER:
      return {
        ...state,
        filterData: new BinaryCreditCycleListFilterData()
      };

    case actions.FETCH_ITEM:
      return {
        ...state,
        isFetching: true,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_SUCCESS:
      return {
        ...state,
        currentBinaryCreditCycle: (<any>action).payload.data,
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_FAILURE:
      return {
        ...state,
        currentBinaryCreditCycle: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.FETCH_TRANSACTIONS:
      return {
        ...state,
        isFetchingTransactions: true,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: false
      };

    case actions.FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        currentBinaryCreditCycle: {
          ...state.currentBinaryCreditCycle,
          transactions: (<any>action).payload.data
        },
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: true,
        isFetchTransactionsFailure: false
      };

    case actions.FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        currentBinaryCreditCycle: {
          ...state.currentBinaryCreditCycle,
          transactions: null
        },
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: true
      };

    case actions.FETCH_TRANSACTIONS_STATE_RESET:
      return {
        ...state,
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: false
      };

    case actions.ACTION_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    default:
      return state;
  }
}
