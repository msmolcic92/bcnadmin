// Action types

export const FETCH_LIST = 'binaryCreditCycles/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'binaryCreditCycles/FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'binaryCreditCycles/FETCH_LIST_FAILURE';

export const FETCH_ITEM = 'binaryCreditCycles/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'binaryCreditCycles/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_FAILURE = 'binaryCreditCycles/FETCH_ITEM_FAILURE';
export const FETCH_ITEM_STATE_RESET = 'binaryCreditCycles/FETCH_ITEM_STATE_RESET';

export const FETCH_TRANSACTIONS = 'binaryCreditCycles/FETCH_TRANSACTIONS';
export const FETCH_TRANSACTIONS_SUCCESS = 'binaryCreditCycles/FETCH_TRANSACTIONS_SUCCESS';
export const FETCH_TRANSACTIONS_FAILURE = 'binaryCreditCycles/FETCH_TRANSACTIONS_FAILURE';
export const FETCH_TRANSACTIONS_STATE_RESET = 'binaryCreditCycles/FETCH_TRANSACTIONS_STATE_RESET';

export const ACTION_STATE_RESET = 'binaryCreditCycles/ACTION_STATE_RESET';
export const RESET_LIST_FILTER = 'binaryCreditCycles/RESET_LIST_FILTER';
