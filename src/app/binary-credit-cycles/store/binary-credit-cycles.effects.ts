import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { BinaryCreditCyclesService } from 'api-client/binary-credit-cycles.service';
import { TransactionService } from 'api-client/transactions.service';
import { BinaryCreditCycleListFilterData } from './binary-credit-cycles.state';
import * as actions from './binary-credit-cycles.actions';

@Injectable()
export class BinaryCreditCyclesEffects {
  constructor(
    private action$: Actions,
    private binaryCreditCyclesService: BinaryCreditCyclesService,
    private transactionsService: TransactionService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: BinaryCreditCycleListFilterData) => {
      return this.binaryCreditCyclesService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_LIST_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchDetails$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.binaryCreditCyclesService
        .fetch(params.id)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_SUCCESS,
          payload: {
            data: response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_ITEM_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchTransactions$: Observable<Action> = this.action$
    .ofType(actions.FETCH_TRANSACTIONS)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.transactionsService
        .fetchList(params.currency, params.filterData)
        .map((response: any) => ({
          type: actions.FETCH_TRANSACTIONS_SUCCESS,
          payload: {
            data: response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_TRANSACTIONS_FAILURE,
            payload: errorResponse.error
          })
        );
    });
}
