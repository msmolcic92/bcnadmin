import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import { Store } from '@ngrx/store';
import * as actions from './store/binary-credit-cycles.actions';
import {
  BinaryCreditCyclesState,
  BinaryCreditCycleRecord
} from './store/binary-credit-cycles.state';

@Injectable()
export class BinaryCreditCyclesDetailsResolver
  implements Resolve<BinaryCreditCycleRecord> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('binaryCreditCycles')
      .filter(
        (binaryCreditCyclesState: BinaryCreditCyclesState) =>
          binaryCreditCyclesState.isFetchSuccess ||
          binaryCreditCyclesState.isFetchFailure
      )
      .map(
        (binaryCreditCyclesState: BinaryCreditCyclesState) =>
          binaryCreditCyclesState.currentBinaryCreditCycle
      )
      .take(1);
  }
}
