import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';

import { BinaryCreditCyclesService } from '../../api-client/binary-credit-cycles.service';
import { TransactionService } from '../../api-client/transactions.service';

import { binaryCreditCyclesReducer } from './store/binary-credit-cycles.state';
import { BinaryCreditCyclesEffects } from './store/binary-credit-cycles.effects';

import { BinaryCreditCyclesRoutingModule } from './binary-credit-cycles-routing.module';
import { BinaryCreditCyclesDataResolver } from './binary-credit-cycles-data-resolver.service';
import { BinaryCreditCyclesRootComponent } from './binary-credit-cycles-root.component';
import { BinaryCreditCycleListComponent } from './binary-credit-cycle-list.component';
import { BinaryCreditCycleListFilterComponent } from './binary-credit-cycle-list-filter.component';
import { BinaryCreditCyclesScreenComponent } from './binary-credit-cycles-screen.component';
import { BinaryCreditCyclesDetailsScreenComponent } from './binary-credit-cycles-details-screen.component';
import { BinaryCreditCyclesDetailsResolver } from './binary-credit-cycles-details-resolver.service';
import { binaryCreditCyclesTransactionComponent } from './binary-credit-cycles-transactions.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    SharedModule,
    BinaryCreditCyclesRoutingModule,
    StoreModule.forFeature('binaryCreditCycles', binaryCreditCyclesReducer),
    EffectsModule.forFeature([BinaryCreditCyclesEffects])
  ],
  declarations: [
    BinaryCreditCyclesRootComponent,
    BinaryCreditCycleListComponent,
    BinaryCreditCycleListFilterComponent,
    BinaryCreditCyclesScreenComponent,
    BinaryCreditCyclesDetailsScreenComponent,
    binaryCreditCyclesTransactionComponent
  ],
  providers: [
    BinaryCreditCyclesService,
    TransactionService,
    BinaryCreditCyclesDataResolver,
    BinaryCreditCyclesDetailsResolver
  ]
})
export class BinaryCreditCyclesModule {}
