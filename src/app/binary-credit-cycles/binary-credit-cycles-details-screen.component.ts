import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaginationConfig } from '../core/store/helpers';
import {
  BinaryCreditCycleRecord,
  BinaryCreditCyclesState
} from './store/binary-credit-cycles.state';
import * as actions from './store/binary-credit-cycles.actions';

@Component({
  selector: 'binary-credit-cycles-details-screen',
  templateUrl: 'binary-credit-cycles-details-screen.component.html'
})
export class BinaryCreditCyclesDetailsScreenComponent
  implements OnDestroy, OnInit {
  record: BinaryCreditCycleRecord;
  transactions: any[];
  currency = 'btc';
  isFetchingTransactions = true;
  isAlive = true;

  transactionsPagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.record = data.binaryCreditCycleDetails;
      if (this.record.commission_id > 0) {
        this.fetchTransactions();
      }
    });

    this.store
      .select('binaryCreditCycles')
      .takeWhile(() => this.isAlive)
      .subscribe((binaryCreditCyclesState: BinaryCreditCyclesState) => {
        this.isFetchingTransactions =
          binaryCreditCyclesState.isFetchingTransactions;

        if (binaryCreditCyclesState.isFetchSuccess) {
          this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
          this.record = binaryCreditCyclesState.currentBinaryCreditCycle;
        }

        if (binaryCreditCyclesState.isFetchTransactionsSuccess) {
          this.store.dispatch({
            type: actions.FETCH_TRANSACTIONS_STATE_RESET
          });
          this.transactions =
            binaryCreditCyclesState.currentBinaryCreditCycle.transactions.data;

          this.transactionsPagination = {
            itemPerPage:
              binaryCreditCyclesState.currentBinaryCreditCycle.transactions
                .results_per_page,
            currentPage:
              binaryCreditCyclesState.currentBinaryCreditCycle.transactions
                .page_number,
            totalItems:
              binaryCreditCyclesState.currentBinaryCreditCycle.transactions
                .total_results
          };
        }
      });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  fetchTransactions() {
    this.store.dispatch({
      type: actions.FETCH_TRANSACTIONS,
      payload: {
        currency: 'btc',
        filterData: {
          used_for_filter: 'commission_tree',
          used_id_filter: this.record.id,
          page_number: this.transactionsPagination.currentPage,
          results_per_page: this.transactionsPagination.itemPerPage
        }
      }
    });

    this.isFetchingTransactions = true;
  }

  handlePageChange(pageNumber: any) {
    this.transactionsPagination.currentPage = pageNumber;
    this.fetchTransactions();
  }
}
