import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-binary-credit-cycle-transaction',
  templateUrl: 'binary-credit-cycles-transactions.component.html',
  styleUrls: ['../transactions/transactions-screen.component.scss']
})
export class binaryCreditCyclesTransactionComponent {
  @Input() isLoading;
  @Input() memberId;
  @Input() transactions = null;

  expanding = null;

  constructor() {}

  toggle(t) {
    // If there's an expanded row, collapse it.
    if (this.expanding) {
      this.expanding.expanded = false;
    }

    // If previously expanded row is the same
    // as the clicked row, set it to 'null',
    // indicating that there are no rows expanded.
    // Expand the new row otherwise.
    if (this.expanding === t) {
      this.expanding = null;
    } else {
      this.expanding = t;
      t.expanded = true;
    }
  }

  determineTransactionType(senderId: number, receiverId: number): string {
    if (this.memberId === senderId && this.memberId === receiverId) {
      return 'Neutral';
    } else if (this.memberId === senderId) {
      return 'Debit';
    } else if (this.memberId === receiverId) {
      return 'Credit';
    }

    return null;
  }
}
