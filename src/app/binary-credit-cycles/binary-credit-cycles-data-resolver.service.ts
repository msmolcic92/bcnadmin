import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { BinaryCreditCyclesState } from './store/binary-credit-cycles.state';
import * as actions from './store/binary-credit-cycles.actions';

@Injectable()
export class BinaryCreditCyclesDataResolver {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('binaryCreditCycles');

    state$.take(1).subscribe((state: BinaryCreditCyclesState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter(
        (state: BinaryCreditCyclesState) =>
          state.isFetchSuccess || state.isFetchFailure
      )
      .take(1);
  }
}
