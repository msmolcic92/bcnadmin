import { Component, Input } from '@angular/core';

@Component({
  selector: 'binary-credit-cycle-list',
  templateUrl: './binary-credit-cycle-list.component.html',
  styleUrls: ['binary-credit-cycle-list.component.scss']
})
export class BinaryCreditCycleListComponent {
  @Input() items: Array<any> = [];
  @Input() isLoading: boolean;
  expandedItems = [];

  toggleItemExpanded(item) {
    if (this.isItemExpanded(item)) {
      this.expandedItems = this.expandedItems.filter(i => i !== item);
    } else {
      this.expandedItems = this.expandedItems.concat(item);
    }
  }

  isItemExpanded(item) {
    return this.expandedItems.filter(i => i === item).length;
  }
}
