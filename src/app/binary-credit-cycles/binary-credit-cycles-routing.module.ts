import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinaryCreditCyclesRootComponent } from './binary-credit-cycles-root.component';
import { BinaryCreditCyclesScreenComponent } from './binary-credit-cycles-screen.component';
import { BinaryCreditCyclesDetailsScreenComponent } from './binary-credit-cycles-details-screen.component';
import { BinaryCreditCyclesDataResolver } from './binary-credit-cycles-data-resolver.service';
import { BinaryCreditCyclesDetailsResolver } from './binary-credit-cycles-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Binary Credit Cycles'
    },
    component: BinaryCreditCyclesRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: BinaryCreditCyclesScreenComponent,
        resolve: {
          binaryCreditCycles: BinaryCreditCyclesDataResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Details'
        },
        component: BinaryCreditCyclesDetailsScreenComponent,
        resolve: {
          binaryCreditCycleDetails: BinaryCreditCyclesDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BinaryCreditCyclesRoutingModule {}
