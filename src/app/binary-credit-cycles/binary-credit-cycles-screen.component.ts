import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig } from '../core/store/helpers';
import {
  BinaryCreditCyclesState,
  BinaryCreditCycleRecord,
  BinaryCreditCycleList,
  BinaryCreditCycleListFilterData
} from './store/binary-credit-cycles.state';
import * as actions from './store/binary-credit-cycles.actions';

@Component({
  selector: 'binary-credit-cycles-screen',
  templateUrl: './binary-credit-cycles-screen.component.html'
})
export class BinaryCreditCyclesScreenComponent implements OnDestroy, OnInit {
  binaryCreditCycles: BinaryCreditCycleList;
  filterData: BinaryCreditCycleListFilterData = new BinaryCreditCycleListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  isAlive = true;
  filterShown = false;
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.binaryCreditCycles = routeData.binaryCreditCycles.data;
      this.filterData = routeData.binaryCreditCycles.filterData;

      this.store
        .select('binaryCreditCycles')
        .takeWhile(() => this.isAlive)
        .subscribe((state: BinaryCreditCyclesState) => {
          this.isTableLoading = state.isFetching;
          if (state.isFetchSuccess) {
            this.store.dispatch({ type: actions.ACTION_STATE_RESET });
            this.binaryCreditCycles = state.data;
            this.filterData = state.filterData;

            // TODO: Reduce the pagination-related boilerplate
            this.pagination = {
              itemPerPage: state.data.results_per_page,
              currentPage: state.data.page_number,
              totalItems: state.data.total_results
            };
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: BinaryCreditCycleListFilterData = new BinaryCreditCycleListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
