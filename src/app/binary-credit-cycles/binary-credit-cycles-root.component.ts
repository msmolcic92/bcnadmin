import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as actions from './store/binary-credit-cycles.actions';

@Component({
  template: '<router-outlet></router-outlet>'
})
export class BinaryCreditCyclesRootComponent implements OnDestroy {
  constructor(private store: Store<any>) {}

  ngOnDestroy() {
    this.store.dispatch({
      type: actions.RESET_LIST_FILTER
    });
  }
}
