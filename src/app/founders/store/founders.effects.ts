import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { FoundersService } from 'api-client/founders.service';
import * as actions from './founders.actions';

@Injectable()
export class FoundersEffects {
  constructor(
    private action$: Actions,
    private foundersService: FoundersService
  ) {}

  @Effect()
  fetchFounders$ = makeSimpleEffect(
    this.action$,
    this.foundersService.fetchList,
    actions.FOUNDERS_FETCH,
    actions.FOUNDERS_FETCH_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  fetchFounder$ = makeSimpleEffect(
    this.action$,
    this.foundersService.fetch,
    actions.FOUNDER_FETCH,
    actions.FOUNDER_FETCH_SUCCESS,
    actions.ON_ERROR
  );
}
