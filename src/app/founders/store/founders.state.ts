import { Action } from '@ngrx/store';
import * as actions from './founders.actions';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';

export interface FounderListFilterData {
  memid_filter?: number;
  username_filter?: string;
  email_filter?: string;
  promo_nr_filter?: number;
  order_by?: string;
  order?: string;
}

export interface Founder {
  memid: number;
  pool1: number;
  pool2: number;
  pool3: number;
  promotion_time: number;
  last_invoice_id: number;
  last_invoice_time: number;
  update_time: number;
  promo_nr: number;
  gpu: number;
  member: {
    username: string;
    email: string;
    fname: string;
    lname: string;
  };
}

export interface FoundersState extends DefaultState {
  founders: DataList<Founder>;
  currentFounder: Founder;
  filterData: FounderListFilterData;
}

// State initialization.
export const INITIAL_STATE: FoundersState = {
  founders: null,
  currentFounder: null,
  filterData: {},
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FOUNDERS_FETCH]: (state: FoundersState, action: Action) => {
    const filter = { ...(<any>action).payload };
    delete filter.page_number;
    delete filter.results_per_page;
    return {
      ...state,
      ...AutoStates.new(),
      filterData: filter
    };
  },
  [actions.FOUNDERS_FETCH_SUCCESS]: (state: FoundersState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      founders: { ...(<any>action).payload }
    };
  },
  [actions.FOUNDERS_RESET]: (state: FoundersState) => {
    return {
      ...state,
      founders: null,
      filterData: null
    };
  },
  [actions.RESET_LIST_FILTER]: (state: FoundersState) => {
    return {
      ...state,
      filterData: null
    };
  },
  [actions.FOUNDER_FETCH]: (state: FoundersState) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FOUNDER_FETCH_SUCCESS]: (state: FoundersState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      currentFounder: {
        ...state.currentFounder,
        ...(<any>action).payload
      }
    };
  },
  [actions.FOUNDER_RESET]: (state: FoundersState) => {
    return {
      ...state,
      currentFounder: null
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Default reducer.
export function foundersReducer (
  state: FoundersState = INITIAL_STATE,
  action: Action
): FoundersState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
