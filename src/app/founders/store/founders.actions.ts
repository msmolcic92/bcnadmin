// Generic actions.
export const START_REQUEST = 'founders/START_REQUEST';
export const END_REQUEST = 'founders/END_REQUEST';
export const ON_ERROR = 'founders/ON_ERROR';

// Specific actions.
export const FOUNDERS_FETCH = 'founders/FETCH'; // Legacy, to be removed
export const FETCH_LIST = 'founders/FETCH'; // New, to be renamed
export const FOUNDERS_FETCH_SUCCESS = 'founders/FETCH_SUCCESS';
export const FOUNDERS_RESET = 'founders/RESET';
export const RESET_LIST_FILTER = 'founders/RESET_LIST_FILTER';
export const FOUNDER_FETCH = 'founder/FETCH';
export const FOUNDER_FETCH_SUCCESS = 'founder/FETCH_SUCCESS';
export const FOUNDER_RESET = 'founder/RESET';
