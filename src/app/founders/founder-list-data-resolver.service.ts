import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { FoundersState, Founder } from './store/founders.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/founders.actions';

@Injectable()
export class FounderListDataResolver implements Resolve<DataList<Founder>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('founders');

    state$.take(1).subscribe((state: FoundersState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter((state: FoundersState) => !!(state.hasError || state.founders))
      .take(1);
  }
}
