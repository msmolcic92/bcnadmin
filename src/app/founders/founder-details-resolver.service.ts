import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from './store/founders.actions';
import {
  FoundersState,
  Founder
} from './store/founders.state';

@Injectable()
export class FounderDetailsResolver implements Resolve<Founder> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.store.dispatch({
      type: actions.FOUNDER_FETCH,
      payload: { id: route.params.id }
    });

    return this.store
      .select('founders')
      .filter(
        (foundersState: FoundersState) =>
          foundersState.currentFounder && !foundersState.requesting
      )
      .map((foundersState: FoundersState) => {
        return foundersState.currentFounder;
      })
      .take(1);
  }
}
