import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FoundersEffects } from './store/founders.effects';
import { FoundersRoutingModule } from './founders-routing.module';
import { FoundersRootComponent } from './founders-root.component';
import { FoundersScreenComponent } from './founders-screen.component';
import { FounderDetailsScreenComponent } from './founder-details-screen.component';
import { FoundersFilterComponent } from './founders-filter.component';
import { FounderListDataResolver } from './founder-list-data-resolver.service';
import { FounderDetailsResolver } from './founder-details-resolver.service';
import { FoundersService } from '../../api-client/founders.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([
      FoundersEffects
    ]),
    FoundersRoutingModule,
    NgbModule,
    SharedModule
  ],
  declarations: [
    FoundersRootComponent,
    FounderDetailsScreenComponent,
    FoundersScreenComponent,
    FoundersFilterComponent
  ],
  providers: [FounderListDataResolver, FounderDetailsResolver, FoundersService]
})
export class FoundersModule {}
