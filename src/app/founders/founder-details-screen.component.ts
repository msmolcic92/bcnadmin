import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Founder } from './store/founders.state';
import * as actions from './store/founders.actions';

@Component({
  selector: 'founder-details-screen',
  templateUrl: 'founder-details-screen.component.html'
})
export class FounderDetailsScreenComponent implements OnDestroy, OnInit {
  founder: Founder;

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.founder = data.founder;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FOUNDER_RESET });
  }
}
