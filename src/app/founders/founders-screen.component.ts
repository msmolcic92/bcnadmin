import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  Founder,
  FounderListFilterData,
  FoundersState
} from './store/founders.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/founders.actions';

@Component({
  selector: 'founders-screen',
  templateUrl: 'founders-screen.component.html'
})
export class FoundersScreenComponent implements OnDestroy, OnInit {
  subscription: any;
  founders: Founder[];
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  filterData: FounderListFilterData = {
    memid_filter: null,
    username_filter: null,
    email_filter: null,
    promo_nr_filter: null,
    order_by: null,
    order: null
  };
  isTableLoading = true;
  filterHidden = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const foundersState = routeData.founders as FoundersState
      this.founders = foundersState.founders.data;
      this.filterData = foundersState.filterData;
      this.pagination.totalItems = foundersState.founders.total_results;
      this.pagination.currentPage = foundersState.founders.page_number;
      this.pagination.itemPerPage = foundersState.founders.results_per_page;

      this.subscription = this.store
        .select('founders')
        .subscribe((state: FoundersState) => {
          this.isTableLoading = state.requesting;
          this.founders = state.founders ? state.founders.data : null;

          if (state.founders) {
            this.pagination = {
              currentPage: state.founders.page_number,
              itemPerPage: state.founders.results_per_page,
              totalItems: state.founders.total_results
            };
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.store.dispatch({ type: actions.FOUNDERS_RESET });
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.FOUNDERS_FETCH,
      payload: {
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage,
        ...this.filterData
      }
    });
    this.isTableLoading = true;
  }

  pageChanged(currentPage: number): void {
    this.pagination.currentPage = currentPage;
    this.fetchData();
  }

  filterChanged(filterData: FounderListFilterData): void {
    this.filterData = filterData;
    this.fetchData();
  }

  toggleFilter() {
    this.filterHidden = !this.filterHidden;
  }
}
