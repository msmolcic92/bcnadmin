import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { FounderListFilterData } from './store/founders.state';

@Component({
  selector: 'founders-filter',
  templateUrl: 'founders-filter.component.html'
})
export class FoundersFilterComponent implements OnInit, OnChanges {
  @Input() value: FounderListFilterData;
  @Output()
  valueChange: EventEmitter<FounderListFilterData> = new EventEmitter<
    FounderListFilterData
  >();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      username_filter: [this.value.username_filter],
      email_filter: [this.value.email_filter],
      memid_filter: [this.value.memid_filter],
      promo_nr_filter: [this.value.promo_nr_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };
    this.valueChange.emit(formData);
  }
}
