import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoundersRootComponent } from './founders-root.component';
import { FoundersScreenComponent } from './founders-screen.component';
import { FounderDetailsScreenComponent } from './founder-details-screen.component';
import { FounderDetailsResolver } from './founder-details-resolver.service';
import { FounderListDataResolver } from './founder-list-data-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Founders'
    },
    component: FoundersRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: FoundersScreenComponent,
        resolve: {
          founders: FounderListDataResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Founder Details'
        },
        component: FounderDetailsScreenComponent,
        resolve: {
          founder: FounderDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoundersRoutingModule {}
