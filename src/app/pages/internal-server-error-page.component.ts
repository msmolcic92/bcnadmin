import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'internal-server-error-page',
  templateUrl: 'internal-server-error-page.component.html'
})
export class InternalServerErrorPageComponent {
  constructor(private location: Location) {}

  goBack() {
    this.location.back();
  }
}
