import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ModalModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { adminReducer } from './store/admin.state';
import { AdminEffects } from './store/admin.effects';
import { AdminService } from '../../api-client/admin.service';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AdminScreenComponent } from './screen/admin-screen.component';
import { AdminListComponent } from './list/admin-list.component';
import { AdminDataResolver } from './details/admin-data-resolver.service';
import { AdminListResolver } from './list/admin-list-resolver.service';
import { AdminAddUpdateScreenComponent } from './screen/admin-add-update-screen.component';
import { AdminFormComponent } from './form/admin-form.component';
import { AdminDetailsScreenComponent } from './screen/admin-details-screen.component';
import { AdminGroupsComponent } from './details/admin-groups.component';
import { ChangePasswordModalContentComponent } from './modal/change-password-modal-content.component';
import { AdminAccessComponent } from './details/admin-access.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([AdminEffects]),
    StoreModule.forFeature('admin', adminReducer),
    AdminRoutingModule
  ],
  declarations: [
    AdminScreenComponent,
    AdminListComponent,
    AdminAddUpdateScreenComponent,
    AdminFormComponent,
    AdminDetailsScreenComponent,
    AdminGroupsComponent,
    ChangePasswordModalContentComponent,
    AdminAccessComponent
  ],
  entryComponents: [
    ChangePasswordModalContentComponent
  ],
  providers: [AdminService, AdminDataResolver, AdminListResolver]
})
export class AdminModule {}
