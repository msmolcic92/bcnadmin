import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import * as actions from '../store/admin.actions';
import { equalsTo } from 'app/shared/validation/equals-to.validator';

export class AdminFormData {
  id?: number;
  username?: string;
  email?: string;
  fname?: string;
  lname?: string;
}

const FORM_VALIDATION_MESSAGES = {
  username: {
    required: 'Username is required.'
  },
  email: {
    required: 'Email address is required.',
    email: 'Invalid email address.'
  },
  password: {
    required: 'You must enter a password.',
    minlength: 'The password must be at least 6 characters long.'
  },
  password_confirmation: {
    equalsTo: 'Passwords do not match.'
  },
  fname: {
    required: 'First Name is required.'
  },
  lname: {
    required: 'Last Name is required.'
  }
};

@Component({
  selector: 'admin-form',
  templateUrl: 'admin-form.component.html'
})
export class AdminFormComponent extends BaseForm implements OnInit {
  @Input() formData: any;
  @Input() isEdit: boolean;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new AdminFormData();
    }

    this.form = this.formBuilder.group({
      username: [this.formData.username, Validators.required],
      email: [
        this.formData.email,
        Validators.compose([Validators.required, Validators.email])
      ],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      password_confirmation: [
        '',
        Validators.compose([Validators.required, equalsTo('password')])
      ],
      fname: [this.formData.fname, Validators.required],
      lname: [this.formData.lname, Validators.required]
    });

    if (this.isEdit) {
      this.form.removeControl('password');
      this.form.removeControl('password_confirmation');
    }
  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? actions.EDIT_ITEM_SUCCESS
      : actions.ADD_ITEM_SUCCESS;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(actions.ON_ERROR)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const actionType = this.isEdit ? actions.EDIT_ITEM : actions.ADD_ITEM;

    this.store.dispatch({
      type: actionType,
      payload: {
        id: this.formData.id,
        data: { ...this.form.value }
      }
    });
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
