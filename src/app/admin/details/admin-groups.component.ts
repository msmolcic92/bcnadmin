import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'admin-groups',
  templateUrl: 'admin-groups.component.html'
})
export class AdminGroupsComponent {
  @Input() groupStateUpdating: boolean;
  @Input() groups: any[];
  @Output() groupChange: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  handleGroupChange(group: any): void {
    this.groupChange.emit(group);
  }
}
