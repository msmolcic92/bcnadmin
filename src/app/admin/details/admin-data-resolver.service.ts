import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Admin, AdminState } from '../store/admin.state';
import { AdminService } from '../../../api-client/admin.service';
import * as actions from '../store/admin.actions';

@Injectable()
export class AdminDataResolver implements Resolve<Admin> {
  constructor(
    private adminService: AdminService,
    private router: Router,
    private store: Store<any>
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('admin')
      .filter((admin: AdminState) => admin.currentAdmin && !admin.requesting)
      .map((accountState: AdminState) => accountState.currentAdmin)
      .take(1);
  }
}
