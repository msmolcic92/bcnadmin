import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'admin-access',
  templateUrl: 'admin-access.component.html'
})
export class AdminAccessComponent {
  @Input() accessStateUpdating: boolean;
  @Input() accessGroups: any;
  @Output() accessStateChange: EventEmitter<any> = new EventEmitter<any>();

  activePanelIds = new Array<any>();

  constructor() {}

  public beforeChange($event: NgbPanelChangeEvent): void {
    if ($event.nextState === true) {
      this.activePanelIds.push($event.panelId);
    } else {
      this.activePanelIds = this.activePanelIds.filter(id => id !== $event.panelId);
    }
  }

  handleAccessStateChange(item: any): void {
    this.accessStateChange.emit(item);
  }
}
