import { Action } from '@ngrx/store';
import {
  AutoStates,
  DataList,
  DefaultState,
  defaultStateValues,
  makeDefaultHandlers,
  PaginationConfig
} from '../../core/store/helpers';
import * as actions from './admin.actions';

export interface Admin {
  id: number;
  username: string;
  fname: string;
  lname: string;
  email: string;
  groups: {
    group_id: any;
    group_name: string;
    group_template_data: string;
  }[];
}

export interface AdminAccess {
  id: string;
  title: string;
  group: string;
  sub_group: string;
}

export interface AdminGroup {
  id: number;
  description?: string;
  name: string;
  is_system?: boolean;
}

export interface AdminState extends DefaultState {
  admins: DataList<Admin>;
  currentAdmin: Admin;
  currentAccess: AdminAccess[];
  adminGroups: DataList<AdminGroup>;
  isDeleting: boolean;
  isDeleteSuccess: boolean;
  isDeleteFailure: boolean;
}

// State initialization.
export const INITIAL_STATE: AdminState = {
  admins: {
    data: null,
    last_page: 0,
    page_number: 0,
    results_per_page: 0,
    total_results: 0
  },
  currentAdmin: null,
  currentAccess: null,
  adminGroups: null,
  isDeleting: false,
  isDeleteSuccess: false,
  isDeleteFailure: false,
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.ADMIN_RESET_CURRENT_STATE]: (state: AdminState): AdminState => {
    return { ...state, currentAdmin: null, currentAccess: null };
  },
  [actions.FETCH_LIST]: (state: AdminState): AdminState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_LIST_SUCCESS]: (state: AdminState, action: Action) => {
    return { ...state, ...AutoStates.success(), admins: (<any>action).payload };
  },
  [actions.FETCH_ITEM]: (state: AdminState): AdminState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: AdminState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      currentAdmin: (<any>action).payload
    };
  },
  [actions.FETCH_ADMIN_GROUP_LIST]: (state: AdminState): AdminState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ADMIN_GROUP_LIST_SUCCESS]: (
    state: AdminState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      adminGroups: (<any>action).payload
    };
  },
  [actions.ADD_ITEM]: (state: AdminState): AdminState => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.DELETE_ITEM]: (state: AdminState, action: Action): AdminState => {
    return {
      ...state,
      isDeleting: true,
      isDeleteSuccess: false,
      isDeleteFailure: false
    };
  },
  [actions.DELETE_ITEM_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    const totalResults = state.admins.total_results - 1;
    const lastPage = Math.ceil(totalResults / state.admins.results_per_page);
    const pageNumber =
      state.admins.page_number > lastPage ? lastPage : state.admins.page_number;

    return {
      ...state,
      admins: {
        results_per_page: state.admins.results_per_page,
        last_page: lastPage,
        page_number: pageNumber,
        total_results: totalResults,
        data: state.admins.data.filter(
          admin => admin.id !== (<any>action).payload.id
        )
      },
      isDeleting: false,
      isDeleteSuccess: true,
      isDeleteFailure: false
    };
  },
  [actions.DELETE_ITEM_FAILURE]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      isDeleting: false,
      isDeleteSuccess: false,
      isDeleteFailure: true
    };
  },
  [actions.DELETE_ITEM_STATE_RESET]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      isDeleting: false,
      isDeleteSuccess: false,
      isDeleteFailure: false
    };
  },
  [actions.EDIT_ITEM]: (state: AdminState, action: Action): AdminState => {
    return {
      ...state,
      ...AutoStates.new(),
      currentAdmin: {
        ...state.currentAdmin,
        ...(<any>action).payload
      }
    };
  },
  [actions.EDIT_ITEM_SUCCESS]: (state: AdminState): AdminState => {
    return { ...state, ...AutoStates.success() };
  },
  [actions.ADD_ADMIN_TO_GROUP]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.ADD_ADMIN_TO_GROUP_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    const group = (<any>action).payload.group;
    return {
      ...state,
      ...AutoStates.success(),
      currentAdmin: {
        ...state.currentAdmin,
        groups: state.currentAdmin.groups.concat({
          group_id: group.id,
          group_name: group.name,
          group_template_data: group.data
        })
      }
    };
  },
  [actions.REMOVE_ADMIN_FROM_GROUP]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.REMOVE_ADMIN_FROM_GROUP_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.success(),
      currentAdmin: {
        ...state.currentAdmin,
        groups: state.currentAdmin.groups.filter(
          group => group.group_id !== (<any>action).payload.group.id
        )
      }
    }
  },
  [actions.FETCH_ADMIN_ACCESS_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.success(),
      currentAccess: [...(<any>action).payload.data]
    };
  },
  [actions.ADD_ADMIN_ACCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.ADD_ADMIN_ACCESS_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.success(),
      currentAccess: state.currentAccess.map((access: AdminAccess) => {
        return (<any>action).payload.endpoints.includes(access.id)
          ? {
              ...access,
              enabled: true
            }
          : access;
      })
    };
  },
  [actions.DELETE_ADMIN_ACCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.DELETE_ADMIN_ACCESS_SUCCESS]: (
    state: AdminState,
    action: Action
  ): AdminState => {
    return {
      ...state,
      ...AutoStates.success(),
      currentAccess: state.currentAccess.map((access: AdminAccess) => {
        return (<any>action).payload.endpoint === access.id
          ? {
              ...access,
              enabled: false
            }
          : access;
      })
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Reducers
export function adminReducer(
  state: AdminState = INITIAL_STATE,
  action: Action
): AdminState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
