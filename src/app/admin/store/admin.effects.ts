import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { AdminService } from '../../../api-client/admin.service';
import * as actions from './admin.actions';

@Injectable()
export class AdminEffects {
  constructor(
    private action$: Actions,
    private store: Store<any>,
    private adminService: AdminService,
    private router: Router
  ) {}

  @Effect()
  fetchAdminList$ = makeSimpleEffect(
    this.action$,
    this.adminService.fetchList,
    actions.FETCH_LIST,
    actions.FETCH_LIST_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  fetchAdmin$ = makeSimpleEffect(
    this.action$,
    this.adminService.fetch,
    actions.FETCH_ITEM,
    actions.FETCH_ITEM_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  addAdmin$: Observable<Action> = this.action$
    .ofType(actions.ADD_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .addAdmin(payload.data)
        .map((response: any) => ({
          type: actions.ADD_ITEM_SUCCESS,
          payload: response
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  editAdmin$: Observable<Action> = this.action$
    .ofType(actions.EDIT_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .editAdmin(payload.id, payload.data)
        .map((response: any) => ({
          type: actions.EDIT_ITEM_SUCCESS,
          payload: response
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  deleteAdmin$: Observable<Action> = this.action$
    .ofType(actions.DELETE_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .deleteAdmin(payload.id)
        .map((response: any) => ({
          type: actions.DELETE_ITEM_SUCCESS,
          payload: {
            id: payload.id
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.DELETE_ITEM_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchAdminGroups$ = makeSimpleEffect(
    this.action$,
    this.adminService.fetchAdminGroupList,
    actions.FETCH_ADMIN_GROUP_LIST,
    actions.FETCH_ADMIN_GROUP_LIST_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  addAdminToGroup$: Observable<Action> = this.action$
    .ofType(actions.ADD_ADMIN_TO_GROUP)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .addAdminToGroup(payload.adminId, payload.group.id)
        .map((response: any) => ({
          type: actions.ADD_ADMIN_TO_GROUP_SUCCESS,
          payload: {
            group: payload.group
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  removeAdminFromGroup$: Observable<Action> = this.action$
    .ofType(actions.REMOVE_ADMIN_FROM_GROUP)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .removeAdminFromGroup(payload.adminId, payload.group.id)
        .map((response: any) => ({
          type: actions.REMOVE_ADMIN_FROM_GROUP_SUCCESS,
          payload: {
            group: payload.group
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchAdminAccess$ = makeSimpleEffect(
    this.action$,
    this.adminService.fetchAdminAccess,
    actions.FETCH_ADMIN_ACCESS,
    actions.FETCH_ADMIN_ACCESS_SUCCESS,
    actions.ON_ERROR
  );

  @Effect()
  addAdminAccess$: Observable<Action> = this.action$
    .ofType(actions.ADD_ADMIN_ACCESS)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .addAdminAccess(payload)
        .map((response: any) => ({
          type: actions.ADD_ADMIN_ACCESS_SUCCESS,
          payload: {
            endpoints: payload.endpoints
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  deleteAdminAccess$: Observable<Action> = this.action$
    .ofType(actions.DELETE_ADMIN_ACCESS)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.adminService
        .removeAdminAccess(payload)
        .map((response: any) => ({
          type: actions.DELETE_ADMIN_ACCESS_SUCCESS,
          payload: {
            endpoint: payload.endpoint
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });
}
