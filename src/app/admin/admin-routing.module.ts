import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminScreenComponent } from './screen/admin-screen.component';
import { AdminDataResolver } from './details/admin-data-resolver.service';
import { AdminListResolver } from './list/admin-list-resolver.service';
import { AdminAddUpdateScreenComponent } from './screen/admin-add-update-screen.component';
import { AdminDetailsScreenComponent } from 'app/admin/screen/admin-details-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Admin'
    },
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: AdminScreenComponent,
        resolve: {
          admins: AdminListResolver
        }
      },
      {
        path: 'add',
        data: {
          title: 'Add New Admin',
          isEdit: false
        },
        component: AdminAddUpdateScreenComponent
      },
      {
        path: ':id/edit',
        data: {
          title: 'Update Admin',
          isEdit: true
        },
        component: AdminAddUpdateScreenComponent,
        resolve: {
          adminData: AdminDataResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Admin Details'
        },
        component: AdminDetailsScreenComponent,
        resolve: {
          adminData: AdminDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
