import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../../shared/store/app-actions.service';
import { ChangePasswordModalContentComponent } from '../modal/change-password-modal-content.component';
import { Admin, AdminState } from '../store/admin.state';
import * as actions from '../store/admin.actions';
import * as _ from 'lodash';

@Component({
  selector: 'admin-details-screen',
  templateUrl: 'admin-details-screen.component.html'
})
export class AdminDetailsScreenComponent implements OnDestroy, OnInit {
  admin: Admin;
  accessGroups: any;
  groups: any;

  groupStateUpdating = false;
  accessStateUpdating = false;
  isAlive = true;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.store.dispatch({ type: actions.FETCH_ADMIN_GROUP_LIST });

    this.route.data.subscribe(routeData => {
      this.admin = routeData.adminData;
      this.store.dispatch({
        type: actions.FETCH_ADMIN_ACCESS,
        payload: { id: this.admin.id }
      });
    });

    this.subscribeToStore();
    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
    this.store.dispatch({ type: actions.ADMIN_RESET_CURRENT_STATE });
  }

  subscribeToStore(): void {
    this.store
      .select('admin')
      .takeWhile(() => this.isAlive)
      .subscribe((adminState: AdminState) => {
        if (adminState.requesting) {
          return;
        }

        if (adminState.currentAdmin) {
          this.admin = adminState.currentAdmin;
        }

        if (adminState.currentAccess) {
          this.accessGroups = _(adminState.currentAccess)
            .groupBy(access => access.group)
            .map((value, key) => ({
              name: key,
              data: _(value)
                .groupBy(item => item.sub_group)
                .map((v, k) => ({
                  name: k,
                  data: v
                }))
                .value()
            }))
            .value();

          this.accessStateUpdating = false;
        }

        if (adminState.adminGroups && this.admin.groups) {
          this.groups = adminState.adminGroups.data.map(adminGroup => ({
            ...adminGroup,
            checked: !!this.admin.groups.find(
              group => group.group_id === adminGroup.id
            )
          }));

          this.groupStateUpdating = false;
        }
      });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(
        actions.ADD_ADMIN_TO_GROUP_SUCCESS,
        actions.REMOVE_ADMIN_FROM_GROUP_SUCCESS
      )
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'Successfully updated admin groups!'
        );
      });

    this.appAction$
      .ofType(
        actions.ADD_ADMIN_ACCESS,
        actions.DELETE_ADMIN_ACCESS
      )
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'Successfully updated admin access!'
        );
      });

    this.appAction$
      .ofType(actions.ON_ERROR)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'error',
          'Ooops!',
          'An error occured while performing the action.'
        );
      });
  }

  handleAccessStateChange(item: any) {
    this.accessStateUpdating = true;

    if (item.enabled) {
      this.store.dispatch({
        type: actions.DELETE_ADMIN_ACCESS,
        payload: {
          id: this.admin.id,
          endpoint: item.id
        }
      });
    } else {
      this.store.dispatch({
        type: actions.ADD_ADMIN_ACCESS,
        payload: {
          id: this.admin.id,
          endpoints: [item.id]
        }
      });
    }
  }

  handleGroupChange(group: any) {
    this.groupStateUpdating = true;

    if (group.checked) {
      this.store.dispatch({
        type: actions.REMOVE_ADMIN_FROM_GROUP,
        payload: {
          adminId: this.admin.id,
          group: group
        }
      });
    } else {
      this.store.dispatch({
        type: actions.ADD_ADMIN_TO_GROUP,
        payload: {
          adminId: this.admin.id,
          group: group
        }
      });
    }
  }

  handleChangePasswordClick() {
    this._openEditModal(
      ChangePasswordModalContentComponent,
      {
        adminId: this.admin.id
      },
      'Password has been successfully changed.'
    );
  }

  _openEditModal(
    componentClass: any,
    bindings: object = {},
    successMessage?: string
  ) {
    const modalInstance = this.modalService.open(componentClass, {
      keyboard: false
    });

    const componentInstance = modalInstance.componentInstance;
    Object.keys(bindings).forEach((attr: string) => {
      componentInstance[attr] = bindings[attr];
    });

    return modalInstance.result
      .then(result => {
        if (successMessage) {
          this.toasterService.pop('success', 'Success!', successMessage);
        }

        return result;
      })
      .catch(() => {});
  }
}
