import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Admin, AdminState } from '../store/admin.state';
import { AdminFormData } from '../form/admin-form.component';
import * as actions from '../store/admin.actions';

@Component({
  selector: 'admin-add-update-screen',
  templateUrl: 'admin-add-update-screen.component.html'
})
export class AdminAddUpdateScreenComponent implements OnDestroy, OnInit {
  formData: AdminFormData;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (routeData.adminData) {
        const admin = routeData.adminData;
        this.formData = {
          id: admin.id,
          username: admin.username,
          fname: admin.fname,
          lname: admin.lname,
          email: admin.email
        };
      }
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.ADMIN_RESET_CURRENT_STATE });
  }

  handleFormSuccess(response): void {
    this.navigateToAdminList();
  }

  handleFormCancel(): void {
    this.navigateToAdminList();
  }

  navigateToAdminList(): void {
    this.router.navigate(['admin']);
  }
}
