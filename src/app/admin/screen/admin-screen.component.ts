import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../../shared/store/app-actions.service';
import { DataList, PaginationConfig } from '../../core/store/helpers';
import { Admin, AdminState } from '../store/admin.state';
import * as actions from '../store/admin.actions';

@Component({
  selector: 'admin-screen',
  templateUrl: './admin-screen.component.html',
  styleUrls: ['../admin.component.scss']
})
export class AdminScreenComponent implements OnDestroy, OnInit {
  adminList: DataList<Admin>;
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  isTableLoading = true;
  isAlive = true;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.adminList = routeData.admins;

      this.store
        .select('admin')
        .takeWhile(() => this.isAlive)
        .subscribe((adminState: AdminState) => {
          this.isTableLoading = adminState.requesting || adminState.isDeleting;

          if (adminState.admins || adminState.isDeleteSuccess) {
            if (adminState.isDeleteSuccess) {
              this.store.dispatch({ type: actions.DELETE_ITEM_STATE_RESET });
            }
            this.adminList = adminState.admins;
            this.pagination = {
              currentPage: this.adminList.page_number,
              itemPerPage: this.adminList.results_per_page,
              totalItems: this.adminList.total_results
            };
          } else {
            this.adminList = null;
          }
        });
    });

    this.store.dispatch({ type: actions.FETCH_ADMIN_GROUP_LIST });
    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.DELETE_ITEM_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'The item has been successfully deleted.'
        );
      });

    this.appAction$
      .ofType(actions.DELETE_ITEM_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'error',
          'Ooops!',
          'An error occured. Item could not be deleted.'
        );
      });
  }

  handleItemDeleteClick(id: number): void {
    this.store.dispatch({
      type: actions.DELETE_ITEM,
      payload: {
        id: id
      }
    });
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  handlePageChange(pageNumber: any): void {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
