import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Admin, AdminState } from '../store/admin.state';
import { AdminService } from '../../../api-client/admin.service';
import { DataList, PaginationConfig } from '../../core/store/helpers';
import * as actions from '../store/admin.actions';

@Injectable()
export class AdminListResolver implements Resolve<DataList<Admin>> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const state$ = this.store.select('admin');

    this.store.dispatch({ type: actions.FETCH_LIST });

    return state$
      .filter((state: AdminState) => !!(state.hasError || state.admins))
      .map((state: AdminState) => state.admins)
      .take(1);
  }
}
