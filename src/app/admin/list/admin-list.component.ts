import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'admin-list',
  templateUrl: './admin-list.component.html'
})
export class AdminListComponent {
  @Input() admins: any = [];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  handleItemDeleteClick(id: number): void {
    if (confirm('Are you sure you want delete this item?')) {
      this.itemDeleteClick.emit(id);
    }
  }
}
