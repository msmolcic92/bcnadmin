import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { BaseForm } from '../../shared/base-form';
import { equalsTo } from '../../shared/validation/equals-to.validator';
import { AppActions } from '../../shared/store/app-actions.service';
import { Admin } from '../store/admin.state';
import * as actions from '../store/admin.actions';

const FORM_VALIDATION_MESSAGES = {
  password: {
    required: 'You must enter a password.',
    minlength: 'The password must be at least 6 characters long.'
  },
  password_confirmation: {
    equalsTo: 'Passwords do not match.'
  }
};

@Component({
  selector: 'change-password-modal-content',
  templateUrl: 'change-password-modal-content.component.html'
})
export class ChangePasswordModalContentComponent extends BaseForm implements OnInit {
  @Input() adminId: number;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private modalInstance: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    this.form = this.formBuilder.group({
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      password_confirmation: [
        '',
        Validators.compose([Validators.required, equalsTo('password')])
      ]
    });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.EDIT_ITEM_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.modalInstance.close();
      });

    this.appAction$
      .ofType(actions.ON_ERROR)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload.response);
      });
  }

  submit(): void {
    this.clearFormErrors();

    this.store.dispatch({
      type: actions.EDIT_ITEM,
      payload: {
        id: this.adminId,
        data: {
          ...this.form.value
        }
      }
    });
  }

  cancel(): void {
    this.modalInstance.dismiss();
  }
}
