import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import { DataList, PaginationConfig } from '../core/store/helpers';
import { CronLog, CronsState, CronsListFilterData } from './store/crons.state';
import * as actions from './store/crons.actions';

@Component({
  selector: 'crons-screen',
  templateUrl: 'crons-screen.component.html'
})
export class CronsScreenComponent implements OnDestroy, OnInit {
  cronsList: DataList<CronLog>;
  filterData = new CronsListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  filterShown = false;
  isTableLoading = true;
  isAlive = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.cronsList = routeData.crons.cronsList;
      this.filterData = routeData.crons.filterData;

      this.store
        .select('crons')
        .takeWhile(() => this.isAlive)
        .subscribe((state: CronsState) => {
          this.isTableLoading = state.isFetching;

          if (state.isFetchSuccess) {
            this.store.dispatch({ type: actions.FETCH_LIST_STATE_RESET });

            this.cronsList = state.cronsList;
            this.filterData = state.filterData;

            // TODO: Reduce the pagination-related boilerplate
            this.pagination = {
              itemPerPage: this.cronsList.results_per_page,
              currentPage: this.cronsList.page_number,
              totalItems: this.cronsList.total_results
            };
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        filterData: {
          ...this.filterData,
          page_number: this.pagination.currentPage,
          results_per_page: this.pagination.itemPerPage
        }
      }
    });

    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: CronsListFilterData = new CronsListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
