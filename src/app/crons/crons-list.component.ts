import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CronLog } from './store/crons.state';

@Component({
  selector: 'crons-list',
  templateUrl: 'crons-list.component.html'
})
export class CronsListComponent {
  @Input() crons: CronLog[];
  @Input() isLoading: boolean;

  constructor() {}
}
