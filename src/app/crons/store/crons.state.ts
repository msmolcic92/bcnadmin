import { Action } from '@ngrx/store';
import { DataList } from '../../core/store/helpers';
import * as actions from './crons.actions';

// Model

export class CronLog {
  id: number;
  timestamp: number;
  script_start_time: number;
  script_end_time: number;
  execution_time: string;
  script_name: string;
  script_parameters: string;
  script_server: string;
  memory_usage: string;
}

export class CronsListFilterData {
  page_number?: number;
  results_per_page?: number;
  script_name_filter?: string;
  script_server_filter?: string;
  script_start_time_start_filter?: number;
  script_start_time_end_filter?: number;
  execution_time_start_filter?: number = 0;
  execution_time_end_filter?: number = 60000;
  date_from_filter?: number;
  date_to_filter?: number;
  order_by?: string;
  order_direction?: string;
}

export interface CronsState {
  cronsList: DataList<CronLog>;
  filterData: CronsListFilterData;
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchFailure: boolean;
}

export const INITIAL_STATE: CronsState = {
  cronsList: null,
  filterData: new CronsListFilterData(),
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false
};

// Reducers

export function cronsReducer(
  state: CronsState = INITIAL_STATE,
  action: Action
): CronsState {
  switch (action.type) {
    case actions.FETCH_LIST:
      return {
        ...state,
        isFetching: true,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_SUCCESS:
      return {
        ...state,
        cronsList: {
          ...state.cronsList,
          ...(<any>action).payload.data
        },
        filterData: {
          ...state.filterData,
          ...(<any>action).payload.filterData
        },
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_FAILURE:
      return {
        ...state,
        cronsList: null,
        filterData: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.FETCH_LIST_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.RESET_LIST_FILTER:
      return {
        ...state,
        filterData: null
      };

    default:
      return state;
  }
}
