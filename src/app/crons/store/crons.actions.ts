// Action types

export const FETCH_LIST = 'crons/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'crons/FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'crons/FETCH_LIST_FAILURE';
export const FETCH_LIST_STATE_RESET = 'crons/FETCH_LIST_STATE_RESET';

export const RESET_LIST_FILTER = 'crons/RESET_LIST_FILTER';
