import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { CronsService } from '../../../api-client/crons.service';
import { CronsListFilterData } from './crons.state';
import * as actions from './crons.actions';

@Injectable()
export class CronsEffects {

  constructor(
    private action$: Actions,
    private cronsService: CronsService
  ) { }

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.cronsService
        .fetchList(payload.filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData: payload.filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.FETCH_LIST_FAILURE,
          payload: errorResponse.error
        }));
    });
}
