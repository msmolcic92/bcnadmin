import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CronsListDataResolver } from './crons-list-data-resolver.service';
import { CronsRootComponent } from './crons-root.component';
import { CronsScreenComponent } from './crons-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Crons'
    },
    component: CronsRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: CronsScreenComponent,
        resolve: {
          crons: CronsListDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CronsRoutingModule { }
