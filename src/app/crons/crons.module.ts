import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { cronsReducer } from './store/crons.state';
import { CronsEffects } from './store/crons.effects';
import { CronsService } from '../../api-client/crons.service';
import { CronsRoutingModule } from './crons-routing.module';
import { CronsListDataResolver } from './crons-list-data-resolver.service';
import { CronsRootComponent } from './crons-root.component';
import { CronsScreenComponent } from './crons-screen.component';
import { CronsListFilterComponent } from './crons-list-filter.component';
import { CronsListComponent } from './crons-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([CronsEffects]),
    StoreModule.forFeature('crons', cronsReducer),
    CronsRoutingModule
  ],
  declarations: [
    CronsRootComponent,
    CronsScreenComponent,
    CronsListFilterComponent,
    CronsListComponent
  ],
  providers: [
    CronsService,
    CronsListDataResolver
  ]
})
export class CronsModule {}
