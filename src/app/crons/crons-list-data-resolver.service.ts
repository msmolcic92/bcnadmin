import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { CronLog, CronsState } from './store/crons.state';
import * as actions from './store/crons.actions';

@Injectable()
export class CronsListDataResolver implements Resolve<any> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('crons');

    state$.take(1).subscribe((state: CronsState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: {
          filterData: state.filterData
        }
      });
    });

    return state$
      .filter(
        (state: CronsState) => state.isFetchSuccess || state.isFetchFailure
      )
      .take(1);
  }
}
