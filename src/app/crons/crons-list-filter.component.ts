import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { CronsListFilterData } from './store/crons.state';

@Component({
  selector: 'crons-list-filter',
  templateUrl: 'crons-list-filter.component.html'
})
export class CronsListFilterComponent implements OnInit, OnDestroy, OnChanges {
  @Input() value: CronsListFilterData;
  @Output()
  valueChange: EventEmitter<CronsListFilterData> = new EventEmitter<
    CronsListFilterData
  >();

  form: FormGroup;
  isAlive = true;

  executionStartTimeOptionList = [
    { value: 60, label: '> 1 minute' },
    { value: 300, label: '> 5 minutes' },
    { value: 600, label: '> 10 minutes' },
    { value: 1800, label: '> 30 minutes' }
  ];

  executionEndTimeOptionList = [
    { value: 1800, label: '< 30 minutes' },
    { value: 600, label: '< 10 minutes' },
    { value: 300, label: '< 5 minutes' },
    { value: 60, label: '< 1 minute' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        normalizeFilterFormDates(this.form);

        normalizeFilterFormDates(this.form, {
          startDateKey: 'script_start_time_start_filter',
          endDateKey: 'script_start_time_end_filter'
        });
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      script_name_filter: [this.value.script_name_filter],
      script_server_filter: [this.value.script_server_filter],
      script_start_time_start_filter: [
        this.value.script_start_time_start_filter
      ],
      script_start_time_end_filter: [this.value.script_start_time_end_filter],
      execution_time_start_filter: [this.value.execution_time_start_filter],
      execution_time_end_filter: [this.value.execution_time_end_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    if (formData.script_start_time_end_filter) {
      formData.script_start_time_end_filter = endOfTheDayTimestamp(
        formData.script_start_time_end_filter
      );
    }

    formData.execution_time_start_filter = +formData.execution_time_start_filter;
    formData.execution_time_end_filter = +formData.execution_time_end_filter;

    this.valueChange.emit(formData);
  }
}
