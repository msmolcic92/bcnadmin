import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from '../shared/helpers/date-helpers';
import {
  TransactionListFilterData,
  TransactionsState
} from './store/transactions.state';
import * as actions from './store/transactions.actions';

@Component({
  selector: 'transactions-filter',
  templateUrl: 'transactions-filter.component.html',
  styleUrls: ['transactions-filter.component.scss']
})
export class TransactionsFilterComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() value: TransactionListFilterData;
  @Output()
  valueChange: EventEmitter<TransactionListFilterData> = new EventEmitter<
    TransactionListFilterData
  >();

  form: FormGroup;
  transactionTypeOptionList: Array<any>;
  isAlive = true;

  constructor(private store: Store<any>, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        normalizeFilterFormDates(this.form);
      });

    this.store
      .select('transactions')
      .filter((state: TransactionsState) => !state.requesting)
      .takeWhile(() => this.isAlive)
      .subscribe((state: TransactionsState) => {
        if (state.transactionTypes) {
          this.transactionTypeOptionList = [
            {
              type_id: null,
              type_wallet: state.currency,
              type_default_label: 'All'
            },
            ...state.transactionTypes[state.currency]
          ];
        } else {
          this.store.dispatch({ type: actions.FETCH_TRANSACTION_TYPES });
        }
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      sender_id_filter: [this.value.sender_id_filter],
      sender_username_filter: [this.value.sender_username_filter],
      sender_email_filter: [this.value.sender_email_filter],
      receiver_id_filter: [this.value.receiver_id_filter],
      receiver_username_filter: [this.value.receiver_username_filter],
      receiver_email_filter: [this.value.receiver_email_filter],
      involved_member_id_filter: [this.value.involved_member_id_filter],
      involved_username_filter: [this.value.involved_username_filter],
      involved_email_filter: [this.value.involved_email_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter],
      used_for_filter: [this.value.used_for_filter],
      transaction_id_filter: [this.value.transaction_id_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };
    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    formData.used_for_filter =
      formData.used_for_filter === 'null' || formData.used_for_filter === null
        ? null
        : formData.used_for_filter;
    formData.sender_id_filter = +formData.sender_id_filter || null;
    formData.receiver_id_filter = +formData.receiver_id_filter || null;
    formData.involved_member_id_filter =
      +formData.involved_member_id_filter || null;

    this.valueChange.emit(formData);
  }
}
