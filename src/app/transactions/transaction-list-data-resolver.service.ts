import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { TransactionService } from '../../api-client/transactions.service';
import {
  Transaction,
  TransactionsState,
  TransactionListFilterData
} from './store/transactions.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/transactions.actions';

@Injectable()
export class TransactionListDataResolver
  implements Resolve<DataList<Transaction>> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const filterData = new TransactionListFilterData();
    filterData.transaction_id_filter = route.queryParams.transaction_id || null;

    this.store.dispatch({
      type: actions.TRANSACTIONS_FETCH,
      payload: {
        currency: route.params.currency,
        filterData: filterData
      }
    });

    return this.store
      .select('transactions')
      .filter(
        (state: TransactionsState) =>
          (state.transactions || state.hasError) && !state.requesting
      )
      .map((state: TransactionsState) => state.transactions)
      .take(1);
  }
}
