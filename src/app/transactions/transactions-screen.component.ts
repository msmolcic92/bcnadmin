import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  Transaction,
  TransactionsState,
  TransactionListFilterData
} from './store/transactions.state';
import { AccountState } from '../account/store/account.state';
import * as accountActions from '../account/store/account.actions';
import * as actions from './store/transactions.actions';

const DEFAULT_FILTER_DATA = {
  sender_id_filter: null,
  receiver_id_filter: null,
  sender_username_filter: null,
  receiver_username_filter: null,
  sender_email_filter: null,
  receiver_email_filter: null,
  used_for_filter: null,
  date_from_filter: null,
  date_to_filter: null,
  involved_member_id_filter: null,
  involved_email_filter: null,
  involved_username_filter: null,
  transaction_id_filter: null
};

@Component({
  selector: 'transactions-screen',
  templateUrl: './transactions-screen.component.html',
  styleUrls: ['./transactions-screen.component.scss']
})
export class TransactionsScreenComponent implements OnInit, OnDestroy {
  transactionId: number;
  transactions: any;
  currency: string;
  currentUserId: number;
  filterData = new TransactionListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  expanding = null;
  filterShown = false;
  isTableLoading = true;
  isAlive = true;

  constructor(
    private store: Store<any>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((queryParams: any) => {
      this.transactionId = +queryParams.transaction_id || null;
    });

    this.route.data.subscribe((data: any) => {
      const { transactions } = data;
      this.transactions = transactions.data.map(t => ({
        expanded: false,
        ...t
      }));
      this.pagination.totalItems = transactions.total_results;
      this.pagination.currentPage = transactions.page_number;
      this.pagination.itemPerPage = transactions.results_per_page;
    });

    this.store
      .select('transactions')
      .takeWhile(() => this.isAlive)
      .subscribe((state: TransactionsState) => {
        if (this.currency !== state.currency) {
          this.currency = state.currency;
          this.filterData = { ...DEFAULT_FILTER_DATA };
          if (this.transactionId) {
            this.filterData.transaction_id_filter = this.transactionId;
            this.transactionId = null;
          }
        }

        this.transactions = state.transactions ? state.transactions.data : null;

        this.isTableLoading = state.requesting;
        if (state.transactions) {
          this.pagination = {
            currentPage: state.transactions.page_number,
            itemPerPage: state.transactions.results_per_page,
            totalItems: state.transactions.total_results
          };
        }
      });

    // Note: We just know that the account data
    // is fetched by the system, so we don't need
    // to trigger it from here once again.
    this.store
      .select('account')
      .filter((accountState: AccountState) => accountState.isFetchSuccess)
      .map((accountState: AccountState) => accountState.data)
      .takeWhile(() => this.isAlive)
      .subscribe((account: any) => {
        this.currentUserId = account.id;
      });
  }

  ngOnDestroy() {
    this.store.dispatch({ type: actions.TRANSACTIONS_RESET });
    this.isAlive = false;
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.TRANSACTIONS_FETCH,
      payload: {
        currency: this.currency,
        filterData: {
          ...this.filterData,
          page_number: this.pagination.currentPage,
          results_per_page: this.pagination.itemPerPage
        }
      }
    });

    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handlePageChange(currentPage: number): void {
    this.pagination.currentPage = currentPage;
    this.fetchData();
  }

  handleFilterChange(newFilterData: TransactionListFilterData): void {
    this.filterData = newFilterData;
    this.fetchData();
  }

  toggle(t) {
    // If there's an expanded row, collapse it.
    if (this.expanding) {
      this.expanding.expanded = false;
    }

    // If previously expanded row is the same
    // as the clicked row, set it to 'null',
    // indicating that there are no rows expanded.
    // Expand the new row otherwise.
    if (this.expanding === t) {
      this.expanding = null;
    } else {
      this.expanding = t;
      t.expanded = true;
    }
  }

  determineTransactionType(senderId: number, receiverId: number): string {
    if (
      (this.currentUserId !== senderId && this.currentUserId !== receiverId) ||
      (this.currentUserId === senderId && this.currentUserId === receiverId)
    ) {
      return 'Neutral';
    } else if (this.currentUserId === senderId) {
      return 'Credit';
    } else if (this.currentUserId === receiverId) {
      return 'Debit';
    }

    return null;
  }
}
