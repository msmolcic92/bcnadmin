import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PaginationModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { transactionsReducer } from './store/transactions.state';
import { TransactionsRootComponent } from './transactions-root.component';
import { TransactionsScreenComponent } from './transactions-screen.component';
import { TransactionsFilterComponent } from './transactions-filter.component';
import { TransactionService } from '../../api-client/transactions.service';
import { TransactionListDataResolver } from './transaction-list-data-resolver.service';
import { TransactionRoutingModule } from './transactions-routing.module';
import { TransactionsEffects } from './store/transactions.effects';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule,
    TransactionRoutingModule,
    NgbModule,
    EffectsModule.forFeature([TransactionsEffects]),
    StoreModule.forFeature('transactions', transactionsReducer),
    SharedModule
  ],
  declarations: [
    TransactionsRootComponent,
    TransactionsScreenComponent,
    TransactionsFilterComponent
  ],
  providers: [TransactionService, TransactionListDataResolver]
})
export class TransactionsModule {}
