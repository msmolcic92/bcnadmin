import { Action } from '@ngrx/store';
import {
  AutoStates,
  DataList,
  DefaultState,
  defaultStateValues,
  makeDefaultHandlers,
  PaginationConfig
} from '../../core/store/helpers';
import * as actions from './transactions.actions';

export class TransactionListFilterData {
  sender_id_filter?: number;
  receiver_id_filter?: number;
  sender_username_filter?: string;
  receiver_username_filter?: string;
  sender_email_filter?: string;
  receiver_email_filter?: string;
  used_for_filter?: string;
  date_from_filter?: number;
  date_to_filter?: number;
  involved_member_id_filter?: number;
  involved_username_filter?: string;
  involved_email_filter?: string;
  transaction_id_filter?: number;
}

interface UserInfo {
  username: string;
  fname: string;
  lname: string;
}
export interface Transaction {
  id: number;
  sender: UserInfo;
  receiver: UserInfo;
  amount: number;
  type: string;
  refunded: number;
  giveaway: number;
  info: string;
  other_info: string;
  created_by: string;
  created_on: number;
  status: string;
  // BTC Only
  date_made: number;
  user_for: string;
  notes: string;
  amount_usd: number;
  exchange_rate: string;
}

export interface TransactionType {
  type_id: string;
  type_wallet: string;
  type_default_label: string;
}

export interface TransactionsState extends DefaultState {
  transactions: DataList<Transaction>;
  currency: string;
  filterData: TransactionListFilterData;
  transactionTypes: {
    [key: string]: TransactionType[];
  };
}
const initialState: TransactionsState = {
  transactions: null,
  currency: null,
  filterData: null,
  transactionTypes: null,
  ...defaultStateValues
};

const ACTION_HANDLERS = {
  [actions.TRANSACTION_CHANGE_CURRENCY]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return { ...state, currency: (<any>action).payload };
  },
  [actions.TRANSACTIONS_RESET]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return { ...state, currency: null, transactions: null, filterData: null };
  },
  [actions.TRANSACTIONS_FETCH]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.TRANSACTIONS_FETCH_SUCCESS]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return {
      ...state,
      ...AutoStates.success(),
      currency: (<any>action).payload.currency,
      transactions: (<any>action).payload.data,
      filterData: (<any>action).payload.filterData
    };
  },

  [actions.RESET_LIST_FILTER]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return {
      ...state,
      filterData: null
    };
  },

  [actions.FETCH_TRANSACTION_TYPES]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FETCH_TRANSACTION_TYPES_SUCCESS]: (
    state: TransactionsState,
    action: Action
  ): TransactionsState => {
    return {
      ...state,
      ...AutoStates.success(),
      transactionTypes: (<any>action).payload.items
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

export function transactionsReducer(
  state: TransactionsState = initialState,
  action: Action
): TransactionsState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
