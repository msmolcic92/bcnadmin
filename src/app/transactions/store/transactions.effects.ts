import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { TransactionService } from 'api-client/transactions.service';
import * as actions from './transactions.actions';

@Injectable()
export class TransactionsEffects {
  constructor(
    private action$: Actions,
    private store: Store<any>,
    private service: TransactionService,
    private router: Router
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.TRANSACTIONS_FETCH)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.service
        .fetchList(params.currency, params.filterData)
        .map((response: any) => ({
          type: actions.TRANSACTIONS_FETCH_SUCCESS,
          payload: {
            currency: params.currency,
            data: response,
            filterData: params.filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.ON_ERROR,
          payload: errorResponse.error
        }));
    });

  @Effect()
  fetchTransactionTypes: Observable<Action> = makeSimpleEffect(
    this.action$,
    this.service.fetchTransactionTypes,
    actions.FETCH_TRANSACTION_TYPES,
    actions.FETCH_TRANSACTION_TYPES_SUCCESS,
    actions.ON_ERROR
  );
}
