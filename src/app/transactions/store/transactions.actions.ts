// Generic actions.
export const START_REQUEST = 'transactions/START_REQUEST';
export const END_REQUEST = 'transactions/END_REQUEST';
export const ON_ERROR = 'transactions/ON_ERROR';

// Specific actions.
export const TRANSACTION_CHANGE_CURRENCY = 'transactions/TRANSACTION_CHANGE_CURRENCY';
export const TRANSACTIONS_FETCH = 'transactions/TRANSACTIONS_FETCH';
export const TRANSACTIONS_FETCH_SUCCESS = 'transactions/TRANSACTIONS_FETCH_SUCCESS';
export const TRANSACTIONS_RESET = 'transactions/TRANSACTIONS_RESET';
export const RESET_LIST_FILTER = 'transactions/RESET_LIST_FILTER';

export const FETCH_TRANSACTION_TYPES = 'transactions/FETCH_TRANSACTION_TYPES';
export const FETCH_TRANSACTION_TYPES_SUCCESS = 'transactions/FETCH_TRANSACTION_TYPES_SUCCESS';
