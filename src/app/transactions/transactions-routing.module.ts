import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionsRootComponent } from './transactions-root.component';
import { TransactionsScreenComponent } from './transactions-screen.component';
import { TransactionListDataResolver } from './transaction-list-data-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Wallets'
    },
    component: TransactionsRootComponent,
    children: [
      {
        path: ':currency',
        data: {
          title: ''
        },
        component: TransactionsScreenComponent,
        resolve: {
          transactions: TransactionListDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionRoutingModule {}
