import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { isNumeric } from 'rxjs/util/isNumeric';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { FounderPoolsListFilterData } from './store/founders-pools.state';

@Component({
  selector: 'founders-pools-filter',
  templateUrl: 'founders-pools-filter.component.html'
})
export class FoundersPoolsFilterComponent implements OnInit, OnChanges {
  @Input() value: FounderPoolsListFilterData;
  @Output()
  valueChange: EventEmitter<FounderPoolsListFilterData> = new EventEmitter<
    FounderPoolsListFilterData
  >();
  form: FormGroup;

  paidOptionList = [
    { value: null, label: 'Paid and Not Paid' },
    { value: 1, label: 'Paid' },
    { value: 0, label: 'Not Paid' }
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      date_made_from_filter: [this.value.date_made_from_filter],
      date_made_to_filter: [this.value.date_made_to_filter],
      paid_filter: [this.value.paid_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    formData.paid_filter = isNumeric(formData.paid_filter)
      ? +formData.paid_filter
      : null;

    if (formData.date_made_to_filter) {
      formData.date_made_to_filter = endOfTheDayTimestamp(
        formData.date_made_to_filter
      );
    }

    formData.date_made_from_filter = formData.date_made_from_filter
      ? formData.date_made_from_filter
      : null;

    this.valueChange.emit(formData);
  }
}
