import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from '../shared/shared.module';
import { FoundersPoolsService } from 'api-client/founders-pools.service';
import { FoundersPoolsListDataResolver } from './founders-pools-list-data-resolver.service';
import { FoundersPoolsDetailsResolver } from './founders-pools-details-resolver.service';
import { FoundersPoolsScreenComponent } from './founders-pools-screen.component';
import { FoundersPoolsFormScreenComponent } from './founders-pools-form-screen.component';
import { FoundersPoolsFormComponent } from './founders-pools-form.component';
import { FoundersPoolsRootComponent } from './founders-pools-root.component';
import { FoundersPoolsRoutingModule } from './founders-pools-routing.module';
import { FoundersPoolsEffects } from './store/founders-pools.effects';
import { FoundersPoolsFilterComponent } from './founders-pools-filter.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([FoundersPoolsEffects]),
    SharedModule,
    FoundersPoolsRoutingModule,
  ],
  declarations: [
    FoundersPoolsRootComponent,
    FoundersPoolsScreenComponent,
    FoundersPoolsFormScreenComponent,
    FoundersPoolsFormComponent,
    FoundersPoolsFilterComponent
  ],
  providers: [
    FoundersPoolsService,
    FoundersPoolsListDataResolver,
    FoundersPoolsDetailsResolver
  ]
})
export class FoundersPoolsModule {}
