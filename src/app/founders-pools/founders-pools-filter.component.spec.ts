import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterLinkStubDirective } from 'testing/router-helpers';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { FoundersPoolsFilterComponent } from './founders-pools-filter.component';
import { FoundersPoolsModule } from './founders-pools.module';
import { FounderPoolsListFilterData } from './store/founders-pools.state';


describe('FounderPoolsListFilterComponent', () => {
  const PAID_OPTIONS = [
    { value: null, label: 'Paid and Not Paid' },
    { value: 1, label: 'Paid' },
    { value: 0, label: 'Not Paid' }
  ];

  let component: FoundersPoolsFilterComponent;
  let fixture: ComponentFixture<FoundersPoolsFilterComponent>;
  const expectedFilterData: FounderPoolsListFilterData = {
    date_made_from_filter: 0,
    date_made_to_filter: 0,
    paid_filter: null
  };

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, FoundersPoolsModule],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundersPoolsFilterComponent);
    component = fixture.componentInstance;
    component.value = expectedFilterData;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should obtain the list of member ranks from the store', () => {
    expect(component.paidOptionList).toEqual(PAID_OPTIONS);
  });

  it('should accept the filter data as input "value" parameter', () => {
    expect(component.value).toBe(expectedFilterData);
  });

  it('should emit the filterChange event with updated filter data on submit', () => {
    let outputValue: FounderPoolsListFilterData;
    component.valueChange.subscribe((filterData: FounderPoolsListFilterData) => 
    outputValue = filterData);
    component.submit();
    expect(outputValue).toBeDefined();
  });

  // TODO: Test if particular fields are rendered and particular output values are formatted correctly
});
