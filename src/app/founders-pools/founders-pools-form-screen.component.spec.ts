import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  ActivatedRoute,
  ActivatedRouteStub,
  Router,
  RouterLinkStubDirective,
  RouterStub
} from 'testing/router-helpers';

import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { FoundersPoolsFormScreenComponent } from './founders-pools-form-screen.component';
import { FoundersPoolsModule } from './founders-pools.module';
import { FounderPools } from './store/founders-pools.state';
import { FoundersPoolsItemFormData } from './founders-pools-form.component';

describe('FounderPoolsFormScreenComponent', () => {
  let component: FoundersPoolsFormScreenComponent;
  let fixture: ComponentFixture<FoundersPoolsFormScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  const founderPoolMock: FounderPools = {
    id: 1,
    date_made: 1239842398,
    btc_mined: 0.001,
    founder1_pool: 0.0001,
    founder2_pool: 0.0001,
    founder1_pay: 0.0001,
    founder2_pay: 0.0001,
    paid: 1
  };

  const formDataMock: FoundersPoolsItemFormData = {
    id: 1,
    date_made: 1239842398,
    btc_mined: 0.001,
    paid: 1
  };

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      isEdit: true,
      foundersPoolsDetails: founderPoolMock
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, FoundersPoolsModule],
        providers: [
          { provide: Router, useClass: RouterStub},
          { provide: ActivatedRoute, useValue: activatedRoute }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundersPoolsFormScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // beforeEach(
  //   inject([Store], (store: StoreMock<any>) => {
  //     store.mockState('members', {
  //       data: {
  //         data: []
  //       },
  //       filterData: {}
  //     });
  //   })
  // );

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be pass the details to the component via the router', () => {
    expect(component.formData).toEqual(formDataMock);
  });

  it('should navigate to list when calling handleFormSuccess', () => {
    const routerMock = TestBed.get(Router);
    spyOn(routerMock, 'navigate');
    component.handleFormSuccess({});
    expect(routerMock.navigate).toHaveBeenCalled();
  });

  it('should navigate to list when calling handleFormCancel', () => {
    const routerMock = TestBed.get(Router);
    spyOn(routerMock, 'navigate');
    component.handleFormCancel();
    expect(routerMock.navigate).toHaveBeenCalled();
  });
});
