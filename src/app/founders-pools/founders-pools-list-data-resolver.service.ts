import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { FoundersPoolsState, FounderPools } from './store/founders-pools.state';
import { DataList, PaginationConfig } from '../core/store/helpers';
import * as actions from './store/founders-pools.actions';

@Injectable()
export class FoundersPoolsListDataResolver
  implements Resolve<DataList<FounderPools>> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('foundersPools');

    state$.take(1).subscribe((state: FoundersPoolsState) => {
      this.store.dispatch({
        type: actions.FOUNDERS_POOLS_FETCH
      });
    });

    return state$
      .filter(
        (state: FoundersPoolsState) => !!(state.hasError || state.foundersPools)
      )
      .take(1);
  }
}
