import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed
} from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { RouterLinkStubDirective } from 'testing/router-helpers';

import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { StoreMock } from './../../testing/store-helpers';
import { FoundersPoolsItemFormData } from './founders-pools-form.component';
import { FoundersPoolsFormComponent } from './founders-pools-form.component';
import { FoundersPoolsModule } from './founders-pools.module';

describe('FounderPoolsFormComponent', () => {
  let component: FoundersPoolsFormComponent;
  let fixture: ComponentFixture<FoundersPoolsFormComponent>;
  let store: StoreMock<any>;
  const formDataMock: FoundersPoolsItemFormData = {
    id: 1,
    date_made: 1239842398,
    btc_mined: 0.001
  };
  const formDataUpdate: FoundersPoolsItemFormData = {
    date_made: 1239842398,
    btc_mined: 0.1
  };
  const formDataInvalid: any = {
    date_made: '',
    btc_mined: '',
  };

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('foundersPools', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, FoundersPoolsModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundersPoolsFormComponent);
    component = fixture.componentInstance;
    component.formData = formDataMock;
    component.isEdit = true;
    fixture.detectChanges();
  });

  function updateForm(founderPool: any) {
    component.form.setValue({
      date_made: founderPool.date_made,
      btc_mined: founderPool.btc_mined,
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be pass the details to the component via the input variables', () => {
    expect(component.isEdit).toEqual(true);
    expect(component.formData).toEqual(formDataMock);
  });

  it('should create a form object on component instantiation', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should dispatch an create/update action when doing a submit of the form', () => {
    spyOn(store, 'dispatch');
    component.submit();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it(
    'form value should update from form changes and still be valid',
    fakeAsync(() => {
      updateForm(formDataUpdate);
      expect(component.form.value).toEqual(formDataUpdate);
      expect(component.form.valid).toBeTruthy();
    })
  );

  it(
    'form.valid should be false when data is invalid',
    fakeAsync(() => {
      updateForm(formDataInvalid);
      expect(component.form.valid).toBeFalsy();
    })
  );
});
