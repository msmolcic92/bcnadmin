import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { FoundersPoolsItemFormData } from './founders-pools-form.component';
import * as actions from './store/founders-pools.actions';

@Component({
  selector: 'founders-pools-form-screen',
  templateUrl: 'founders-pools-form-screen.component.html'
})
export class FoundersPoolsFormScreenComponent implements OnDestroy, OnInit {
  formData: FoundersPoolsItemFormData;
  currency: string;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (routeData.foundersPoolsDetails) {
        const selectedItem = routeData.foundersPoolsDetails;
        this.formData = {
          id: selectedItem.id,
          date_made: selectedItem.date_made,
          btc_mined: selectedItem.btc_mined,
          paid: selectedItem.paid
        };
      }
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
  }

  handleFormSuccess($event): void {
    this.navigateToList();
  }

  handleFormCancel(): void {
    this.navigateToList();
  }

  navigateToList(): void {
    this.router.navigate(['founders-pools']);
  }
}
