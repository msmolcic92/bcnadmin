import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import {
  FoundersPoolsState,
  FounderPools,
  getFoundersPoolsItem
} from './store/founders-pools.state';
import * as actions from './store/founders-pools.actions';

@Injectable()
export class FoundersPoolsDetailsResolver implements Resolve<FounderPools> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('foundersPools')
      .filter((state: FoundersPoolsState) => !state.requesting)
      .map((state: FoundersPoolsState) => {
        return getFoundersPoolsItem(state, +route.params.id);
      })
      .take(1);
  }
}
