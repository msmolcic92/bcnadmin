import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FoundersPoolsRootComponent } from './founders-pools-root.component';
import { FoundersPoolsListDataResolver } from './founders-pools-list-data-resolver.service';
import { FoundersPoolsDetailsResolver } from './founders-pools-details-resolver.service';
import { FoundersPoolsScreenComponent } from './founders-pools-screen.component';
import { FoundersPoolsFormScreenComponent } from './founders-pools-form-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Founders Pools'
    },
    component: FoundersPoolsRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: FoundersPoolsScreenComponent,
        resolve: {
          foundersPools: FoundersPoolsListDataResolver
        }
      },
      {
        path: 'add',
        data: {
          title: 'Add Founder Pool',
          isEdit: false
        },
        component: FoundersPoolsFormScreenComponent
      },
      {
        path: ':id/edit',
        data: {
          title: 'Edit Founder Pool',
          isEdit: true
        },
        component: FoundersPoolsFormScreenComponent,
        resolve: {
          foundersPoolsDetails: FoundersPoolsDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoundersPoolsRoutingModule {}
