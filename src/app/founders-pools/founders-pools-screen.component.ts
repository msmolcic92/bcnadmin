import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import { PaginationConfig } from '../core/store/helpers';
import {
  FounderPools,
  FoundersPoolsState,
  FounderPoolsListFilterData
} from './store/founders-pools.state';
import * as actions from './store/founders-pools.actions';

@Component({
  templateUrl: 'founders-pools-screen.component.html',
  selector: 'founders-pools-screen'
})
export class FoundersPoolsScreenComponent implements OnInit, OnDestroy {
  foundersPools: FounderPools[] = [];
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  filterData: FounderPoolsListFilterData = {
    date_made_from_filter: 0,
    date_made_to_filter: 0,
    paid_filter: null
  };
  isTableLoading = true;
  filterShown = false;
  isAlive = true;

  constructor(
    private route: ActivatedRoute,
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const foundersPoolsState = routeData.foundersPools as FoundersPoolsState;
      this.foundersPools = foundersPoolsState.foundersPools.data;
      this.pagination.totalItems =
        foundersPoolsState.foundersPools.total_results;
      this.pagination.currentPage =
        foundersPoolsState.foundersPools.page_number;
      this.pagination.itemPerPage =
        foundersPoolsState.foundersPools.results_per_page;

      this.store
        .select('foundersPools')
        .takeWhile(() => this.isAlive)
        .subscribe((state: FoundersPoolsState) => {
          this.isTableLoading = state.requesting;
          this.foundersPools = state.foundersPools
            ? state.foundersPools.data
            : null;
          if (state.foundersPools) {
            this.pagination = {
              currentPage: state.foundersPools.page_number,
              itemPerPage: state.foundersPools.results_per_page,
              totalItems: state.foundersPools.total_results
            };
          }

          if (state.filterData) {
            this.filterData = state.filterData;
          }
        });
    });

    this.subscribeToActions();
  }

  ngOnDestroy() {
    this.isAlive = false;
    this.store.dispatch({ type: actions.FOUNDERS_POOLS_RESET });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.DELETE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'The item has been successfully deleted.'
        );
      });

    this.appAction$
      .ofType(actions.DELETE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'error',
          'Ooops!',
          'An error occured. Item could not be deleted.'
        );
      });
  }

  fetchData(): void {
    this.store.dispatch({
      type: actions.FOUNDERS_POOLS_FETCH,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });

    this.isTableLoading = true;
  }

  pageChanged(currentPage: number): void {
    this.pagination.currentPage = currentPage;
    this.fetchData();
  }

  filterChanged(filterData: FounderPoolsListFilterData): void {
    this.filterData = filterData;
    this.fetchData();
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleItemDeleteClick(id: number): void {
    if (confirm('Are you sure you want to permanently delete this item?')) {
      this.store.dispatch({
        type: actions.DELETE,
        payload: {
          id: id
        }
      });
    }
  }
}
