import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import { FounderPoolsListFilterData } from './founders-pools.state';
import { FoundersPoolsService } from 'api-client/founders-pools.service';
import * as actions from './founders-pools.actions';

@Injectable()
export class FoundersPoolsEffects {
  constructor(
    private action$: Actions,
    private foundersPoolsService: FoundersPoolsService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FOUNDERS_POOLS_FETCH)
    .map(toPayload)
    .switchMap((filterData: FounderPoolsListFilterData) => {
      return this.foundersPoolsService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FOUNDERS_POOLS_FETCH_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.foundersPoolsService
        .fetch(params.id)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_SUCCESS,
          payload: response
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.ON_ERROR,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  create$: Observable<Action> = this.action$
    .ofType(actions.CREATE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.foundersPoolsService
        .create(payload.data)
        .map((response: any) => ({
          type: actions.CREATE_SUCCESS,
          payload: {
            id: response.id,
            ...payload
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.CREATE_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  update$: Observable<Action> = this.action$
    .ofType(actions.UPDATE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.foundersPoolsService
        .update(payload.id, payload.data)
        .map((response: any) => ({
          type: actions.UPDATE_SUCCESS,
          payload
        }))
        .catch(errorResponse => Observable.of({
          type: actions.UPDATE_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  delete$: Observable<Action> = this.action$
    .ofType(actions.DELETE)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.foundersPoolsService
        .delete(payload.id)
        .map((response: any) => ({
          type: actions.DELETE_SUCCESS,
          payload
        }))
        .catch(errorResponse => Observable.of({
          type: actions.DELETE_FAILURE,
          payload: errorResponse.error
        }));
    });
}
