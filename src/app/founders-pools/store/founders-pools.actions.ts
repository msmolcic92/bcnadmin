// Generic actions.
export const START_REQUEST = 'foundersPools/START_REQUEST';
export const END_REQUEST = 'foundersPools/END_REQUEST';
export const ON_ERROR = 'foundersPools/ON_ERROR';

// Specific actions.
export const FOUNDERS_POOLS_FETCH = 'foundersPools/FETCH'; // Legacy, to be removed
export const FOUNDERS_POOLS_FETCH_SUCCESS = 'foundersPools/FETCH_SUCCESS';
export const FOUNDERS_POOLS_RESET = 'foundersPools/RESET';
export const RESET_LIST_FILTER = 'foundersPools/RESET_LIST_FILTER';

export const FETCH_ITEM = 'foundersPools/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'foundersPools/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_STATE_RESET = 'foundersPools/FETCH_ITEM_STATE_RESET';

export const CREATE = 'foundersPools/CREATE';
export const CREATE_SUCCESS = 'foundersPools/CREATE_SUCCESS';
export const CREATE_FAILURE = 'foundersPools/CREATE_FAILURE';

export const UPDATE = 'foundersPools/UPDATE';
export const UPDATE_SUCCESS = 'foundersPools/UPDATE_SUCCESS';
export const UPDATE_FAILURE = 'foundersPools/UPDATE_FAILURE';

export const DELETE = 'foundersPools/DELETE';
export const DELETE_SUCCESS = 'foundersPools/DELETE_SUCCESS';
export const DELETE_FAILURE = 'foundersPools/DELETE_FAILURE';
