import { AdminLogsListFilterData } from './../../admin-logs/store/admin-logs.state';
import { filterQueryId } from '@angular/core/src/view/util';
import { Action } from '@ngrx/store';
import * as actions from './founders-pools.actions';
import {
  DataList,
  DefaultState,
  defaultStateValues,
  AutoStates,
  makeDefaultHandlers
} from '../../core/store/helpers';

export interface FounderPools {
  id: number;
  date_made: number;
  btc_mined: number;
  founder1_pool: number;
  founder2_pool: number;
  founder1_pay: number;
  founder2_pay: number;
  paid: number;
}

export interface FounderPoolsListFilterData {
  date_made_from_filter?: number;
  date_made_to_filter?: number;
  paid_filter?: number;
}

export interface FoundersPoolsState extends DefaultState {
  foundersPools: DataList<FounderPools>;
  filterData: FounderPoolsListFilterData;
}

// State initialization.
export const INITIAL_STATE: FoundersPoolsState = {
  foundersPools: {
    data: [],
    total_results: 0,
    page_number: 1,
    last_page: 1,
    results_per_page: 25
  },
  filterData: {},
  ...defaultStateValues
};

// Action handlers declaration.
const ACTION_HANDLERS = {
  [actions.FOUNDERS_POOLS_FETCH]: (
    state: FoundersPoolsState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.new()
    };
  },
  [actions.FOUNDERS_POOLS_FETCH_SUCCESS]: (
    state: FoundersPoolsState,
    action: Action
  ) => {
    return {
      ...state,
      ...AutoStates.success(),
      foundersPools: { ...(<any>action).payload.data },
      filterData: { ...state.filterData, ...(<any>action).payload.filterData }
    };
  },
  [actions.FOUNDERS_POOLS_RESET]: (state: FoundersPoolsState) => {
    return {
      ...INITIAL_STATE
    };
  },
  [actions.RESET_LIST_FILTER]: (state: FoundersPoolsState) => {
    return {
      ...state,
      filterData: null
    };
  },
  [actions.FETCH_ITEM]: (state: FoundersPoolsState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.FETCH_ITEM_SUCCESS]: (state: FoundersPoolsState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      foundersPools: {
        ...state.foundersPools,
        data: addOrUpdateFoundersPoolsItem(state, (<any>action).payload)
      }
    };
  },
  [actions.CREATE]: (state: FoundersPoolsState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.CREATE_SUCCESS]: (state: FoundersPoolsState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      foundersPools: {
        ...updatePagination(state, 1),
        data: addOrUpdateFoundersPoolsItem(state, (<any>action).payload)
      }
    };
  },
  [actions.UPDATE]: (state: FoundersPoolsState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.UPDATE_SUCCESS]: (state: FoundersPoolsState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      foundersPools: {
        ...state.foundersPools,
        data: addOrUpdateFoundersPoolsItem(state, (<any>action).payload)
      }
    };
  },
  [actions.DELETE]: (state: FoundersPoolsState) => {
    return { ...state, ...AutoStates.new() };
  },
  [actions.DELETE_SUCCESS]: (state: FoundersPoolsState, action: Action) => {
    return {
      ...state,
      ...AutoStates.success(),
      foundersPools: {
        ...updatePagination(state, -1),
        data: state.foundersPools.data.filter(
          earning => earning.id !== (<any>action).payload.id
        )
      }
    };
  },
  ...makeDefaultHandlers(
    actions.START_REQUEST,
    actions.END_REQUEST,
    actions.ON_ERROR
  )
};

// Default reducer.
export function foundersPoolsReducer(
  state: FoundersPoolsState = INITIAL_STATE,
  action: Action
): FoundersPoolsState {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}

export const getFoundersPoolsItem = (
  state: FoundersPoolsState,
  id: number
): FounderPools => {
  return state.foundersPools.data.filter(item => item.id === id)[0];
};

const addOrUpdateFoundersPoolsItem = (
  state: FoundersPoolsState,
  foundersPoolsItem: FounderPools
): FounderPools[] => {
  if (
    !state.foundersPools.data.find(item => item.id === foundersPoolsItem.id)
  ) {
    return [...state.foundersPools.data, { ...foundersPoolsItem }];
  }

  return state.foundersPools.data.filter(
    item =>
      item.id === foundersPoolsItem.id
        ? { ...item, ...foundersPoolsItem }
        : item
  );
};

const updatePagination = (
  state: FoundersPoolsState,
  itemCount: number
): DataList<any> => {
  const totalResults = state.foundersPools.total_results + itemCount;
  const lastPage = Math.ceil(
    totalResults / state.foundersPools.results_per_page
  );
  const pageNumber =
    state.foundersPools.page_number > lastPage
      ? lastPage
      : state.foundersPools.page_number;

  return {
    ...state.foundersPools,
    last_page: lastPage,
    page_number: pageNumber,
    total_results: totalResults
  };
};
