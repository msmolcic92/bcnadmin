import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { BaseForm } from '../shared/base-form';
import { AppActions } from '../shared/store/app-actions.service';
import * as actions from './store/founders-pools.actions';

export class FoundersPoolsItemFormData {
  id?: number;
  date_made: number;
  btc_mined: number;
  paid?: number;
}

const FORM_VALIDATION_MESSAGES = {
  date_made: {
    required: 'Date made is required.'
  },
  btc_mined: {
    required: 'BTC mined is required.'
  }
};

@Component({
  selector: 'founders-pools-form',
  templateUrl: 'founders-pools-form.component.html'
})
export class FoundersPoolsFormComponent extends BaseForm implements OnInit {
  @Input() formData: any;
  @Input() isEdit: boolean;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  buildForm(): void {
    if (!this.formData) {
      this.formData = new FoundersPoolsItemFormData();
    }

    this.form = this.formBuilder.group({
      date_made: [this.formData.date_made, Validators.required],
      btc_mined: [+this.formData.btc_mined, Validators.required]
    });
  }

  getPoolAndPayInfo() {
    const btcMined = this.form.value ? this.form.value.btc_mined : 0;
    const founder1_pool = btcMined * 0.01;
    const founder2_pool = btcMined * 0.005;

    return {
      founder1_pool,
      founder2_pool,
      founder1_pay: founder1_pool / 500,
      founder2_pay: founder2_pool / 500
    };
  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? actions.UPDATE_SUCCESS
      : actions.CREATE_SUCCESS;

    const failureType = this.isEdit
      ? actions.UPDATE_FAILURE
      : actions.CREATE_FAILURE;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(failureType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload);
      });
  }

  submit(): void {
    this.clearFormErrors();
    const actionType = this.isEdit ? actions.UPDATE : actions.CREATE;

    const data = { ...this.form.value, paid: this.formData.paid ? 1 : 0 };

    this.store.dispatch({
      type: actionType,
      payload: {
        id: this.formData.id,
        data
      }
    });
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
