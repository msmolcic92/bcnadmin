import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { DateFromUnixPipe } from 'app/shared/pipes';
import { SharedModule } from 'app/shared/shared.module';
import {
  ActivatedRoute,
  ActivatedRouteStub,
  Router,
  RouterLinkStubDirective,
  RouterStub
} from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';
import { FoundersPoolsScreenComponent } from './founders-pools-screen.component';

describe('FoundersPoolsScreenComponent', () => {
  const TEST_POOLS = [
    {
      id: 1,
      date_made: 1239842398,
      btc_mined: 0.001,
      founder1_pool: 0.0001,
      founder2_pool: 0.0001,
      founder1_pay: 0.0001,
      founder2_pay: 0.0001,
      paid: 0
    },
    {
      id: 2,
      date_made: 1239842398,
      btc_mined: 0.002,
      founder1_pool: 0.0002,
      founder2_pool: 0.0002,
      founder1_pay: 0.0002,
      founder2_pay: 0.0002,
      paid: 0
    },
    {
      id: 3,
      date_made: 1239842398,
      btc_mined: 0.003,
      founder1_pool: 0.0003,
      founder2_pool: 0.0003,
      founder1_pay: 0.0003,
      founder2_pay: 0.0003,
      paid: 0
    }
  ];

  let comp: FoundersPoolsScreenComponent;
  let fixture: ComponentFixture<FoundersPoolsScreenComponent>;
  let linkDebugElements: DebugElement[];
  let linkDirectiveInstances: RouterLinkStubDirective[];
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      foundersPools: {
        foundersPools: {
          data: TEST_POOLS
        },
        filterData: {}
      }
    });

    store = new StoreMock();
    store.mockState('foundersPools', {
      foundersPools: {
        data: TEST_POOLS
      },
      filterData: {}
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [FoundersPoolsScreenComponent, RouterLinkStubDirective],
        providers: [
          { provide: Router, useClass: RouterStub },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundersPoolsScreenComponent);
    comp = fixture.componentInstance;
    comp.foundersPools = TEST_POOLS;
    fixture.detectChanges();
    linkDebugElements = fixture.debugElement.queryAll(
      By.directive(RouterLinkStubDirective)
    );

    linkDirectiveInstances = linkDebugElements.map(de =>
      de.injector.get(RouterLinkStubDirective)
    );
  });

  it('should be created', () => {
    expect(comp).toBeTruthy();
  });

  it('should show a spinner if input `isLoading` param is truthy', () => {
    comp.isTableLoading = true;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeTruthy(`<app-spinner> isn't rendered`);
  });

  it('should not show a spinner if input `isLoading` param is falsy', () => {
    comp.isTableLoading = false;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeNull(`<app-spinner> is rendered but shouldn't be`);
  });

  it('should render the passed founders pools list rows', () => {
    const memberEls = fixture.debugElement.queryAll(
      By.css('.founders-pools-list-item')
    );
    expect(memberEls.length).toBe(TEST_POOLS.length);
  });

  it('should render each founders pool information in the table rows', () => {
    // Note: only checking first one
    const memberEl = fixture.debugElement.query(
      By.css('.founders-pools-list-item')
    );
    const memberElTextContent = memberEl.nativeElement.textContent;
    const expectedMember = TEST_POOLS[0];
    expect(memberElTextContent).toContain(expectedMember.id);
    expect(memberElTextContent).toContain(expectedMember.btc_mined);
    expect(memberElTextContent).toContain(
      new DateFromUnixPipe().transform(expectedMember.date_made)
    );
    expect(memberElTextContent).toContain(expectedMember.founder1_pay);
    expect(memberElTextContent).toContain(expectedMember.founder1_pool);
    expect(memberElTextContent).toContain(expectedMember.founder2_pay);
    expect(memberElTextContent).toContain(expectedMember.founder2_pool);
    expect(memberElTextContent).toContain(expectedMember.paid ? 'Yes' : 'No');

  });
});
