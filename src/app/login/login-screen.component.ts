import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import * as actions from '../account/store/account.actions';

@Component({
  selector: 'login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.scss']
})
export class LoginScreenComponent implements OnInit {
  redirectUrl: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.redirectUrl = (params && params.redirectUrl) || null;
    });
  }

  handleLoginSuccess($event) {
    this.store.dispatch({ type: actions.ACCOUNT_FETCH });

    // Wait for the isAuthenticated to be true,
    // then redirect
    this.store
      .select('session')
      .filter((sessionData: any) => sessionData.isAuthenticated)
      .take(1)
      .subscribe((sessionData: any) => {
        this.router.navigate([this.redirectUrl || '/dashboard']);
      });
  }
}
