import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { SessionState } from '../core/store/session.state';
import * as actions from '../core/store/session.actions';
import { ModalDirective } from 'ngx-bootstrap';

const FORM_VALIDATION_MESSAGES = {
  _form: {
    incorrect: 'The username and password are not valid'
  },
  username: {
    required: 'Please enter your username'
  },
  password: {
    required: 'Please enter your password'
  }
};

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  @Output() loginSuccess: EventEmitter<any> = new EventEmitter();
  @ViewChild('tfaModal') tfaModal: ModalDirective;

  form: FormGroup;
  isSubmitted = false;

  formData = {
    username: '',
    password: ''
  };

  formErrors = {
    _form: [],
    username: [],
    password: []
  };

  code = new FormControl();
  private sessionSubscription: any;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<SessionState>
  ) {}

  ngOnInit() {
    this.buildForm();

    this.sessionSubscription = this.store
      .select((state: any) => state.session)
      .subscribe((sessionState: any) => {
        if (sessionState.isAuthenticated) {
          return this.loginSuccess.emit(sessionState.tokenData);
        }

        if (sessionState.loginError) {
          if (sessionState.loginError === 'Second authentication step failed') {
            this.tfaModal.show();
          } else {
            this.onSubmitError(sessionState.loginError);
          }
        }
      });
  }

  ngOnDestroy() {
    this.sessionSubscription.unsubscribe();
  }

  onVerificationCodeConfirmed(): void {
    const credentials = this.form.value;
    credentials.code = this.code.value;

    this.store.dispatch({
      type: actions.SESSION_LOGIN,
      payload: credentials
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    this.clearFormError();

    const credentials = this.form.value;
    this.store.dispatch({
      type: actions.SESSION_LOGIN,
      payload: credentials
    });
  }

  onSubmitError(errorMessage) {
    this.clearFormError();

    if (errorMessage) {
      this.formErrors._form.push(errorMessage);
    }
  }

  onValueChanged(data?: any) {
    if (!this.form) {
      return;
    }
    const { form } = this;

    for (const field in this.formErrors) {
      if (field !== '_form') {
        this.formErrors[field] = [];
      }

      const control = form.get(field);
      if (!control || !control.dirty || control.valid) {
        continue;
      }

      const errorMessages = FORM_VALIDATION_MESSAGES[field];
      // tslint:disable-next-line:forin
      for (const errorKey in control.errors) {
        this.formErrors[field].push(errorMessages[errorKey]);
      }
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      username: [this.formData.username, Validators.required],
      password: [this.formData.password, Validators.required]
    });

    this.form.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  clearFormError() {
    if (
      this.formErrors &&
      this.formErrors._form &&
      this.formErrors._form.length
    ) {
      this.formErrors._form = [];
    }
  }
}
