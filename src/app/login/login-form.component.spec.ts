import { FormGroup } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { AppModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';
import * as actions from 'app/core/store/session.actions';
import { StoreMock } from 'testing/store-helpers';
import { LoginModule } from './login.module';
import { LoginFormComponent } from './login-form.component';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  let store: StoreMock<any>;
  const formDataValid = {
    username: 'test',
    password: 'test123'
  };
  const formDataInvalid = {
    username: '',
    password: ''
  };

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('foundersPools', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, LoginModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function updateForm(formData: any) {
    component.form.setValue({
      username: formData.username,
      password: formData.password
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form object on component instantiation', () => {
    spyOn(component, 'buildForm');
    component.ngOnInit();
    expect(component.buildForm).toHaveBeenCalled();
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should dispatch with the SESSION_LOGIN action when submitting the form', () => {
    let dispatchecAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchecAction = action)
    );
    component.onSubmit();
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchecAction.type).toEqual(actions.SESSION_LOGIN);
  });

  it('form value should update from form changes and still be valid', () => {
    updateForm(formDataValid);
    expect(component.form.value).toEqual(formDataValid);
    expect(component.form.valid).toBeTruthy();
  });

  it('form.valid should be false when data is invalid', fakeAsync(() => {
    updateForm(formDataInvalid);
    expect(component.form.valid).toBeFalsy();
  }));

  it('should call onValueChanged when the value of a form control changes', () => {
    spyOn(component, 'onValueChanged');
    updateForm(formDataValid);
    expect(component.onValueChanged).toHaveBeenCalled();
  });
});
