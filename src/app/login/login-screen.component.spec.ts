import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub
} from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';
import * as actions from '../account/store/account.actions';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { LoginModule } from './login.module';
import { LoginScreenComponent } from './login-screen.component';


describe('LoginScreenComponent', () => {
  let component: LoginScreenComponent;
  let fixture: ComponentFixture<LoginScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;
  let router: RouterStub;

  beforeEach(() => {
    router = new RouterStub();
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.queryParamsSubject.next({
      redirectUrl: '/test'
    });

    store = new StoreMock();
    store.mockState('session', {
      sessionData: {
        data: [1, 2, 3]
      },
      isAuthenticated: true,
      filterData: {}
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule, LoginModule],
        providers: [
          { provide: Router, useValue: router },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the redirect url on component initialization', () => {
    component.ngOnInit();
    expect(component.redirectUrl).toEqual('/test');
  });

  it('should dispatch with ACCOUNT_FETCH action when handling login success', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    component.handleLoginSuccess({});
    expect(store.dispatch).toHaveBeenCalled();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(actions.ACCOUNT_FETCH);
  });

  it('should navigate to redirectUrl when handling login success', () => {
    let redirectUrl;
    spyOn(router, 'navigate').and.callFake(
      url => (redirectUrl = url)
    );
    component.handleLoginSuccess({});
    expect(router.navigate).toHaveBeenCalled();
    expect(redirectUrl).toEqual(['/test']);
  });
});
