import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SessionService } from '../../api-client/session.service';
import { LoginRoutingModule } from './login-routing.module';
import { LoginFormComponent } from './login-form.component';
import { LoginScreenComponent } from './login-screen.component';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    ModalModule
  ],
  declarations: [
    LoginFormComponent,
    LoginScreenComponent,
  ]
})
export class LoginModule { }
