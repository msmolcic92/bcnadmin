import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig } from '../core/store/helpers';
import {
  UnsubscribeListState,
  Unsubscriber,
  UnsubscribeList,
  UnsubscribeListFilterData
} from './store/unsubscribe-list.state';
import * as actions from './store/unsubscribe-list.actions';

@Component({
  selector: 'unsubscribe-list-screen',
  templateUrl: './unsubscribe-list-screen.component.html'
})
export class UnsubscribeListScreenComponent implements OnDestroy, OnInit {
  private subscription: any;
  unsubscribeList: UnsubscribeList;
  filterShown: boolean = false;
  filterData: UnsubscribeListFilterData = new UnsubscribeListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.unsubscribeList = routeData.unsubscribeList.data;
      this.filterData = routeData.unsubscribeList.filterData;

      this.subscription = this.store
        .select('unsubscribeList')
        .subscribe((state: UnsubscribeListState) => {
          this.isTableLoading = state.isFetching;

          if (state.isFetchSuccess || state.isDeleteSuccess) {
            this.store.dispatch({ type: actions.ACTION_STATE_RESET });
            this.unsubscribeList = state.data;
            this.filterData = state.filterData;

            // TODO: Reduce the pagination-related boilerplate
            this.pagination = {
              itemPerPage: state.data.results_per_page,
              currentPage: state.data.page_number,
              totalItems: state.data.total_results
            };
          }
        });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: UnsubscribeListFilterData = new UnsubscribeListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }

  handleItemDeleteClick(itemId) {
    this.store.dispatch({
      type: actions.DELETE,
      payload: itemId
    });
  }
}
