/* global confirm */
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'unsubscribe-list',
  templateUrl: './unsubscribe-list.component.html'
})
export class UnsubscribeListComponent {
  @Input() items: Array<any> = [];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  handleItemDeleteClick(itemId: number) {
    // TODO: Will be using reliable confirmation soon (non-PrimeNG)
    if (confirm('You\'re going to permanently delete this item. Are you sure?')) {
      this.itemDeleteClick.emit(itemId);
    }
  }
}
