import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { UnsubscribeListService } from '../../api-client/unsubscribe-list.service';
import { UnsubscribeListState } from './store/unsubscribe-list.state';
import * as actions from './store/unsubscribe-list.actions';

@Injectable()
export class UnsubscribeListResolver {
  constructor(
    private unsubscribeListService: UnsubscribeListService,
    private router: Router,
    private store: Store<any>
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('unsubscribeList');

    state$.take(1).subscribe((state: UnsubscribeListState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter(
        (state: UnsubscribeListState) =>
          state.isFetchSuccess || state.isFetchFailure
      )
      .take(1);
  }
}
