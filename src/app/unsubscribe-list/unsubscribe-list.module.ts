import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarModule,
  DataTableModule,
  SharedModule as PrimeSharedModule,
  InputTextModule,
  TooltipModule,
  ConfirmDialogModule,
  ConfirmationService
} from 'primeng/primeng';

import { SharedModule } from '../shared/shared.module';

import { UnsubscribeListRoutingModule } from './unsubscribe-list-routing.module';
import { UnsubscribeListService } from '../../api-client/unsubscribe-list.service';
import { UnsubscribeListResolver } from './unsubscribe-list-resolver.service';
import { UnsubscribeListEffects } from './store/unsubscribe-list.effects';
import { UnsubscribeListRootComponent } from './unsubscribe-list-root.component';
import { UnsubscribeListComponent } from './unsubscribe-list.component';
import { UnsubscribeListFilterComponent } from './unsubscribe-list-filter.component';
import { UnsubscribeListScreenComponent } from './unsubscribe-list-screen.component';
import { UnsubscriberAddScreenComponent } from './unsubscriber-add-screen.component';
import { UnsubscriberFormComponent } from './unsubscriber-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarModule,
    InputTextModule,
    DataTableModule,
    PrimeSharedModule,
    TooltipModule,
    ConfirmDialogModule,
    NgbModule,
    SharedModule,
    UnsubscribeListRoutingModule,
    EffectsModule.forFeature([
      UnsubscribeListEffects
    ])
  ],
  declarations: [
    UnsubscribeListRootComponent,
    UnsubscribeListComponent,
    UnsubscribeListFilterComponent,
    UnsubscribeListScreenComponent,
    UnsubscriberAddScreenComponent,
    UnsubscriberFormComponent
  ],
  providers: [
    ConfirmationService,
    UnsubscribeListService,
    UnsubscribeListResolver
  ]
})
export class UnsubscribeListModule { }
