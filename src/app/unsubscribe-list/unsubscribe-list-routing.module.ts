import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnsubscribeListRootComponent } from './unsubscribe-list-root.component';
import { UnsubscribeListScreenComponent } from './unsubscribe-list-screen.component';
import { UnsubscribeListResolver } from './unsubscribe-list-resolver.service';
import { UnsubscriberAddScreenComponent } from './unsubscriber-add-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Unsubscribe List'
    },
    component: UnsubscribeListRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: UnsubscribeListScreenComponent,
        resolve: {
          unsubscribeList: UnsubscribeListResolver
        }
      },
      {
        path: 'add',
        component: UnsubscriberAddScreenComponent,
        data: {
          title: 'Add Unsubscriber'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnsubscribeListRoutingModule { }
