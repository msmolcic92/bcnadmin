import { Action } from '@ngrx/store';
import * as actions from './unsubscribe-list.actions';
import { SubmitError } from '../../shared/base-form';

// Feature types

export class Unsubscriber {
  id: number;
  email: string;
  date_made: Date;
  ip: string;
}

export class UnsubscribeList {
  data: Unsubscriber[];
  count?: number; // OBSOLETE
  page_number?: number;
  results_per_page?: number;
  total_results?: number;
}

export class UnsubscribeListFilterData {
  page_number?: number = 1;
  results_per_page?: number = 25;
  date_from_filter?: number;
  date_to_filter?: number;
  email_filter?: string;
  ip_filter?: string;
  order_by?: string;
  order?: string;
}

export interface UnsubscribeListState {
  data: UnsubscribeList;
  filterData?: UnsubscribeListFilterData;
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchFailure: boolean;
  isCreating: boolean;
  isCreateSuccess: boolean;
  isCreateFailure: boolean;
  isDeleting: boolean;
  isDeleteSuccess: boolean;
  isDeleteFailure: boolean;
  // TEMP (until a different unified way of handling forms state has emerged)
  formError: SubmitError;
}

export const INITIAL_STATE: UnsubscribeListState = {
  data: null,
  filterData: new UnsubscribeListFilterData(),
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false,
  isCreating: false,
  isCreateSuccess: false,
  isCreateFailure: false,
  isDeleting: false,
  isDeleteSuccess: false,
  isDeleteFailure: false,
  formError: null
};

// Reducers

export function unsubscribeListReducer(
  state: UnsubscribeListState = INITIAL_STATE,
  action: Action
): UnsubscribeListState {
  switch (action.type) {
    case actions.FETCH_LIST:
      return {
        ...state,
        isFetching: true
      };

    case actions.FETCH_LIST_SUCCESS:
      return {
        ...state,
        data: { ...state.data, ...(<any>action).payload.data },
        filterData: {
          ...state.filterData,
          ...(<any>action).payload.filterData
        },
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_FAILURE:
      return {
        ...state,
        data: null,
        filterData: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.DELETE:
      return {
        ...state,
        isDeleting: true
      };

    case actions.RESET_LIST_FILTER:
      return {
        ...state,
        filterData: null
      };

    case actions.DELETE_SUCCESS:
      return {
        ...state,
        data: {
          count: state.data.count - 1,
          data: state.data.data.filter(
            unsubscriber => unsubscriber.id !== (<any>action).payload
          )
        },
        isDeleting: false,
        isDeleteSuccess: true,
        isDeleteFailure: false
      };

    case actions.DELETE_FAILURE:
      return {
        ...state,
        isDeleting: false,
        isDeleteSuccess: false,
        isDeleteFailure: true
      };

    case actions.CREATE:
      return {
        ...state,
        isCreating: true
      };

    case actions.CREATE_SUCCESS:
      return {
        ...state,
        // TODO: Freshly added item is missing IP address and Date Made fields.
        // data: {
        //   count: state.data.count + 1,
        //   data: [ ...state.data.data, (<any>action).payload ]
        // },
        isCreating: false,
        isCreateSuccess: true,
        isCreateFailure: false
      };

    case actions.CREATE_FAILURE:
      return {
        ...state,
        isCreating: false,
        isCreateSuccess: false,
        isCreateFailure: true,
        formError: { ...(<any>action).payload }
      };

    case actions.ACTION_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false,
        isCreating: false,
        isCreateSuccess: false,
        isCreateFailure: false,
        isDeleting: false,
        isDeleteSuccess: false,
        isDeleteFailure: false,
        formError: null
      };

    default:
      return state;
  }
}
