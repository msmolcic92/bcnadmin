// Action types

export const FETCH_LIST = 'unsubscribeList/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'unsubscribeList/FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'unsubscribeList/FETCH_LIST_FAILURE';
export const RESET_LIST_FILTER = 'unsubscribeList/RESET_LIST_FILTER';

export const FETCH = 'unsubscribeList/FETCH';
export const FETCH_SUCCESS = 'unsubscribeList/FETCH_SUCCESS';
export const FETCH_FAILURE = 'unsubscribeList/FETCH_FAILURE';

export const DELETE = 'unsubscribeList/DELETE';
export const DELETE_SUCCESS = 'unsubscribeList/DELETE_SUCCESS';
export const DELETE_FAILURE = 'unsubscribeList/DELETE_FAILURE';

export const CREATE = 'unsubscribeList/CREATE';
export const CREATE_SUCCESS = 'unsubscribeList/CREATE_SUCCESS';
export const CREATE_FAILURE = 'unsubscribeList/CREATE_FAILURE';

export const ACTION_STATE_RESET = 'unsubscribeList/ACTION_STATE_RESET';
