import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UnsubscribeListService } from 'api-client/unsubscribe-list.service';
import { UnsubscriberFormData } from '../unsubscriber-form.component';
import { UnsubscribeListFilterData } from './unsubscribe-list.state';
import * as actions from './unsubscribe-list.actions';

@Injectable()
export class UnsubscribeListEffects {

  constructor(
    private action$: Actions,
    private unsubscribeListService: UnsubscribeListService
  ) { }

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: UnsubscribeListFilterData) => {
      return this.unsubscribeListService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.FETCH_LIST_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  create$: Observable<Action> = this.action$
    .ofType(actions.CREATE)
    .map(toPayload)
    .switchMap((unsubscriberData: UnsubscriberFormData) => {
      return this.unsubscribeListService
        .create(unsubscriberData)
        .map((response: any) => ({
          type: actions.CREATE_SUCCESS
        }))
        .catch(errorResponse => Observable.of({
          type: actions.CREATE_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  delete$: Observable<Action> = this.action$
    .ofType(actions.DELETE)
    .map(toPayload)
    .switchMap((unsubscriberId: number) => {
      return this.unsubscribeListService
        .delete(unsubscriberId)
        .map((response: any) => ({
          type: actions.DELETE_SUCCESS,
          payload: unsubscriberId
        }))
        .catch(errorResponse => Observable.of({
          type: actions.DELETE_FAILURE,
          payload: errorResponse.error
        }));
    });
}
