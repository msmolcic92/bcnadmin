import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { UnsubscribeListFilterData } from './store/unsubscribe-list.state';
import DateUtility from '../shared/helpers/date-utility';

@Component({
  selector: 'unsubscribe-list-filter',
  templateUrl: 'unsubscribe-list-filter.component.html'
})
export class UnsubscribeListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: UnsubscribeListFilterData;
  @Output()
  valueChange: EventEmitter<UnsubscribeListFilterData> = new EventEmitter<
    UnsubscribeListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      email_filter: [this.value.email_filter],
      ip_filter: [this.value.ip_filter],
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    this.valueChange.emit(formData);
  }
}
