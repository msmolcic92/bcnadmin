import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'unsubscriber-add-screen',
  templateUrl: './unsubscriber-add-screen.component.html'
})
export class UnsubscriberAddScreenComponent {

  constructor(
    private router: Router
  ) { }

  handleFormSuccess(response): void {
    // TODO: Notify success.
    this.router.navigate(['unsubscribe-list']);
  }

  handleFormCancel(): void {
    this.router.navigate(['unsubscribe-list']);
  }

}
