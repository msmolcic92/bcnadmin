import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { BaseForm } from '../shared/base-form';
import { UnsubscribeListState } from './store/unsubscribe-list.state';
import * as actions from './store/unsubscribe-list.actions';

const FORM_VALIDATION_MESSAGES = {
  email: {
    required: 'Email address is required.',
    email: 'Invalid email address.'
  }
};

export class UnsubscriberFormData {
  email?: string;
}

@Component({
  selector: 'unsubscriber-form',
  templateUrl: './unsubscriber-form.component.html'
})
export class UnsubscriberFormComponent extends BaseForm implements OnInit {
  @Input() formData: UnsubscriberFormData;

  constructor(private formBuilder: FormBuilder, private store: Store<any>) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToStore();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new UnsubscriberFormData();
    }

    this.form = this.formBuilder.group({
      email: [
        this.formData.email,
        Validators.compose([Validators.required, Validators.email])
      ]
    });
  }

  subscribeToStore(): void {
    this.store
      .select('unsubscribeList')
      .takeWhile(() => this.isAlive)
      .subscribe((unsubscribeListState: UnsubscribeListState) => {
        if (unsubscribeListState.isCreateSuccess) {
          this.store.dispatch({ type: actions.ACTION_STATE_RESET });
          return this.formSuccess.next(unsubscribeListState.data);
        }

        if (unsubscribeListState.formError) {
          this.onSubmitError(unsubscribeListState.formError);
        }
      });
  }

  submit(): void {
    this.store.dispatch({
      type: actions.CREATE,
      payload: {
        ...this.form.value
      }
    });
  }

  cancel(): void {
    this.store.dispatch({ type: actions.ACTION_STATE_RESET });
    this.formCancel.next(true);
  }
}
