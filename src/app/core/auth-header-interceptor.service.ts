import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { TokenStorageService } from '../auth/token-storage.service';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {
  constructor(
    private tokenStorageService: TokenStorageService,
    private store: Store<any>
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    req = req.clone({
      headers: req.headers.set('Content-Type', 'application/json')
    });

    const sessionData$ = this.store.select('session');

    sessionData$
      .filter((sessionData: any) => sessionData.isAuthenticated)
      .distinctUntilChanged()
      .map((sessionData: any) => sessionData.tokenData)
      .subscribe((tokenData: any) => {
        if (tokenData.accessToken) {
          req = req.clone({
            headers: req.headers.set('Authorization', `ABearer ${tokenData.accessToken}`)
          });
        }
      });

    // Delete `Authorization` header completely once the user becomes non-authenticated
    sessionData$
      .filter((sessionData: any) => !sessionData.isAuthenticated)
      .distinctUntilChanged()
      .subscribe(() => {
        req = req.clone({
          headers: req.headers.delete('Authorization')
        });
      });

    return next.handle(req);
  }
}

export const authHeaderInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthHeaderInterceptor,
  multi: true
};
