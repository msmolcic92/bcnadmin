import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HTTP_INTERCEPTORS,
  HttpResponse
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import * as actions from './store/session.actions';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Note: We need to pass "authentication request" (e.g., any request
    // in order to obtain a token) 401 error further to the caller.
    const isAuthenticationRequest =
      req.url && req.url.indexOf('access_token') !== -1;

    return next.handle(req).catch((error: HttpResponse<any>) => {
      if (error.status === 401 && !isAuthenticationRequest) {
        const success$ = this.appAction$
          .ofType(actions.SESSION_REFRESH_TOKEN_SUCCESS)
          .switchMap(() => {
            // Replay the request once we're re-authenticated
            console.log(
              'Successfully re-authenticated. Retrying the previously failed request.'
            );

            return this.store
              .select('session')
              .map((sessionData: any) => sessionData.tokenData)
              .switchMap((tokenData: any) => {
                return next.handle(
                  req.clone({
                    setHeaders: {
                      Authorization: `ABearer ${tokenData.accessToken}`
                    }
                  })
                );
              });
          });

        const failure$ = this.appAction$
          .ofType(actions.SESSION_REFRESH_TOKEN_FAILURE)
          .switchMap(() => Observable.throw(error));

        this.store
          .select('session')
          .filter((sessionData: any) => {
            return (
              sessionData.isAuthenticated &&
              !sessionData.isAuthenticating &&
              !sessionData.isAuthenticatingWithRefreshToken
            );
          })
          .take(1)
          .subscribe((sessionData: any) => {
            console.log('The authentication session has expired.');
            this.store.dispatch({ type: actions.SESSION_EXPIRED });
          });

        return Observable.merge(success$, failure$);
      }

      if (error.status === 403) {
        const isUpdateOrDeleteAction =
          ['POST', 'PUT', 'PATCH', 'DELETE'].indexOf(req.method) !== -1;

        if (isUpdateOrDeleteAction) {
          this.toasterService.pop(
            'error',
            'Access Restricted!',
            "You don't have permissions to perform this action"
          );
        } else {
          this.router.navigate(['']);
        }
      }

      if (error.status === 404 || error.status === 500) {
        this.router.navigate([`${error.status}`]);
      }

      return Observable.throw(error);
    });
  }
}

export const httpErrorInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: HttpErrorInterceptor,
  multi: true
};
