import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AccountState } from '../account/store/account.state';
import { ACCOUNT_FETCH } from '../account/store/account.actions';

@Component({
  selector: 'app-header-account-block',
  templateUrl: './app-header-account-block.component.html'
})
export class AppHeaderAccountBlockComponent implements OnInit, OnDestroy {

  private storeSubscription: any;
  accountData?: any;
  isFetching: boolean = false;

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.store.dispatch({ type: ACCOUNT_FETCH });
    this.storeSubscription = this.store
      .select('account')
      .subscribe((accountState: AccountState) => {
        this.accountData = accountState.data;
        this.isFetching = accountState.isFetching;
      });
  }

  ngOnDestroy() {
    this.storeSubscription.unsubscribe();
  }
}
