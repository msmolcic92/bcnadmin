import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SESSION_LOGOUT } from './store/session.actions';

@Component({
  template: ''
})
export class LogoutComponent implements OnInit {
  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.store.dispatch({ type: SESSION_LOGOUT });
  }
}
