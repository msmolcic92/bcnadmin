import { Action } from '@ngrx/store';
import { Actions, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

export interface DefaultState {
  requesting: boolean,
  hasError: boolean,
  error?: any
}

export const defaultStateValues = {
  requesting: false,
  hasError: false,
  error: null,
};

export interface DataList<T> {
  data: T[],
  last_page: number,
  page_number: number,
  results_per_page: number,
  total_results: number,
}

export interface PaginationConfig {
  currentPage: number,
  itemPerPage: number,
  totalItems: number,
}
export const AutoStates = {
  new: (): DefaultState => {
    return {
      requesting: true,
      hasError: false,
    };
  },
  success: (): DefaultState => {
    return {
      requesting: false,
      hasError: false,
    };
  },
  hasError: (error): DefaultState => {
    return {
      requesting: false,
      hasError: true,
      error,
    };
  }
};

export function makeDefaultHandlers(START_REQUEST: string, END_REQUEST: string, ON_ERROR: string): any {
  return {
    [START_REQUEST]: (state: any) => {
      return { ...state, ...AutoStates.new() };
    },
    [END_REQUEST]: (state: any) => {
      return { ...state, requesting: false };
    },
    [ON_ERROR]: (state: any, action: Action) => {
      return { ...state, ...AutoStates.hasError((<any>action).payload) };
    }
  };
}

// Effects
export function makeSimpleEffect(action$: Actions,
                                 method: Function,
                                 actionType: string,
                                 successType: string,
                                 errorType: string,
                                 onSuccess: Function = () => {},
                                 onFailure: Function = () => {}): Observable<Action> {
  return action$.ofType(actionType)
    .map(toPayload)
    .switchMap((payload) => {
      return method(payload)
        .do((res) => onSuccess(res, payload), (res) => onFailure(res, payload))
        .map((res: any) => ({
          type: successType,
          payload: res
        }))
        .catch(errRes => Observable.of({
          type: errorType,
          payload: errRes.error
        }))
    });

}
