import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { TokenStorageService } from '../../auth/token-storage.service';
import { SessionService } from 'api-client/session.service';
import { TfaService } from 'api-client/tfa.service';
import { TfaData } from './session.state';
import * as actions from './session.actions';

@Injectable()
export class SessionEffects {
  constructor(
    private action$: Actions,
    private sessionService: SessionService,
    private tfaService: TfaService,
    private tokenStorageService: TokenStorageService,
    private router: Router
  ) {}

  @Effect()
  login$: Observable<Action> = this.action$
    .ofType(actions.SESSION_LOGIN)
    .map(toPayload)
    .switchMap(credentials => {
      return this.sessionService
        .login(credentials)
        .map((response: any) => {
          return {
            type: actions.SESSION_LOGIN_SUCCESS,
            payload: {
              tokenType: response.token_type,
              accessToken: response.access_token,
              accessTokenExpiresAt: null, // TODO: convert response.expires_in to a `Date`
              refreshToken: response.refresh_token
            }
          };
        })
        .do(action => {
          this.tokenStorageService.store(action.payload);
        })
        .catch(errorResponse => {
          const { error } = errorResponse.error;

          return Observable.of({
            type: actions.SESSION_LOGIN_FAILURE,
            payload: error
          });
        });
    });

  @Effect()
  sessionExpiration$: Observable<Action> = this.action$
    .ofType(actions.SESSION_EXPIRED)
    .map(() => {
      // TODO: Take this from state instead
      const { refreshToken } = this.tokenStorageService.retrieve();

      return {
        type: actions.SESSION_REFRESH_TOKEN,
        payload: refreshToken
      };
    });

  @Effect()
  loginWithRefreshToken$: Observable<Action> = this.action$
    .ofType(actions.SESSION_REFRESH_TOKEN)
    .map(toPayload)
    .switchMap(refreshToken => {
      return this.sessionService
        .loginWithRefreshToken(refreshToken)
        .map((response: any) => ({
          type: actions.SESSION_REFRESH_TOKEN_SUCCESS,
          payload: {
            tokenType: response.token_type,
            accessToken: response.access_token,
            accessTokenExpiresAt: null, // TODO: convert response.expires_in to a `Date`
            refreshToken: response.refresh_token
          }
        }))
        .do(action => {
          this.tokenStorageService.store(action.payload);
        })
        .catch(errorResponse => {
          const { error } = errorResponse.error;

          return Observable.of({
            type: actions.SESSION_REFRESH_TOKEN_FAILURE,
            payload: error
          });
        });
    });

  @Effect()
  refreshTokenFailure$: Observable<Action> = this.action$
    .ofType(actions.SESSION_REFRESH_TOKEN_FAILURE)
    .mapTo({ type: actions.SESSION_LOGOUT_SUCCESS });

  @Effect()
  logout$: Observable<Action> = this.action$
    .ofType(actions.SESSION_LOGOUT)
    .mapTo({ type: actions.SESSION_LOGOUT_SUCCESS });

  @Effect({ dispatch: false })
  logoutSuccess$: Observable<Action> = this.action$
    .ofType(actions.SESSION_LOGOUT_SUCCESS)
    .do(() => {
      this.tokenStorageService.clear();
      this.router.navigate(['/login']);
    });

  @Effect()
  fetchTfa$: Observable<Action> = this.action$
    .ofType(actions.TFA_FETCH)
    .mapTo(toPayload)
    .switchMap(() => {
      return this.tfaService
        .fetch()
        .map((response: any) => ({
          type: actions.TFA_FETCH_SUCCESS,
          payload: { ...response }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.TFA_FETCH_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  initializeTfaSetup$: Observable<Action> = this.action$
    .ofType(actions.TFA_INITIALIZE_SETUP)
    .map(toPayload)
    .switchMap((tfaData: TfaData) => {
      return this.tfaService
        .post(tfaData)
        .map((response: any) => ({
          type: actions.TFA_INITIALIZE_SETUP_SUCCESS,
          payload: { ...tfaData, ...response }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.TFA_INITIALIZE_SETUP_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  enableTfa$: Observable<Action> = this.action$
    .ofType(actions.TFA_ENABLE)
    .map(toPayload)
    .switchMap((tfaData: TfaData) => {
      return this.tfaService
        .post(tfaData)
        .map((response: any) => ({
          type: actions.TFA_ENABLE_SUCCESS,
          payload: { ...response }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.TFA_ENABLE_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  removeTfa$: Observable<Action> = this.action$
    .ofType(actions.TFA_REMOVE)
    .map(toPayload)
    .switchMap(data => {
      return this.tfaService
        .remove(data)
        .map((response: any) => ({
          type: actions.TFA_REMOVE_SUCCESS
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.TFA_REMOVE_FAILURE,
            payload: errorResponse.error
          })
        );
    });
}
