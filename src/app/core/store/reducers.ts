import { combineReducers } from '@ngrx/store';

import { SessionState, sessionReducer } from './session.state';
import { AccountState, accountReducer } from '../../account/store/account.state';
import { UnsubscribeListState, unsubscribeListReducer } from '../../unsubscribe-list/store/unsubscribe-list.state';
import { foundersReducer } from '../../founders/store/founders.state';
import { volumesReducer } from '../../volumes/store/volumes.state';
import { foundersPoolsReducer } from 'app/founders-pools/store/founders-pools.state';


// Root state

export const reducers: any = {
  session: sessionReducer,
  account: accountReducer,
  unsubscribeList: unsubscribeListReducer,
  founders: foundersReducer,
  foundersPools: foundersPoolsReducer,
  volumes: volumesReducer
};
