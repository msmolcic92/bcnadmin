import { Action } from '@ngrx/store';
import * as actions from './session.actions';

// Model

export class TfaData {
  id: number;
  tfa_method: string;
  qr_url: string;
  key: string;
}

export interface SessionState {
  isAuthenticated: boolean;
  isAuthenticating?: boolean;
  isAuthenticatingWithRefreshToken?: boolean;
  tokenData?: {
    tokenType?: string;
    accessToken?: string;
    accessTokenExpiresAt?: Date;
    refreshToken?: string;
  },
  loginError?: string,
  tfaData?: TfaData,
  isFetchingTfa?: boolean,
  isTfaFetchSuccess?: boolean,
  isTfaFetchFailure?: boolean,
  isTfaInitializing?: boolean,
  isTfaInitializeSuccess?: boolean,
  isTfaInitializeFailure?: boolean,
  isEnablingTfa?: boolean,
  isTfaEnableSuccess?: boolean,
  isTfaEnableFailure?: boolean,
  isRemovingTfa?: boolean,
  isTfaRemoveSuccess?: boolean,
  isTfaRemoveFailure?: boolean
}

export const INITIAL_STATE: SessionState = {
  isAuthenticated: false
};

// Reducers

export function sessionReducer(state = INITIAL_STATE, action: Action): SessionState {
  switch (action.type) {
    case actions.SESSION_LOGIN:
      return {
        ...state,
        isAuthenticating: true
      };

    case actions.SESSION_REFRESH_TOKEN:
      return {
        ...state,
        isAuthenticating: true,
        isAuthenticatingWithRefreshToken: true
      };

    case actions.SESSION_LOGIN_SUCCESS:
    case actions.SESSION_REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        tokenData: { ...(<any>action).payload },
        isAuthenticating: false,
        isAuthenticatingWithRefreshToken: false,
        loginError: null
      };

    case actions.SESSION_LOGIN_FAILURE:
    case actions.SESSION_REFRESH_TOKEN_FAILURE:
      return {
        ...state,
        isAuthenticated: false,
        tokenData: null,
        isAuthenticating: false,
        isAuthenticatingWithRefreshToken: false,
        loginError: (<any>action).payload
      };

    case actions.SESSION_EXPIRED:
      return {
        ...state,
        isAuthenticated: false
      };

    case actions.SESSION_LOGOUT:
      return {
        isAuthenticated: false
      };

    case actions.TFA_FETCH:
      return {
        ...state,
        isFetchingTfa: true,
        isTfaFetchSuccess: false,
        isTfaFetchFailure: false
      };

    case actions.TFA_FETCH_SUCCESS:
      return {
        ...state,
        tfaData: { ...state.tfaData, ...(<any>action).payload },
        isFetchingTfa: false,
        isTfaFetchSuccess: true,
        isTfaFetchFailure: false
      };

    case actions.TFA_FETCH_FAILURE:
      return {
        ...state,
        isFetchingTfa: false,
        isTfaFetchSuccess: false,
        isTfaFetchFailure: true
      };

    case actions.TFA_INITIALIZE_SETUP:
      return {
        ...state,
        isTfaInitializing: true,
        isTfaInitializeSuccess: false,
        isTfaInitializeFailure: false
      };

    case actions.TFA_INITIALIZE_SETUP_SUCCESS:
      return {
        ...state,
        tfaData: { ...(<any>action).payload },
        isTfaInitializing: false,
        isTfaInitializeSuccess: true,
        isTfaInitializeFailure: false
      };

    case actions.TFA_INITIALIZE_SETUP_FAILURE:
      return {
        ...state,
        isTfaInitializing: false,
        isTfaInitializeSuccess: false,
        isTfaInitializeFailure: true
      };

    case actions.TFA_ENABLE:
      return {
        ...state,
        isEnablingTfa: true,
        isTfaEnableSuccess: false,
        isTfaEnableFailure: false
      };

    case actions.TFA_ENABLE_SUCCESS:
      return {
        ...state,
        tfaData: {
          id: (<any>action).payload.id,
          qr_url: null,
          key: null,
          tfa_method: state.tfaData.tfa_method
        },
        isEnablingTfa: false,
        isTfaEnableSuccess: true,
        isTfaEnableFailure: false
      };

    case actions.TFA_ENABLE_FAILURE:
      return {
        ...state,
        isEnablingTfa: false,
        isTfaEnableSuccess: false,
        isTfaEnableFailure: true
      };

    case actions.TFA_REMOVE:
      return {
        ...state,
        isRemovingTfa: true,
        isTfaRemoveSuccess: false,
        isTfaRemoveFailure: false
      };

    case actions.TFA_REMOVE_SUCCESS:
      return {
        ...state,
        tfaData: new TfaData(),
        isRemovingTfa: false,
        isTfaRemoveSuccess: true,
        isTfaRemoveFailure: false
      };

    case actions.TFA_REMOVE_FAILURE:
      return {
        ...state,
        isRemovingTfa: false,
        isTfaRemoveSuccess: false,
        isTfaRemoveFailure: true
      };

    case actions.TFA_REMOVE_STATE_RESET:
      return {
        ...state,
        isRemovingTfa: false,
        isTfaRemoveSuccess: false,
        isTfaRemoveFailure: false
      };

    default:
      return state;
  }
}

// Selectors

// [TODO]
