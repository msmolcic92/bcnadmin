import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AccountState } from '../../account/store/account.state';
import * as navbarActions from '../../shared/store/navbar.actions';

@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './app-sidebar-nav.component.html'
})
export class AppSidebarNavComponent implements OnDestroy, OnInit {
  menuTemplateName? = '';
  isAlive = true;
  isFetching = false;

  constructor(private elementRef: ElementRef, private store: Store<any>) {}

  ngOnInit() {
    // Note: We just know that the account data
    // is fetched by the system, so we don't need
    // to trigger it from here once again

    this.store
      .select('account')
      .takeWhile(() => this.isAlive)
      .subscribe((accountState: AccountState) => {
        const adminGroup =
          (accountState.data &&
            accountState.data.groups &&
            accountState.data.groups[0]) ||
          {};

        this.menuTemplateName =
          (adminGroup.group_template_data &&
            adminGroup.group_template_data.menu_template_name) ||
          '';
        this.isFetching = accountState.isFetching;
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  isActiveMenu(menuTemplateName = '') {
    return this.menuTemplateName === menuTemplateName;
  }

  @HostListener('document: click', ['$event'])
  handleDocumentClick(event) {
    // Close the navigation menu if user clicks somewhere outside the
    // navigation menu and outside the hamburger menu icon.
    if (
      !this.elementRef.nativeElement.contains(event.target) &&
      !event.target.classList.contains('mobile-sidebar-toggler')
    ) {
      this.store.dispatch({ type: navbarActions.CLOSE });
    }
  }
}
