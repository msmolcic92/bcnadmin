import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberListResolver } from './list/member-list-resolver.service';
import { MemberDataResolver } from './details/member-data-resolver.service';
import { MembersRootComponent } from './members-root.component';
import { MembersScreenComponent } from './list/members-screen.component';
import { MemberDetailComponent } from './details/members-detail.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Members'
    },
    component: MembersRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: MembersScreenComponent,
        resolve: {
          members: MemberListResolver
        }
      },
      {
        path: ':id',
        data: {
          title: 'Member Detail'
        },
        component: MemberDetailComponent,
        resolve: {
          memberData: MemberDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule {}
