import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkStubDirective } from 'testing/router-helpers';
import { AppModule } from '../../app.module';
import { SharedModule } from '../../shared/shared.module';
import { MembersModule } from '../members.module';
import { MemberListComponent } from './member-list.component';

describe('MemberListComponent', () => {
  const TEST_MEMBERS = [
    {
      id: 1,
      fname: 'FName1',
      lname: 'LName1',
      username: 'Username1',
      reg_date: 1495141706,
      verified: 1
    },
    {
      id: 2,
      fname: 'FName2',
      lname: 'LName2',
      username: 'Username2',
      reg_date: 1495141706,
      verified: 0
    },
    {
      id: 3,
      fname: 'FName3',
      lname: 'LName3',
      username: 'Username3',
      reg_date: 1495141706,
      verified: 1
    }
  ];

  let comp: MemberListComponent;
  let fixture: ComponentFixture<MemberListComponent>;
  let linkDebugElements: DebugElement[];
  let linkDirectiveInstances: RouterLinkStubDirective[];

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule/*, MembersModule*/],
        declarations: [MemberListComponent, RouterLinkStubDirective],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberListComponent);
    comp = fixture.componentInstance;
    comp.members = TEST_MEMBERS;
    fixture.detectChanges();

    linkDebugElements = fixture.debugElement.queryAll(
      By.directive(RouterLinkStubDirective)
    );

    linkDirectiveInstances = linkDebugElements.map(de =>
      de.injector.get(RouterLinkStubDirective)
    );
  });

  it('should be created', () => {
    expect(comp).toBeTruthy();
  });

  it('should show a spinner if input `isLoading` param is truthy', () => {
    comp.isLoading = true;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeTruthy(`<app-spinner> isn't rendered`);
  });

  it('should not show a spinner if input `isLoading` param is falsy', () => {
    comp.isLoading = false;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeNull(`<app-spinner> is rendered but shouldn't be`);
  });

  it('should render the passed member list rows', () => {
    const memberEls = fixture.debugElement.queryAll(
      By.css('.member-list-item')
    );
    expect(memberEls.length).toBe(TEST_MEMBERS.length);
  });

  it('should render each member username as a link to that member details screen', () => {
    // Note: only checking first one
    const memberEl = fixture.debugElement.query(By.css('.member-list-item'));
    const memberElTextContent = memberEl.nativeElement.textContent;
    const expectedMember = TEST_MEMBERS[0];

    expect(memberElTextContent).toContain(expectedMember.username);

    expect(linkDirectiveInstances.length).toEqual(TEST_MEMBERS.length);
    expect(linkDirectiveInstances[0].linkParams).toEqual([
      '../members',
      TEST_MEMBERS[0].id
    ]);
  });

  it('should render each member first name and last name consequently', () => {
    // Note: only checking first one
    const memberEl = fixture.debugElement.query(By.css('.member-list-item'));
    const memberElTextContent = memberEl.nativeElement.textContent;
    const expectedMember = TEST_MEMBERS[0];

    expect(memberElTextContent).toContain(expectedMember.fname);
    expect(memberElTextContent).toContain(expectedMember.lname);
    expect(memberElTextContent).toContain(
      `${expectedMember.fname} ${expectedMember.lname}`
    );
  });

  it('should render a "check" icon for verified members', () => {
    // Note: only checking first one
    const memberEl = fixture.debugElement.queryAll(
      By.css('.member-list-item')
    )[0];
    const expectedMember = TEST_MEMBERS[0];
    const checkIconEl = memberEl.query(By.css('.icon-check'));

    expect(checkIconEl).toBeTruthy();
  });

  it('should render a "No" text in a "Verified" column for non-verified members', () => {
    // Note: only checking second one
    const memberEl = fixture.debugElement.queryAll(
      By.css('.member-list-item')
    )[1];
    const expectedMember = TEST_MEMBERS[1];
    const verifiedColEl = memberEl.query(By.css('.verified-col'));
    const checkIconEl = memberEl.query(By.css('.icon-check'));

    expect(verifiedColEl.nativeElement.textContent).toContain('No');
    expect(checkIconEl).toBeNull();
  });
});
