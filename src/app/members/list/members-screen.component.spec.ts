import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  ActivatedRoute,
  ActivatedRouteStub,
  Router,
  RouterLinkStubDirective,
  RouterStub
} from 'testing/router-helpers';
import { Store, StoreMock } from 'testing/store-helpers';

import { AppModule } from '../../app.module';
import { SharedModule } from '../../shared/shared.module';
import { MembersModule } from '../members.module';
import { MemberListFilterData } from '../store/members.state';
import { FETCH_LIST } from '../store/members.actions';
import { MembersScreenComponent } from './members-screen.component';
import { PaginationConfig } from 'app/core/store/helpers';

describe('MembersScreenComponent', () => {
  let component: MembersScreenComponent;
  let fixture: ComponentFixture<MembersScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;
  const filterDataMock = {
    page_number: 1,
    results_per_page: 25,
    username_filter: '',
    email_filter: '',
    fname_filter: '',
    lname_filter: '',
    rank_filter: null,
    sponsor_id_filter: 1,
    sponsor_username_filter: '',
    verified_filter: 0
  };
  const paginationMock: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  }

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      members: {
        data: {
          data: []
        },
        filterData: new MemberListFilterData()
      }
    });

    store = new StoreMock();
    store.mockState('members', {
      data: {
        data: [],
        results_per_page: paginationMock.itemPerPage,
        page_number: paginationMock.currentPage,
        total_results: paginationMock.totalItems
      },
      filterData: filterDataMock,
      isFetchSuccess: true
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, MembersModule],
        providers: [
          { provide: Router, useClass: RouterStub },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should render the filter component when filterShown is true', () => {
    component.filterShown = true;
    fixture.detectChanges();
    const filterElement = fixture.debugElement.query(
      By.css('member-list-filter')
    );
    expect(filterElement).toBeTruthy('Filter isn\'t rendered');
  });

  it('should not render the filter component when filterShown is false', () => {
    component.filterShown = false;
    fixture.detectChanges();
    const filterElement = fixture.debugElement.query(
      By.css('member-list-filter')
    );
    expect(filterElement).toBeNull('Filter is being rendered');
  });

  it('should toggle the filterShown value on toggleFilter() call', () => {
    component.filterShown = true;
    component.toggleFilter();
    fixture.detectChanges();
    let filterElement = fixture.debugElement.query(
      By.css('member-list-filter')
    );
    expect(filterElement).toBeNull('Filter is being rendered');
    component.toggleFilter();
    fixture.detectChanges();
    filterElement = fixture.debugElement.query(By.css('member-list-filter'));
    expect(filterElement).toBeTruthy('Filter is not being rendered');
    // TODO
  });

  // Filter data

  it('should initialize filter data from passed state', () => {
    expect(component.filterData).toEqual(filterDataMock);
  });

  it('should update filter data on filter change', () => {
    const filterDataChanged = new MemberListFilterData();
    component.handleFilterChange(filterDataChanged);
    expect(component.filterData).toEqual(filterDataChanged);
  });

  it('should dispatch the "members/FETCH_LIST" action on filter change', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    component.handleFilterChange(new MemberListFilterData());
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(FETCH_LIST);
  });

  // Pagination (TODO: Make generic reusable function to be reused across tests)

  it('should initialize pagination from passed state', () => {
    expect(component.pagination).toEqual(paginationMock);
  });

  it('should update current page value on page change', () => {
    component.handlePageChange(5);
    expect(component.pagination.currentPage).toBe(5);
  });

  it('should dispatch the "members/FETCH_LIST" action on page change', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    component.handlePageChange(5);
    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(FETCH_LIST);
  });
});
