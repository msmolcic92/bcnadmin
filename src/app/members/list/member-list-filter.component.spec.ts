import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub,
  RouterLinkStubDirective
} from 'testing/router-helpers';
import { Store, StoreMock } from 'testing/store-helpers';
import { AppModule } from '../../app.module';
import { SharedModule } from '../../shared/shared.module';
import { MembersModule } from '../members.module';
import { MemberListFilterComponent } from './member-list-filter.component';
import {
  Member,
  MembersState,
  MemberListFilterData
} from '../store/members.state';

describe('MemberListFilterComponent', () => {
  const MEMBER_RANKS = [
    { rank: 1, rank_label: 'Miner' },
    { rank: 2, rank_label: 'Builder' },
    { rank: 3, rank_label: 'Pro Builder' },
    { rank: 4, rank_label: 'Master Builder' },
    { rank: 5, rank_label: 'Monster Builder' }
  ];

  let component: MemberListFilterComponent;
  let fixture: ComponentFixture<MemberListFilterComponent>;
  let store: StoreMock<any>;
  const expectedFilterData = new MemberListFilterData();

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('memberRanks', {
      isFetchSuccess: true,
      entities: MEMBER_RANKS
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, MembersModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberListFilterComponent);
    component = fixture.componentInstance;
    component.value = expectedFilterData;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should obtain the list of member ranks from the store', () => {
    expect(component.rankList).toEqual(MEMBER_RANKS);
  });

  it('should accept the filter data as input "value" parameter', () => {
    expect(component.value).toBe(expectedFilterData);
  });

  it('should emit the filterChange event with updated filter data on submit', () => {
    let outputValue: MemberListFilterData;
    component.valueChange.subscribe((filterData: MemberListFilterData) => outputValue = filterData);

    component.submit();
    expect(outputValue).toBeDefined();
  });

  // TODO: Test if particular fields are rendered and particular output values are formatted correctly
});
