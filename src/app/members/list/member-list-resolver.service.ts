import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { MembersState } from '../store/members.state';
import * as actions from '../store/members.actions';

@Injectable()
export class MemberListResolver {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('members');

    state$.take(1).subscribe((state: MembersState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: { ...state.filterData }
      });
    });

    return state$
      .filter(
        (state: MembersState) =>
          state.isFetchSuccess || state.isFetchFailure
      )
      .take(1);
  }
}
