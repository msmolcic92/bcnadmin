import { Component, Input } from '@angular/core';

@Component({
  selector: 'member-list',
  templateUrl: './member-list.component.html'
})
export class MemberListComponent {
  @Input() members: any = [];
  @Input() isLoading: boolean;
}
