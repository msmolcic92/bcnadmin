import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { MemberListFilterData } from '../store/members.state';
import {
  MemberRanksState,
  MemberRank
} from '../../shared/store/lookup-objects.state';
import * as lookupObjectsActions from '../../shared/store/lookup-objects.actions';
import { isNumeric } from 'rxjs/util/isNumeric';

@Component({
  selector: 'member-list-filter',
  templateUrl: './member-list-filter.component.html'
})
export class MemberListFilterComponent implements OnDestroy, OnInit, OnChanges {
  @Input() value: MemberListFilterData;
  @Output()
  valueChange: EventEmitter<MemberListFilterData> = new EventEmitter<
    MemberListFilterData
  >();

  form: FormGroup;
  memberRanksSubscription: Subscription;

  verifiedOptionList = [
    { value: null, label: 'Both verified and non-verified' },
    { value: 1, label: 'Verified only' },
    { value: 0, label: 'Non-verified only' }
  ];

  rankList: MemberRank[] = [];

  constructor(private store: Store<any>, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildForm();

    this.memberRanksSubscription = this.store
      .select('memberRanks')
      .filter(
        (memberRanksState: MemberRanksState) => memberRanksState.isFetchSuccess
      )
      .map((memberRanksState: MemberRanksState) => memberRanksState.entities)
      .subscribe((rankList: MemberRank[]) => {
        this.rankList = rankList;
      });

    this.store.dispatch({ type: lookupObjectsActions.MEMBER_RANKS_FETCH });
  }

  ngOnDestroy() {
    this.memberRanksSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      username_filter: [this.value.username_filter],
      email_filter: [this.value.email_filter],
      fname_filter: [this.value.fname_filter],
      lname_filter: [this.value.lname_filter],
      verified_filter: [this.value.verified_filter],
      sponsor_id_filter: [this.value.sponsor_id_filter],
      sponsor_username_filter: [this.value.sponsor_username_filter],
      rank_filter: [this.value.rank_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    formData.verified_filter = isNumeric(formData.verified_filter)
      ? +formData.verified_filter
      : null;

    formData.sponsor_id_filter = formData.sponsor_id_filter || null;
    formData.sponsor_username_filter = formData.sponsor_username_filter || null;
    formData.rank_filter = +formData.rank_filter || null;

    this.valueChange.emit(formData);
  }
}
