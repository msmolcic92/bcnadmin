import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PaginationConfig, DataList } from '../../core/store/helpers';
import {
  MembersState,
  MemberListFilterData,
  Member
} from '../store/members.state';
import * as actions from '../store/members.actions';

@Component({
  selector: 'members-screen',
  templateUrl: './members-screen.component.html'
})
export class MembersScreenComponent implements OnDestroy, OnInit {
  private subscription: any;

  members: DataList<Member>;
  filterShown = false;
  filterData: MemberListFilterData = new MemberListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };
  isTableLoading = true;

  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit() {
    this.route.data.subscribe(routeData => {
      const membersState = routeData.members as MembersState;
      this.members = membersState && membersState.data;
      this.filterData = membersState && membersState.filterData;

      this.subscription = this.store
        .select('members')
        .subscribe((state: MembersState) => {
          this.isTableLoading = state.isFetching;
          if (state.isFetchSuccess) {
            this.store.dispatch({ type: actions.FETCH_LIST_STATE_RESET });
            this.members = state.data;
            this.filterData = state.filterData;

            // TODO: Reduce the pagination-related boilerplate
            this.pagination = {
              itemPerPage: state.data.results_per_page,
              currentPage: state.data.page_number,
              totalItems: state.data.total_results
            };
          }
        });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        ...this.filterData,
        page_number: this.pagination.currentPage,
        results_per_page: this.pagination.itemPerPage
      }
    });
    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: MemberListFilterData = new MemberListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }
}
