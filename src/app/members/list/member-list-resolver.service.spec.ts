import { TestBed, inject } from '@angular/core/testing';
import { Store, StoreMock } from 'testing/store-helpers';
import { MemberListResolver } from './member-list-resolver.service';
import { FETCH_LIST } from '../store/members.actions';

describe('MemberListResolver', () => {
  const TEST_MEMBERS_STATE = {
    isFetchSuccess: true,
    data: {
      data: [
        { id: 1, username: 'username1' },
        { id: 2, username: 'username2' },
        { id: 3, username: 'username3' }
      ]
    }
  };

  let activatedRouteSnapshot;
  let store: StoreMock<any>;
  let resolver: MemberListResolver;

  beforeEach(() => {
    activatedRouteSnapshot = {};

    store = new StoreMock<any>();
    store.mockState('members', TEST_MEMBERS_STATE);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberListResolver, { provide: Store, useValue: store }]
    });

    resolver = TestBed.get(MemberListResolver);
  });

  it('should dispatch the FETCH_LIST action', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    resolver.resolve(activatedRouteSnapshot);

    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(FETCH_LIST);
  });

  it('should obtain the member list from the store', () => {
    let result;
    resolver
      .resolve(activatedRouteSnapshot)
      .subscribe(_result => (result = _result));

    expect(result).toBeDefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(TEST_MEMBERS_STATE.data);
  });
});
