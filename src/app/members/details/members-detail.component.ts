import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { Member, MembersState } from '../store/members.state';
import { PaginationConfig } from '../../core/store/helpers';
import { MemberUsernameEditModalContentComponent } from './member-username-edit-modal-content.component';
import { MemberEmailEditModalContentComponent } from './member-email-edit-modal-content.component';
import { MemberRankEditModalContentComponent } from './member-rank-edit-modal-content.component';
import { MemberCreditWalletModalContentComponent } from './member-credit-wallet-modal-content.component';
import * as actions from '../store/members.actions';

@Component({
  selector: 'app-member-detail-access',
  templateUrl: './members-detail.component.html',
  styleUrls: ['./members-detail.component.scss']
})
export class MemberDetailComponent implements OnInit, OnDestroy {
  member: Member;
  invoices: any[];
  transactions: any[];
  currency = 'btc';

  isFetchingInvoices = true;
  isFetchingTransactions = true;
  isAlive = true;

  transactionsPagination: PaginationConfig = {
    itemPerPage: 10,
    currentPage: 1,
    totalItems: 0
  };

  invoicesPagination: PaginationConfig = {
    itemPerPage: 10,
    currentPage: 1,
    totalItems: 0
  };

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.member = data.memberData;
      this.fetchInvoices();
      this.fetchTransactions();
    });

    this.store
      .select('members')
      .takeWhile(() => this.isAlive)
      .subscribe((memberState: MembersState) => {
        this.isFetchingInvoices = memberState.isFetchingInvoices;
        this.isFetchingTransactions = memberState.isFetchingTransactions;

        if (memberState.isFetchSuccess) {
          this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
          this.member = memberState.currentMember;
        }

        if (memberState.isFetchInvoicesSuccess) {
          this.store.dispatch({ type: actions.FETCH_INVOICES_STATE_RESET });
          this.invoices = memberState.currentMember.invoices.data;

          this.invoicesPagination = {
            itemPerPage: memberState.currentMember.invoices.results_per_page,
            currentPage: memberState.currentMember.invoices.page_number,
            totalItems: memberState.currentMember.invoices.total_results
          };
        }

        if (memberState.isFetchTransactionsSuccess) {
          this.store.dispatch({ type: actions.FETCH_TRANSACTIONS_STATE_RESET });
          this.transactions = memberState.currentMember.transactions.data;

          this.transactionsPagination = {
            itemPerPage:
              memberState.currentMember.transactions.results_per_page,
            currentPage: memberState.currentMember.transactions.page_number,
            totalItems: memberState.currentMember.transactions.total_results
          };
        }
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  fetchInvoices() {
    this.store.dispatch({
      type: actions.FETCH_INVOICES,
      payload: {
        memid_filter: this.member.id,
        page_number: this.invoicesPagination.currentPage,
        results_per_page: this.invoicesPagination.itemPerPage
      }
    });

    this.isFetchingInvoices = true;
  }

  fetchTransactions() {
    this.store.dispatch({
      type: actions.FETCH_TRANSACTIONS,
      payload: {
        currency: this.currency,
        filterData: {
          involved_member_id_filter: this.member.id,
          page_number: this.transactionsPagination.currentPage,
          results_per_page: this.transactionsPagination.itemPerPage
        }
      }
    });
    this.isFetchingTransactions = true;
  }

  handleCurrencySelect(currency) {
    this.currency = currency;
    this.transactionsPagination.currentPage = 1;
    this.fetchTransactions();
  }

  editUsername() {
    this._openEditModal(
      MemberUsernameEditModalContentComponent,
      {
        memberId: this.member.id,
        username: this.member.username
      },
      'The member username has been successfully updated.'
    );
  }

  editEmail() {
    this._openEditModal(
      MemberEmailEditModalContentComponent,
      {
        memberId: this.member.id,
        email: this.member.email
      },
      'The member email has been successfully updated.'
    );
  }

  editRank() {
    this._openEditModal(
      MemberRankEditModalContentComponent,
      {
        memberId: this.member.id,
        rank: this.member.rank
      },
      'The member rank has been successfully updated.'
    );
  }

  creditWallet() {
    this._openEditModal(
      MemberCreditWalletModalContentComponent,
      {
        memberId: this.member.id
      },
      'Transaction has been successful.'
    )
      .then((result: any) => {
        this.fetchInvoices();

        if (
          this.currency &&
          result.currency &&
          this.currency === result.currency
        ) {
          this.fetchTransactions();
        }
      })
      .catch(() => {});
  }

  _openEditModal(
    componentClass: any,
    bindings: object = {},
    successMessage?: string
  ) {
    const modalInstance = this.modalService.open(componentClass, {
      keyboard: false
    });

    const componentInstance = modalInstance.componentInstance;
    Object.keys(bindings).forEach((attr: string) => {
      componentInstance[attr] = bindings[attr];
    });

    return modalInstance.result
      .then(result => {
        if (successMessage) {
          this.toasterService.pop('success', 'Success!', successMessage);
        }

        return result;
      })
      .catch(() => {});
  }

  handleTransactionPageChange(pageNumber: any) {
    this.transactionsPagination.currentPage = pageNumber;
    this.fetchTransactions();
  }

  handleInvoicePageChange(pageNumber: any) {
    this.invoicesPagination.currentPage = pageNumber;
    this.fetchInvoices();
  }
}
