import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import { MembersState } from '../store/members.state';
import {
  MemberRanksState,
  MemberRank
} from '../../shared/store/lookup-objects.state';
import * as actions from '../store/members.actions';
import * as lookupObjectsActions from '../../shared/store/lookup-objects.actions';

export class MemberRankEditFormData {
  rank?: number;
}

@Component({
  selector: 'member-rank-edit-modal-content',
  templateUrl: './member-rank-edit-modal-content.component.html'
})
export class MemberRankEditModalContentComponent extends BaseForm implements OnInit {
  @Input() memberId: number;
  @Input() rank: number;

  rankList: MemberRank[] = [];
  formData: MemberRankEditFormData = {
    rank: null
  };

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private modalInstance: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.formData.rank = this.rank;
    super.ngOnInit();
    this.subscribeToStore();
    this.subscribeToActions();

    this.store.dispatch({ type: lookupObjectsActions.MEMBER_RANKS_FETCH });
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new MemberRankEditFormData();
    }

    this.form = this.formBuilder.group({
      rank: [this.formData.rank, Validators.required]
    });
  }

  subscribeToStore(): void {
    this.store
      .select('memberRanks')
      .filter(
        (memberRanksState: MemberRanksState) => memberRanksState.isFetchSuccess
      )
      .map((memberRanksState: MemberRanksState) => memberRanksState.entities)
      .takeWhile(() => this.isAlive)
      .subscribe((rankList: MemberRank[]) => {
        this.rankList = rankList;
      });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.RANK_UPDATE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.store.dispatch({
          type: actions.FETCH_ITEM,
          payload: { id: this.memberId }
        });
        this.modalInstance.close();
      });

    this.appAction$
      .ofType(actions.RANK_UPDATE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload.response);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const newRank = +this.form.value.rank;

    this.store.dispatch({
      type: actions.RANK_UPDATE,
      payload: {
        memberId: this.memberId,
        rank: newRank
      }
    });
  }

  cancel(): void {
    this.modalInstance.dismiss('Cancelled by the user');
  }
}
