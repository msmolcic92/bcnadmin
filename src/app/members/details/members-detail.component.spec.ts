import { filter } from 'rxjs/operator/filter';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from '@angular/core/testing';
import {
  Router,
  RouterStub,
  ActivatedRoute,
  ActivatedRouteStub,
  RouterLinkStubDirective
} from 'testing/router-helpers';
import { Store, StoreMock } from 'testing/store-helpers';
import { AppModule } from '../../app.module';
import { SharedModule } from '../../shared/shared.module';
import { MembersModule } from '../members.module';
import { MemberDetailComponent } from './members-detail.component';
import { Member, MembersState } from '../store/members.state';

describe('MemberDetailComponent', () => {
  const TEST_MEMBER_DATA: Member = {
    id: 1,
    username: 'test_user',
    fname: 'Test',
    lname: 'User',
    email: 'test@example.com',
    phone: '1234543123',
    skype: '',
    facebook: '',
    twitter: '',
    reg_date: 1239842398,
    verified: 1239842398,
    shares_mine1: 0,
    shares_mine2: 0,
    shares_mine3: 0,
    gpu_shares: 0,
    rank: 0,
    rank_label: '',
    login_attempt_date: 1239842398,
    login_attempt_ip: '0.0.0.0',
    invoices: {
      data: [],
      last_page: 1,
      page_number: 1,
      results_per_page: 25,
      total_results: 0,
    },
    transactions: {
      data: [],
      last_page: 1,
      page_number: 1,
      results_per_page: 25,
      total_results: 0,
    }
  };

  let component: MemberDetailComponent;
  let fixture: ComponentFixture<MemberDetailComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      memberData: { ...TEST_MEMBER_DATA }
    });

    store = new StoreMock();
    store.mockState('memberDetail', {
      currentMember: { ...TEST_MEMBER_DATA }
    });

    store.mockState('transactions', {
      transactions: {
        data: []
      },
      currency: 'btc',
      filterData: null
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [MemberDetailComponent, RouterLinkStubDirective],
        providers: [
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the member with the current member of the state', () => {
    expect(component.member).toEqual(TEST_MEMBER_DATA);
  });

  it('should keep track of currently selected currency', () => {
    expect(component.currency).toEqual('btc');
    component.handleCurrencySelect('eth');
    expect(component.currency).toEqual('eth');
  });

  it('should fetch the invoices for the currently selected currency when initialized', () => {
    spyOn(component, 'fetchInvoices');
    component.ngOnInit();
    expect(component.fetchInvoices).toHaveBeenCalled();
  });

  it('should fetch the invoices with the currently selected currency', () => {
    spyOn(store, 'dispatch');
    component.fetchInvoices();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('should render the MembersTransactionsComponent correctly', () => {
    const filterElement = fixture.debugElement.query(
      By.css('app-member-transaction')
    );
    expect(filterElement).toBeTruthy();
  });

  it('should open the correct modal to edit the member\'s username', () => {
    spyOn(component, 'editUsername');
    const buttonEl = fixture.debugElement.query(
      By.css('#edit-username-button')
    );
    buttonEl.triggerEventHandler('click', null);
    expect(component.editUsername).toHaveBeenCalled();
  });

  it('should open the correct modal to edit the member\'s email', () => {
    spyOn(component, 'editEmail');
    const buttonEl = fixture.debugElement.query(
      By.css('#edit-email-button')
    );
    buttonEl.triggerEventHandler('click', null);
    expect(component.editEmail).toHaveBeenCalled();
  });

  it('should open the correct modal to edit the member\'s rank', () => {
    spyOn(component, 'editRank');
    const buttonEl = fixture.debugElement.query(
      By.css('#edit-rank-button')
    );
    buttonEl.triggerEventHandler('click', null);
    expect(component.editRank).toHaveBeenCalled();
  });

  it('should open the correct modal to debit/credit a member\'s wallet', () => {
    spyOn(component, 'creditWallet');
    const buttonEl = fixture.debugElement.query(
      By.css('#credit-wallet-button')
    );
    buttonEl.triggerEventHandler('click', null);
    expect(component.creditWallet).toHaveBeenCalled();
  });
});
