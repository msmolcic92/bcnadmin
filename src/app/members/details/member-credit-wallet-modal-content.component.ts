import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import { MembersState } from '../store/members.state';
import * as actions from '../store/members.actions';

export class MemberCreditWalletFormData {
  amount?: number;
  wallet: string;
  isDebit: boolean;
}

const FORM_VALIDATION_MESSAGES = {
  wallet: {
    required: 'Please choose a wallet for the transaction.'
  },
  amount: {
    required: 'Please specify the amount for the debit or credit transaction.'
  }
};

@Component({
  selector: 'member-credit-wallet-modal-content',
  templateUrl: './member-credit-wallet-modal-content.component.html'
})
export class MemberCreditWalletModalContentComponent extends BaseForm implements OnInit {
  @Input() memberId: number;

  formData: MemberCreditWalletFormData = {
    amount: null,
    wallet: 'btc',
    isDebit: false
  };

  // TODO: Move to "shared" part of the store somewhere
  walletList = [
    { value: 'btc', label: 'Bitcoin' },
    { value: 'eth', label: 'ETH' },
    { value: 'etc', label: 'ETC' },
    { value: 'cc', label: 'CC' },
    { value: 'dash', label: 'Dash' },
    { value: 'zec', label: 'Zcash' },
    { value: 'xmr', label: 'XMR' },
    { value: 'coinpay', label: 'Coinpay' },
    { value: 'bch', label: 'BCH' }
  ];

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private modalInstance: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit() {
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new MemberCreditWalletFormData();
    }

    this.form = this.formBuilder.group({
      wallet: [this.formData.wallet, Validators.required],
      amount: [this.formData.amount, Validators.required]
    });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.WALLET_CREDIT_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.modalInstance.close({ currency: this.form.value.wallet });
      });

    this.appAction$
      .ofType(actions.WALLET_CREDIT_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload.response);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const wallet = this.form.value.wallet as string;
    const amount = +this.form.value.amount;

    this.store.dispatch({
      type: actions.WALLET_CREDIT,
      payload: {
        memberId: this.memberId,
        wallet,
        amount: this.formData.isDebit ? -amount : amount
      }
    });
  }

  cancel(): void {
    this.modalInstance.dismiss('Cancelled by the user');
  }

  setOperationIsDebit(isDebit: boolean) {
    this.formData.isDebit = isDebit;
  }

  getSelectedWalletLabel(walletValue: string) {
    return this._getWalletLabel(this.form.value.wallet as string);
  }

  _getWalletLabel(walletValue: string) {
    const selectedWallet = this.walletList.filter(
      (wallet: any) => wallet.value === walletValue
    )[0] as any;

    if (!selectedWallet) {
      return '';
    }

    return selectedWallet.label;
  }
}
