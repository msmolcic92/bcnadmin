import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-member-transaction',
  templateUrl: 'members-transactions.component.html',
  styleUrls: [
    // ANTIPATTERN ALERT: This isn't intended for reuse,
    // its the opposite - meant to be a component encapsulation
    // thing. If reuse is needed - then it should be either global
    // style or special component should be created that
    // will have scoped styles defined and be reused in both places.
    // TODO: Refactor
    '../../transactions/transactions-screen.component.scss'
  ]
})
export class MemberTransactionComponent {
  @Input() isLoading;
  @Input() transactions = null;
  @Input() currency = 'btc';
  @Input() memberId = null;
  @Output() currencySelect: EventEmitter<string> = new EventEmitter<string>();

  CURRENCIES = [
    { name: 'Bitcoin', currency: 'btc' },
    { name: 'ETH', currency: 'eth' },
    { name: 'CC', currency: 'cc' },
    { name: 'ETC', currency: 'etc' },
    { name: 'Dash', currency: 'dash' },
    { name: 'Zcash', currency: 'zec' },
    { name: 'XMR', currency: 'xmr' },
    { name: 'Coinpay', currency: 'coinpay' },
    { name: 'BCH', currency: 'bch' }
  ];
  expanding = null;

  constructor() {}

  handleTabSelect(currency: string) {
    this.currencySelect.next(currency);
  }

  toggle(t) {
    // If there's an expanded row, collapse it.
    if (this.expanding) {
      this.expanding.expanded = false;
    }

    // If previously expanded row is the same
    // as the clicked row, set it to 'null',
    // indicating that there are no rows expanded.
    // Expand the new row otherwise.
    if (this.expanding === t) {
      this.expanding = null;
    } else {
      this.expanding = t;
      t.expanded = true;
    }
  }

  determineTransactionType(senderId: number, receiverId: number): string {
    if (this.memberId === senderId && this.memberId === receiverId) {
      return 'Neutral';
    } else if (this.memberId === senderId) {
      return 'Debit';
    } else if (this.memberId === receiverId) {
      return 'Credit';
    }

    return null;
  }
}
