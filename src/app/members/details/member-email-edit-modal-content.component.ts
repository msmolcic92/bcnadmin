import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import { MembersState } from '../store/members.state';
import * as actions from '../store/members.actions';

export class MemberEmailEditFormData {
  email?: string;
}

const FORM_VALIDATION_MESSAGES = {
  email: {
    required: 'Email address is required.',
    email: 'Invalid email address.'
  }
};

@Component({
  selector: 'member-email-edit-modal-content',
  templateUrl: './member-email-edit-modal-content.component.html'
})
export class MemberEmailEditModalContentComponent extends BaseForm implements OnInit {
  @Input() memberId: number;
  @Input() email: string;

  formData: MemberEmailEditFormData = {
    email: ''
  };

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private modalInstance: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit() {
    this.formData.email = this.email;
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new MemberEmailEditFormData();
    }

    this.form = this.formBuilder.group({
      email: [this.formData.email, [Validators.required, Validators.email]]
    });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.EMAIL_UPDATE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.modalInstance.close();
      });

    this.appAction$
      .ofType(actions.EMAIL_UPDATE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload.response);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const newEmail = this.form.value.email as string;

    this.store.dispatch({
      type: actions.EMAIL_UPDATE,
      payload: {
        memberId: this.memberId,
        email: newEmail
      }
    });
  }

  cancel(): void {
    this.modalInstance.dismiss('Cancelled by the user');
  }
}
