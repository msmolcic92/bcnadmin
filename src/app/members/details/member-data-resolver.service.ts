import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { MembersState, Member } from '../store/members.state';
import * as actions from '../store/members.actions';

@Injectable()
export class MemberDataResolver implements Resolve<Member> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: { id: route.params.id }
    });

    return this.store
      .select('members')
      .filter(
        (member: MembersState) => member.isFetchSuccess || member.isFetchFailure
      )
      .map((membersState: MembersState) => membersState.currentMember)
      .take(1);
  }
}
