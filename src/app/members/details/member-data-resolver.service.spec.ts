import { TestBed, inject } from '@angular/core/testing';
import { Store, StoreMock } from 'testing/store-helpers';
import { MemberDataResolver } from './member-data-resolver.service';
import { FETCH_ITEM } from '../store/members.actions';

describe('MemberDataResolver', () => {
  const TEST_MEMBER_DATA = {
    id: 1,
    username: 'username1'
  };

  const TEST_MEMBERS_STATE = {
    data: {
      data: [TEST_MEMBER_DATA]
    },
    currentMember: TEST_MEMBER_DATA,
    isFetchSuccess: true
  };

  let activatedRouteSnapshot;
  let store: StoreMock<any>;
  let resolver: MemberDataResolver;

  beforeEach(() => {
    activatedRouteSnapshot = {
      params: {
        id: 1
      }
    };

    store = new StoreMock<any>();
    store.mockState('members', TEST_MEMBERS_STATE);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MemberDataResolver,
        { provide: Store, useValue: store }
      ]
    });

    resolver = TestBed.get(MemberDataResolver);
  });

  it('should dispatch the FETCH_ITEM action', () => {
    let dispatchedAction;
    spyOn(store, 'dispatch').and.callFake(
      action => (dispatchedAction = action)
    );
    resolver.resolve(activatedRouteSnapshot, null);

    expect(dispatchedAction).toBeTruthy();
    expect(dispatchedAction.type).toBeDefined();
    expect(dispatchedAction.type).toEqual(FETCH_ITEM);
  });

  it('should obtain the correct member from the store', () => {
    let result;
    resolver
      .resolve(activatedRouteSnapshot, null)
      .subscribe(_result => (result = _result));
    expect(result).toEqual(TEST_MEMBER_DATA);
  });
});
