import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Store, Action } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import { MembersState } from '../store/members.state';
import * as actions from '../store/members.actions';

export class MemberUsernameEditFormData {
  username?: string;
}

const FORM_VALIDATION_MESSAGES = {
  username: {
    required: 'Username is required.'
  }
};

@Component({
  selector: 'member-username-edit-modal-content',
  templateUrl: './member-username-edit-modal-content.component.html'
})
export class MemberUsernameEditModalContentComponent extends BaseForm implements OnInit {
  @Input() memberId: number;
  @Input() username: string;

  formData: MemberUsernameEditFormData = {
    username: ''
  };

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private modalInstance: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    this.formData.username = this.username;
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new MemberUsernameEditFormData();
    }

    this.form = this.formBuilder.group({
      username: [this.formData.username, Validators.required]
    });
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.USERNAME_UPDATE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.modalInstance.close();
      });

    this.appAction$
      .ofType(actions.USERNAME_UPDATE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload.response);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const newUsername = this.form.value.username as string;

    this.store.dispatch({
      type: actions.USERNAME_UPDATE,
      payload: {
        memberId: this.memberId,
        username: newUsername
      }
    });
  }

  cancel(): void {
    this.modalInstance.dismiss('Cancelled by the user');
  }
}
