import { MemberListFilterData } from './members.state';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import { InvoicesService } from 'api-client/invoices.service';
import { MembersService } from 'api-client/members.service';
import { TransactionService } from 'api-client/transactions.service';
import { Actions, TestActions } from 'testing/effects-helpers';
import { MembersServiceStub } from 'testing/stubs/services/members.service.stub';
import { InvoicesServiceStub } from 'testing/stubs/services/invoices.service.stub';
import { TransactionServiceStub } from 'testing/stubs/services/transaction.service.stub';
import { MembersEffects } from './members.effects';
import * as actions from './members.actions';

describe('MembersEffects', () => {
  const invoicesService: InvoicesServiceStub = new InvoicesServiceStub();
  const transactionService: TransactionServiceStub = new TransactionServiceStub();
  const membersService: MembersServiceStub = new MembersServiceStub();
  let effects: MembersEffects;
  // tslint:disable-next-line:prefer-const
  let actions$: ReplaySubject<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MembersEffects,
        provideMockActions(() => actions$),
        { provide: MembersService, useValue: membersService },
        { provide: InvoicesService, useValue: invoicesService },
        { provide: TransactionService, useValue: transactionService }
      ]
    });
    effects = TestBed.get(MembersEffects);
  });

  describe('fetchList$', () => {
    it('should call the memberService.fetchList(...) method', () => {
      spyOn(membersService, 'fetchList').and.returnValue(
        Observable.of([1, 2, 3])
      );
      const filterData = new MemberListFilterData();
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.FETCH_LIST,
        payload: {
          ...filterData,
          page_number: 1,
          results_per_page: 25
        }
      });
      effects.fetchList$.subscribe();
      expect(membersService.fetchList).toHaveBeenCalled();
    });

    it('should map to members/FETCH_LIST_SUCCESS action on success', () => {
      spyOn(membersService, 'fetchList').and.returnValue(Observable.of({}));
      const filterData = new MemberListFilterData();
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.FETCH_LIST,
        payload: {
          ...filterData,
          page_number: 1,
          results_per_page: 25
        }
      });
      effects.fetchList$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.FETCH_LIST_SUCCESS);
    });

    it('should map to members/FETCH_LIST_FAILURE action on failure', () => {
      spyOn(membersService, 'fetchList').and.returnValue(
        Observable.throw(new Error('An error ocurred fetching the list'))
      );
      const filterData = new MemberListFilterData();
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.FETCH_LIST,
        payload: {
          ...filterData,
          page_number: 1,
          results_per_page: 25
        }
      });
      effects.fetchList$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.FETCH_LIST_FAILURE);
    });
  });

  describe('usernameUpdate$', () => {
    it('should call the membersService.updateUsername(...) method', () => {
      spyOn(membersService, 'updateUsername').and.returnValue(
        Observable.of({})
      );
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.USERNAME_UPDATE,
        payload: {}
      });
      effects.usernameUpdate$.subscribe();
      expect(membersService.updateUsername).toHaveBeenCalled();
    });

    it('should map to members/USERNAME_UPDATE_SUCCESS action on success', () => {
      spyOn(membersService, 'updateUsername').and.returnValue(
        Observable.of({})
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.USERNAME_UPDATE,
        payload: {}
      });
      effects.usernameUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.USERNAME_UPDATE_SUCCESS);
    });

    it('should map to members/USERNAME_UPDATE_FAILURE action on failure', () => {
      spyOn(membersService, 'updateUsername').and.returnValue(
        Observable.throw(new Error('An error ocurred fetching the list'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.USERNAME_UPDATE,
        payload: {}
      });
      effects.usernameUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.USERNAME_UPDATE_FAILURE);
    });
  });

  describe('usernameUpdate$', () => {
    it('should call the membersService.updateEmail(...) method', () => {
      spyOn(membersService, 'updateEmail').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.EMAIL_UPDATE,
        payload: {}
      });
      effects.emailUpdate$.subscribe();
      expect(membersService.updateEmail).toHaveBeenCalled();
    });

    it('should map to members/EMAIL_UPDATE_SUCCESS action on success', () => {
      spyOn(membersService, 'updateEmail').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.EMAIL_UPDATE,
        payload: {}
      });
      effects.emailUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.EMAIL_UPDATE_SUCCESS);
    });

    it('should map to members/EMAIL_UPDATE_FAILURE action on failure', () => {
      spyOn(membersService, 'updateEmail').and.returnValue(
        Observable.throw(new Error('An error ocurred fetching the list'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.EMAIL_UPDATE,
        payload: {}
      });
      effects.emailUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.EMAIL_UPDATE_FAILURE);
    });
  });

  describe('rankUpdate$', () => {
    it('should call the membersService.updateRank(...) method', () => {
      spyOn(membersService, 'updateRank').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.RANK_UPDATE,
        payload: {}
      });
      effects.rankUpdate$.subscribe();
      expect(membersService.updateRank).toHaveBeenCalled();
    });

    it('should map to members/RANK_UPDATE_SUCCESS action on success', () => {
      spyOn(membersService, 'updateRank').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.RANK_UPDATE,
        payload: {}
      });
      effects.rankUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.RANK_UPDATE_SUCCESS);
    });

    it('should map to members/RANK_UPDATE_FAILURE action on failure', () => {
      spyOn(membersService, 'updateRank').and.returnValue(
        Observable.throw(new Error('An error ocurred fetching the list'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.RANK_UPDATE,
        payload: {}
      });
      effects.rankUpdate$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.RANK_UPDATE_FAILURE);
    });
  });

  describe('walletCredit$', () => {
    it('should call the membersService.creditWallet(...) method', () => {
      spyOn(membersService, 'creditWallet').and.returnValue(Observable.of({}));
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.WALLET_CREDIT,
        payload: {}
      });
      effects.walletCredit$.subscribe();
      expect(membersService.creditWallet).toHaveBeenCalled();
    });

    it('should map to members/WALLET_CREDIT_SUCCESS action on success', () => {
      spyOn(membersService, 'creditWallet').and.returnValue(Observable.of({}));
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.WALLET_CREDIT,
        payload: {}
      });
      effects.walletCredit$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.WALLET_CREDIT_SUCCESS);
    });

    it('should map to members/WALLET_CREDIT_FAILURE action on failure', () => {
      spyOn(membersService, 'creditWallet').and.returnValue(
        Observable.throw(new Error('An error ocurred fetching the list'))
      );
      let actionType = '';
      actions$ = new ReplaySubject(1);
      actions$.next({
        type: actions.WALLET_CREDIT,
        payload: {}
      });
      effects.walletCredit$.subscribe(result => {
        actionType = result.type;
      });
      expect(actionType).toEqual(actions.WALLET_CREDIT_FAILURE);
    });
  });
});
