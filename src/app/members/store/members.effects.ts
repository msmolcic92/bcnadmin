import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { MembersService } from 'api-client/members.service';
import { InvoicesService } from 'api-client/invoices.service';
import { TransactionService } from 'api-client/transactions.service';
import { MemberListFilterData } from './members.state';
import * as actions from './members.actions';

function makeMemberAttrUpdateEffect(
  action$: Actions,
  effectFn: Function,
  actionTypes: Array<string>
): Observable<Action> {
  return action$
    .ofType(actionTypes[0])
    .map(toPayload)
    .switchMap(payload => {
      return effectFn(payload)
        .map((response: any) => ({
          type: actionTypes[1],
          payload: {
            memberId: payload.memberId,
            response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actionTypes[2],
            payload: {
              memberId: payload.memberId,
              response: errorResponse.error
            }
          })
        );
    });
}

@Injectable()
export class MembersEffects {
  constructor(
    private action$: Actions,
    private membersService: MembersService,
    private invoicesService: InvoicesService,
    private transactionsService: TransactionService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((filterData: MemberListFilterData) => {
      return this.membersService
        .fetchList(filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_LIST_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.membersService
        .fetch(params.id)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_SUCCESS,
          payload: {
            data: response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_ITEM_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchInvoices$: Observable<Action> = this.action$
    .ofType(actions.FETCH_INVOICES)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.invoicesService
        .fetchList(params)
        .map((response: any) => ({
          type: actions.FETCH_INVOICES_SUCCESS,
          payload: {
            data: response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_INVOICES_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  fetchTransactions$: Observable<Action> = this.action$
    .ofType(actions.FETCH_TRANSACTIONS)
    .map(toPayload)
    .switchMap((params: any) => {
      return this.transactionsService
        .fetchList(params.currency, params.filterData)
        .map((response: any) => ({
          type: actions.FETCH_TRANSACTIONS_SUCCESS,
          payload: {
            data: response
          }
        }))
        .catch(errorResponse =>
          Observable.of({
            type: actions.FETCH_TRANSACTIONS_FAILURE,
            payload: errorResponse.error
          })
        );
    });

  @Effect()
  usernameUpdate$: Observable<Action> = makeMemberAttrUpdateEffect(
    this.action$,
    payload => {
      return this.membersService.updateUsername(payload.memberId, {
        username: payload.username
      });
    },
    [
      actions.USERNAME_UPDATE,
      actions.USERNAME_UPDATE_SUCCESS,
      actions.USERNAME_UPDATE_FAILURE
    ]
  );

  @Effect()
  emailUpdate$: Observable<Action> = makeMemberAttrUpdateEffect(
    this.action$,
    payload => {
      return this.membersService.updateEmail(payload.memberId, {
        email: payload.email
      });
    },
    [
      actions.EMAIL_UPDATE,
      actions.EMAIL_UPDATE_SUCCESS,
      actions.EMAIL_UPDATE_FAILURE
    ]
  );

  @Effect()
  rankUpdate$: Observable<Action> = makeMemberAttrUpdateEffect(
    this.action$,
    payload => {
      return this.membersService.updateRank(payload.memberId, {
        rank: payload.rank
      });
    },
    [
      actions.RANK_UPDATE,
      actions.RANK_UPDATE_SUCCESS,
      actions.RANK_UPDATE_FAILURE
    ]
  );

  @Effect()
  walletCredit$: Observable<Action> = makeMemberAttrUpdateEffect(
    this.action$,
    payload => {
      return this.membersService.creditWallet(payload.wallet, {
        memid: payload.memberId,
        amount: payload.amount
      });
    },
    [
      actions.WALLET_CREDIT,
      actions.WALLET_CREDIT_SUCCESS,
      actions.WALLET_CREDIT_FAILURE
    ]
  );
}
