import { Action } from '@ngrx/store';
import { DataList } from '../../core/store/helpers';
import { Invoice } from '../../invoices/store/invoices.state';
import { Transaction } from '../../transactions/store/transactions.state';
import * as actions from './members.actions';

// Model
export class MemberListFilterData {
  page_number? = 1;
  results_per_page? = 25;
  username_filter?: string;
  email_filter?: string;
  fname_filter?: string;
  lname_filter?: string;
  rank_filter?: number = null;
  sponsor_id_filter?: number;
  sponsor_username_filter?: string;
  verified_filter?: number = null;
}

export interface Member {
  id: number,
  username: string,
  email: string,
  fname: string,
  lname: string,
  phone: string,
  skype: string,
  facebook: string,
  twitter: string,
  reg_date: number,
  verified: number,
  shares_mine1: number,
  shares_mine2: number,
  shares_mine3: number,
  gpu_shares: number,
  rank: number,
  rank_label: string,
  login_attempt_date: number,
  login_attempt_ip: string,
  invoices: DataList<Invoice>,
  transactions: DataList<Transaction>
}

export interface MembersState {
  data: DataList<Member>;
  currentMember: Member;
  filterData?: MemberListFilterData;
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchFailure: boolean;
  isFetchingInvoices: boolean;
  isFetchInvoicesSuccess: boolean;
  isFetchInvoicesFailure: boolean;
  isFetchingTransactions: boolean;
  isFetchTransactionsSuccess: boolean;
  isFetchTransactionsFailure: boolean;
  isRemoving: boolean;
  isRemoveSuccess: boolean;
  isRemoveFailure: boolean;
}

export const INITIAL_STATE: MembersState = {
  data: null,
  currentMember: null,
  filterData: null,
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false,
  isFetchingInvoices: false,
  isFetchInvoicesSuccess: false,
  isFetchInvoicesFailure: false,
  isFetchingTransactions: false,
  isFetchTransactionsSuccess: false,
  isFetchTransactionsFailure: false,
  isRemoving: false,
  isRemoveSuccess: false,
  isRemoveFailure: false
};

// Reducers
export function membersReducer(
  state: MembersState = INITIAL_STATE,
  action: Action
): MembersState {
  switch (action.type) {
    case actions.FETCH_LIST:
      return {
        ...state,
        isFetching: true
      };

    case actions.FETCH_LIST_SUCCESS:
      return {
        ...state,
        data: { ...state.data, ...(<any>action).payload.data },
        filterData: { ...state.filterData, ...(<any>action).payload.filterData },
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_FAILURE:
      return {
        ...state,
        data: null,
        filterData: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.FETCH_LIST_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.RESET_LIST_FILTER:
      return {
        ...state,
        filterData: null
      };

    case actions.FETCH_ITEM:
      return {
        ...state,
        isFetching: true,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_SUCCESS:
      return {
        ...state,
        currentMember: (<any>action).payload.data,
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_FAILURE:
      return {
        ...state,
        currentMember: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.FETCH_ITEM_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_INVOICES:
      return {
        ...state,
        isFetchingInvoices: true,
        isFetchInvoicesSuccess: false,
        isFetchInvoicesFailure: false
      };

    case actions.FETCH_INVOICES_SUCCESS:
      return {
        ...state,
        currentMember: {
          ...state.currentMember,
          invoices: (<any>action).payload.data
        },
        isFetchingInvoices: false,
        isFetchInvoicesSuccess: true,
        isFetchInvoicesFailure: false
      };

    case actions.FETCH_INVOICES_FAILURE:
      return {
        ...state,
        currentMember: {
          ...state.currentMember,
          invoices: null
        },
        isFetchingInvoices: false,
        isFetchInvoicesSuccess: false,
        isFetchInvoicesFailure: true
      };

    case actions.FETCH_INVOICES_STATE_RESET:
      return {
        ...state,
        isFetchingInvoices: false,
        isFetchInvoicesSuccess: false,
        isFetchInvoicesFailure: false
      };

    case actions.FETCH_TRANSACTIONS:
      return {
        ...state,
        isFetchingTransactions: true,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: false
      };

    case actions.FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        currentMember: {
          ...state.currentMember,
          transactions: (<any>action).payload.data
        },
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: true,
        isFetchTransactionsFailure: false
      };

    case actions.FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        currentMember: {
          ...state.currentMember,
          transactions: null
        },
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: true
      };

    case actions.FETCH_TRANSACTIONS_STATE_RESET:
      return {
        ...state,
        isFetchingTransactions: false,
        isFetchTransactionsSuccess: false,
        isFetchTransactionsFailure: false
      };

    // TODO: Reduce the boilerplate by abstracting
    // into separate function (or better - sub-reducer)
    case actions.USERNAME_UPDATE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          data: (state.data.data || []).map((member: Member) => {
            return member.id === (<any>action).payload.memberId
              ? {
                  ...member,
                  username: (<any>action).payload.username
                }
              : member;
            })
        }
      };

    case actions.EMAIL_UPDATE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          data: (state.data.data || []).map((member: Member) => {
            return member.id === (<any>action).payload.memberId
              ? {
                  ...member,
                  email: (<any>action).payload.email
                }
              : member;
            })
        }
      };

    case actions.RANK_UPDATE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          data: (state.data.data || []).map((member: Member) => {
            return member.id === (<any>action).payload.memberId
              ? {
                  ...member,
                  rank: (<any>action).payload.rank
                }
              : member;
            })
        }
      };

    case actions.WALLET_CREDIT_SUCCESS:
      // TODO
      return state;

    default:
      return state;
  }
}
