import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { membersReducer } from './store/members.state';
import { MembersEffects } from './store/members.effects';
import { MembersRoutingModule } from './members-routing.module';
import { MembersService } from '../../api-client/members.service';
import { InvoicesService } from '../../api-client/invoices.service';
import { TransactionService } from '../../api-client/transactions.service';
import { MemberListResolver } from './list/member-list-resolver.service';
import { MembersRootComponent } from './members-root.component';
import { MembersScreenComponent } from './list/members-screen.component';
import { MemberUsernameEditModalContentComponent } from './details/member-username-edit-modal-content.component';
import { MemberEmailEditModalContentComponent } from './details/member-email-edit-modal-content.component';
import { MemberRankEditModalContentComponent } from './details/member-rank-edit-modal-content.component';
import { MemberCreditWalletModalContentComponent } from './details/member-credit-wallet-modal-content.component';
import { MemberListComponent } from './list/member-list.component';
import { MemberListFilterComponent } from './list/member-list-filter.component';
import { MemberDetailComponent } from './details/members-detail.component';
import { MemberTransactionComponent } from './details/members-transactions.component';
import { MemberDataResolver } from './details/member-data-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MembersRoutingModule,
    TabsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([MembersEffects]),
    StoreModule.forFeature('members', membersReducer)
  ],
  declarations: [
    MembersRootComponent,
    MembersScreenComponent,
    MemberDetailComponent,
    MemberTransactionComponent,
    MemberUsernameEditModalContentComponent,
    MemberEmailEditModalContentComponent,
    MemberRankEditModalContentComponent,
    MemberCreditWalletModalContentComponent,
    MemberListComponent,
    MemberListFilterComponent
  ],
  entryComponents: [
    MemberUsernameEditModalContentComponent,
    MemberEmailEditModalContentComponent,
    MemberRankEditModalContentComponent,
    MemberCreditWalletModalContentComponent
  ],
  providers: [
    MembersService,
    MemberListResolver,
    MemberDataResolver,
    TransactionService,
    InvoicesService
  ]
})
export class MembersModule {}
