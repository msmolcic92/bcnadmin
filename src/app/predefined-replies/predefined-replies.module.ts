import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PredefinedRepliesEffects } from './store/predefined-replies.effects';
import { PredefinedRepliesRoutingModule } from './predefined-replies-routing.module';
import { PredefinedRepliesRootComponent } from './predefined-replies-root.component';
import { PredefinedRepliesScreenComponent } from './predefined-replies-screen.component';
import { PredefinedRepliesListComponent } from './predefined-replies-list.component';
import { PredefinedRepliesFormComponent } from './predefined-replies-form.component';
import { PredefinedRepliesFormScreenComponent } from './predefined-replies-form-screen.component';
import { PredefinedRepliesService } from '../../api-client/predefined-replies.service';
import { PredefinedRepliesDetailsResolver } from './predefined-replies-details-resolver.service';
import { SharedModule } from '../shared/shared.module';
import { reducer } from './store/predefined-replies.reducer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([PredefinedRepliesEffects]),
    StoreModule.forFeature('predefinedReplies', reducer),
    PredefinedRepliesRoutingModule,
    NgbModule,
    SharedModule
  ],
  declarations: [
    PredefinedRepliesRootComponent,
    PredefinedRepliesScreenComponent,
    PredefinedRepliesListComponent,
    PredefinedRepliesFormComponent,
    PredefinedRepliesFormScreenComponent
  ],
  providers: [PredefinedRepliesService, PredefinedRepliesDetailsResolver]
})
export class PredefinedRepliesModule {}
