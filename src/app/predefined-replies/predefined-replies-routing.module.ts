import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredefinedRepliesRootComponent } from './predefined-replies-root.component';
import { PredefinedRepliesScreenComponent } from './predefined-replies-screen.component';
import { PredefinedRepliesFormScreenComponent } from './predefined-replies-form-screen.component';
import { PredefinedRepliesDetailsResolver } from './predefined-replies-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Predefined Replies'
    },
    component: PredefinedRepliesRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: PredefinedRepliesScreenComponent
      },
      {
        path: 'add',
        data: {
          title: 'Add reply',
          isEdit: false
        },
        component: PredefinedRepliesFormScreenComponent
      },
      {
        path: ':id/edit',
        data: {
          title: 'Edit reply',
          isEdit: true
        },
        component: PredefinedRepliesFormScreenComponent,
        resolve: {
          item: PredefinedRepliesDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PredefinedRepliesRoutingModule {}
