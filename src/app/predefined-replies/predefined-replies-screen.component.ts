import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import {
  getPaginationInitialState,
  DeleteFailureAction,
  DeleteSuccessAction
} from '../shared/helpers';
import { PredefinedReply } from './store/predefined-replies.model';
import { actions, actionCreators } from './store/predefined-replies.actions';
import { PredefinedRepliesState, selectors } from './store/predefined-replies.reducer';

@Component({
  selector: 'predefined-replies-screen',
  templateUrl: 'predefined-replies-screen.component.html'
})
export class PredefinedRepliesScreenComponent implements OnDestroy, OnInit {
  data: PredefinedReply[];
  pagination = getPaginationInitialState();

  isLoading = true;
  isAlive = true;

  constructor(
    private route: ActivatedRoute,
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.store.dispatch(actionCreators.fetchListAction());

    this.store
      .select('predefinedReplies')
      .takeWhile(() => this.isAlive)
      .subscribe((state: PredefinedRepliesState) => {
        this.isLoading = !selectors.isListFetched(state);
        this.data = selectors.selectAll(state);
        this.pagination = selectors.selectPagination(state);
      });

    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
    this.store.dispatch(actionCreators.fetchListStateResetAction());
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.DELETE_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: DeleteSuccessAction) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'The item has been successfully deleted.'
        );
      });

    this.appAction$
      .ofType(actions.DELETE_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: DeleteFailureAction) => {
        this.toasterService.pop(
          'error',
          'Ooops!',
          'An error occured. Item could not be deleted.'
        );
        this.store.dispatch(
          actionCreators.deleteStateResetAction(action.payload.id)
        );
      });
  }

  fetchData() {
    this.store.dispatch(
      actionCreators.fetchListAction({
        page_number: this.pagination.pageNumber,
        results_per_page: this.pagination.pageSize
      })
    );

    this.isLoading = true;
  }

  handlePageChange(pageNumber: any) {
    this.pagination.pageNumber = pageNumber;
    this.fetchData();
  }

  handleItemDeleteClick(id: number): void {
    this.store.dispatch(actionCreators.deleteAction(id));
  }
}
