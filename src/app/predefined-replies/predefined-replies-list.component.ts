import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PredefinedReply } from './store/predefined-replies.model';

@Component({
  selector: 'predefined-replies-list',
  templateUrl: 'predefined-replies-list.component.html'
})
export class PredefinedRepliesListComponent {
  @Input() data: PredefinedReply[];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  expandedItems = new Array<PredefinedReply>();

  constructor() {}

  toggleItemExpanded(item: PredefinedReply): void {
    if (this.isItemExpanded(item)) {
      this.expandedItems = this.expandedItems.filter(i => i !== item);
    } else {
      this.expandedItems = this.expandedItems.concat(item);
    }
  }

  isItemExpanded(item: PredefinedReply): boolean {
    return !!this.expandedItems.find(i => i === item);
  }

  handleItemDeleteClick(id: number) {
    if (confirm('Are you sure you want to permanently delete this item?')) {
      this.itemDeleteClick.emit(id);
    }
  }
}
