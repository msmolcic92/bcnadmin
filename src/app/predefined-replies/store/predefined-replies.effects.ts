import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { PredefinedRepliesService } from '../../../api-client/predefined-replies.service';
import {
  FetchListAction,
  FetchItemAction,
  CreateAction,
  DeleteAction,
  UpdateAction
} from '../../shared/helpers';
import { actions, actionCreators } from './predefined-replies.actions';

@Injectable()
export class PredefinedRepliesEffects {
  constructor(
    private action$: Actions,
    private service: PredefinedRepliesService
  ) {}

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType<FetchListAction>(actions.FETCH_LIST)
    .switchMap(action => {
      return this.service
        .fetchList(action.payload.filterData)
        .map((response: any) =>
          actionCreators.fetchListSuccessAction(
            response,
            action.payload.filterData
          )
        )
        .catch(errorResponse =>
          Observable.of(
            actionCreators.fetchListFailureAction(errorResponse.error)
          )
        );
    });

  @Effect()
  fetch$: Observable<Action> = this.action$
    .ofType<FetchItemAction>(actions.FETCH_ITEM)
    .switchMap(action => {
      return this.service
        .fetch(action.payload.id)
        .map((response: any) =>
          actionCreators.fetchItemSuccessAction(action.payload.id, response)
        )
        .catch(errorResponse =>
          Observable.of(
            actionCreators.fetchItemFailureAction(
              action.payload.id,
              errorResponse.error
            )
          )
        );
    });

  @Effect()
  create$: Observable<Action> = this.action$
    .ofType<CreateAction>(actions.CREATE)
    .switchMap(action => {
      return this.service
        .create(action.payload.item)
        .map((response: any) =>
          actionCreators.createSuccessAction({
            id: response.id,
            ...action.payload.item
          })
        )
        .catch(errorResponse =>
          Observable.of(actionCreators.createFailureAction(errorResponse.error))
        );
    });

  @Effect()
  update$: Observable<Action> = this.action$
    .ofType<UpdateAction>(actions.UPDATE)
    .switchMap(action => {
      return this.service
        .update(action.payload.item.id, action.payload.item.changes)
        .map((response: any) =>
          actionCreators.updateSuccessAction(action.payload.item)
        )
        .catch(errorResponse =>
          Observable.of(
            actionCreators.updateFailureAction(
              action.payload.item.id,
              errorResponse.error
            )
          )
        );
    });

  @Effect()
  delete$: Observable<Action> = this.action$
    .ofType<DeleteAction>(actions.DELETE)
    .switchMap(action => {
      return this.service
        .delete(action.payload.id)
        .map((response: any) =>
          actionCreators.deleteSuccessAction(action.payload.id)
        )
        .catch(errorResponse =>
          Observable.of(
            actionCreators.deleteFailureAction(
              action.payload.id,
              errorResponse.error
            )
          )
        );
    });
}
