import {
  createDefaultActions,
  makeActionCreatorsFor,
  DefaultActions
} from '../../shared/helpers';
import { PredefinedReply } from './predefined-replies.model';

export const featureName = 'Predefined Replies';
export const actions = createDefaultActions(featureName);
export const actionCreators = makeActionCreatorsFor<
  PredefinedReply,
  DefaultActions
>(actions);
