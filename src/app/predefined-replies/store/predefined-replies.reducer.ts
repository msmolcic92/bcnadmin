import { Action } from '@ngrx/store';
import {
  FeatureState,
  getInitialState,
  createDefaultActionHandlers,
  createDefaultSelectors
} from '../../shared/helpers';
import { PredefinedReply } from './predefined-replies.model';
import { actions } from './predefined-replies.actions';

// Models
export interface PredefinedRepliesState extends FeatureState<PredefinedReply> {}

// Action handlers
const actionHandlers = createDefaultActionHandlers<PredefinedReply>(actions);

// Reducers
export function reducer(
  state: PredefinedRepliesState = getInitialState<PredefinedReply>(),
  action: Action
): PredefinedRepliesState {
  const handler = actionHandlers[action.type];
  return handler ? handler(state, action) : state;
}

// Selectors
export const selectors = createDefaultSelectors<
  PredefinedRepliesState,
  PredefinedReply
>();
