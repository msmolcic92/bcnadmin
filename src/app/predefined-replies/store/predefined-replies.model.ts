export interface PredefinedReply {
  id: number;
  date_added: number;
  name: string;
  content: string;
  is_public: number;
  memid: number;
}
