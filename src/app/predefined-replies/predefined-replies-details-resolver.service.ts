import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { PredefinedReply } from './store/predefined-replies.model';
import { actionCreators } from './store/predefined-replies.actions';
import { PredefinedRepliesState, selectors } from './store/predefined-replies.reducer';

@Injectable()
export class PredefinedRepliesDetailsResolver implements Resolve<PredefinedReply> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot) {
    this.store.dispatch(actionCreators.fetchItemAction(route.params.id));

    return this.store
      .select('predefinedReplies')
      .filter((state: PredefinedRepliesState) =>
        selectors.isSelectedItemFetchCompleted(state)
      )
      .map((state: PredefinedRepliesState) => selectors.getSelectedItem(state))
      .take(1);
  }
}
