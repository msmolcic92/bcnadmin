import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PredefinedReplyFormData } from './predefined-replies-form.component';
import { actionCreators } from './store/predefined-replies.actions';

@Component({
  selector: 'predefined-replies-form-screen',
  templateUrl: 'predefined-replies-form-screen.component.html'
})
export class PredefinedRepliesFormScreenComponent implements OnDestroy, OnInit {
  formData: PredefinedReplyFormData;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (routeData.item) {
        this.formData = {
          id: routeData.item.id,
          name: routeData.item.name,
          content: routeData.item.content,
          is_public: routeData.item.is_public
        };
      }
    });
  }

  ngOnDestroy(): void {
    if (this.isEdit) {
      this.store.dispatch(
        actionCreators.updateStateResetAction(this.formData.id)
      );

      this.store.dispatch(
        actionCreators.fetchItemStateResetAction(this.formData.id)
      );
    } else {
      this.store.dispatch(actionCreators.createStateResetAction());
    }
  }

  handleFormSuccess($event): void {
    this.navigateToList();
  }

  handleFormCancel(): void {
    this.navigateToList();
  }

  navigateToList(): void {
    this.router.navigate(['predefined-replies']);
  }
}
