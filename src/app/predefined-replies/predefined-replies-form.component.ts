import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppActions } from '../shared/store/app-actions.service';
import {
  CreateSuccessAction,
  CreateFailureAction,
  UpdateSuccessAction,
  UpdateFailureAction
} from '../shared/helpers';
import { BaseForm } from '../shared/base-form';
import { actions, actionCreators } from './store/predefined-replies.actions';

export class PredefinedReplyFormData {
  id?: number;
  name?: string;
  content?: string;
  is_public?: number;
}

const FORM_VALIDATION_MESSAGES = {
  name: {
    required: 'Name is required.'
  },
  content: {
    required: 'Content is required.'
  },
  is_public: {
    required: 'Public flag is required.'
  }
};

@Component({
  selector: 'predefined-replies-form',
  templateUrl: 'predefined-replies-form.component.html'
})
export class PredefinedRepliesFormComponent extends BaseForm implements OnInit {
  @Input() formData: PredefinedReplyFormData;
  @Input() isEdit: boolean;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  buildForm(): void {
    if (!this.formData) {
      this.formData = new PredefinedReplyFormData();
    }

    this.form = this.formBuilder.group({
      name: [this.formData.name, Validators.required],
      content: [this.formData.content, Validators.required],
      is_public: [this.formData.is_public || 0, Validators.required]
    });
  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? actions.UPDATE_SUCCESS
      : actions.CREATE_SUCCESS;

    const failureType = this.isEdit
      ? actions.UPDATE_FAILURE
      : actions.CREATE_FAILURE;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: CreateSuccessAction | UpdateSuccessAction) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(failureType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: CreateFailureAction | UpdateFailureAction) => {
        this.onSubmitError(action.payload.error);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const data = { ...this.form.value };
    data.is_public = +data.is_public;

    if (!this.isEdit) {
      this.store.dispatch(actionCreators.createAction(data));
    } else {
      this.store.dispatch(actionCreators.updateAction(this.formData.id, data));
    }
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
