import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

// Global imports for commonly used rxjs operators and observable functions.
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/zip';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/find';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/takeUntil';

import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects'

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { ToasterModule } from 'angular2-toaster/angular2-toaster';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

// App component
import { AppComponent } from './app.component';

// State management
import { reducers } from './core/store/reducers';
import {
  SessionEffects,
  AccountEffects,
  LookupObjectsEffects
} from './core/store/effects';
import { LookupObjectsService } from './shared/lookup-objects.service';

// Routing Module
import { AppRoutingModule } from './app-routing.module';

// Pages
import { PageNotFoundComponent } from './pages/page-not-found.component';
import { InternalServerErrorPageComponent } from './pages/internal-server-error-page.component';

// Core stuff
import { AppLayoutComponent } from './core/app-layout.component';
import { SimpleLayoutComponent } from './core/simple-layout.component';
import { AppHeaderComponent } from './core/app-header.component';
import { AppHeaderAccountBlockComponent } from './core/app-header-account-block.component';
import { LogoutComponent } from './core/logout.component';

// Core / Navigation
import { AppSidebarNavComponent } from './core/nav/app-sidebar-nav.component';
import { AppSidebarNavDefaultMenuComponent } from './core/nav/app-sidebar-nav-default-menu.component';
import { AppSidebarNavSupportBasicMenuComponent } from './core/nav/app-sidebar-nav-support-basic-menu.component';
import { AppSidebarNavSupportPlusMenuComponent } from './core/nav/app-sidebar-nav-support-plus-menu.component';
import { AppSidebarNavSupportDeluxeMenuComponent } from './core/nav/app-sidebar-nav-support-deluxe-menu.component';
import { AppSidebarNavSupportFullMenuComponent } from './core/nav/app-sidebar-nav-support-full-menu.component';
import { AppSidebarNavFullMenuComponent } from './core/nav/app-sidebar-nav-full-menu.component';

// Auth features
import { TokenStorageService } from './auth/token-storage.service';
import { SessionService } from '../api-client/session.service';
import { AuthenticatedGuard } from './auth/authenticated.guard';

// HTTP interceptors
import { authHeaderInterceptorProvider } from './core/auth-header-interceptor.service';
import { httpErrorInterceptorProvider } from './core/http-error-interceptor.service';

// Account feature
// NOTE: This is explicitly importing the entire AccountModule
// which breaks lazy-loading (bundle is pre-packaged in this case)
// but we need this for "store" effects to work.
// Will need to find a workaround.
import { AccountService } from '../api-client/account.service';
import { UnsubscribeListService } from '../api-client/unsubscribe-list.service';
import { TfaService } from 'api-client/tfa.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { MembersService } from '../api-client/members.service';

// Shared features
import { appActionsSubjectProvider } from './shared/store/app-actions-subject.service';
import { AppActions } from './shared/store/app-actions.service';

@NgModule({
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    }),
    EffectsModule.forRoot([
      SessionEffects,
      AccountEffects,
      LookupObjectsEffects,
    ]),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    ToasterModule,
    NgbModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    AppLayoutComponent,
    SimpleLayoutComponent,
    AppHeaderComponent,
    AppHeaderAccountBlockComponent,
    AppSidebarNavComponent,
    AppSidebarNavDefaultMenuComponent,
    AppSidebarNavSupportBasicMenuComponent,
    AppSidebarNavSupportPlusMenuComponent,
    AppSidebarNavSupportDeluxeMenuComponent,
    AppSidebarNavSupportFullMenuComponent,
    AppSidebarNavFullMenuComponent,
    LogoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    PageNotFoundComponent,
    InternalServerErrorPageComponent
  ],
  providers: [
    httpErrorInterceptorProvider,
    authHeaderInterceptorProvider,
    SessionService,
    TokenStorageService,
    AuthenticatedGuard,
    LookupObjectsService,
    AccountService, // Probably TEMP here
    UnsubscribeListService,
    TfaService,
    MembersService,
    appActionsSubjectProvider,
    AppActions,
    { provide: APP_BASE_HREF, useValue: '/' }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

  constructor(
    private sessionService: SessionService
  ) {
    this.sessionService.tryRestoreLocalSession();
  }
}
