import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsRootComponent } from './news-root.component';
import { NewsScreenComponent } from './news-screen.component';
import { NewsDetailsScreenComponent } from './news-details-screen.component';
import { NewsFormScreenComponent } from './news-form-screen.component';
import { NewsDetailsResolver } from './news-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'News'
    },
    component: NewsRootComponent,
    children: [
      {
        path: '',
        data: {
          title: ''
        },
        component: NewsScreenComponent
      },
      {
        path: 'add',
        data: {
          title: 'Add news',
          isEdit: false
        },
        component: NewsFormScreenComponent
      },
      {
        path: ':id',
        data: {
          title: 'Details'
        },
        component: NewsDetailsScreenComponent,
        resolve: {
          item: NewsDetailsResolver
        }
      },
      {
        path: ':id/edit',
        data: {
          title: 'Edit news',
          isEdit: true
        },
        component: NewsFormScreenComponent,
        resolve: {
          item: NewsDetailsResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule {}
