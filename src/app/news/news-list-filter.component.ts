import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { NewsCategoriesState } from '../news-categories/store/news-categories.state';
import { NewsListFilterData } from './store/news.model';
import { NewsState } from './store/news.reducer';
import * as newsCategoriesActions from '../news-categories/store/news-categories.actions';

@Component({
  selector: 'news-list-filter',
  templateUrl: 'news-list-filter.component.html'
})
export class NewsListFilterComponent implements OnInit, OnDestroy, OnChanges {
  @Input() value: NewsListFilterData;
  @Output()
  valueChange: EventEmitter<NewsListFilterData> = new EventEmitter<
    NewsListFilterData
  >();

  isAlive = true;
  form: FormGroup;
  categoryOptionList: Array<any>;

  constructor(private store: Store<any>, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.store.dispatch({
      type: newsCategoriesActions.FETCH_LIST,
      payload: {
        // 1000 - max. number of results to fetch.
        results_per_page: 1000
      }
    });

    this.buildForm();

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe(
        (data: any) => {
          normalizeFilterFormDates(this.form, {
            startDateKey: 'valid_until_from',
            endDateKey: 'valid_until_to'
          });

          normalizeFilterFormDates(this.form, {
            startDateKey: 'valid_from_from',
            endDateKey: 'valid_from_to'
          });
        }
      );

    this.store
      .select('newsCategories')
      .filter((state: NewsCategoriesState) => !state.requesting && !state.hasError)
      .takeWhile(() => this.isAlive)
      .subscribe((state: NewsCategoriesState) => {
        if (state.data && state.data.data) {
          this.categoryOptionList = [
            ...state.data.data
          ];
        }
      });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      valid_from_from: [this.value.valid_from_from],
      valid_from_to: [this.value.valid_from_to],
      valid_until_from: [this.value.valid_until_from],
      valid_until_to: [this.value.valid_until_to],
      status: [this.value.status],
      category_id: [this.value.category_id]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.valid_until_to) {
      formData.valid_until_to = endOfTheDayTimestamp(formData.valid_until_to);
    }

    if (formData.valid_from_to) {
      formData.valid_from_to = endOfTheDayTimestamp(formData.valid_from_to);
    }

    this.valueChange.emit(formData);
  }
}
