import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { NewsItemFormData } from './news-form.component';
import { actionCreators } from './store/news.actions';

@Component({
  selector: 'news-form-screen',
  templateUrl: 'news-form-screen.component.html'
})
export class NewsFormScreenComponent implements OnDestroy, OnInit {
  formData: NewsItemFormData;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (routeData.item) {
        const selectedItem = routeData.item;
        this.formData = {
          id: selectedItem.id,
          categ_id: selectedItem.categ_id,
          title: selectedItem.title,
          content: selectedItem.content,
          valid_from: selectedItem.valid_from,
          valid_until: selectedItem.valid_until,
          status: selectedItem.status
        };
      }
    });
  }

  ngOnDestroy(): void {
    if (this.isEdit) {
      this.store.dispatch(
        actionCreators.updateStateResetAction(this.formData.id)
      );

      this.store.dispatch(
        actionCreators.fetchItemStateResetAction(this.formData.id)
      );
    } else {
      this.store.dispatch(actionCreators.createStateResetAction());
    }
  }

  handleFormSuccess($event): void {
    this.navigateToList();
  }

  handleFormCancel(): void {
    this.navigateToList();
  }

  navigateToList(): void {
    this.router.navigate(['news']);
  }
}
