export interface NewsItem {
  id: number;
  categ_id: number;
  categ_title?: string;
  valid_from?: number;
  valid_until?: number;
  date_modified?: number;
  date_added?: number;
  title: string;
  content: string;
  status?: number;
}

export class NewsListFilterData {
  valid_from_from?: number;
  valid_from_to?: number;
  valid_until_from?: number;
  valid_until_to?: number;
  status?: string;
  category_id?: number;
}
