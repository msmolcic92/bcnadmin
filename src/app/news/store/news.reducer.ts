import { Action } from '@ngrx/store';
import {
  FeatureState,
  getInitialState,
  createDefaultActionHandlers,
  createDefaultSelectors
} from '../../shared/helpers';
import { NewsItem, NewsListFilterData } from './news.model';
import { actions } from './news.actions';

// Models
export interface NewsState extends FeatureState<NewsItem, NewsListFilterData> {}

// Action handlers
const actionHandlers = createDefaultActionHandlers<NewsItem>(actions);

// Reducers
export function reducer(
  state: NewsState = getInitialState<NewsItem, NewsListFilterData>(),
  action: Action
): NewsState {
  const handler = actionHandlers[action.type];
  return handler ? handler(state, action) : state;
}

// Selectors
export const selectors = createDefaultSelectors<
  NewsState,
  NewsItem,
  NewsListFilterData
>();
