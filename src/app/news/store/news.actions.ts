import {
  createDefaultActions,
  makeActionCreatorsFor,
  DefaultActions
} from '../../shared/helpers';
import { NewsItem } from './news.model';

export const featureName = 'News';
export const actions = createDefaultActions(featureName);
export const actionCreators = makeActionCreatorsFor<
  NewsItem,
  DefaultActions
>(actions);
