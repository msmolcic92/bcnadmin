import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NewsEffects } from './store/news.effects';
import { NewsRoutingModule } from './news-routing.module';
import { NewsRootComponent } from './news-root.component';
import { NewsScreenComponent } from './news-screen.component';
import { NewsListComponent } from './news-list.component';
import { NewsListFilterComponent } from './news-list-filter.component';
import { NewsDetailsScreenComponent } from './news-details-screen.component';
import { NewsFormComponent } from './news-form.component';
import { NewsFormScreenComponent } from './news-form-screen.component';
import { NewsService } from '../../api-client/news.service';
import { SharedModule } from '../shared/shared.module';
import { reducer } from './store/news.reducer';
import { NewsCategoriesModule } from '../news-categories/news-categories.module';
import { NewsDetailsResolver } from './news-details-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([NewsEffects]),
    StoreModule.forFeature('news', reducer),
    NewsRoutingModule,
    NgbModule,
    SharedModule,
    NewsCategoriesModule
  ],
  declarations: [
    NewsRootComponent,
    NewsDetailsScreenComponent,
    NewsScreenComponent,
    NewsListComponent,
    NewsListFilterComponent,
    NewsFormComponent,
    NewsFormScreenComponent
  ],
  providers: [NewsService, NewsDetailsResolver]
})
export class NewsModule {}
