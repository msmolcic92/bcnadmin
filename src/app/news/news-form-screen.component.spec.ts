import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  ActivatedRoute,
  ActivatedRouteStub,
  Router,
  RouterLinkStubDirective,
  RouterStub
} from 'testing/router-helpers';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { NewsItem } from './store/news.model';
import { NewsFormScreenComponent } from './news-form-screen.component';
import { NewsItemFormData } from './news-form.component';
import { NewsModule } from './news.module';

describe('NewsFormScreenComponent', () => {
  let component: NewsFormScreenComponent;
  let fixture: ComponentFixture<NewsFormScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  const newsItemMock: NewsItem = {
    id: 1,
    title: 'The News',
    content: 'Awesome content',
    categ_id: 2,
    valid_from: 1510301723,
    valid_until: 1512893723,
    status: 1,
    date_added: 1510301723,
    date_modified: 1510388123
  };

  const formDataMock: NewsItemFormData = {
    id: 1,
    title: 'The News',
    content: 'Awesome content',
    categ_id: 2,
    status: 1,
    valid_from: 1510301723,
    valid_until: 1512893723
  };

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.dataSubject.next({
      item: newsItemMock
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, NewsModule],
        providers: [
          { provide: Router, useClass: RouterStub },
          { provide: ActivatedRoute, useValue: activatedRoute }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsFormScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be pass the item to the component via the router', () => {
    expect(component.formData).toEqual(formDataMock);
  });

  it('should navigate to list when calling handleFormSuccess', () => {
    const routerMock = TestBed.get(Router);
    spyOn(routerMock, 'navigate');
    component.handleFormSuccess({});
    expect(routerMock.navigate).toHaveBeenCalled();
  });

  it('should navigate to list when calling handleFormCancel', () => {
    const routerMock = TestBed.get(Router);
    spyOn(routerMock, 'navigate');
    component.handleFormCancel();
    expect(routerMock.navigate).toHaveBeenCalled();
  });
});
