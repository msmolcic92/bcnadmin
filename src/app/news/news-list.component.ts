import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NewsItem } from './store/news.model';

@Component({
  selector: 'news-list',
  templateUrl: 'news-list.component.html'
})
export class NewsListComponent {
  @Input() data: NewsItem[];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  handleItemDeleteClick(id: number) {
    if (confirm('Are you sure you want to permanently delete this item?')) {
      this.itemDeleteClick.emit(id);
    }
  }
}
