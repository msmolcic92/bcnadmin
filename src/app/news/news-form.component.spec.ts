import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed
} from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { RouterLinkStubDirective } from 'testing/router-helpers';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { StoreMock } from './../../testing/store-helpers';
import { NewsItemFormData, NewsFormComponent } from './news-form.component';
import { NewsModule } from './news.module';

describe('NewsFormComponent', () => {
  let component: NewsFormComponent;
  let fixture: ComponentFixture<NewsFormComponent>;
  let store: StoreMock<any>;
  const formDataMock: NewsItemFormData = {
    id: 1,
    title: 'The Title',
    content: 'Some content',
    categ_id: 1,
    valid_from: 1510301723,
    valid_until: 1512893723,
    status: 1
  };
  const formDataUpdate: NewsItemFormData = {
    title: 'Modified Title',
    content: 'Modified content',
    categ_id: 2,
    valid_from: 1510301723,
    valid_until: 1512893723,
    status: 1
  };
  const formDataInvalid: NewsItemFormData = {
    title: '',
    content: '',
    categ_id: null,
    valid_from: null,
    valid_until: null,
    status: null
  };

  beforeEach(() => {
    store = new StoreMock();
    store.mockState('news', {});
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkStubDirective],
        imports: [SharedModule, AppModule, NewsModule],
        providers: [{ provide: Store, useValue: store }],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsFormComponent);
    component = fixture.componentInstance;
    component.formData = formDataMock;
    component.isEdit = true;
    fixture.detectChanges();
  });

  function updateForm(data: NewsItemFormData) {
    component.form.setValue({
      title: data.title,
      content: data.content,
      categ_id: data.categ_id,
      valid_from: data.valid_from,
      valid_until: data.valid_until,
      status: data.status
    });
  }

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be pass the details to the component via the input variables', () => {
    expect(component.isEdit).toEqual(true);
    expect(component.formData).toEqual(formDataMock);
  });

  it('should create a form object on component instantiation', () => {
    expect(component.form instanceof FormGroup).toBe(true);
  });

  it('should dispatch an create/update action when doing a submit of the form', () => {
    spyOn(store, 'dispatch');
    component.submit();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it(
    'form value should update from form changes and still be valid',
    fakeAsync(() => {
      updateForm(formDataUpdate);
      expect(component.form.value).toEqual(formDataUpdate);
      expect(component.form.valid).toBeTruthy();
    })
  );

  it(
    'form.valid should be false when data is invalid',
    fakeAsync(() => {
      updateForm(formDataInvalid);
      expect(component.form.valid).toBeFalsy();
    })
  );
});
