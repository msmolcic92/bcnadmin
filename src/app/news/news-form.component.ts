import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppActions } from '../shared/store/app-actions.service';
import { normalizeFilterFormDates } from '../shared/helpers/filter-form-helpers';
import {
  CreateSuccessAction,
  CreateFailureAction,
  UpdateSuccessAction,
  UpdateFailureAction
} from '../shared/helpers';
import { BaseForm } from '../shared/base-form';
import { NewsCategoriesState } from '../news-categories/store/news-categories.state';
import { actions as newsActions, actionCreators } from './store/news.actions';
import * as newsCategoriesActions from '../news-categories/store/news-categories.actions';

export class NewsItemFormData {
  id?: number;
  categ_id?: number;
  title?: string;
  content?: string;
  valid_from?: number;
  valid_until?: number;
  status?: number;
}

const FORM_VALIDATION_MESSAGES = {
  categ_id: {
    required: 'Category is required.'
  },
  title: {
    required: 'Title is required.'
  },
  content: {
    required: 'Content is required.'
  },
  valid_from: {
    required: 'Valid from is required.'
  },
  valid_until: {
    required: 'Valid until is required.'
  },
  status: {
    required: 'Status is required.'
  }
};

@Component({
  selector: 'news-form',
  templateUrl: 'news-form.component.html'
})
export class NewsFormComponent extends BaseForm implements OnInit {
  @Input() formData: any;
  @Input() isEdit: boolean;

  categoryOptionList: Array<any>;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    this.store.dispatch({
      type: newsCategoriesActions.FETCH_LIST,
      payload: {
        // Fetching only active categories.
        status: 1,
        // 1000 - max. number of results to fetch.
        results_per_page: 1000
      }
    });

    this.store
      .select('newsCategories')
      .filter(
        (state: NewsCategoriesState) => !state.requesting && !state.hasError
      )
      .takeWhile(() => this.isAlive)
      .subscribe((state: NewsCategoriesState) => {
        if (state.data && state.data.data) {
          this.categoryOptionList = [...state.data.data];
        }
      });

    super.ngOnInit();
    this.subscribeToActions();
  }

  buildForm(): void {
    if (!this.formData) {
      this.formData = new NewsItemFormData();
    }

    this.form = this.formBuilder.group({
      categ_id: [this.formData.categ_id, Validators.required],
      title: [this.formData.title, Validators.required],
      content: [this.formData.content, Validators.required],
      valid_from: [this.formData.valid_from, Validators.required],
      valid_until: [this.formData.valid_until, Validators.required],
      status: [this.formData.status || 0, Validators.required]
    });

    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        normalizeFilterFormDates(this.form, {
          startDateKey: 'valid_from',
          endDateKey: 'valid_until'
        });
      });
  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? newsActions.UPDATE_SUCCESS
      : newsActions.CREATE_SUCCESS;

    const failureType = this.isEdit
      ? newsActions.UPDATE_FAILURE
      : newsActions.CREATE_FAILURE;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: CreateSuccessAction | UpdateSuccessAction) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(failureType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: CreateFailureAction | UpdateFailureAction) => {
        this.onSubmitError(action.payload.error);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const data = { ...this.form.value };
    data.status = +data.status;
    data.categ_id = +data.categ_id;

    if (!this.isEdit) {
      this.store.dispatch(actionCreators.createAction(data));
    } else {
      this.store.dispatch(actionCreators.updateAction(this.formData.id, data));
    }
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
