import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { AppModule } from '../app.module';
import { DateFromUnixPipe } from '../shared/pipes';
import { SharedModule } from '../shared/shared.module';
import {
  ActivatedRoute,
  ActivatedRouteStub,
  Router,
  RouterStub
} from 'testing/router-helpers';
import { StoreMock } from 'testing/store-helpers';
import { getInitialState } from '../shared/helpers';
import { NewsItem, NewsListFilterData } from './store/news.model';
import { NewsScreenComponent } from './news-screen.component';
import { NewsListComponent } from './news-list.component';

describe('NewsScreenComponent', () => {
  const TEST_NEWS = [
    {
      id: 1,
      title: 'First Title',
      content: 'Content Number One',
      categ_id: 1,
      categ_title: 'Default',
      valid_from: 1510301723,
      valid_until: 1512893723,
      status: 1,
      date_added: 1510301723,
      date_modified: 1510388123
    },
    {
      id: 2,
      title: 'Second Title',
      content: 'Content Number Two',
      categ_id: 2,
      categ_title: 'Political',
      valid_from: 1510301723,
      valid_until: 1512893723,
      status: 1,
      date_added: 1510301723,
      date_modified: 1510388123
    },
    {
      id: 3,
      title: 'Third Title',
      content: 'Content Number Three',
      categ_id: 3,
      categ_title: 'Finances',
      valid_from: 1510301723,
      valid_until: 1512893723,
      status: 0,
      date_added: 1510301723,
      date_modified: 1510388123
    }
  ];

  let component: NewsScreenComponent;
  let fixture: ComponentFixture<NewsScreenComponent>;
  let activatedRoute: ActivatedRouteStub;
  let store: StoreMock<any>;

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    store = new StoreMock();
    store.mockState('news', {
      ...getInitialState<NewsItem, NewsListFilterData>(),
      data: {
        ids: [1, 2, 3],
        entities: {
          [1]: TEST_NEWS[0],
          [2]: TEST_NEWS[1],
          [3]: TEST_NEWS[2]
        }
      }
    });
  });

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, AppModule],
        declarations: [NewsScreenComponent, NewsListComponent],
        providers: [
          { provide: Router, useClass: RouterStub },
          { provide: ActivatedRoute, useValue: activatedRoute },
          { provide: Store, useValue: store }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsScreenComponent);
    component = fixture.componentInstance;
    component.data = TEST_NEWS;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should show a spinner if input `isLoading` param is truthy', () => {
    component.isLoading = true;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeTruthy(`<app-spinner> isn't rendered`);
  });

  it('should not show a spinner if input `isLoading` param is falsy', () => {
    component.isLoading = false;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('app-spinner'));
    expect(spinnerEl).toBeNull(`<app-spinner> is rendered but shouldn't be`);
  });

  it('should render the passed news list rows', () => {
    const newsItems = fixture.debugElement.queryAll(
      By.css('.news-list-item')
    );
    expect(newsItems.length).toBe(TEST_NEWS.length);
  });

  it('should render each news information in the table rows', () => {
    // Note: only checking first one
    const newsItem = fixture.debugElement.query(
      By.css('.news-list-item')
    );
    const newsItemTextContent = newsItem.nativeElement.textContent;
    const expectedMember = TEST_NEWS[0];
    expect(newsItemTextContent).toContain(expectedMember.id);
    expect(newsItemTextContent).toContain(expectedMember.categ_title);
    expect(newsItemTextContent).toContain(expectedMember.title);
    expect(newsItemTextContent).toContain(
      new DateFromUnixPipe().transform(expectedMember.valid_from || expectedMember.date_added, 'dateOnly')
    );
    expect(newsItemTextContent).toContain(
      new DateFromUnixPipe().transform(expectedMember.valid_until, 'dateOnly')
    );
    expect(newsItemTextContent).toContain(
      new DateFromUnixPipe().transform(expectedMember.date_modified, 'dateOnly')
    );
    expect(newsItemTextContent).toContain(
      new DateFromUnixPipe().transform(expectedMember.date_added, 'dateOnly')
    );
    expect(newsItemTextContent).toContain(expectedMember.status ? 'Active' : 'Inactive');
  });
});
