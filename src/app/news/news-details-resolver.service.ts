import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { NewsItem } from './store/news.model';
import { actionCreators } from './store/news.actions';
import { NewsState, selectors } from './store/news.reducer';

@Injectable()
export class NewsDetailsResolver implements Resolve<NewsItem> {
  constructor(private router: Router, private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot) {
    this.store.dispatch(actionCreators.fetchItemAction(route.params.id));

    return this.store
      .select('news')
      .filter((state: NewsState) =>
        selectors.isSelectedItemFetchCompleted(state)
      )
      .map((state: NewsState) => selectors.getSelectedItem(state))
      .take(1);
  }
}
