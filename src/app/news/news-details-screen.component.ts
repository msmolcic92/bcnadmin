import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsItem } from './store/news.model';
import { actionCreators } from './store/news.actions';

@Component({
  selector: 'news-details-screen',
  templateUrl: 'news-details-screen.component.html'
})
export class NewsDetailsScreenComponent implements OnDestroy, OnInit {
  item: NewsItem;

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => (this.item = data.item));
  }

  ngOnDestroy(): void {
    this.store.dispatch(actionCreators.fetchItemStateResetAction(this.item.id));
  }
}
