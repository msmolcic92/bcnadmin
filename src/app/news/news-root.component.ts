import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { actionCreators } from './store/news.actions';

@Component({
  template: '<router-outlet></router-outlet>'
})
export class NewsRootComponent implements OnDestroy {
  constructor(private store: Store<any>) {}

  ngOnDestroy() {
    this.store.dispatch(actionCreators.resetListFilterAction());
  }
}
