import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EarningListDataResolver } from './list/earning-list-data-resolver.service';
import { EarningsRootComponent } from './earnings-root.component';
import { EarningsScreenComponent } from './earnings-screen.component';
import { EarningAddUpdateScreenComponent } from './earning-add-update-screen.component';
import { EarningDataResolver } from './earning-data-resolver.service';
import { EarningDetailsScreenComponent } from './earning-details-screen.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Earnings'
    },
    component: EarningsRootComponent,
    children: [
      {
        path: ':currency',
        data: {
          title: ''
        },
        component: EarningsScreenComponent,
        resolve: {
          earnings: EarningListDataResolver
        }
      },
      {
        path: ':currency/add',
        data: {
          title: 'Add Payment Record',
          isEdit: false
        },
        component: EarningAddUpdateScreenComponent
      },
      {
        path: ':currency/:id/edit',
        data: {
          title: 'Update Payment Record',
          isEdit: true
        },
        component: EarningAddUpdateScreenComponent,
        resolve: {
          data: EarningDataResolver
        }
      },
      {
        path: ':currency/:id',
        data: {
          title: 'Payment Record'
        },
        component: EarningDetailsScreenComponent,
        resolve: {
          data: EarningDataResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EarningsRoutingModule { }
