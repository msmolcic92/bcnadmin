import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster/angular2-toaster';
import { AppActions } from '../shared/store/app-actions.service';
import { DataList, PaginationConfig } from '../core/store/helpers';
import {
  Earning,
  EarningsState,
  EarningListFilterData
} from './store/earnings.state';
import * as actions from './store/earnings.actions';

@Component({
  selector: 'earnings-screen',
  templateUrl: 'earnings-screen.component.html'
})
export class EarningsScreenComponent implements OnDestroy, OnInit {
  currency: string;
  earningList: DataList<Earning>;
  filterData = new EarningListFilterData();
  pagination: PaginationConfig = {
    itemPerPage: 25,
    currentPage: 1,
    totalItems: 0
  };

  filterShown = false;
  isTableLoading = true;
  isAlive = true;

  constructor(
    private route: ActivatedRoute,
    private store: Store<any>,
    private appAction$: AppActions,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.earningList = routeData.earnings.earningList;
      this.filterData = routeData.earnings.filterData;
      this.currency = routeData.earnings.currency;

      this.store
        .select('earnings')
        .takeWhile(() => this.isAlive)
        .subscribe((earningsState: EarningsState) => {
          this.isTableLoading =
            earningsState.isFetching || earningsState.isDeleting;

          if (earningsState.isFetchSuccess || earningsState.isDeleteSuccess) {
            if (earningsState.isFetchSuccess) {
              this.store.dispatch({ type: actions.FETCH_LIST_STATE_RESET });
            }

            if (earningsState.isDeleteSuccess) {
              this.store.dispatch({ type: actions.DELETE_ITEM_STATE_RESET });
            }

            this.earningList = earningsState.earningList;
            this.filterData = earningsState.filterData;
            this.currency = earningsState.currency;

            // TODO: Reduce the pagination-related boilerplate
            this.pagination = {
              itemPerPage: this.earningList.results_per_page,
              currentPage: this.earningList.page_number,
              totalItems: this.earningList.total_results
            };
          }
        });
    });

    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  subscribeToActions(): void {
    this.appAction$
      .ofType(actions.DELETE_ITEM_SUCCESS)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'success',
          'Success!',
          'The item has been successfully deleted.'
        );
      });

    this.appAction$
      .ofType(actions.DELETE_ITEM_FAILURE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.toasterService.pop(
          'error',
          'Ooops!',
          'An error occured. Item could not be deleted.'
        );
      });
  }

  fetchData() {
    this.store.dispatch({
      type: actions.FETCH_LIST,
      payload: {
        currency: this.currency,
        filterData: {
          ...this.filterData,
          page_number: this.pagination.currentPage,
          results_per_page: this.pagination.itemPerPage
        }
      }
    });

    this.isTableLoading = true;
  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  handleFilterChange(
    newFilterData: EarningListFilterData = new EarningListFilterData()
  ) {
    this.filterData = newFilterData;
    this.fetchData();
  }

  handlePageChange(pageNumber: any) {
    this.pagination.currentPage = pageNumber;
    this.fetchData();
  }

  handleItemDeleteClick(id: number): void {
    this.store.dispatch({
      type: actions.DELETE_ITEM,
      payload: {
        currency: this.currency,
        id: id
      }
    });
  }
}
