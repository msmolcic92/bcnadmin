import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList } from '../core/store/helpers';
import { Earning, EarningsState } from './store/earnings.state';
import * as actions from './store/earnings.actions';

@Injectable()
export class EarningDataResolver implements Resolve<any> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch({
      type: actions.FETCH_ITEM,
      payload: {
        currency: route.params.currency,
        id: route.params.id
      }
    });

    return this.store
      .select('earnings')
      .filter(
        (earningsState: EarningsState) =>
          earningsState.isFetchSuccess || earningsState.isFetchFailure
      )
      .map((earningsState: EarningsState) => ({
        selectedItem: earningsState.selectedItem,
        currency: earningsState.currency
      }))
      .take(1);
  }
}
