import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { earningsReducer } from './store/earnings.state';
import { EarningsEffects } from './store/earnings.effects';
import { EarningsService } from '../../api-client/earnings.service';
import { EarningsRoutingModule } from './earnings-routing.module';
import { EarningListDataResolver } from './list/earning-list-data-resolver.service';
import { EarningsRootComponent } from './earnings-root.component';
import { EarningsScreenComponent } from './earnings-screen.component';
import { EarningListFilterComponent } from './list/earning-list-filter.component';
import { EarningListComponent } from './list/earning-list.component';
import { EarningAddUpdateScreenComponent } from './earning-add-update-screen.component';
import { EarningFormComponent } from './form/earning-form.component';
import { EarningDataResolver } from './earning-data-resolver.service';
import { EarningDetailsScreenComponent } from './earning-details-screen.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
    EffectsModule.forFeature([EarningsEffects]),
    StoreModule.forFeature('earnings', earningsReducer),
    EarningsRoutingModule
  ],
  declarations: [
    EarningsRootComponent,
    EarningsScreenComponent,
    EarningListFilterComponent,
    EarningListComponent,
    EarningAddUpdateScreenComponent,
    EarningFormComponent,
    EarningDetailsScreenComponent
  ],
  providers: [
    EarningsService,
    EarningListDataResolver,
    EarningDataResolver
  ]
})
export class EarningsModule {}
