import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { DataList } from '../../core/store/helpers';
import { Earning, EarningsState } from '../store/earnings.state';
import * as actions from '../store/earnings.actions';

@Injectable()
export class EarningListDataResolver implements Resolve<any> {
  constructor(private store: Store<any>) {}

  resolve(route: ActivatedRouteSnapshot) {
    const state$ = this.store.select('earnings');

    state$.take(1).subscribe((state: EarningsState) => {
      this.store.dispatch({
        type: actions.FETCH_LIST,
        payload: {
          filterData: state.filterData,
          currency: route.params.currency
        }
      });
    });

    return state$
      .filter(
        (state: EarningsState) => state.isFetchSuccess || state.isFetchFailure
      )
      .take(1);
  }
}
