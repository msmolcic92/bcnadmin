import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Earning } from '../store/earnings.state';

@Component({
  selector: 'earning-list',
  templateUrl: 'earning-list.component.html'
})
export class EarningListComponent {
  @Input() earnings: Earning[];
  @Input() isLoading: boolean;
  @Output() itemDeleteClick: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  handleItemDeleteClick(id: number) {
    if (confirm('Are you sure you want to permanently delete this item?')) {
      this.itemDeleteClick.emit(id);
    }
  }
}
