import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { normalizeFilterFormDates } from 'app/shared/helpers/filter-form-helpers';
import { endOfTheDayTimestamp } from 'app/shared/helpers/date-helpers';
import { EarningListFilterData } from '../store/earnings.state';

@Component({
  selector: 'earning-list-filter',
  templateUrl: 'earning-list-filter.component.html'
})
export class EarningListFilterComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input() value: EarningListFilterData;
  @Output()
  valueChange: EventEmitter<EarningListFilterData> = new EventEmitter<
    EarningListFilterData
  >();

  form: FormGroup;
  formValuesSubscription: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();

    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        normalizeFilterFormDates(this.form);
      }
    );
  }

  ngOnDestroy() {
    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form && changes.value) {
      this.form.patchValue(changes.value.currentValue);
    }
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      date_from_filter: [this.value.date_from_filter],
      date_to_filter: [this.value.date_to_filter]
    });
  }

  submit() {
    this.emitFormValue();
  }

  emitFormValue() {
    const formData = { ...this.form.value };

    if (formData.date_to_filter) {
      formData.date_to_filter = endOfTheDayTimestamp(formData.date_to_filter);
    }

    this.valueChange.emit(formData);
  }
}
