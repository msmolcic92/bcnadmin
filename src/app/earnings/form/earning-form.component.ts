import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Store, Action } from '@ngrx/store';
import { BaseForm } from '../../shared/base-form';
import { AppActions } from '../../shared/store/app-actions.service';
import { EarningsState } from '../store/earnings.state';
import { dateGreaterThanOrEqualToday } from '../../shared/validation/date-greater-than-or-equal-today.validator';
import DateUtility from '../../shared/helpers/date-utility';
import * as actions from '../store/earnings.actions';

export class EarningFormData {
  id?: number;
  timestamp?: number;
  pay_mine1?: number;
  pay_mine2?: number;
  pay_mine3?: number;
}

const PAY_AMOUNT_MIN = 0.0000001;
const PAY_AMOUNT_MIN_MESSAGE = `Pay amount must be at least ${PAY_AMOUNT_MIN.toFixed(
  7
)}`;

const FORM_VALIDATION_MESSAGES = {
  timestamp: {
    required: 'Timestamp is required.',
    dateGreaterThanOrEqualToday:
      "The time must be greater than or equal to today's date."
  },
  pay_mine1: {
    required: 'Pay amount for mine 1 is required.',
    min: PAY_AMOUNT_MIN_MESSAGE
  },
  pay_mine2: {
    required: 'Pay amount for mine 2 is required.',
    min: PAY_AMOUNT_MIN_MESSAGE
  },
  pay_mine3: {
    required: 'Pay amount for mine 3 is required.',
    min: PAY_AMOUNT_MIN_MESSAGE
  }
};

@Component({
  selector: 'earning-form',
  templateUrl: 'earning-form.component.html'
})
export class EarningFormComponent extends BaseForm implements OnInit {
  @Input() currency: string;
  @Input() formData: any;
  @Input() isEdit: boolean;

  payAmountMinStep = PAY_AMOUNT_MIN;

  constructor(
    private store: Store<any>,
    private appAction$: AppActions,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(FORM_VALIDATION_MESSAGES);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribeToActions();
  }

  protected buildForm(): void {
    if (!this.formData) {
      this.formData = new EarningFormData();
    }

    this.form = this.formBuilder.group({
      timestamp: [
        this.formData.timestamp,
        Validators.compose([dateGreaterThanOrEqualToday(), Validators.required])
      ],
      pay_mine1: [
        this.formData.pay_mine1,
        Validators.compose([
          Validators.min(PAY_AMOUNT_MIN),
          Validators.required
        ])
      ],
      pay_mine2: [
        this.formData.pay_mine2,
        Validators.compose([
          Validators.min(PAY_AMOUNT_MIN),
          Validators.required
        ])
      ],
      pay_mine3: [
        this.formData.pay_mine3,
        Validators.compose([
          Validators.min(PAY_AMOUNT_MIN),
          Validators.required
        ])
      ]
    });
  }

  subscribeToActions(): void {
    const successType = this.isEdit
      ? actions.UPDATE_ITEM_SUCCESS
      : actions.ADD_ITEM_SUCCESS;
    const failureType = this.isEdit
      ? actions.UPDATE_ITEM_FAILURE
      : actions.ADD_ITEM_FAILURE;

    this.appAction$
      .ofType(successType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.formSuccess.next(true);
      });

    this.appAction$
      .ofType(failureType)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        this.onSubmitError((<any>action).payload);
      });
  }

  submit(): void {
    this.clearFormErrors();

    const actionType = this.isEdit ? actions.UPDATE_ITEM : actions.ADD_ITEM;

    const data = { ...this.form.value };

    this.store.dispatch({
      type: actionType,
      payload: {
        currency: this.currency,
        id: this.formData.id,
        data: data
      }
    });
  }

  cancel(): void {
    this.formCancel.next(true);
  }
}
