import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Earning, EarningsState } from './store/earnings.state';
import { EarningFormData } from './form/earning-form.component';
import * as actions from './store/earnings.actions';

@Component({
  selector: 'earning-add-update-screen',
  templateUrl: 'earning-add-update-screen.component.html'
})
export class EarningAddUpdateScreenComponent implements OnDestroy, OnInit {
  formData: EarningFormData;
  currency: string;
  isEdit: boolean;

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(paramsData => {
      this.currency = paramsData.currency;
    });

    this.route.data.subscribe(routeData => {
      this.isEdit = routeData.isEdit;

      if (routeData.data) {
        const selectedItem = routeData.data.selectedItem;
        this.formData = {
          id: selectedItem.id,
          pay_mine1: +selectedItem.pay_mine1,
          pay_mine2: +selectedItem.pay_mine2,
          pay_mine3: +selectedItem.pay_mine3,
          timestamp: selectedItem.timestamp
        };
      }
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
  }

  handleFormSuccess(response): void {
    this.navigateToEarningList();
  }

  handleFormCancel(): void {
    this.navigateToEarningList();
  }

  navigateToEarningList(): void {
    this.router.navigate(['earnings', this.currency]);
  }
}
