import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Earning } from './store/earnings.state';
import * as actions from './store/earnings.actions';

@Component({
  selector: 'earning-details-screen',
  templateUrl: 'earning-details-screen.component.html'
})
export class EarningDetailsScreenComponent implements OnDestroy, OnInit {
  data: Earning;
  currency: string;

  constructor(private store: Store<any>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.data = routeData.data.selectedItem;
      this.currency = routeData.data.currency;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch({ type: actions.FETCH_ITEM_STATE_RESET });
  }
}
