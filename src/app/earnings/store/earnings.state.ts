import { Action } from '@ngrx/store';
import { DataList } from '../../core/store/helpers';
import * as actions from './earnings.actions';

// Model

export class Earning {
  id: number;
  date: string;
  timestamp: number;
  pay_mine1: string;
  pay_mine2: string;
  pay_mine3: string;
  cron_start: number;
  cron_end: number;
  memory_peak: number;
  result: string;
  status_data: string; // Possible 'status_data' options: Paid, Not Paid, Paying.
  memory_data: string;
  runtime_data: string;
  action_data: {
    total_paid: string;
    total_wallet: string;
    total_reinvested: string;
    mines_wallet: {
      mine1: string;
      mine2: string;
      mine3: string;
    };
    mines_reinvestment: {
      mine1: string;
      mine2: string;
      mine3: string;
    };
  };
}

export class EarningListFilterData {
  page_number?: number;
  results_per_page?: number;
  date_from_filter?: number;
  date_to_filter?: number;
}

export interface EarningsState {
  earningList: DataList<Earning>;
  filterData: EarningListFilterData;
  currency: string;
  selectedItem: Earning;
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchFailure: boolean;
  isDeleting: boolean;
  isDeleteSuccess: boolean;
  isDeleteFailure: boolean;
}

export const INITIAL_STATE: EarningsState = {
  earningList: null,
  filterData: new EarningListFilterData(),
  currency: null,
  selectedItem: null,
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false,
  isDeleting: false,
  isDeleteSuccess: false,
  isDeleteFailure: false
};

// Reducers

export function earningsReducer(
  state: EarningsState = INITIAL_STATE,
  action: Action
): EarningsState {
  switch (action.type) {
    case actions.FETCH_LIST:
    case actions.FETCH_ITEM:
      return {
        ...state,
        isFetching: true,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_LIST_SUCCESS:
      return {
        ...state,
        earningList: {
          ...state.earningList,
          ...(<any>action).payload.data
        },
        filterData: {
          ...state.filterData,
          ...(<any>action).payload.filterData
        },
        currency: (<any>action).payload.currency,
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_SUCCESS:
      return {
        ...state,
        selectedItem: { ...(<any>action).payload.data },
        currency: (<any>action).payload.currency,
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    // TODO: These should be separate concerns,
    // failing to load a single item shouldn't
    // swipe the filters and list state out!
    // TODO: Refactor!
    case actions.FETCH_LIST_FAILURE:
    case actions.FETCH_ITEM_FAILURE:
      return {
        ...state,
        earningList: null,
        filterData: null,
        selectedItem: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    case actions.FETCH_LIST_STATE_RESET:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.FETCH_ITEM_STATE_RESET:
      return {
        ...state,
        selectedItem: null,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: false
      };

    case actions.RESET_LIST_FILTER:
      return {
        ...state,
        filterData: null
      };

    case actions.DELETE_ITEM:
      return {
        ...state,
        isDeleting: true,
        isDeleteSuccess: false,
        isDeleteFailure: false
      };

    case actions.DELETE_ITEM_SUCCESS:
      const totalResults = state.earningList.total_results - 1;
      const lastPage = Math.ceil(
        totalResults / state.earningList.results_per_page
      );
      const pageNumber =
        state.earningList.page_number > lastPage
          ? lastPage
          : state.earningList.page_number;

      return {
        ...state,
        earningList: {
          results_per_page: state.earningList.results_per_page,
          last_page: lastPage,
          page_number: pageNumber,
          total_results: totalResults,
          data: state.earningList.data.filter(
            earning => earning.id !== (<any>action).payload.id
          )
        },
        isDeleting: false,
        isDeleteSuccess: true,
        isDeleteFailure: false
      };

    case actions.DELETE_ITEM_FAILURE:
      return {
        ...state,
        isDeleting: false,
        isDeleteSuccess: false,
        isDeleteFailure: true
      };

    case actions.DELETE_ITEM_STATE_RESET:
      return {
        ...state,
        isDeleting: false,
        isDeleteSuccess: false,
        isDeleteFailure: false
      };

    default:
      return state;
  }
}
