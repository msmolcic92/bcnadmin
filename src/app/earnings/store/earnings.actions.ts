// Action types

export const FETCH_LIST = 'earnings/FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'earnings/FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'earnings/FETCH_LIST_FAILURE';
export const FETCH_LIST_STATE_RESET = 'earnings/FETCH_LIST_STATE_RESET';

export const FETCH_ITEM = 'earnings/FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'earnings/FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_FAILURE = 'earnings/FETCH_ITEM_FAILURE';
export const FETCH_ITEM_STATE_RESET = 'earnings/FETCH_ITEM_STATE_RESET';
export const RESET_LIST_FILTER = 'earnings/RESET_LIST_FILTER';

export const ADD_ITEM = 'earnings/ADD_ITEM';
export const ADD_ITEM_SUCCESS = 'earnings/ADD_ITEM_SUCCESS';
export const ADD_ITEM_FAILURE = 'earnings/ADD_ITEM_FAILURE';

export const UPDATE_ITEM = 'earnings/UPDATE_ITEM';
export const UPDATE_ITEM_SUCCESS = 'earnings/UPDATE_ITEM_SUCCESS';
export const UPDATE_ITEM_FAILURE = 'earnings/UPDATE_ITEM_FAILURE';

export const DELETE_ITEM = 'earnings/DELETE_ITEM';
export const DELETE_ITEM_SUCCESS = 'earnings/DELETE_ITEM_SUCCESS';
export const DELETE_ITEM_FAILURE = 'earnings/DELETE_ITEM_FAILURE';
export const DELETE_ITEM_STATE_RESET = 'earnings/DELETE_ITEM_STATE_RESET';
