import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { EarningsService } from '../../../api-client/earnings.service';
import { EarningListFilterData } from './earnings.state';
import * as actions from './earnings.actions';

@Injectable()
export class EarningsEffects {

  constructor(
    private action$: Actions,
    private earningsService: EarningsService
  ) { }

  @Effect()
  fetchList$: Observable<Action> = this.action$
    .ofType(actions.FETCH_LIST)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.earningsService
        .fetchList(payload.currency, payload.filterData)
        .map((response: any) => ({
          type: actions.FETCH_LIST_SUCCESS,
          payload: {
            data: response,
            filterData: payload.filterData,
            currency: payload.currency
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.FETCH_LIST_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  fetchItem$: Observable<Action> = this.action$
    .ofType(actions.FETCH_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.earningsService
        .fetch(payload.currency, payload.id)
        .map((response: any) => ({
          type: actions.FETCH_ITEM_SUCCESS,
          payload: {
            data: response,
            currency: payload.currency
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.FETCH_ITEM_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  addItem$: Observable<Action> = this.action$
    .ofType(actions.ADD_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.earningsService
        .create(payload.currency, payload.data)
        .map((response: any) => ({
          type: actions.ADD_ITEM_SUCCESS,
          payload: response
        }))
        .catch(errorResponse => Observable.of({
          type: actions.ADD_ITEM_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  updateItem$: Observable<Action> = this.action$
    .ofType(actions.UPDATE_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.earningsService
        .update(payload.currency, payload.id, payload.data)
        .map((response: any) => ({
          type: actions.UPDATE_ITEM_SUCCESS
        }))
        .catch(errorResponse => Observable.of({
          type: actions.UPDATE_ITEM_FAILURE,
          payload: errorResponse.error
        }));
    });

  @Effect()
  deleteItem$: Observable<Action> = this.action$
    .ofType(actions.DELETE_ITEM)
    .map(toPayload)
    .switchMap((payload: any) => {
      return this.earningsService
        .delete(payload.currency, payload.id)
        .map((response: any) => ({
          type: actions.DELETE_ITEM_SUCCESS,
          payload: {
            id: payload.id
          }
        }))
        .catch(errorResponse => Observable.of({
          type: actions.DELETE_ITEM_FAILURE,
          payload: errorResponse.error
        }));
    });
}
