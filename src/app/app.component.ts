import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel
} from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AppActions } from './shared/store/app-actions.service';
import * as navbarActions from './shared/store/navbar.actions';

const SIDEBAR_MOBILE_SHOW_CLASS_NAME = 'sidebar-mobile-show';

@Component({
  selector: 'body',
  template: `
    <router-outlet></router-outlet>
    <toaster-container [toasterconfig]="toasterConfig"></toaster-container>
  `
})
export class AppComponent implements OnDestroy, OnInit {
  @HostBinding('class') public mobileSidebarClass;

  toasterConfig: ToasterConfig = new ToasterConfig({
    tapToDismiss: true,
    animation: 'fade'
  });

  isAlive = true;

  constructor(
    private router: Router,
    private slimLoadingBarService: SlimLoadingBarService,
    private appActions$: AppActions,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.slimLoadingBarService.reset();
        this.slimLoadingBarService.progress = 30;
        this.slimLoadingBarService.start();
        // Close the navigation menu on mobile devices when navigated.
        this.store.dispatch({ type: navbarActions.CLOSE });
      } else if (event instanceof NavigationEnd) {
        this.slimLoadingBarService.complete();
        window.scrollTo(0, 0);
      } else if (event instanceof NavigationCancel) {
        this.slimLoadingBarService.complete();
      }
    });

    this.subscribeToActions();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  subscribeToActions(): void {
    this.appActions$
      .ofType(navbarActions.OPEN, navbarActions.CLOSE, navbarActions.TOGGLE)
      .takeWhile(() => this.isAlive)
      .subscribe((action: Action) => {
        switch (action.type) {
          case navbarActions.CLOSE:
            this.mobileSidebarClass = '';
            break;
          case navbarActions.OPEN:
            this.mobileSidebarClass = SIDEBAR_MOBILE_SHOW_CLASS_NAME;
            break;
          case navbarActions.TOGGLE:
            if (this.mobileSidebarClass) {
              this.mobileSidebarClass = '';
            } else {
              this.mobileSidebarClass = SIDEBAR_MOBILE_SHOW_CLASS_NAME;
            }
            break;
        }
      });
  }
}
