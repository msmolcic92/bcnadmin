import { Action } from '@ngrx/store';
import * as actions from './lookup-objects.actions';

// Member ranks

export interface MemberRank {
  rank: number;
  rank_label?: string;
}

export interface MemberRanksState {
  entities: MemberRank[];
  isFetching?: boolean;
  isFetchSuccess?: boolean;
  isFetchFailure?: boolean;
}

const MEMBER_RANKS_INITIAL_STATE: MemberRanksState = {
  entities: [],
  isFetching: false,
  isFetchSuccess: false,
  isFetchFailure: false
};

// Reducers

export function memberRanksReducer(
  state: MemberRanksState = MEMBER_RANKS_INITIAL_STATE,
  action: Action
): MemberRanksState {
  switch (action.type) {
    case actions.MEMBER_RANKS_FETCH:
      return {
        ...state,
        isFetching: true
      };

    case actions.MEMBER_RANKS_FETCH_SUCCESS:
      return {
        ...state,
        entities: (<any>action).payload,
        isFetching: false,
        isFetchSuccess: true,
        isFetchFailure: false
      };

    case actions.MEMBER_RANKS_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFetchSuccess: false,
        isFetchFailure: true
      };

    default:
      return state;
  }
}
