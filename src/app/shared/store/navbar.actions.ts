export const OPEN = '[Navbar] Open';
export const CLOSE = '[Navbar] Close';
export const TOGGLE = '[Navbar] Toggle';
