// Action types

export const MEMBER_RANKS_FETCH = 'lookupObjects/MEMBER_RANKS_FETCH';
export const MEMBER_RANKS_FETCH_SUCCESS = 'lookupObjects/MEMBER_RANKS_FETCH_SUCCESS';
export const MEMBER_RANKS_FETCH_FAILURE = 'lookupObjects/MEMBER_RANKS_FETCH_FAILURE';
