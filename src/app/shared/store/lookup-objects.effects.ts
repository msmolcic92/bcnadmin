import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { makeSimpleEffect } from '../../core/store/helpers';
import { LookupObjectsService } from '../lookup-objects.service';
import * as actions from './lookup-objects.actions';

@Injectable()
export class LookupObjectsEffects {

  constructor(
    private action$: Actions,
    private lookupObjectsService: LookupObjectsService
  ) { }

  @Effect()
  memberRanksFetch$: Observable<Action> = makeSimpleEffect(
    this.action$,
    () => this.lookupObjectsService.fetchMemberRanks(),
    actions.MEMBER_RANKS_FETCH,
    actions.MEMBER_RANKS_FETCH_SUCCESS,
    actions.MEMBER_RANKS_FETCH_FAILURE
  );
}
