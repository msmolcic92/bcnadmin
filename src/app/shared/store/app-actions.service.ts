import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

// NOTE: See this about the "Dispatcher" subscriptions approach:
// https://netbasal.com/listening-for-actions-in-ngrx-store-a699206d2210

@Injectable()
export class AppActions {
  _actionSubject = new Subject<Action>();

  ofType(...actionTypes) {
    return this._actionSubject
      .filter((action: Action) => actionTypes.includes(action.type));
  }

  nextAction(action: Action) {
    this._actionSubject.next(action);
  }
}
