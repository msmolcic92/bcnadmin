import { Injectable } from '@angular/core';
import { Action, ActionsSubject } from '@ngrx/store';
import { AppActions } from './app-actions.service';

// NOTE: See this about the "Dispatcher" subscriptions approach:
// https://netbasal.com/listening-for-actions-in-ngrx-store-a699206d2210

// NOTE: We will need to use ActionsSubject instead, once we're upgraded to ngrx/store 4.x

@Injectable()
export class AppActionsSubject extends ActionsSubject {
  constructor(
    private appAction$: AppActions
  ) {
    super();
  }

  next(action: Action) {
    super.next(action);
    this.appAction$.nextAction(action);
  }
}

export const appActionsSubjectProvider = {
  provide: ActionsSubject,
  useClass: AppActionsSubject
};
