import { FormControl } from '@angular/forms';

export function equalsTo(otherCtrlName: string) {

  let thisCtrl: FormControl;
  let otherCtrl: FormControl;

  return function validate(control: FormControl) {

    if (!control.parent) {
      return null;
    }

    if (!thisCtrl) {
      thisCtrl = control;
      otherCtrl = control.parent.get(otherCtrlName) as FormControl;

      if (!otherCtrl) {
        throw new Error(`validate(): ${otherCtrlName} control is not found in the parent group`);
      }

      otherCtrl.valueChanges.subscribe(() => {
        thisCtrl.updateValueAndValidity();
      });
    }

    if (!otherCtrl) {
      return null;
    }

    if (otherCtrl.value !== thisCtrl.value) {
      return {
        equalsTo: true
      };
    }

    return null;
  }

}