import { ValidatorFn, AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export function dateGreaterThanOrEqualToday(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (!control || !control.value) {
      return null;
    }

    const utcNow = moment.utc().startOf('day').unix();
    const utcDate = control.value;

    if (utcDate < utcNow) {
      return {
        dateGreaterThanOrEqualToday: true
      };
    }

    return null;
  };
}
