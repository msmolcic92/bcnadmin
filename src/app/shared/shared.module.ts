import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { memberRanksReducer } from './store/lookup-objects.state';
import {
  ThreeStateCheckboxComponent,
  BCNFieldErrorComponent,
  BCNFormErrorComponent,
  BCNFormGroupComponent,
  SpinnerComponent,
  DateInputComponent,
  NumberInputComponent,
  ConfirmSelectComponent,
  PaginationComponent
} from './components';
import { DateFromUnixPipe } from './pipes/date-from-unix.pipe';
import { Nl2BrPipe } from './pipes/nl2br.pipe';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    StoreModule.forFeature('memberRanks', memberRanksReducer)
  ],
  exports: [
    DateFromUnixPipe,
    Nl2BrPipe,
    ThreeStateCheckboxComponent,
    BCNFieldErrorComponent,
    BCNFormErrorComponent,
    BCNFormGroupComponent,
    SpinnerComponent,
    DateInputComponent,
    NumberInputComponent,
    ConfirmSelectComponent,
    PaginationComponent
  ],
  declarations: [
    DateFromUnixPipe,
    Nl2BrPipe,
    ThreeStateCheckboxComponent,
    BCNFieldErrorComponent,
    BCNFormErrorComponent,
    BCNFormGroupComponent,
    SpinnerComponent,
    DateInputComponent,
    NumberInputComponent,
    ConfirmSelectComponent,
    PaginationComponent
  ],
  providers: [DatePipe]
})
export class SharedModule {}
