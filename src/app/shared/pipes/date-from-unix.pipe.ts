import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

export const DEFAULT_DATE_FORMAT = 'MM/DD/YYYY';
export const DEFAULT_TIME_FORMAT = 'hh:mm a';
export const DEFAULT_DATETIME_FORMAT = `${DEFAULT_DATE_FORMAT} ${DEFAULT_TIME_FORMAT}`;

@Pipe({
  name: 'dateFromUnix'
})
export class DateFromUnixPipe implements PipeTransform {
  transform(value: number, formatToken?: string): string {
    if (!value) {
      return '';
    }

    const format = formatToken
      ? (formatToken === 'dateOnly' && DEFAULT_DATE_FORMAT) ||
        (formatToken === 'timeOnly' && DEFAULT_TIME_FORMAT) ||
        formatToken
      : DEFAULT_DATETIME_FORMAT;

    return moment.utc(moment.unix(value)).format(format);
  }
}
