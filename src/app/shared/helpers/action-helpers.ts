import { Action } from '@ngrx/store';

// *** Actions *** //
export interface DefaultActions {
  FETCH_LIST: string;
  FETCH_LIST_SUCCESS: string;
  FETCH_LIST_FAILURE: string;
  FETCH_LIST_STATE_RESET: string;
  RESET_LIST_FILTER: string;

  FETCH_ITEM: string;
  FETCH_ITEM_SUCCESS: string;
  FETCH_ITEM_FAILURE: string;
  FETCH_ITEM_STATE_RESET: string;

  CREATE: string;
  CREATE_SUCCESS: string;
  CREATE_FAILURE: string;
  CREATE_STATE_RESET: string;

  UPDATE: string;
  UPDATE_SUCCESS: string;
  UPDATE_FAILURE: string;
  UPDATE_STATE_RESET: string;

  DELETE: string;
  DELETE_SUCCESS: string;
  DELETE_FAILURE: string;
  DELETE_STATE_RESET: string;
}

export function createDefaultActions(featureName: string): DefaultActions {
  return {
    FETCH_LIST: `[${featureName}] Fetch List`,
    FETCH_LIST_SUCCESS: `[${featureName}] Fetch List Success`,
    FETCH_LIST_FAILURE: `[${featureName}] Fetch List Failure`,
    FETCH_LIST_STATE_RESET: `[${featureName}] Fetch List State Reset`,
    RESET_LIST_FILTER: `[${featureName}] Reset List Filter`,

    FETCH_ITEM: `[${featureName}] Fetch Item`,
    FETCH_ITEM_SUCCESS: `[${featureName}] Fetch Item Success`,
    FETCH_ITEM_FAILURE: `[${featureName}] Fetch Item Failure`,
    FETCH_ITEM_STATE_RESET: `[${featureName}] Fetch Item State Reset`,

    CREATE: `[${featureName}] Create`,
    CREATE_SUCCESS: `[${featureName}] Create Success`,
    CREATE_FAILURE: `[${featureName}] Create Failure`,
    CREATE_STATE_RESET: `[${featureName}] Create State Reset`,

    UPDATE: `[${featureName}] Update`,
    UPDATE_SUCCESS: `[${featureName}] Update Success`,
    UPDATE_FAILURE: `[${featureName}] Update Failure`,
    UPDATE_STATE_RESET: `[${featureName}] Update State Reset`,

    DELETE: `[${featureName}] Delete`,
    DELETE_SUCCESS: `[${featureName}] Delete Success`,
    DELETE_FAILURE: `[${featureName}] Delete Failure`,
    DELETE_STATE_RESET: `[${featureName}] Delete State Reset`
  };
}

// *** Typed actions *** //

// Fetch list
export class FetchListAction implements Action {
  readonly type;

  constructor(type: string, public payload: { filterData?: any }) {
    this.type = type;
  }
}

export class FetchListSuccessAction implements Action {
  readonly type;

  constructor(type: string, public payload: { data: any; filterData?: any }) {
    this.type = type;
  }
}

export class FetchListFailureAction implements Action {
  readonly type;

  constructor(type: string, public payload: { error: any }) {
    this.type = type;
  }
}

export class FetchListStateResetAction implements Action {
  readonly type;

  constructor(type: string) {
    this.type = type;
  }
}

export class ResetListFilterAction implements Action {
  readonly type;

  constructor(type: string) {
    this.type = type;
  }
}

// Fetch item
export class FetchItemAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

export class FetchItemSuccessAction implements Action {
  readonly type;

  constructor(
    type: string,
    public payload: { item: { id: number; changes: any } }
  ) {
    this.type = type;
  }
}

export class FetchItemFailureAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number; error: any }) {
    this.type = type;
  }
}

export class FetchItemStateResetAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

// Create
export class CreateAction implements Action {
  readonly type;

  constructor(type: string, public payload: { item: any }) {
    this.type = type;
  }
}

export class CreateSuccessAction implements Action {
  readonly type;

  constructor(type: string, public payload: { item: any }) {
    this.type = type;
  }
}

export class CreateFailureAction implements Action {
  readonly type;

  constructor(type: string, public payload: { error: any }) {
    this.type = type;
  }
}

export class CreateStateResetAction implements Action {
  readonly type;

  constructor(type: string) {
    this.type = type;
  }
}

// Update
export class UpdateAction implements Action {
  readonly type;

  constructor(
    type: string,
    public payload: { item: { id: number; changes: any } }
  ) {
    this.type = type;
  }
}

export class UpdateSuccessAction implements Action {
  readonly type;

  constructor(
    type: string,
    public payload: { item: { id: number; changes: any } }
  ) {
    this.type = type;
  }
}

export class UpdateFailureAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number; error: any }) {
    this.type = type;
  }
}

export class UpdateStateResetAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

// Delete
export class DeleteAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

export class DeleteSuccessAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

export class DeleteFailureAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number; error: any }) {
    this.type = type;
  }
}

export class DeleteStateResetAction implements Action {
  readonly type;

  constructor(type: string, public payload: { id: number }) {
    this.type = type;
  }
}

export function makeActionCreatorsFor<T, A extends DefaultActions>(actions: A) {
  return {
    fetchListAction: (filterData?: any) => {
      return new FetchListAction(actions.FETCH_LIST, { filterData });
    },
    fetchListSuccessAction: (data: any, filterData?: any) => {
      return new FetchListSuccessAction(actions.FETCH_LIST_SUCCESS, {
        data,
        filterData
      });
    },
    fetchListFailureAction: (error: any) => {
      return new FetchListFailureAction(actions.FETCH_LIST_FAILURE, { error });
    },
    fetchListStateResetAction: () => {
      return new FetchListStateResetAction(actions.FETCH_LIST_STATE_RESET);
    },
    resetListFilterAction: () => {
      return new ResetListFilterAction(actions.RESET_LIST_FILTER);
    },
    fetchItemAction: (id: number) => {
      return new FetchItemAction(actions.FETCH_ITEM, { id });
    },
    fetchItemSuccessAction: (id: number, changes: T) => {
      return new FetchItemSuccessAction(actions.FETCH_ITEM_SUCCESS, {
        item: { id, changes }
      });
    },
    fetchItemFailureAction: (id: number, error: any) => {
      return new FetchItemFailureAction(actions.FETCH_ITEM_FAILURE, {
        id,
        error
      });
    },
    fetchItemStateResetAction: (id: number) => {
      return new FetchItemStateResetAction(actions.FETCH_ITEM_STATE_RESET, {
        id
      });
    },
    createAction: (item: T) => {
      return new CreateAction(actions.CREATE, { item });
    },
    createSuccessAction: (item: T) => {
      return new CreateSuccessAction(actions.CREATE_SUCCESS, { item });
    },
    createFailureAction: (error: any) => {
      return new CreateFailureAction(actions.CREATE_FAILURE, { error });
    },
    createStateResetAction: () => {
      return new CreateStateResetAction(actions.CREATE_STATE_RESET);
    },
    updateAction: (id: number, changes: T) => {
      return new UpdateAction(actions.UPDATE, {
        item: { id, changes }
      });
    },
    updateSuccessAction: (item: { id: number; changes: T }) => {
      return new UpdateSuccessAction(actions.UPDATE_SUCCESS, { item });
    },
    updateFailureAction: (id: number, error: any) => {
      return new UpdateFailureAction(actions.UPDATE_FAILURE, { id, error });
    },
    updateStateResetAction: (id: number) => {
      return new UpdateStateResetAction(actions.UPDATE_STATE_RESET, { id });
    },
    deleteAction: (id: number) => {
      return new DeleteAction(actions.DELETE, { id });
    },
    deleteSuccessAction: (id: number) => {
      return new DeleteSuccessAction(actions.DELETE_SUCCESS, { id });
    },
    deleteFailureAction: (id: number, error: any) => {
      return new DeleteFailureAction(actions.DELETE_SUCCESS, { id, error });
    },
    deleteStateResetAction: (id: number) => {
      return new DeleteStateResetAction(actions.DELETE_STATE_RESET, { id });
    }
  };
}
