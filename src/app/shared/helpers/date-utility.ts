import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

export default class DateUtility {
  /**
   * Converts the provided angular bootstrap date structure value
   * into a unix timestamp value.
   * @param date Angular bootstrap date structure value.
   */
  static convertNgbDateToUnixTimestamp(date: NgbDateStruct): number {
    if (!date) {
      return null;
    }

    return moment.utc({
      year: date.year,
      month: date.month - 1,
      date: date.day
    }).unix();
  }

  /**
   * Converts the provided unix timestamp value into an angular
   * bootstrap date structure value.
   * @param timestamp Unix timestamp value.
   */
  static convertUnixTimestampToNgbDate(timestamp: number): NgbDateStruct {
    if (!timestamp) {
      return null;
    }

    const date = moment.unix(timestamp).utc();

    return {
      year: date.year(),
      month: date.month() + 1,
      day: date.date()
    };
  }
}
