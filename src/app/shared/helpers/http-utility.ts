import { HttpParams } from '@angular/common/http';

export default class HttpUtility {
  /**
   * Creates URL search parameters for the HTTP request.
   * @param params Input parameters.
   */
  static createQueryParameters(params: any): HttpParams {
    if (!params) {
      return null;
    }

    let queryParameters = new HttpParams();

    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const value = params[key];
        if (value !== undefined && value !== null && value !== '') {
          queryParameters = queryParameters.set(key, value);
        }
      }
    }

    return queryParameters;
  }
}
