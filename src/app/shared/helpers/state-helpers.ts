import { createSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { EntitySelectorsNum } from '@ngrx/entity/src/models';
import {
  createDefaultActions,
  DefaultActions,
  FetchListSuccessAction,
  FetchItemAction,
  FetchItemSuccessAction,
  FetchItemFailureAction,
  FetchItemStateResetAction,
  CreateSuccessAction,
  UpdateAction,
  UpdateSuccessAction,
  UpdateFailureAction,
  UpdateStateResetAction,
  DeleteAction,
  DeleteSuccessAction,
  DeleteFailureAction,
  DeleteStateResetAction
} from './action-helpers';

// Models
export class RequestStatus {
  public static get Initial(): string {
    return 'initial';
  }

  public static get Pending(): string {
    return 'pending';
  }

  public static get Success(): string {
    return 'success';
  }

  public static get Failure(): string {
    return 'failure';
  }
}

export enum PaginationChange {
  Increment,
  Decrement
}

export interface PaginationAction {
  change: PaginationChange;
  count: number;
}

export interface FeatureSelectors<TState, TEntity, TEntityFilter = {}>
  extends EntitySelectorsNum<TEntity, TState> {
  selectData: (state: TState) => DataState<TEntity>;
  selectFilterData: (state: TState) => TEntityFilter;
  selectPagination: (state: TState) => PaginationState;
  selectRequestStatus: (state: TState) => RequestStatusState;
  getSelectedItemId: (state: TState) => number;
  getSelectedItem: (state: TState) => TEntity;
  getSelectedItemRequestStatuses: (state: TState) => ItemRequestStatuses;
  isListFetched: (state: TState) => boolean;
  isSelectedItemFetchCompleted: (state: TState) => boolean;
}

// Data state
export interface DataState<T> extends EntityState<T> {
  selectedItemId: number | null;
}

// Pagination state
export interface PaginationState {
  lastPage: number;
  pageNumber: number;
  pageSize: number;
  itemCount: number;
}

export function getPaginationInitialState(): PaginationState {
  return {
    lastPage: 1,
    pageNumber: 1,
    pageSize: 25,
    itemCount: 0
  };
}

// Request Status state
export interface RequestStatusState {
  byId: {
    [key: number]: ItemRequestStatuses;
  };
  listFetch: RequestStatus;
  create: RequestStatus;
}

export interface ItemRequestStatuses {
  fetch: RequestStatus;
  update: RequestStatus;
  delete: RequestStatus;
}

export function getRequestStatusInitialState(): RequestStatusState {
  return {
    byId: {},
    listFetch: RequestStatus.Initial,
    create: RequestStatus.Initial
  };
}

// Module state
export interface FeatureState<TEntity, TFilter = {}> {
  data: DataState<TEntity>;
  filterData: TFilter;
  pagination: PaginationState;
  requestStatus: RequestStatusState;
}

export function getInitialState<TEntity, TFilter = {}>() {
  return {
    data: createEntityAdapter<TEntity>().getInitialState({
      selectedItemId: null
    }),
    filterData: <TFilter>{},
    pagination: getPaginationInitialState(),
    requestStatus: getRequestStatusInitialState()
  };
}

// Fetch list
function createFetchListHandlers<T, A extends EntityAdapter<T>>(
  actions: DefaultActions,
  adapter: A
): any {
  return {
    [actions.FETCH_LIST]: (state: FeatureState<T>) => {
      return {
        ...state,
        requestStatus: requestStatusReducer(state.requestStatus, {
          listFetch: RequestStatus.Pending
        })
      };
    },
    [actions.FETCH_LIST_SUCCESS]: (
      state: FeatureState<T>,
      action: FetchListSuccessAction
    ) => {
      const data = action.payload.data;
      return {
        ...state,
        data: adapter.addAll(data.data, state.data),
        filterData: filterDataReducer(state.filterData, action),
        pagination: {
          pageSize: data.results_per_page,
          lastPage: data.last_page,
          pageNumber: data.page_number,
          itemCount: data.total_results
        },
        requestStatus: requestStatusReducer(state.requestStatus, {
          listFetch: RequestStatus.Success
        })
      };
    },
    [actions.FETCH_LIST_FAILURE]: (state: FeatureState<T>) => {
      return {
        ...state,
        requestStatus: requestStatusReducer(state.requestStatus, {
          listFetch: RequestStatus.Failure
        })
      };
    },
    [actions.FETCH_LIST_STATE_RESET]: (state: FeatureState<T>) => {
      return {
        ...state,
        data: adapter.removeAll(state.data),
        requestStatus: requestStatusReducer(state.requestStatus, {
          listFetch: RequestStatus.Initial
        })
      };
    },
    [actions.RESET_LIST_FILTER]: (state: FeatureState<T>) => {
      return {
        ...state,
        filterData: {}
      };
    }
  };
}

// Fetch item
function createFetchItemHandlers<T, A extends EntityAdapter<T>>(
  actions: DefaultActions,
  adapter: A
): any {
  return {
    [actions.FETCH_ITEM]: (state: FeatureState<T>, action: FetchItemAction) => {
      return {
        ...state,
        data: { ...state.data, selectedItemId: action.payload.id },
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            fetch: RequestStatus.Pending
          }
        )
      };
    },
    [actions.FETCH_ITEM_SUCCESS]: (
      state: FeatureState<T>,
      action: FetchItemSuccessAction
    ) => {
      const item = action.payload.item;
      return {
        ...state,
        data: {
          ...state.data.ids.find(id => id === item.id)
            ? adapter.updateOne(item, state.data)
            : adapter.addOne(item.changes, state.data)
        },
        requestStatus: requestStatusByIdReducer(state.requestStatus, item.id, {
          fetch: RequestStatus.Success
        })
      };
    },
    [actions.FETCH_ITEM_FAILURE]: (
      state: FeatureState<T>,
      action: FetchItemFailureAction
    ) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            fetch: RequestStatus.Failure
          }
        )
      };
    },
    [actions.FETCH_ITEM_STATE_RESET]: (
      state: FeatureState<T>,
      action: FetchItemStateResetAction
    ) => {
      return {
        ...state,
        data: { ...state.data, selectedItemId: null },
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            fetch: RequestStatus.Initial
          }
        )
      };
    }
  };
}

// Create item
function createCreateItemHandlers<T, A extends EntityAdapter<T>>(
  actions: DefaultActions,
  adapter: A
): any {
  return {
    [actions.CREATE]: (state: FeatureState<T>) => {
      return {
        ...state,
        requestStatus: requestStatusReducer(state.requestStatus, {
          create: RequestStatus.Pending
        })
      };
    },
    [actions.CREATE_SUCCESS]: (
      state: FeatureState<T>,
      action: CreateSuccessAction
    ) => {
      return {
        ...state,
        data: adapter.addOne(action.payload.item, state.data),
        pagination: paginationReducer(state.pagination, {
          change: PaginationChange.Increment,
          count: 1
        }),
        requestStatus: requestStatusReducer(state.requestStatus, {
          create: RequestStatus.Success
        })
      };
    },
    [actions.CREATE_FAILURE]: (state: FeatureState<T>) => {
      return {
        ...state,
        requestStatus: requestStatusReducer(state.requestStatus, {
          create: RequestStatus.Failure
        })
      };
    },
    [actions.CREATE_STATE_RESET]: (state: FeatureState<T>) => {
      return {
        ...state,
        requestStatus: requestStatusReducer(state.requestStatus, {
          create: RequestStatus.Initial
        })
      };
    }
  };
}

// Update item
function createUpdateItemHandlers<T, A extends EntityAdapter<T>>(
  actions: DefaultActions,
  adapter: A
): any {
  return {
    [actions.UPDATE]: (state: FeatureState<T>, action: UpdateAction) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.item.id,
          {
            update: RequestStatus.Pending
          }
        )
      };
    },
    [actions.UPDATE_SUCCESS]: (
      state: FeatureState<T>,
      action: UpdateSuccessAction
    ) => {
      return {
        ...state,
        data: adapter.updateOne(action.payload.item, state.data),
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.item.id,
          {
            update: RequestStatus.Success
          }
        )
      };
    },
    [actions.UPDATE_FAILURE]: (
      state: FeatureState<T>,
      action: UpdateFailureAction
    ) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            update: RequestStatus.Failure
          }
        )
      };
    },
    [actions.UPDATE_STATE_RESET]: (
      state: FeatureState<T>,
      action: UpdateStateResetAction
    ) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            update: RequestStatus.Initial
          }
        )
      };
    }
  };
}

// Delete item
function createDeleteItemHandlers<T, A extends EntityAdapter<T>>(
  actions: DefaultActions,
  adapter: A
): any {
  return {
    [actions.DELETE]: (state: FeatureState<T>, action: DeleteAction) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            delete: RequestStatus.Pending
          }
        )
      };
    },
    [actions.DELETE_SUCCESS]: (
      state: FeatureState<T>,
      action: DeleteSuccessAction
    ) => {
      return {
        ...state,
        data: adapter.removeOne(action.payload.id, state.data),
        pagination: paginationReducer(state.pagination, {
          change: PaginationChange.Decrement,
          count: 1
        }),
        requestStatus: deleteRequestStatusById(
          state.requestStatus,
          action.payload.id
        )
      };
    },
    [actions.DELETE_FAILURE]: (
      state: FeatureState<T>,
      action: DeleteFailureAction
    ) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            delete: RequestStatus.Failure
          }
        )
      };
    },
    [actions.DELETE_STATE_RESET]: (
      state: FeatureState<T>,
      action: DeleteStateResetAction
    ) => {
      return {
        ...state,
        requestStatus: requestStatusByIdReducer(
          state.requestStatus,
          action.payload.id,
          {
            delete: RequestStatus.Initial
          }
        )
      };
    }
  };
}

export function createDefaultActionHandlers<T>(actions: DefaultActions) {
  const adapter: EntityAdapter<T> = createEntityAdapter<T>();
  return {
    ...createFetchListHandlers<T, EntityAdapter<T>>(actions, adapter),
    ...createFetchItemHandlers<T, EntityAdapter<T>>(actions, adapter),
    ...createCreateItemHandlers<T, EntityAdapter<T>>(actions, adapter),
    ...createUpdateItemHandlers<T, EntityAdapter<T>>(actions, adapter),
    ...createDeleteItemHandlers<T, EntityAdapter<T>>(actions, adapter)
  };
}

// Reducers
export function filterDataReducer(
  filterData: any,
  action: FetchListSuccessAction
) {
  return {
    ...filterData,
    ...action.payload.filterData
  };
}

export function paginationReducer(
  state: PaginationState,
  action: PaginationAction
): PaginationState {
  let itemCount = state.itemCount;

  switch (action.change) {
    case PaginationChange.Increment:
      itemCount += action.count;
      break;
    case PaginationChange.Decrement:
      itemCount -= action.count;
      break;
    default:
      return state;
  }

  const lastPage = Math.ceil(itemCount / state.pageSize);
  const pageNumber = state.pageNumber > lastPage ? lastPage : state.pageNumber;

  return {
    pageSize: state.pageSize,
    lastPage: lastPage,
    pageNumber: pageNumber,
    itemCount: itemCount
  };
}

export function requestStatusByIdReducer(
  state: RequestStatusState,
  id: number,
  change: any
): RequestStatusState {
  return {
    ...state,
    byId: {
      ...state.byId,
      [id]: { ...state.byId[id], ...change }
    }
  };
}

export function requestStatusReducer(
  state: RequestStatusState,
  requestStatusData: any
): RequestStatusState {
  return {
    ...state,
    ...requestStatusData
  };
}

function deleteRequestStatusById(
  state: RequestStatusState,
  id: number
): RequestStatusState {
  const newState = { ...state };
  delete newState.byId[id];
  return newState;
}

// Selectors
export function createDefaultSelectors<
  TState extends FeatureState<TEntity, TEntityFilter>,
  TEntity,
  TEntityFilter = {}
>(): FeatureSelectors<TState, TEntity, TEntityFilter> {
  const selectData = (state: TState) => state.data;
  const selectFilterData = (state: TState) => state.filterData;
  const selectPagination = (state: TState) => state.pagination;
  const selectRequestStatus = (state: TState) => state.requestStatus;
  const adapter: EntityAdapter<TEntity> = createEntityAdapter<TEntity>();
  const entitySelectors = adapter.getSelectors<TState>(selectData);
  const getSelectedItemId = (state: TState) => state.data.selectedItemId;
  const getSelectedItem = createSelector(
    entitySelectors.selectEntities,
    getSelectedItemId,
    (entities, selectedItemId) => {
      return entities[selectedItemId];
    }
  );
  const getSelectedItemRequestStatuses = createSelector(
    selectRequestStatus,
    getSelectedItemId,
    (requestStatusState, selectedItemId) => {
      return requestStatusState.byId[selectedItemId];
    }
  );
  const isSelectedItemFetchCompleted = createSelector(
    getSelectedItemRequestStatuses,
    selectedItemRequestStatuses => {
      const fetchStatus = selectedItemRequestStatuses.fetch;
      return (
        fetchStatus === RequestStatus.Success ||
        fetchStatus === RequestStatus.Failure
      );
    }
  );
  const isListFetched = createSelector(
    selectRequestStatus,
    requestStatus =>
      requestStatus.listFetch === RequestStatus.Success ||
      requestStatus.listFetch === RequestStatus.Failure
  );
  return {
    selectAll: entitySelectors.selectAll,
    selectEntities: entitySelectors.selectEntities,
    selectIds: <(
      state: FeatureState<TEntity, TEntityFilter>
    ) => number[]>entitySelectors.selectIds,
    selectTotal: entitySelectors.selectTotal,
    selectData: selectData,
    selectFilterData: selectFilterData,
    selectPagination: selectPagination,
    selectRequestStatus: selectRequestStatus,
    getSelectedItemId: getSelectedItemId,
    getSelectedItem: getSelectedItem,
    getSelectedItemRequestStatuses: getSelectedItemRequestStatuses,
    isSelectedItemFetchCompleted: isSelectedItemFetchCompleted,
    isListFetched: isListFetched
  };
}
