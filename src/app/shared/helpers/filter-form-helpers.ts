import { FormGroup } from '@angular/forms';
import { SECONDS_IN_A_DAY } from '../constants';

export function normalizeFilterFormDates(
  form: FormGroup,
  options: {
    startDateKey?: string;
    endDateKey?: string;
  } = {}
) {
  if (!form) return;
  const _startDateKey = (options && options.startDateKey) || 'date_from_filter';
  const _endDateKey = (options && options.endDateKey) || 'date_to_filter';

  const {
    [_startDateKey]: date_from_filter,
    [_endDateKey]: date_to_filter
  } = form.value;
  if (date_from_filter && date_to_filter && date_from_filter > date_to_filter) {
    form.patchValue({
      [_startDateKey]: date_to_filter,
      [_endDateKey]: date_from_filter
    });
  }
}
