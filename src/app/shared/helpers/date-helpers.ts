import { FormGroup } from '@angular/forms';
import { SECONDS_IN_A_DAY } from '../constants';

// Note: the `unixTimestamp` is in seconds
export function floorTimestampToDays(unixTimestamp: number) {
  if (!unixTimestamp) { return unixTimestamp; }
  if (unixTimestamp === NaN) { return null; }

  const days = Math.floor(unixTimestamp / SECONDS_IN_A_DAY);
  return days * SECONDS_IN_A_DAY;
}

export function endOfTheDayTimestamp(unixTimestamp: number) {
  if (!unixTimestamp) { return unixTimestamp; }
  if (unixTimestamp === NaN) { return null; }

  return floorTimestampToDays(unixTimestamp) + SECONDS_IN_A_DAY - 1;
}
