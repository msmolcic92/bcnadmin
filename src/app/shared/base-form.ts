import { Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

export interface ValidationErrors {
  [key: string]: string[];
}

export interface ValidationMessages {
  [key: string]: {
    [key: string]: string;
  };
}

/**
 * Error returned from the backend API after form submit.
 */
export interface SubmitError {
  error_code: ErrorCode;
  error_messages: ErrorMessage[];
}

export interface ErrorMessage {
  field: string;
  message: string;
}

export enum ErrorCode {
  ValidationError = 1,
  UnexpectedError = 2
}

export abstract class BaseForm implements OnDestroy, OnInit {
  @Output() formSuccess: EventEmitter<any> = new EventEmitter();
  @Output() formCancel: EventEmitter<any> = new EventEmitter();

  form: FormGroup;
  formErrors: ValidationErrors = { _form: [] };

  protected isAlive = true;

  constructor(private validationMessages?: ValidationMessages) {}

  /**
   *  Implements form creation logic.
   */
  protected abstract buildForm(): void;

  ngOnInit(): void {
    this.buildForm();
    this.createFormErrorFields();
    this.subscribeToValueChanges();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  private createFormErrorFields(): void {
    for (const key in this.form.controls) {
      if (this.form.controls.hasOwnProperty(key)) {
        this.formErrors[key] = [];
      }
    }
  }

  private subscribeToValueChanges(): void {
    this.form.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  private onValueChanged(data?: any): void {
    if (!this.form) {
      return;
    }

    const { form } = this;

    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      if (field !== '_form') {
        this.formErrors[field] = [];
      }

      const control = form.get(field);

      if (!control || !control.dirty || control.valid || !this.validationMessages) {
        continue;
      }

      const errorMessages = this.validationMessages[field];

      for (const errorKey in control.errors) {
        if (!control.errors.hasOwnProperty(errorKey)) {
          continue;
        }

        this.formErrors[field].push(errorMessages[errorKey]);
      }
    }
  }

  protected onSubmitError(submitError: SubmitError): void {
    this.clearFormErrors();

    if (submitError.error_code === ErrorCode.UnexpectedError) {
      submitError.error_messages.forEach(errorMessage => {
        this.formErrors._form.push(errorMessage.message);
      });
    }

    if (submitError.error_code === ErrorCode.ValidationError) {
      submitError.error_messages.forEach(errorMessage => {
        if (this.formErrors[errorMessage.field]) {
          this.formErrors[errorMessage.field].push(errorMessage.message);
        } else {
          this.formErrors._form.push(errorMessage.message);
        }
      });
    }
  }

  clearFormErrors() {
    for (const key in this.formErrors) {
      if (this.formErrors.hasOwnProperty(key)) {
        this.formErrors[key] = [];
      }
    }
  }
}
