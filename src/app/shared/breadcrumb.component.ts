import { Component, OnInit } from '@angular/core';
import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  PRIMARY_OUTLET
} from '@angular/router';

@Component({
  selector: 'breadcrumbs',
  template: `
    <ol class="breadcrumb">
      <li
        *ngFor="let breadcrumb of breadcrumbs"
        class="breadcrumb-item"
        [ngClass]="{active: breadcrumb.active}"
      >
        <a *ngIf="!breadcrumb.active" [routerLink]="breadcrumb.url">{{breadcrumb.label}}</a>
        <span *ngIf="breadcrumb.active" [routerLink]="breadcrumb.url">{{breadcrumb.label}}</span>
      </li>
    </ol>
  `
})
export class BreadcrumbsComponent implements OnInit {
  breadcrumbs: Array<any>;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        this.breadcrumbs = [];
        let currentRoute = this.route.root;
        let url = '';

        do {
          const childrenRoutes = currentRoute.children;
          currentRoute = null;

          childrenRoutes
            .filter(route => route.outlet === PRIMARY_OUTLET)
            .forEach(route => {
              currentRoute = route;
              const routeSnapshot = route.snapshot;

              url += `/${routeSnapshot.url
                .map(segment => segment.path)
                .join('/')}`;

              const breadcrumb = <any>{
                label: routeSnapshot.data.title,
                url: url
              };

              this.breadcrumbs.push(breadcrumb);
            });
        } while (currentRoute);

        // Filter out the Breadcrumbs without labels and set the last one as active.
        this.breadcrumbs = this.breadcrumbs.filter(
          breadcrumb => breadcrumb.label
        );

        if (this.breadcrumbs && this.breadcrumbs.length) {
          this.breadcrumbs[this.breadcrumbs.length - 1].active = true;
        }
      });
  }
}
