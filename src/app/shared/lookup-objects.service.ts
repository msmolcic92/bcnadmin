import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';

@Injectable()
export class LookupObjectsService {
  constructor(private http: HttpClient) {}

  fetchMemberRanks() {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/invoice/rank-labels`
    );
  }
}
