import { Component } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `
    <div class="bcn-spinner-background">
      <div class="bcn-spinner">Loading...</div>
    </div>
  `,
  styles: [`
    .bcn-spinner-background {
      background: rgba(200, 200, 200, 0.2);
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }
    .bcn-spinner {
      top: 50%;
      left: 50%;
      font-size: 5px;
      width: 1em;
      height: 1em;
      border-radius: 50%;
      text-indent: -9999em;
      position: absolute;
      -webkit-animation: load5 1.1s infinite ease;
      animation: load5 1.1s infinite ease;
      -webkit-transform: translateZ(0);
      -ms-transform: translateZ(0);
      transform: translateZ(0);
    }
    @-webkit-keyframes load5 {
      0%,
      100% {
        box-shadow: 0em -2.6em 0em 0em #0ba7da, 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.5), -1.8em -1.8em 0 0em rgba(11,167,218, 0.7);
      }
      12.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.7), 1.8em -1.8em 0 0em #0ba7da, 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.5);
      }
      25% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.5), 1.8em -1.8em 0 0em rgba(11,167,218, 0.7), 2.5em 0em 0 0em #0ba7da, 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      37.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.5), 2.5em 0em 0 0em rgba(11,167,218, 0.7), 1.75em 1.75em 0 0em #0ba7da, 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      50% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.5), 1.75em 1.75em 0 0em rgba(11,167,218, 0.7), 0em 2.5em 0 0em #0ba7da, -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      62.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.5), 0em 2.5em 0 0em rgba(11,167,218, 0.7), -1.8em 1.8em 0 0em #0ba7da, -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      75% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.5), -1.8em 1.8em 0 0em rgba(11,167,218, 0.7), -2.6em 0em 0 0em #0ba7da, -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      87.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.5), -2.6em 0em 0 0em rgba(11,167,218, 0.7), -1.8em -1.8em 0 0em #0ba7da;
      }
    }
    @keyframes load5 {
      0%,
      100% {
        box-shadow: 0em -2.6em 0em 0em #0ba7da, 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.5), -1.8em -1.8em 0 0em rgba(11,167,218, 0.7);
      }
      12.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.7), 1.8em -1.8em 0 0em #0ba7da, 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.5);
      }
      25% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.5), 1.8em -1.8em 0 0em rgba(11,167,218, 0.7), 2.5em 0em 0 0em #0ba7da, 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      37.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.5), 2.5em 0em 0 0em rgba(11,167,218, 0.7), 1.75em 1.75em 0 0em #0ba7da, 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      50% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.5), 1.75em 1.75em 0 0em rgba(11,167,218, 0.7), 0em 2.5em 0 0em #0ba7da, -1.8em 1.8em 0 0em rgba(11,167,218, 0.2), -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      62.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.5), 0em 2.5em 0 0em rgba(11,167,218, 0.7), -1.8em 1.8em 0 0em #0ba7da, -2.6em 0em 0 0em rgba(11,167,218, 0.2), -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      75% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.5), -1.8em 1.8em 0 0em rgba(11,167,218, 0.7), -2.6em 0em 0 0em #0ba7da, -1.8em -1.8em 0 0em rgba(11,167,218, 0.2);
      }
      87.5% {
        box-shadow: 0em -2.6em 0em 0em rgba(11,167,218, 0.2), 1.8em -1.8em 0 0em rgba(11,167,218, 0.2), 2.5em 0em 0 0em rgba(11,167,218, 0.2), 1.75em 1.75em 0 0em rgba(11,167,218, 0.2), 0em 2.5em 0 0em rgba(11,167,218, 0.2), -1.8em 1.8em 0 0em rgba(11,167,218, 0.5), -2.6em 0em 0 0em rgba(11,167,218, 0.7), -1.8em -1.8em 0 0em #0ba7da;
      }
    }
  `]
})
export class SpinnerComponent {

}
