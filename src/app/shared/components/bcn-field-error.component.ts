import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bcn-field-error',
  template: `
    <div *ngIf="errorMessages.length" class="form-control-feedback">
      <div *ngFor="let error of errorMessages">
        {{ error }}
      </div>
    </div>
  `
})
export class BCNFieldErrorComponent {
  @Input() errorMessages: string[];

  constructor() {}
}
