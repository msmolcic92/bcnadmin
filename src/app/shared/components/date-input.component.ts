import { Subscription } from 'rxjs/Subscription';
import {
  Component,
  OnDestroy,
  Input,
  EventEmitter,
  Output,
  ElementRef,
  forwardRef,
  HostListener
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';
import { DateFromUnixPipe } from '../pipes/date-from-unix.pipe';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'date-input',
  template: `
    <div class="input-group" [formGroup]="form">
      <input
        type="text"
        class="form-control"
        ngbDatepicker
        #d="ngbDatepicker"
        (click)="openCalendar(d)"
        [placeholder]="placeholder"
        formControlName="value"
      />

      <div
        title="Clear input"
        class="input-group-addon cursor-pointer"
        (click)="clearValue()"
      ><i class="icon-close"></i></div>

      <div
        title="Open calendar"
        class="input-group-addon cursor-pointer"
        (click)="d.toggle()"
      ><i class="icon-calendar"></i></div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateInputComponent),
      multi: true
    }
  ]
})
export class DateInputComponent implements ControlValueAccessor, OnDestroy {
  @Input() disabled: boolean;
  @Input() name: string;
  @Input() placeholder: string;
  @Output() change: EventEmitter<any> = new EventEmitter();

  formValuesSubscription: Subscription;
  form: FormGroup;
  value: NgbDateStruct;
  ngbDatepickerInstance: any;
  _onChangeHandler = (value: any) => {}; // noop initially

  constructor(
    private elementRef: ElementRef,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
    this.formValuesSubscription = this.form.valueChanges.subscribe(
      (data: any) => {
        this.propagateChange(this.form.value.value);
      }
    );
  }

  ngOnDestroy() {
    this.ngbDatepickerInstance = null;

    if (this.formValuesSubscription) {
      this.formValuesSubscription.unsubscribe();
    }
  }

  @HostListener('document: click', ['$event'])
  handleDocumentClick(event: MouseEvent) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.closeCalendar();
    }
  }

  // ControlValueAccessor implementation

  writeValue(value: number) {
    this.value = this.parseInputValue(value);
    this.form.patchValue({ value: this.value }, { emitEvent: false });
  }

  registerOnChange(fn: any) {
    this._onChangeHandler = fn;
  }

  registerOnTouched() {}

  // Custom

  buildForm() {
    this.form = this.formBuilder.group({
      value: [this.value]
    });
  }

  propagateChange(newValue) {
    const formattedValue = this.formatOutputValue(newValue);
    this.change.emit(formattedValue);
    this._onChangeHandler(formattedValue);
  }

  openCalendar(datepickerInstance: any) {
    this.ngbDatepickerInstance = datepickerInstance;
    datepickerInstance.open();
  }

  closeCalendar() {
    if (this.ngbDatepickerInstance) {
      this.ngbDatepickerInstance.close();
    }
  }

  clearValue() {
    this.value = null;
    this.form.patchValue({ value: null });
  }

  parseInputValue(value: number) {
    if (!value) return null;
    const date = new Date(value * 1000);

    return {
      year: date.getUTCFullYear(),
      month: date.getUTCMonth() + 1,
      day: date.getUTCDate()
    };
  }

  formatOutputValue(value: NgbDateStruct) {
    if (!value) return null;
    const date = new Date(Date.UTC(value.year, value.month - 1, value.day));
    const timestamp = date.getTime() / 1000;

    return timestamp;
  }
}
