import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ChangeDetectorRef,
  forwardRef
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export const THREE_STATE_CHECKBOX_COMPONENT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ThreeStateCheckboxComponent),
  multi: true
};

@Component({
  selector: 'three-state-checkbox',
  template: `
    <div
      [ngStyle]="style"
      [ngClass]="'checkbox'"
      [class]="styleClass"
    >
      <div class="hidden-accessible">
        <input
          #input
          type="text"
          [attr.id]="inputId"
          [name]="name"
          [attr.tabindex]="tabindex"
          readonly
          [disabled]="disabled"
          (keyup)="onKeyup($event)"
          (keydown)="onKeydown($event)"
          (focus)="onFocus()"
          (blur)="onBlur()"
        >
      </div>
      <label *ngIf="reversed" class="checkbox-label">{{labelText}}</label>
      <div
        class="checkbox-box"
        (click)="onClick($event, input)"
      >
        <span
          class="checkbox-icon fa"
          [ngClass]="{
            'invisible': value === null,
            'fa-square-o': value === null,
            'fa-check': value === true,
            'fa-close': value === false
          }"
        ></span>
      </div>
      <label *ngIf="!reversed" class="checkbox-label">{{labelText}}</label>
    </div>
  `,
  styles: [
    `
    .hidden-accessible {
      border: 0;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    input, select {
      transform: scale(0);
    }

    .checkbox {
      display: inline-block;
      cursor: pointer;
      vertical-align: middle;
      margin-right: .25em;
      box-sizing: border-box;
    }

    .checkbox-box {
      width: 1.125em;
      height: 1.125em;
      line-height: 1.125em;
      border: 1px solid #E3E8EC;
      -moz-border-radius: 0.25em;
      -webkit-border-radius: 0.25em;
      border-radius: 0.25em;
      text-align: center;
      box-sizing: border-box;
      display: inline-block;
    }

    .checkbox-icon {
      display: block;
      box-sizing: border-box;
    }

    .checkbox-icon .invisible {
      visibility: hidden;
    }

    .checkbox-label {
      display: inline-block;
      box-sizing: border-box;
    }
  `
  ],
  providers: [THREE_STATE_CHECKBOX_COMPONENT_VALUE_ACCESSOR]
})
export class ThreeStateCheckboxComponent implements ControlValueAccessor {
  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  @Input() disabled: boolean;
  @Input() name: string;
  @Input() tabindex: number;
  @Input() inputId: string;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() labelText: string;
  @Input() reversed: boolean;
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  focus: boolean;
  value: any;

  onModelChange: Function = () => {};
  onModelTouched: Function = () => {};

  onClick(event: Event, input: HTMLInputElement) {
    if (this.disabled) {
      return;
    }

    this.toggle(event);
    this.focus = true;
    input.focus();
  }

  onKeydown(event: KeyboardEvent) {
    if (event.keyCode === 32) {
      event.preventDefault();
    }
  }

  onKeyup(event: KeyboardEvent) {
    if (event.keyCode === 32) {
      this.toggle(event);
      event.preventDefault();
    }
  }

  toggle(event: Event) {
    if (this.value === null || this.value === undefined) {
      this.value = true;
    } else if (this.value === true) {
      this.value = false;
    } else if (this.value === false) {
      this.value = null;
    }

    this.onModelChange(this.value);
    this.onChange.emit({
      originalEvent: event,
      value: this.value
    });
  }

  onFocus() {
    this.focus = true;
  }

  onBlur() {
    this.focus = false;
    this.onModelTouched();
  }

  registerOnChange(fn: Function): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: Function): void {
    this.onModelTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value;
    this.changeDetectorRef.markForCheck();
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }
}
