import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bcn-form-group',
  template: `
    <div
      class="form-group row"
      [ngClass]="{ 'has-danger': errorMessages.length }"
    >
      <ng-content></ng-content>
    </div>
  `
})
export class BCNFormGroupComponent {
  @Input() errorMessages: string[];

  constructor() {}
}
