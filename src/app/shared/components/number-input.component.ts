import {
  Component,
  OnDestroy,
  Input,
  EventEmitter,
  Output,
  ElementRef,
  forwardRef,
  HostListener
} from '@angular/core';

import {
  FormBuilder,
  FormGroup,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';

@Component({
  selector: 'number-input',
  template: `
    <div class="input-group" [formGroup]="form">
      <input
        type="text"
        class="form-control"
        [placeholder]="placeholder"
        formControlName="value"
      />
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumberInputComponent),
      multi: true
    }
  ]
})
export class NumberInputComponent implements ControlValueAccessor, OnDestroy {
  @Input() disabled: boolean;
  @Input() name: string;
  @Input() placeholder: string;
  @Output() change: EventEmitter<any> = new EventEmitter();

  isAlive = true;
  value: any;
  form: FormGroup;
  regEx = new RegExp('^[0-9]*$');
  _onChangeHandler = (value: any) => {};

  @HostListener('input', ['$event'])
  onInput(event) {
    if (!this.isValidInput(this.form.value.value)) {
      this.writeValue(null);
      this.propagateChange(null);
    }
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event) {
    const e = <KeyboardEvent>event;
    let keyCode = e.keyCode;

    if (
      // Allow: Delete, Backspace, Tab, Escape, Enter
      [46, 8, 9, 27, 13].indexOf(keyCode) !== -1 ||
      // Allow: Ctrl+A
      (keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+V
      (keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Home, End, Left, Right
      (keyCode >= 35 && e.keyCode <= 39)
    ) {
      // Let it happen, don't do anything.
      return;
    }

    // Convert Numpad keys into real numbers.
    if (keyCode >= 96 && keyCode <= 105) {
      keyCode -= 48;
    }

    // Ensure that it is a number and stop the keypress.
    const char = String.fromCharCode(keyCode);

    if (this.isValidInput(char)) {
      return;
    } else {
      e.preventDefault();
    }
  }

  constructor(
    private elementRef: ElementRef,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        this.propagateChange(this.form.value.value);
      });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  // ControlValueAccessor implementation

  writeValue(value: number) {
    this.value = value;
    this.form.patchValue({ value: this.value }, { emitEvent: false });
  }

  registerOnChange(fn: any) {
    this._onChangeHandler = fn;
  }

  registerOnTouched() {}

  // Custom

  buildForm() {
    this.form = this.formBuilder.group({
      value: [this.value]
    });
  }

  propagateChange(newValue) {
    this.change.emit(newValue);
    this._onChangeHandler(newValue);
  }

  isValidInput(input: string): boolean {
    return !!this.regEx.test(input);
  }
}
