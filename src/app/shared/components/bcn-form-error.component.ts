import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bcn-form-error',
  template: `
    <div *ngIf="errorMessages.length" class="alert alert-danger text-center">
      <div *ngFor="let error of errorMessages">
        {{ error }}
      </div>
    </div>
  `
})
export class BCNFormErrorComponent {
  @Input() errorMessages: string[];

  constructor() {}
}
