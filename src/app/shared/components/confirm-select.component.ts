import {
  Component,
  OnDestroy,
  Input,
  EventEmitter,
  Output,
  ElementRef,
  forwardRef,
  HostListener,
  OnChanges,
  SimpleChanges
} from '@angular/core';

import {
  FormBuilder,
  FormGroup,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';

export const ITEM_NAME_PLACEHOLDER = '###ITEM_NAME_PLACEHOLDER###';

@Component({
  selector: 'confirm-select',
  template: `
    <div class="input-group" [formGroup]="form">
      <select
        class="form-control"
        formControlName="value"
        [disabled]="disabled"
      >
        <option
          *ngFor="let item of items"
          [ngValue]="item.id"
        >
          {{item.label}}
        </option>
      </select>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ConfirmSelectComponent),
      multi: true
    }
  ]
})
export class ConfirmSelectComponent
  implements ControlValueAccessor, OnChanges, OnDestroy {
  @Input() disabled = false;
  @Input() items: Array<any> = [];
  @Input() selectedItemId: any = null;
  @Input()
  confirmMessage = `Are you sure you want to select ${ITEM_NAME_PLACEHOLDER}?`;
  @Output() itemChange: EventEmitter<any> = new EventEmitter<any>();

  isAlive = true;
  previousValue: any = null;
  form: FormGroup;
  _onChangeHandler = (value: any) => {};

  constructor(
    private elementRef: ElementRef,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
    this.form.valueChanges
      .takeWhile(() => this.isAlive)
      .subscribe((data: any) => {
        const id = data.value;
        const item = this.items.find(i => i.id === id);
        const message = this.confirmMessage.replace(ITEM_NAME_PLACEHOLDER, item.label);

        if (confirm(message)) {
          this.propagateChange(id);
          this.previousValue = id;
        } else {
          this.writeValue(this.previousValue);
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      this.form &&
      changes.selectedItemId &&
      changes.selectedItemId.currentValue
    ) {
      this.previousValue = changes.selectedItemId.currentValue;
      this.writeValue(changes.selectedItemId.currentValue);
    }
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  // ControlValueAccessor implementation

  writeValue(value: number) {
    this.form.patchValue({ value: value }, { emitEvent: false });
  }

  registerOnChange(fn: any) {
    this._onChangeHandler = fn;
  }

  registerOnTouched() {}

  // Custom

  buildForm() {
    this.form = this.formBuilder.group({
      value: [this.selectedItemId]
    });
  }

  propagateChange(newValue) {
    this.itemChange.emit(newValue);
    this._onChangeHandler(newValue);
  }
}
