// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  CLIENT_ID: '1',
  REFRESH_TOKEN_CLIENT_ID: '2',
  CLIENT_SECRET: 'anothersecretpass34',
  API_URL: 'https://dev-api.bitclubnetwork.com/api/v1/admin'
};
