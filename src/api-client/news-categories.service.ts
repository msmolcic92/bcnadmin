import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import autobind from 'autobind-decorator';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
@autobind
export class NewsCategoriesService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<
      any
    >>this.http.get(`${environment.API_URL}/news_category`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetch(id: number) {
    return this.http.get(`${environment.API_URL}/news_category/${id}`);
  }

  create(data: any) {
    return this.http.post(`${environment.API_URL}/news_category`, data);
  }

  update(id: number, data: any) {
    return this.http.put(`${environment.API_URL}/news_category/${id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${environment.API_URL}/news_category/${id}`);
  }
}
