import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
export class InvoicesService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<any>>this.http.get(`${environment.API_URL}/invoice`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetch(invoiceId: number) {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/invoice/${invoiceId}`
    );
  }

  fetchPdfStatus(invoiceId: number) {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/invoice/${invoiceId}/pdf`
    );
  }

  fetchPdf(invoiceId: number) {
    return <Observable<
      any
    >>this.http.get(
      `${environment.API_URL}/invoice/${invoiceId}/pdf?content=1`,
      { responseType: 'blob' }
    );
  }

  confirmInvoice(invoiceId: number) {
    return <Observable<any>>this.http.put(
      `${environment.API_URL}/invoice/${invoiceId}/mark_as_confirmed`, {}
    );
  }
}
