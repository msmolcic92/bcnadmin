import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class FoundersPoolsService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<
      any
    >>this.http.get(`${environment.API_URL}/founder-pool`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetch(id: number) {
    return this.http.get(`${environment.API_URL}/founder-pool/${id}`);
  }

  create(data: any) {
    return this.http.post(`${environment.API_URL}/founder-pool`, data);
  }

  update(id: number, data: any) {
    return this.http.put(`${environment.API_URL}/founder-pool/${id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${environment.API_URL}/founder-pool/${id}`);
  }
}
