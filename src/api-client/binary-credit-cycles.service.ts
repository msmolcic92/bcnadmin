import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
export class BinaryCreditCyclesService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/btree/credit-list`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  fetch(id: number) {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/btree/credit-list/${id}`
    );
  }
}
