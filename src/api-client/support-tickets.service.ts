import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from 'app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class SupportTicketsService {
  constructor(private http: HttpClient) {}

  fetchTicketLists(params: any) {
    return <Observable<
      any
    >>this.http.get(`${environment.API_URL}/support/ticket`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetchTicketReplyList(ticketId, params) {
    return <Observable<
      any
    >>this.http.get(`${environment.API_URL}/support/ticket/${ticketId}/reply`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  submitReply(params) {
    return <Observable<
      any
    >>this.http.post(
      `${environment.API_URL}/support/ticket/${params.id}/reply`,
      { content: params.content, language: 'en' }
    );
  }

  closeTicket(params) {
    return <Observable<
      any
    >>this.http.put(
      `${environment.API_URL}/support/ticket/${params.id}/close`,
      { id: params.id }
    );
  }

  fetchSubjectList() {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/support/ticket/subject-list`
    );
  }

  assignToAdmin(params: any) {
    return <Observable<
      any
    >>this.http.put(
      `${environment.API_URL}/support/ticket/${params.ticketId}/update_assigned_admin`,
      { admin_id: params.adminId }
    );
  }
}
