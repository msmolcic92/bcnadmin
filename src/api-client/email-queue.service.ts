import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class EmailQueueService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<
      any
    >>this.http.get(`${environment.API_URL}/email/queue-list`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetch(params: any) {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/email/queue-list/${params.id}`
    );
  }

  fetchTemplates() {
    return <Observable<any>>this.http.get(
      `${environment.API_URL}/email/templates-list`
    );
  }
}
