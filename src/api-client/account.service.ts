import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
export class AccountService {
  constructor(private http: HttpClient) {}

  fetch() {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/account`);
  }

  fetchLoginAttempts(params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/login_attempts`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  update(accountData) {
    return <Observable<any>>this.http
      .put(`${environment.API_URL}/account`, accountData);
  }
}
