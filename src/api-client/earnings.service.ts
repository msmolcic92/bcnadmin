import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import HttpUtility from 'app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class EarningsService {
  constructor(private http: HttpClient) {}

  fetchList(currency: string, params: any) {
    return this.http
      .get(`${environment.API_URL}/earning/${currency}`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  fetch(currency: string, id: number) {
    return this.http
      .get(`${environment.API_URL}/earning/${currency}/${id}`);
  }

  create(currency: string, data: any) {
    return this.http
      .post(`${environment.API_URL}/earning/${currency}`, data);
  }

  delete(currency: string, id: number) {
    return this.http
      .delete(`${environment.API_URL}/earning/${currency}/${id}`);
  }

  update(currency: string, id: number, data: any) {
    return this.http
      .put(`${environment.API_URL}/earning/${currency}/${id}`, data);
  }
}
