import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class AdminService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/admin`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  fetch(params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/admin/${params.id}`);
  }

  addAdmin(data: any) {
    return <Observable<any>>this.http
      .post(`${environment.API_URL}/admin`, data);
  }

  editAdmin(id: number, data: any) {
    return <Observable<any>>this.http
      .put(`${environment.API_URL}/admin/${id}`, data);
  }

  deleteAdmin(id: number) {
    return <Observable<any>>this.http
      .delete(`${environment.API_URL}/admin/${id}`);
  }

  fetchAdminGroupList() {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/admin_group`);
  }

  addAdminToGroup(adminId: number, groupId: number) {
    return <Observable<any>>this.http
      .post(`${environment.API_URL}/admin_group/${groupId}/admin`, {
        admin_id: adminId
      });
  }

  removeAdminFromGroup(adminId: number, groupId: number) {
    return <Observable<any>>this.http
      .delete(
        `${environment.API_URL}/admin_group/${groupId}/admin/${adminId}`
      );
  }

  fetchAdminAccess(params: { id: number }) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/admin/${params.id}/access`);
  }

  addAdminAccess(params: { id: number; endpoints: string }) {
    return <Observable<any>>this.http
      .post(`${environment.API_URL}/admin/${params.id}/access`, {
        endpoints: params.endpoints
      });
  }

  removeAdminAccess(params: { id: number; endpoint: string }) {
    return <Observable<any>>this.http
      .delete(
        `${environment.API_URL}/admin/${params.id}/access/${params.endpoint}`
      );
  }
}
