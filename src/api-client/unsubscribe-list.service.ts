import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
export class UnsubscribeListService {
  constructor(private http: HttpClient) {}

  fetchList(params: any = {}) {
    return this.http
      .get(`${environment.API_URL}/email/unsubscribe-list`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  delete(unsubscriberId: number) {
    return this.http.delete(
      `${environment.API_URL}/email/unsubscribe-list/${unsubscriberId}`
    );
  }

  create(unsubscriberData: any) {
    return this.http
      .post(`${environment.API_URL}/email/unsubscribe-list`, unsubscriberData);
  }
}
