import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import autobind from 'autobind-decorator';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
@autobind
export class PredefinedRepliesService {
  constructor(private http: HttpClient) {}

  fetchList(params: any = {}) {
    return this.http.get(`${environment.API_URL}/support/ticket/predefined`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }

  fetch(id: number) {
    return this.http.get(`${environment.API_URL}/support/ticket/predefined/${id}`);
  }

  create(data: any) {
    return this.http.post(`${environment.API_URL}/support/ticket/predefined`, data);
  }

  update(id: number, data: any) {
    return this.http.put(`${environment.API_URL}/support/ticket/predefined/${id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${environment.API_URL}/support/ticket/predefined/${id}`);
  }
}
