import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import autobind from 'autobind-decorator';
import { environment } from 'environments/environment';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
@autobind
export class CronsService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return <Observable<any>>this.http.get(`${environment.API_URL}/cron_log`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }
}
