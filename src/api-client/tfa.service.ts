import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { TfaData } from '../app/core/store/session.state';
import HttpUtility from '../app/shared/helpers/http-utility';

@Injectable()
export class TfaService {
  constructor(private http: HttpClient) {}

  fetch() {
    return <Observable<any>>this.http.get(`${environment.API_URL}/tfa`);
  }

  post(tfaData: TfaData) {
    return <Observable<any>>this.http.post(
      `${environment.API_URL}/tfa`,
      tfaData
    );
  }

  remove(params: any) {
    return <Observable<any>>this.http.delete(`${environment.API_URL}/tfa`, {
      params: HttpUtility.createQueryParameters(params)
    });
  }
}
