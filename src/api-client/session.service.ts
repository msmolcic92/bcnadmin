import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { TokenStorageService } from '../app/auth/token-storage.service';
import { environment } from '../environments/environment';
import * as actions from '../app/core/store/session.actions';

@Injectable()
export class SessionService {
  constructor(
    private http: HttpClient,
    private store: Store<any>,
    private tokenStorageService: TokenStorageService
  ) {}

  tryRestoreLocalSession() {
    // NOTE: If there's a valid token already, just
    // pretend its a SESSION_LOGIN_SUCCESS happening
    // with this stored token as a payload
    const tokenData = this.tokenStorageService.retrieve();
    if (tokenData.accessToken) {
      this.store.dispatch({
        type: actions.SESSION_LOGIN_SUCCESS,
        payload: tokenData
      });
    }
  }

  login(credentials) {
    const requestData = {
      client_id: environment.CLIENT_ID,
      client_secret: environment.CLIENT_SECRET,
      grant_type: 'password',
      username: credentials.username,
      password: credentials.password
    };

    if (credentials.code) {
      requestData['code'] = credentials.code;
    }

    return <Observable<any>>this.http
      .post(`${environment.API_URL}/access_token`, requestData);
  }

  loginWithRefreshToken(refreshToken) {
    const requestData = {
      client_id: environment.REFRESH_TOKEN_CLIENT_ID,
      client_secret: environment.CLIENT_SECRET,
      grant_type: 'refresh_token',
      refresh_token: refreshToken
    };

    return <Observable<any>>this.http
      .post(`${environment.API_URL}/access_token`, requestData);
  }

  logout() {
    return <Observable<any>>this.http
      .post(`${environment.API_URL}/revoke_token`, null);
  }
}
