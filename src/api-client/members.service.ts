import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from 'app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class MembersService {
  constructor(private http: HttpClient) {}

  fetchList(params: any) {
    return this.http
      .get(`${environment.API_URL}/member_list`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  fetch(memberId: number) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/member_list/${memberId}`);
  }

  fetchInvoices(params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/invoice`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }

  updateUsername(memberId: number, requestData: any) {
    return <Observable<any>>this.http
      .put(
        `${environment.API_URL}/member_list/update_username/${memberId}`,
        requestData
      );
  }

  updateEmail(memberId: number, requestData: any) {
    return <Observable<any>>this.http
      .put(
        `${environment.API_URL}/member_list/update_email/${memberId}`,
        requestData
      );
  }

  updateRank(memberId: number, requestData: any) {
    return <Observable<any>>this.http
      .put(
        `${environment.API_URL}/member_list/update_rank/${memberId}`,
        requestData
      );
  }

  creditWallet(wallet: string, requestData: any) {
    return <Observable<any>>this.http
      .post(`${environment.API_URL}/${wallet}/credit`, requestData);
  }
}
