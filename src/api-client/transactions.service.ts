import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import HttpUtility from 'app/shared/helpers/http-utility';
import autobind from 'autobind-decorator';

@Injectable()
@autobind
export class TransactionService {
  constructor(private http: HttpClient) {}

  fetchTransactionTypes() {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/transactionType`);
  }

  fetchList(currency: string, params: any) {
    return <Observable<any>>this.http
      .get(`${environment.API_URL}/${currency}`, {
        params: HttpUtility.createQueryParameters(params)
      });
  }
}
